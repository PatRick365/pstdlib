cmake_minimum_required(VERSION 3.16)

project(pstdlib VERSION 1.0.1 DESCRIPTION "patrick standard library")

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_library(pstdlib SHARED
    include/psl_algorithm.h
    include/psl_array.h
    include/psl_atomic.h
    include/psl_bitset.h
    include/psl_char_manip.h
    include/psl_except.h
    include/psl_file.h
    include/psl_file_info.h
    include/psl_file_manip.h
    include/psl_filesystem.h
    include/psl_format.h
    include/psl_from_string.h
    include/psl_functional.h
    include/psl_future.h
    include/psl_initializer_list.h
    include/psl_iodevice.h
    include/psl_iterator.h
    include/psl_limits.h
    include/psl_list.h
    include/psl_logging.h
    include/psl_math.h
    include/psl_memory.h
    include/psl_new.h
    include/psl_optional.h
    include/psl_process.h
    include/psl_pml.h
    include/psl_random.h
    include/psl_stdio.h
    include/psl_string.h
    include/psl_string_builder.h
    include/psl_string_vec.h
    include/psl_string_view.h
    include/psl_tcp_client.h
    include/psl_thread.h
    include/psl_time.h
    include/psl_tuple.h
    include/psl_type_traits.h
    include/psl_units.h
    include/psl_unordered_map.h
    include/psl_unordered_set.h
    include/psl_utility.h
    include/psl_variant.h
    include/psl_variant2.h
    include/psl_vector.h
    include/psl_tcp_server.h
    include/psl_ringbuffer.h
    include/psl_thread_pool.h
    include/psl_message_queue.h
    include/psl_mutex.h
    include/psl_backtrace.h
    include/psl_throw.h
    src/psl_except.cpp
    src/psl_file.cpp
    src/psl_file_info.cpp
    src/psl_file_manip.cpp
    src/psl_filesystem.cpp
    src/psl_iodevice.cpp
    src/psl_logging.cpp
    src/psl_math.cpp
    src/psl_process.cpp
    src/psl_random.cpp
    src/psl_stdio.cpp
    src/psl_string_builder.cpp
    src/psl_tcp_client.cpp
    src/psl_thread.cpp
    src/psl_time.cpp
    src/psl_tcp_server.cpp
    src/psl_pml.cpp
    src/psl_thread_pool.cpp
    src/psl_message_queue.cpp
    src/psl_mutex.cpp
    src/psl_backtrace.cpp
    src/psl_throw.cpp
)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(pstdlib Threads::Threads)

set_target_properties(pstdlib PROPERTIES VERSION ${PROJECT_VERSION} SOVERSION 1)

target_include_directories(pstdlib PUBLIC include PRIVATE src)

target_compile_options(pstdlib PRIVATE -Werror -Wall -Wextra -pedantic -Wstrict-aliasing -Wno-error=unused-parameter)

target_compile_options(pstdlib PUBLIC -fsanitize=address -fno-omit-frame-pointer)
target_link_options(pstdlib PUBLIC -fsanitize=address)

include(GNUInstallDirs)


install(TARGETS pstdlib
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
    
configure_file(pstdlib.pc.in pstdlib.pc @ONLY)

install(FILES ${CMAKE_BINARY_DIR}/pstdlib.pc DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/pkgconfig)

add_subdirectory(extern/ptest)

add_executable(
    pstdlib_tests
    tests/main.cpp
    tests/test_helpers.hpp
    tests/test_psl_algorithm.cpp
    tests/test_psl_array.cpp
    tests/test_psl_bitset.cpp
    tests/test_psl_char_manip.cpp
    tests/test_psl_file.cpp
    tests/test_psl_filesystem.cpp
    tests/test_psl_format.cpp
    tests/test_psl_from_string.cpp
    tests/test_psl_functional.cpp
    tests/test_psl_future.cpp
    tests/test_psl_io_device.cpp
    tests/test_psl_iterator.cpp
    tests/test_psl_limits.cpp
    tests/test_psl_list.cpp
    tests/test_psl_logging.cpp
    tests/test_psl_math.cpp
    tests/test_psl_memory.cpp
    tests/test_psl_optional.cpp
    tests/test_psl_process.cpp
    tests/test_psl_string.cpp
    tests/test_psl_string_view.cpp
    tests/test_psl_tcp_client.cpp
    tests/test_psl_thread.cpp
    tests/test_psl_tuple.cpp
    tests/test_psl_type_traits.cpp
    tests/test_psl_units.cpp
    tests/test_psl_unordered_map.cpp
    tests/test_psl_unordered_set.cpp
    tests/test_psl_utility.cpp
    tests/test_psl_variant.cpp
    tests/test_psl_variant2.cpp
    tests/test_psl_vector.cpp
    tests/test_psl_tcp_server.cpp
    tests/test_psl_ringbuffer.cpp
    tests/test_psl_pml.cpp
    tests/test_psl_thread_pool.cpp
    tests/test_psl_random.cpp
    tests/test_psl_message_queue.cpp
    tests/test_psl_backtrace.cpp
)
target_link_libraries(
    pstdlib_tests
    pstdlib
    ptest
)
