#include "psl_utility.h"
#include "psl_string.h"

#include "ptest/ptest.h"


TEST(psl_utility, swap_int)
{
    int a = 1;
    int b = 2;

    psl::swap(a, b);

    EXPECT_EQ(a, 2);
    EXPECT_EQ(b, 1);
}

TEST(psl_utility, swap_string_sso)
{
    psl::string a("aaa");
    psl::string b("bbb");

    psl::swap(a, b);

    EXPECT_STREQ(a.c_str(), "bbb");
    EXPECT_STREQ(b.c_str(), "aaa");
}

TEST(psl_utility, swap_string_no_sso)
{
    psl::string a("aaaaaaaaaaaa");
    psl::string b("bbbbbbbbbbbb");

    psl::swap(a, b);

    EXPECT_STREQ(a.c_str(), "bbbbbbbbbbbb");
    EXPECT_STREQ(b.c_str(), "aaaaaaaaaaaa");
}

TEST(psl_utility, pair)
{
    {
        auto p = psl::make_pair(5, 5.5);
        EXPECT_EQ(p.first, 5);
        EXPECT_EQ(p.second, 5.5);
    }
    {
        psl::pair<float, float> p1(1.5, 2.5);
        psl::pair<int, int> p2(p1);

        EXPECT_EQ(p2.first, 1);
        EXPECT_EQ(p2.second, 2);
    }

    {
        psl::pair<float, float> p1(1.5, 2.5);
        psl::pair<int, int> p2;
        p2 = p1;

        EXPECT_EQ(p2.first, 1);
        EXPECT_EQ(p2.second, 2);
    }

}
