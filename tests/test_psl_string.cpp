#include "psl_string.h"
#include "psl_vector.h"

#include "ptest/ptest.h"
#include "test_helpers.hpp"


/// @todo //TEST(psl_basic_string, erase_iterator)

TEST(psl_basic_string, constructors)
{
    {
        psl::string str;
        EXPECT_EQ(str.length(), 0);
        EXPECT_STREQ(str.c_str(), "");
        EXPECT_EQ(str, "");
    }
    {
        psl::string str("abc");
        EXPECT_STREQ(str.c_str(), "abc");
        EXPECT_EQ(str, "abc");
        str.clear();
        EXPECT_STREQ(str.c_str(), "");
        EXPECT_EQ(str, "");
    }
    {
        psl::string str("abc", 3);
        EXPECT_STREQ(str.c_str(), "abc");
        EXPECT_EQ(str, "abc");
    }
    {
        psl::string str('a', 3);
        EXPECT_STREQ(str.c_str(), "aaa");
        EXPECT_EQ(str, "aaa");
    }

    {
        psl::string str('a', 3);
        EXPECT_STREQ(str.c_str(), "aaa");
        EXPECT_EQ(str, "aaa");

        psl::string str2(str);
        EXPECT_STREQ(str.c_str(), "aaa");
        EXPECT_STREQ(str2.c_str(), "aaa");
        EXPECT_EQ(str, str2);
        EXPECT_EQ(str, "aaa");
        EXPECT_EQ(str2, "aaa");
    }

    {
        psl::string str1("0123456789");
        psl::string str2(str1.begin(), str1.end());
        EXPECT_EQ(str1.size(), str2.size());
        EXPECT_STREQ(str1.c_str(), str2.c_str());
        EXPECT_EQ(str1, str2);
    }

    {
        psl::string str1("0123456789");
        psl::string str2(str1.begin() + 3, str1.end());
        EXPECT_EQ(str1.size() - 3, str2.size());
        EXPECT_STREQ(str2.c_str(), "3456789");
        EXPECT_EQ(str2, "3456789");
    }

    {
        psl::string str1("0123456789");
        psl::string str2(str1.begin() + 1, str1.begin() + 2);
        EXPECT_EQ(str2.size(), 1);
        EXPECT_STREQ(str2.c_str(), "1");
        EXPECT_EQ(str1, "0123456789");
    }

    {
        psl::string str1("0123456789");
        psl::string str2;
        str2 = str1;
        EXPECT_EQ(str1.size(), str2.size());
        EXPECT_STREQ(str1.c_str(), str2.c_str());
        EXPECT_EQ(str1, str2);
    }
    {
        psl::string str("abc");
        str << '\0' << '\0' << '\0';
        EXPECT_STREQ(str.c_str(), "abc");
        EXPECT_NE(str, "abc");
        EXPECT_NE(str, psl::string("abc"));
    }
}

TEST(psl_basic_string, move_operations)
{
    // move
    {
        psl::string str('a', 3);
        psl::string str2(psl::move(str));
        EXPECT_STREQ(str2.c_str(), "aaa");
        EXPECT_EQ(str.size(), 0);

        str = "123";
        EXPECT_STREQ(str.c_str(), "123");

        psl::string str3 = psl::move(str);
        EXPECT_STREQ(str3.c_str(), "123");
        EXPECT_EQ(str.size(), 0);

        const char *c_str = "12345678901234567890123456789012345678901234567890";
        str = c_str;
        EXPECT_STREQ(str.c_str(), c_str);
    }

    // move empty string
    {
        psl::string str;
        psl::string str2(psl::move(str));
        EXPECT_EQ(str2.size(), 0);
        EXPECT_EQ(str.size(), 0);
    }

    // move assign
    {
        psl::string str('a', 3);
        psl::string str2;
        str2 = psl::move(str);
        EXPECT_STREQ(str2.c_str(), "aaa");
        EXPECT_EQ(str.size(), 0);

        str = "123";
        EXPECT_STREQ(str.c_str(), "123");

        str2 = psl::move(str);
        EXPECT_STREQ(str2.c_str(), "123");
        EXPECT_EQ(str.size(), 0);

        const char *c_str = "12345678901234567890123456789012345678901234567890";
        str = c_str;
        EXPECT_STREQ(str.c_str(), c_str);
    }

    // move assign empty string
    {
        psl::string str;
        psl::string str2;
        str2 = psl::move(str);
        EXPECT_EQ(str2.size(), 0);
        EXPECT_EQ(str.size(), 0);
    }
}

TEST(psl_basic_string, modoperators)
{
    /// operator+
    {
        psl::string str1("abc");
        psl::string str2("xyz");
        EXPECT_EQ(str1 + str2, "abcxyz");
    }
    {
        psl::string str("abc");
        str = str + "xyz";
        EXPECT_STREQ(str.c_str(), "abcxyz");
    }
    {
        psl::string str("abc");
        str = str + "";
        EXPECT_STREQ(str.c_str(), "abc");
    }
    {
        psl::string str("");
        str = str + "";
        EXPECT_STREQ(str.c_str(), "");
    }

    /// operator+=
    {
        psl::string str1("abc");
        psl::string str2("xyz");
        psl::string str3("abcxyz");
        str1 += str2;
        EXPECT_EQ(str1, str3);
    }
    {
        psl::string str("abc");
        str = str += "xyz";
        EXPECT_STREQ(str.c_str(), "abcxyz");
    }
}

TEST(psl_basic_string, erase_index)
{
    {
        psl::string str("abc");
        str.erase(0);
        EXPECT_STREQ(str.c_str(), "bc");
    }
    {
        psl::string str("abc");
        str.erase(1);
        EXPECT_STREQ(str.c_str(), "ac");
    }
    {
        psl::string str("abc");
        str.erase(2);
        EXPECT_STREQ(str.c_str(), "ab");
    }
}

TEST(psl_basic_string, insert_index)
{
    {
        psl::string str("abc");
        str.insert(0, 'X');
        EXPECT_STREQ(str.c_str(), "Xabc");
    }
    {
        psl::string str("abc");
        str.insert(1, 'X');
        EXPECT_STREQ(str.c_str(), "aXbc");
    }
    {
        psl::string str("abc");
        str.insert(2, 'X');
        EXPECT_STREQ(str.c_str(), "abXc");
    }
    {
        psl::string str("abc");
        str.insert(3, 'X');
        EXPECT_STREQ(str.c_str(), "abcX");
    }
}

TEST(psl_basic_string, erase_iterator)
{
    {
        psl::string str("abc");
        str.erase(str.begin());
        EXPECT_STREQ(str.c_str(), "bc");
    }
    {
        psl::string str("abc");
        str.erase(str.begin() + 1);
        EXPECT_STREQ(str.c_str(), "ac");
    }
    {
        psl::string str("abc");
        str.erase(str.begin() + 2);
        EXPECT_STREQ(str.c_str(), "ab");
    }

    {
        psl::string str("abc");
        str.erase(str.begin());
        EXPECT_STREQ(str.c_str(), "bc");
        str.erase(str.begin());
        EXPECT_STREQ(str.c_str(), "c");
        str.erase(str.begin());
        EXPECT_STREQ(str.c_str(), "");
    }

    {
        psl::string str("abc");
        str.erase(str.begin() + 2);
        EXPECT_STREQ(str.c_str(), "ab");
        str.erase(str.begin() + 1);
        EXPECT_STREQ(str.c_str(), "a");
        str.erase(str.begin());
        EXPECT_STREQ(str.c_str(), "");
    }

}

TEST(psl_basic_string, pushpopback)
{
    /// @todo fix
    {
        psl::string str("123");
        EXPECT_STREQ(str.c_str(), "123");
        str.push_back('a');
        EXPECT_STREQ(str.c_str(), "123a");
        str.push_back('b');
        EXPECT_STREQ(str.c_str(), "123ab");
        str.push_back('c');
        EXPECT_STREQ(str.c_str(), "123abc");
    }

    {
        psl::string str;
        for (int i = 0; i < 30; ++i)
            str.push_back('X');

        EXPECT_STREQ(str.c_str(), "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    }

    {
        psl::string str;
        str.push_back('a');
        EXPECT_STREQ(str.c_str(), "a");
        str.push_back('b');
        EXPECT_STREQ(str.c_str(), "ab");
        str.push_back('c');
        EXPECT_STREQ(str.c_str(), "abc");
        str.pop_back();
        EXPECT_STREQ(str.c_str(), "ab");
        str.pop_back();
        EXPECT_STREQ(str.c_str(), "a");
        str.pop_back();
        EXPECT_STREQ(str.c_str(), "");
        EXPECT_EQ(str, "");
        EXPECT_EQ(str, psl::string());
    }

}

TEST(psl_basic_string, resize)
{
    psl::string str;
    str.resize(3);
    EXPECT_EQ(str.size(), 3);
    str.resize(30);
    EXPECT_EQ(str.size(), 30);
    str.resize(10);
    EXPECT_EQ(str.size(), 10);
    str.resize(0);
    EXPECT_EQ(str.size(), 0);
    str.resize(10);
    EXPECT_EQ(str.size(), 10);
}

TEST(psl_basic_string, reserve)
{
    psl::string str;
    str.reserve(10);
    EXPECT_TRUE(str.capacity() >= 10);

    str.reserve(100);
    EXPECT_TRUE(str.capacity() >= 100);

    str.reserve(10);
    EXPECT_TRUE(str.capacity() >= 100);

    str.reserve(1000);
    EXPECT_TRUE(str.capacity() >= 1000);
}

TEST(psl_basic_string, set_num_long)
{
    EXPECT_STREQ(psl::string().set_num(0L).c_str(), "0");
    EXPECT_STREQ(psl::string().set_num(1L).c_str(), "1");
    EXPECT_STREQ(psl::string().set_num(10L).c_str(), "10");
    EXPECT_STREQ(psl::string().set_num(981681L).c_str(), "981681");
    EXPECT_STREQ(psl::string().set_num(21548979451L).c_str(), "21548979451");

    EXPECT_STREQ(psl::string().set_num(-0L).c_str(), "0");
    EXPECT_STREQ(psl::string().set_num(-1L).c_str(), "-1");
    EXPECT_STREQ(psl::string().set_num(-10L).c_str(), "-10");
    EXPECT_STREQ(psl::string().set_num(-981681L).c_str(), "-981681");
    EXPECT_STREQ(psl::string().set_num(-21548979451L).c_str(), "-21548979451");
}


TEST(psl_basic_string, set_num_float)
{
    EXPECT_STREQ(psl::string().set_num(0.0, 0).c_str(), "0");
    EXPECT_STREQ(psl::string().set_num(1.0, 0).c_str(), "1");
    EXPECT_STREQ(psl::string().set_num(100.0, 0).c_str(), "100");
    EXPECT_STREQ(psl::string().set_num(1.0, 1).c_str(), "1.0");
    EXPECT_STREQ(psl::string().set_num(1.0, 2).c_str(), "1.00");
    EXPECT_STREQ(psl::string().set_num(1.0, 3).c_str(), "1.000");
    EXPECT_STREQ(psl::string().set_num(1.101, 3).c_str(), "1.101");
    EXPECT_STREQ(psl::string().set_num(1.00001, 5).c_str(), "1.00001");

    /// @todo fix
    EXPECT_STREQ(psl::string().set_num(0.42199, 5).c_str(), "0.42199");
    EXPECT_STREQ(psl::string().set_num(6.06424, 5).c_str(), "6.06424");
    EXPECT_STREQ(psl::string().set_num(6.44565, 5).c_str(), "6.44565");
    EXPECT_STREQ(psl::string().set_num(1.86472, 5).c_str(), "1.86472");
    EXPECT_STREQ(psl::string().set_num(3.42199, 5).c_str(), "3.42199");
    EXPECT_STREQ(psl::string().set_num(2.13777, 5).c_str(), "2.13777");

    /// @todo fix
    EXPECT_STREQ(psl::string().set_num(-0.42199, 5).c_str(), "-0.42199");
    EXPECT_STREQ(psl::string().set_num(-6.06424, 5).c_str(), "-6.06424");
    EXPECT_STREQ(psl::string().set_num(-6.44565, 5).c_str(), "-6.44565");
    EXPECT_STREQ(psl::string().set_num(-1.86472, 5).c_str(), "-1.86472");
    EXPECT_STREQ(psl::string().set_num(-3.42199, 5).c_str(), "-3.42199");
    EXPECT_STREQ(psl::string().set_num(-2.13777, 5).c_str(), "-2.13777");
}


TEST(psl_basic_string, snprintf)
{
    {
        psl::string str = psl::string().sprintf("%c%c%c", 'a', 'b', 'c');
        EXPECT_STREQ(str.c_str(), "abc");
        EXPECT_EQ(str.size(), 3);
    }
    {
        psl::string str = psl::string().sprintf("%d-%d-%d", 123, 456, 789);
        EXPECT_STREQ(str.c_str(), "123-456-789");
        EXPECT_EQ(str.size(), 11);
    }
}


TEST(psl_basic_string, append_to_empty)
{
    psl::string result;
    psl::string str("abc");

    result.reserve(str.size() * 3);
    for (int i = 0; i < 3; ++i)
    {
        result.append(str);
    }
    EXPECT_STREQ(result.c_str(), "abcabcabc");
}


TEST(psl_basic_string, split)
{
    {
        psl::string str("000");

        auto vec = str.split<psl::vector<psl::string>>('\n');

        EXPECT_EQ(vec.size(), 1);

        EXPECT_STREQ(vec[0].c_str(), "000");
    }
    {
        psl::string str("000\n111");

        auto vec = str.split<psl::vector<psl::string>>('\n');

        EXPECT_EQ(vec.size(), 2);

        EXPECT_STREQ(vec[0].c_str(), "000");
        EXPECT_STREQ(vec[1].c_str(), "111");
    }
    {
        psl::string str("\n\n000\n");

        //<>\n<>\n<000>\n<>

        auto vec = str.split<psl::vector<psl::string>>('\n');

        EXPECT_EQ(vec.size(), 4);

        EXPECT_STREQ(vec[0].c_str(), "");
        EXPECT_STREQ(vec[1].c_str(), "");
        EXPECT_STREQ(vec[2].c_str(), "000");
        EXPECT_STREQ(vec[3].c_str(), "");
    }
    {
        psl::string str("\n\n000\n");

        //<>\n<>\n<000>\n<>
        auto vec = str.split<psl::vector<psl::string>>('\n', true);

        EXPECT_EQ(vec.size(), 1);
        EXPECT_STREQ(vec[0].c_str(), "000");
    }
    {
        psl::string str("000\n111\n222\n");

        auto vec = str.split<psl::vector<psl::string>>('\n');

        EXPECT_EQ(vec.size(), 4);

        EXPECT_STREQ(vec[0].c_str(), "000");
        EXPECT_STREQ(vec[1].c_str(), "111");
        EXPECT_STREQ(vec[2].c_str(), "222");
        EXPECT_STREQ(vec[3].c_str(), "");
    }
    {
        psl::string str("000\n111\n222\n");

        auto vec = str.split<psl::vector<psl::string>>('\n', true);

        EXPECT_EQ(vec.size(), 3);

        EXPECT_STREQ(vec[0].c_str(), "000");
        EXPECT_STREQ(vec[1].c_str(), "111");
        EXPECT_STREQ(vec[2].c_str(), "222");
    }

    {
        psl::string str("\n\n\n");

        // <>\n<>\n<>\n<>
        auto vec = str.split<psl::vector<psl::string>>('\n');

        EXPECT_EQ(vec.size(), 4);

        EXPECT_STREQ(vec[0].c_str(), "");
        EXPECT_STREQ(vec[1].c_str(), "");
        EXPECT_STREQ(vec[2].c_str(), "");
        EXPECT_STREQ(vec[3].c_str(), "");
    }

    {
        psl::string str("\n\n\n");

        auto vec = str.split<psl::vector<psl::string>>('\n', true);

        EXPECT_EQ(vec.size(), 0);
    }




    //    {
    //        psl::string str("000\n111\n222");

    //        auto vec = str.split('\n');

    //        EXPECT_EQ(vec.size(), 3);

    //        EXPECT_STREQ(vec[0].c_str(), "000");
    //        EXPECT_STREQ(vec[1].c_str(), "111");
    //        EXPECT_STREQ(vec[2].c_str(), "222");

    //    }
}


TEST(psl_basic_string, contains)
{
    {
        psl::string str("wasd");
        for (auto c : str)
        {
            EXPECT_TRUE(str.contains(c));
        }
    }

    {
        psl::string str("123456789");
        EXPECT_TRUE(str.contains("1"));
        EXPECT_TRUE(str.contains("12"));
        EXPECT_TRUE(str.contains("123"));
        EXPECT_TRUE(str.contains("1234"));
        EXPECT_TRUE(str.contains("12345"));
        EXPECT_TRUE(str.contains("123456"));
        EXPECT_TRUE(str.contains("1234567"));
        EXPECT_TRUE(str.contains("12345678"));
        EXPECT_TRUE(str.contains("123456789"));
        EXPECT_TRUE(str.contains("23456789"));
        EXPECT_TRUE(str.contains("3456789"));
        EXPECT_TRUE(str.contains("456789"));
        EXPECT_TRUE(str.contains("56789"));
        EXPECT_TRUE(str.contains("6789"));
        EXPECT_TRUE(str.contains("789"));
        EXPECT_TRUE(str.contains("89"));
        EXPECT_TRUE(str.contains("9"));
        EXPECT_TRUE(str.contains("234"));
        EXPECT_TRUE(str.contains("345"));
        EXPECT_TRUE(str.contains("456"));
        EXPECT_TRUE(str.contains("567"));
        EXPECT_TRUE(str.contains("678"));

        EXPECT_FALSE(str.contains("a"));
        EXPECT_FALSE(str.contains("ab"));
        EXPECT_FALSE(str.contains("abc"));

        EXPECT_FALSE(str.contains("13"));
        EXPECT_FALSE(str.contains("135"));
    }
}

TEST(psl_basic_string, find)
{
    {
        psl::string str("0123210");
        EXPECT_EQ(str.find("0"), 0);
        EXPECT_EQ(str.find('0'), 0);
        EXPECT_EQ(str.find("1"), 1);
        EXPECT_EQ(str.find('1'), 1);
        EXPECT_EQ(str.find("2"), 2);
        EXPECT_EQ(str.find('2'), 2);
        EXPECT_EQ(str.find("3"), 3);
        EXPECT_EQ(str.find('3'), 3);

        /// @todo implement rfind(basic_string)
        //EXPECT_EQ(str.rfind("0"), 6);
        EXPECT_EQ(str.rfind('0'), 6);
        //EXPECT_EQ(str.rfind("1"), 5);
        EXPECT_EQ(str.rfind('1'), 5);
        //EXPECT_EQ(str.rfind("2"), 4);
        EXPECT_EQ(str.rfind('2'), 4);
        //EXPECT_EQ(str.rfind("3"), 3);
        EXPECT_EQ(str.rfind('3'), 3);

    }

    {
        psl::string str("123456789");
        EXPECT_EQ(str.find("1"), 0);
        EXPECT_EQ(str.find("12"), 0);
        EXPECT_EQ(str.find("123"), 0);
        EXPECT_EQ(str.find("1234"), 0);
        EXPECT_EQ(str.find("12345"), 0);
        EXPECT_EQ(str.find("123456"), 0);
        EXPECT_EQ(str.find("1234567"), 0);
        EXPECT_EQ(str.find("12345678"), 0);
        EXPECT_EQ(str.find("123456789"), 0);
        EXPECT_EQ(str.find("23456789"), 1);
        EXPECT_EQ(str.find("3456789"), 2);
        EXPECT_EQ(str.find("456789"), 3);
        EXPECT_EQ(str.find("56789"), 4);
        EXPECT_EQ(str.find("6789"), 5);
        EXPECT_EQ(str.find("789"), 6);
        EXPECT_EQ(str.find("89"), 7);
        EXPECT_EQ(str.find("9"), 8);
        EXPECT_EQ(str.find("234"), 1);
        EXPECT_EQ(str.find("345"), 2);
        EXPECT_EQ(str.find("456"), 3);
        EXPECT_EQ(str.find("567"), 4);
        EXPECT_EQ(str.find("678"), 5);

        EXPECT_EQ(str.find("a"), psl::string::npos);
        EXPECT_EQ(str.find("ab"), psl::string::npos);
        EXPECT_EQ(str.find("abc"), psl::string::npos);

        EXPECT_EQ(str.find("13"), psl::string::npos);
        EXPECT_EQ(str.find("135"), psl::string::npos);
    }

    {
        psl::string str("ABCABC");
        EXPECT_EQ(str.find("ABC", 0), 0);
        EXPECT_EQ(str.find("ABC", 1), 3);
        EXPECT_EQ(str.find("ABC", 2), 3);
        EXPECT_EQ(str.find("ABC", 3), 3);
        EXPECT_EQ(str.find("ABC", 4), psl::string::npos);
    }

}



///// DOCTEST PORT:

TEST(psl_basic_string, empty)
{
    psl::string x;
    psl::string y;

    EXPECT_EQ(x.size(),0);
    for(size_t i{0};i<100;++i){
        x.insert(x.begin(),'5');
        y.push_back('3');
    }
    EXPECT_EQ(x.size(),y.size());
    EXPECT_EQ(x.size(),100);
}

TEST(psl_basic_string, default_constructible_iterator)
{
    psl::string::iterator a;
    psl::string::const_iterator b;
}

TEST(psl_basic_string, copy_constructible_iterator)
{
    psl::string v({1,2});
    psl::string::iterator a{v.begin()};
    psl::string::iterator b{a};
    EXPECT_EQ(a, b);
}

TEST(psl_basic_string, copy_assignable_iterator)
{
    psl::string v({1,2});
    psl::string::iterator a{v.begin()};
    psl::string::iterator b{v.end()};
    EXPECT_NE(a, b);
    a = b;
    EXPECT_EQ(a, b);
}

TEST(psl_basic_string, pre_increment_iterator)
{
    psl::string v("1");
    psl::string::iterator a{v.begin()};
    psl::string::iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_EQ(++a, b);
}

TEST(psl_basic_string, post_increment_iterator)
{
    psl::string v("1");
    psl::string::iterator a{v.begin()};
    psl::string::iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_NE(a++, b);
    EXPECT_EQ(a, b);
}

TEST(psl_basic_string, dereference)
{
    psl::string v("01234");
    const char * c_str = "01234";
    {
        size_t cnt = 0;
        for(auto it = v.begin(); it != v.end(); ++it)
        {
            EXPECT_EQ(*it, c_str[cnt++]);
        }
        EXPECT_EQ(cnt, 5);
    }
    auto it = v.begin();
    EXPECT_EQ(*it, '0');
    *it = '9';
    EXPECT_EQ(*it, '9');
}

TEST(psl_basic_string, arrow_operator_iterator)
{
    psl::string v("01234");
    const char * c_str = "01234";

    psl::string::iterator a{v.begin()};
    psl::string::iterator b{v.end()};
    (void)a;
    (void)b;

    {
        size_t cnt = 0;
        for(auto it = v.begin(); it != v.end(); ++it) {
            EXPECT_EQ(*(it.operator->()), c_str[cnt++]);
        }
        EXPECT_EQ(cnt, 5);
    }
}

/**
 * ConstIterator
 */
TEST(psl_basic_string, copy_constructible_const_iterator)
{
    psl::string v("1,2");
    psl::string::const_iterator a{v.begin()};
    psl::string::const_iterator b{a};
    EXPECT_EQ(a, b);
}

TEST(psl_basic_string, copy_assignable_const_iterator)
{
    psl::string v("1,2");
    psl::string::const_iterator a{v.begin()};
    psl::string::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    a = b;
    EXPECT_EQ(a, b);
}

TEST(psl_basic_string, pre_increment_const_iterator)
{
    psl::string v("1");
    psl::string::const_iterator a{v.begin()};
    psl::string::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_EQ(++a, b);
}

TEST(psl_basic_string, post_increment_const_iterator)
{
    psl::string v("1");
    psl::string::const_iterator a{v.begin()};
    psl::string::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_NE(a++, b);
    EXPECT_EQ(a, b);
}

TEST(psl_basic_string, dereference_const_iterator)
{
    psl::string v("01234");
    psl::string::const_iterator beg{v.begin()};
    psl::string::const_iterator end{v.end()};
    {
        size_t cnt = 0;
        for(; beg != end; ++beg) {
            EXPECT_EQ(*beg, v[cnt++]);
        }
        EXPECT_EQ(cnt, 5);
    }
}

TEST(psl_basic_string, substr)
{
    psl::string str("0123456789");
    EXPECT_EQ(str.substr(0, 3).size(), 3);
    EXPECT_STREQ(str.substr(0, 3).c_str(), "012");
    EXPECT_STREQ(str.substr(1, 3).c_str(), "123");
    EXPECT_STREQ(str.substr(2, 3).c_str(), "234");
    EXPECT_STREQ(str.substr(3, 3).c_str(), "345");
    EXPECT_STREQ(str.substr(4, 3).c_str(), "456");
    EXPECT_STREQ(str.substr(5, 3).c_str(), "567");
    EXPECT_STREQ(str.substr(6, 3).c_str(), "678");
    EXPECT_STREQ(str.substr(7, 3).c_str(), "789");

    EXPECT_STREQ(str.substr(0).c_str(), "0123456789");
    EXPECT_STREQ(str.substr(1).c_str(), "123456789");
    EXPECT_STREQ(str.substr(2).c_str(), "23456789");
    EXPECT_STREQ(str.substr(3).c_str(), "3456789");
    EXPECT_STREQ(str.substr(4).c_str(), "456789");
    EXPECT_STREQ(str.substr(5).c_str(), "56789");
    EXPECT_STREQ(str.substr(6).c_str(), "6789");
    EXPECT_STREQ(str.substr(7).c_str(), "789");
    EXPECT_STREQ(str.substr(8).c_str(), "89");
    EXPECT_STREQ(str.substr(9).c_str(), "9");

    EXPECT_EQ(str.substr(0).size(), 10);
    EXPECT_EQ(str.substr(1).size(), 9);
    EXPECT_EQ(str.substr(2).size(), 8);
    EXPECT_EQ(str.substr(3).size(), 7);
    EXPECT_EQ(str.substr(4).size(), 6);
    EXPECT_EQ(str.substr(5).size(), 5);
    EXPECT_EQ(str.substr(6).size(), 4);
    EXPECT_EQ(str.substr(7).size(), 3);
    EXPECT_EQ(str.substr(8).size(), 2);
    EXPECT_EQ(str.substr(9).size(), 1);
}

TEST(psl_basic_string, back_front)
{
    EXPECT_EQ(psl::string("A").back(), 'A');
    EXPECT_EQ(psl::string("AB").back(), 'B');
    EXPECT_EQ(psl::string("ABC").back(), 'C');

    EXPECT_EQ(psl::string("Z").front(), 'Z');
    EXPECT_EQ(psl::string("YZ").front(), 'Y');
    EXPECT_EQ(psl::string("XYZ").front(), 'X');
}

TEST(psl_basic_string, operators)
{
    EXPECT_TRUE(psl::string("A") == "A");
    EXPECT_TRUE(psl::string("AB") == "AB");
    EXPECT_TRUE(psl::string("ABC") == "ABC");

    EXPECT_TRUE(psl::string(".") == ".");
    EXPECT_TRUE(psl::string("..") == "..");

    EXPECT_FALSE(psl::string(".") == "..");
    EXPECT_FALSE(psl::string("..") == ".");
}

TEST(psl_basic_string, size)
{
    EXPECT_EQ(psl::string("A").size(), 1);
    EXPECT_EQ(psl::string("AB").size(), 2);
    EXPECT_EQ(psl::string("ABC").size(), 3);
}

TEST(psl_basic_string, wchar)
{
    {
        psl::wstring str;
        EXPECT_EQ(str.length(), 0);
        EXPECT_STREQ(str.c_str(), L"");
    }
    {
        psl::wstring str(L"abc");
        EXPECT_STREQ(str.c_str(), L"abc");
        EXPECT_EQ(str, str);
    }
    {
        psl::wstring str(L'a', 3);
        EXPECT_STREQ(str.c_str(), L"aaa");
        EXPECT_EQ(str, str);
    }

}

TEST(psl_basic_string, unicode)
{
    {
        psl::string str("é");
        EXPECT_EQ(str.size(), 2);
        EXPECT_STREQ(str.c_str(), "é");
        EXPECT_EQ(static_cast<unsigned char>(str[0]), 0xC3);
        EXPECT_EQ(static_cast<unsigned char>(str[1]), 0xA9);
    }
    {
        psl::string str("olé");
        EXPECT_EQ(str.size(), 4);
        EXPECT_STREQ(str.c_str(), "olé");
        EXPECT_EQ(static_cast<unsigned char>(str[0]), static_cast<unsigned int>('o'));
        EXPECT_EQ(static_cast<unsigned char>(str[1]), static_cast<unsigned int>('l'));
        EXPECT_EQ(static_cast<unsigned char>(str[2]), 0xC3);
        EXPECT_EQ(static_cast<unsigned char>(str[3]), 0xA9);
    }
    {
        psl::string str("😀");
        EXPECT_EQ(str.size(), 4);
        EXPECT_STREQ(str.c_str(), "😀");
        EXPECT_EQ(static_cast<unsigned char>(str[0]), 0xF0);
        EXPECT_EQ(static_cast<unsigned char>(str[1]), 0x9F);
        EXPECT_EQ(static_cast<unsigned char>(str[2]), 0x98);
        EXPECT_EQ(static_cast<unsigned char>(str[3]), 0x80);
    }
    {
        psl::string str;
        str.append(static_cast<char>(0xF0));
        str.append(static_cast<char>(0x9F));
        str.append(static_cast<char>(0x98));
        str.append(static_cast<char>(0x80));
        EXPECT_EQ(str.size(), 4);
        EXPECT_STREQ(str.c_str(), "😀");
    }
}

