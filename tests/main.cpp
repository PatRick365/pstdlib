#include "ptest/ptest.h"


int main(int argc, char **argv)
{
    if (ptest::run_all_tests(argc, argv))
        return 0;

    return 1;
}

