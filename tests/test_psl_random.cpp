#include "psl_random.h"

#include "ptest/ptest.h"


TEST(psl_random, engines)
{
    // psl::mt19937 gen32;
    // gen32.discard(10000 - 1);
    // EXPECT_EQ(gen32(), 4123659995);

    // psl::mt19937_64 gen64;
    // gen64.discard(10000 - 1);
    // EXPECT_EQ(gen64(), 9981545732273789042ull);

    // above test is not working. my implementation based from https://en.wikipedia.org/wiki/Mersenne_Twister produces other results..


    // test adjacent numbers should not be equal
    {
        psl::mt19937 gen(13);
        psl::mt19937::result_type last = 0;
        for (int i = 0; i < 10000; ++i)
        {
            EXPECT_NE(gen(), last);
        }
    }
    {
        psl::mt19937_64 gen(13);
        psl::mt19937_64::result_type last = 0;
        for (int i = 0; i < 10000; ++i)
        {
            EXPECT_NE(gen(), last);
        }
    }
    {
        psl::pcg_32 gen(13);
        psl::pcg_32::result_type last = 0;
        for (int i = 0; i < 10000; ++i)
        {
            EXPECT_NE(gen(), last);
        }
    }
    {
        psl::pcg_64 gen(13);
        psl::pcg_64::result_type last = 0;
        for (int i = 0; i < 10000; ++i)
        {
            EXPECT_NE(gen(), last);
        }
    }
}
