#include "ptest/ptest.h"

#include "psl_from_string.h"

TEST(psl_from_string, basic_tests)
{
    int i = 0;
    bool ok = false;

    i = psl::from_string<int>("0000", &ok);
    EXPECT_EQ(i, 0);
    EXPECT_TRUE(ok);

    i = psl::from_string<int>("123", &ok);
    EXPECT_EQ(i, 123);
    EXPECT_TRUE(ok);

    i = psl::from_string<int>("-12", &ok);
    EXPECT_EQ(i, -12);
    EXPECT_TRUE(ok);

    i = psl::from_string<int>("ABC", &ok);
    EXPECT_EQ(i, 0);
    EXPECT_FALSE(ok);

    i = psl::from_string<int>(psl::string("123"), &ok);
    EXPECT_EQ(i, 123);
    EXPECT_TRUE(ok);
}

TEST(psl_from_string, test_all_basic_types)
{
    {
        bool ok = false;
        auto v = psl::from_string<char>("A", &ok);
        EXPECT_TRUE(ok);
        EXPECT_EQ(v, 'A');
    }
    {
        bool ok = false;
        auto v = psl::from_string<unsigned char>("100", &ok);
        EXPECT_TRUE(ok);
        EXPECT_EQ(v, 100);
    }
    {
        bool ok = false;
        auto v = psl::from_string<int>("666", &ok);
        EXPECT_TRUE(ok);
        EXPECT_EQ(v, 666);
    }
    {
        bool ok = false;
        auto v = psl::from_string<unsigned int>("42", &ok);
        EXPECT_TRUE(ok);
        EXPECT_EQ(v, 42);
    }
    {
        bool ok = false;
        auto v = psl::from_string<long>("1000", &ok);
        EXPECT_TRUE(ok);
        EXPECT_EQ(v, 1000);
    }
    {
        bool ok = false;
        auto v = psl::from_string<unsigned long>("999", &ok);
        EXPECT_TRUE(ok);
        EXPECT_EQ(v, 999);
    }
    {
        bool ok = false;
        auto v = psl::from_string<long long>("999", &ok);
        EXPECT_TRUE(ok);
        EXPECT_EQ(v, 999);
    }
    {
        bool ok = false;
        auto v = psl::from_string<unsigned long long>("999", &ok);
        EXPECT_TRUE(ok);
        EXPECT_EQ(v, 999);
    }
    {
        bool ok = false;
        auto v = psl::from_string<float>("3.14", &ok);
        EXPECT_TRUE(ok);
        EXPECT_FLOAT_EQ(v, 3.14f);
    }
    {
        bool ok = false;
        auto v = psl::from_string<double>("3.14", &ok);
        EXPECT_TRUE(ok);
        EXPECT_FLOAT_EQ(v, 3.14);
    }
    {
        bool ok = false;
        auto v = psl::from_string<long double>("3.14", &ok);
        EXPECT_TRUE(ok);
        EXPECT_FLOAT_EQ(v, 3.14);
    }
}
