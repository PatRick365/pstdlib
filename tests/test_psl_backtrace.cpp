#include "psl_backtrace.h"

#include "ptest/ptest.h"


/*
/usr/lib/libasan.so.8(___interceptor_backtrace+0xa4) [0x7123da8a8834]
/home/pat/workspace/cpp/pstdlib/build/Debug/libpstdlib.so.1(_ZN3psl9backtraceC1Ev+0x144) [0x7123db0a0aba]
/home/pat/workspace/cpp/pstdlib/build/Debug/pstdlib_tests(+0x34836f) [0x5d36474db36f]
/home/pat/workspace/cpp/pstdlib/build/Debug/pstdlib_tests(+0x34874e) [0x5d36474db74e]
/home/pat/workspace/cpp/pstdlib/build/Debug/pstdlib_tests(+0x34881b) [0x5d36474db81b]
/home/pat/workspace/cpp/pstdlib/build/Debug/pstdlib_tests(+0x348838) [0x5d36474db838]
/home/pat/workspace/cpp/pstdlib/build/Debug/extern/ptest/libptest.so.1(_ZN5ptest14run_test_casesERKNS_20test_case_collectionE+0x244) [0x7123db04787c]
/home/pat/workspace/cpp/pstdlib/build/Debug/extern/ptest/libptest.so.1(_ZN5ptest13run_all_testsEiPPc+0x2b4) [0x7123db04800a]
/home/pat/workspace/cpp/pstdlib/build/Debug/pstdlib_tests(+0x46ae9) [0x5d36471d9ae9]
/usr/lib/libc.so.6(+0x27488) [0x7123da235488]
/usr/lib/libc.so.6(__libc_start_main+0x8c) [0x7123da23554c]
/home/pat/workspace/cpp/pstdlib/build/Debug/pstdlib_tests(+0x469f5) [0x5d36471d99f5]
*/

TEST(psl_backtrace, basic_tests)
{
    psl::backtrace bt;
    psl::vector<psl::string_view> frames = bt.frames();
    EXPECT_FALSE(frames.empty());

    auto v = psl::demangle("aef4rds4gdr4gd4r8g4dr8g4dr8g4drg");
    EXPECT_FALSE(v.has_value());

    // for (const auto &frame : frames)
    // {
    //     printf("%s\n", frame.data());
    // }

}
