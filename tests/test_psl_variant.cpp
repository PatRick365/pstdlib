#include "psl_variant.h"
#include "psl_string_vec.h"
#include "psl_type_traits.h"

#include "ptest/ptest.h"

TEST(psl_variant, variant_vector)
{
    auto vvec = psl::string_vector() << "hey" << "you" << "nice" << "person";

    auto v = psl::variant_vector() << 42 << 42U << 42L << 42UL << 42.42F << 42.42 << psl::string("hey") << vvec;

    auto it_is = v.begin();
    ASSERT_TRUE(it_is->is_int());
    ASSERT_TRUE((++it_is)->is_uint());
    ASSERT_TRUE((++it_is)->is_long());
    ASSERT_TRUE((++it_is)->is_ulong());
    ASSERT_TRUE((++it_is)->is_float());
    ASSERT_TRUE((++it_is)->is_double());
    ASSERT_TRUE((++it_is)->is_string());
    ASSERT_TRUE((++it_is)->is_string_vector());

    auto it_get = v.begin();
    EXPECT_EQ(it_get->get_int(), 42);
    EXPECT_EQ((++it_get)->get_uint(), static_cast<unsigned int>(42));
    EXPECT_EQ((++it_get)->get_long(), 42L);
    EXPECT_EQ((++it_get)->get_ulong(), static_cast<unsigned long>(42L));
    EXPECT_EQ((++it_get)->get_float(), 42.42f);
    EXPECT_EQ((++it_get)->get_double(), 42.42);
    EXPECT_STREQ((++it_get)->get_string().c_str(), "hey");
    EXPECT_EQ((++it_get)->get_string_vector(), vvec);

    auto it_get_free = v.begin();
    {
    auto val = psl::get<int>(*(it_get_free));
    EXPECT_EQ(val, 42);
    EXPECT_TRUE((psl::is_same<decltype(val), int>()));
    }
    {
    auto val = psl::get<unsigned int>(*(++it_get_free));
    EXPECT_EQ(val, 42);
    EXPECT_TRUE((psl::is_same<decltype(val), unsigned int>()));
    }
    {
    auto val = psl::get<long>(*(++it_get_free));
    EXPECT_EQ(val, 42);
    EXPECT_TRUE((psl::is_same<decltype(val), long>()));
    }
    {
    auto val = psl::get<unsigned long>(*(++it_get_free));
    EXPECT_EQ(val, 42);
    EXPECT_TRUE((psl::is_same<decltype(val), unsigned long>()));
    }
    {
    auto val = psl::get<float>(*(++it_get_free));
    EXPECT_EQ(val, 42.42f);
    EXPECT_TRUE((psl::is_same<decltype(val), float>()));
    }
    {
    auto val = psl::get<double>(*(++it_get_free));
    EXPECT_EQ(val, 42.42);
    EXPECT_TRUE((psl::is_same<decltype(val), double>()));
    }
    {
    auto val = psl::get<psl::string>(*(++it_get_free));
    EXPECT_EQ(val, "hey");
    EXPECT_TRUE((psl::is_same<decltype(val), psl::string>()));
    }
    {
    auto val = psl::get<psl::string_vector>(*(++it_get_free));
    EXPECT_EQ(val, vvec);
    EXPECT_TRUE((psl::is_same<decltype(val), psl::string_vector>()));
    }
}

TEST(psl_variant, visit)
{
    {
        psl::variant v(13);
        auto fn = [](int i) -> long { return long(i * 2); };
        long l = psl::visit<long, int>(fn, v);
        EXPECT_EQ(l, 26L);
    }
    {
        psl::variant v(13);
        long l = 0;
        auto fn = [&l](int i) -> void { l = (i * 2); };
        psl::visit<void, int>(fn, v);
        EXPECT_EQ(l, 26L);
    }
}
