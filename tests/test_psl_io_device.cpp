#include "psl_iodevice.h"

#include "ptest/ptest.h"


class TestIo : public psl::io_device
{
    static constexpr size_t SIZE = 1000;
public:
    TestIo()
    {
        int counter = 0;
        for (size_t i = 0; i < SIZE; ++i)
        {
            buff_read[i] = counter;

            if (++counter > 9)
                counter = 0;
        }
    }

    bool is_open() const override { return true; }
    bool close() override { return true; }

protected:
    bool write_intern(const uint8_t *data, size_t len) override
    {
        if (buff_write_pos + len > SIZE)
            return false;
        ::memcpy(&buff_write[buff_write_pos], data, len);
        buff_write_pos += len;
        return true;;
    }
    ssize_t read_intern(uint8_t *data, size_t len) override
    {
        if (buff_read_pos + len > SIZE)
            return -1;
        ::memcpy(data, &buff_read[buff_read_pos], len);
        buff_read_pos += len;
        return true;
    }
public:
    char buff_write[SIZE]{};
    size_t buff_write_pos = 0;
    char buff_read[SIZE]{};
    size_t buff_read_pos = 0;
};


//TEST(psl_iodevice, io_pipe)
//{
//    TestIo from;
//    TestIo to;

//    psl::io_pipe pipe(&from, &to);

//    //ASSERT_TRUE(to.write("ABC"));

//    sleep(1);
//    pipe.stop();

//    EXPECT_EQ(to.buff_write[0], '0');
//    EXPECT_EQ(to.buff_write[1], '1');
//    EXPECT_EQ(to.buff_write[2], '2');
//    EXPECT_EQ(to.buff_write[3], '3');
//    EXPECT_EQ(to.buff_write[4], '4');
//    EXPECT_EQ(to.buff_write[5], '5');
//    EXPECT_EQ(to.buff_write[6], '6');
//    EXPECT_EQ(to.buff_write[7], '7');
//    EXPECT_EQ(to.buff_write[8], '8');
//    EXPECT_EQ(to.buff_write[9], '9');
//    EXPECT_EQ(to.buff_write[10], '0');

//}
