#include "psl_algorithm.h"
#include "psl_vector.h"
#include "psl_string.h"
#include "psl_functional.h"


#include "ptest/ptest.h"
#include "test_helpers.hpp"
#include <ctype.h>

TEST(psl_algorithm, find_vec)
{
    psl::vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);

    EXPECT_EQ(psl::find(v.begin(), v.end(), 1), v.begin());
    EXPECT_EQ(psl::find(v.begin(), v.end(), 2), ++v.begin());
    EXPECT_EQ(psl::find(v.begin(), v.end(), 3), v.begin() + 2);
    EXPECT_EQ(psl::find(v.begin(), v.end(), 42), v.end());

}

TEST(psl_algorithm, find_string)
{
    psl::string str;
    str.push_back('1');
    str.push_back('\n');
    str.push_back('3');

    EXPECT_EQ(psl::find(str.begin(), str.end(), '1'), str.begin());
    EXPECT_EQ(psl::find(str.begin(), str.end(), '\n'), ++str.begin());
    EXPECT_EQ(psl::find(str.begin(), str.end(), '3'), str.begin() + 2);
    EXPECT_EQ(psl::find(str.begin(), str.end(), '7'), str.end());

}

TEST(psl_algorithm, find_pos)
{
    psl::vector<int> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);

    EXPECT_EQ(psl::find_pos(v.begin(), v.end(), 1), 0);
    EXPECT_EQ(psl::find_pos(v.begin(), v.end(), 2), 1);
    EXPECT_EQ(psl::find_pos(v.begin(), v.end(), 3), 2);
    EXPECT_EQ(psl::find_pos(v.begin(), v.end(), 42), psl::npos);

}

TEST(psl_algorithm, min)
{
    EXPECT_EQ(psl::min(1, 3), 1);
    EXPECT_EQ(psl::min(3, 1), 1);
    EXPECT_EQ(psl::min(1, 1), 1);
    EXPECT_EQ(psl::min(-234, 234), -234);

    //EXPECT_EQ(psl::min(1, 2, 3), 1);

}

TEST(psl_algorithm, max)
{
    EXPECT_EQ(psl::max(1, 3), 3);
    EXPECT_EQ(psl::max(3, 1), 3);
    EXPECT_EQ(psl::max(1, 1), 1);
    EXPECT_EQ(psl::max(-234, 234), 234);
}

TEST(psl_algorithm, count)
{
    // vector
    {
        psl::vector<psl::string> vec;
        EXPECT_EQ(psl::count(vec.begin(), vec.end(), "XXX"), 0);
    }
    {
        auto vec = psl::vector<psl::string>() << psl::string("AAA")
                                              << psl::string("AAA")
                                              << psl::string("BBB")
                                              << psl::string("CCC");

        EXPECT_EQ(psl::count(vec.begin(), vec.end(), "XXX"), 0);
        EXPECT_EQ(psl::count(vec.begin(), vec.end(), "AAA"), 2);
        EXPECT_EQ(psl::count(vec.begin(), vec.end(), "BBB"), 1);
        EXPECT_EQ(psl::count(vec.begin(), vec.end(), "CCC"), 1);
    }

    // string
    {
        psl::string str;
        EXPECT_EQ(psl::count(str.begin(), str.end(), 'X'), 0);
    }
    {
        psl::string str("AABC");
        EXPECT_EQ(psl::count(str.begin(), str.end(), 'X'), 0);
        EXPECT_EQ(psl::count(str.begin(), str.end(), 'A'), 2);
        EXPECT_EQ(psl::count(str.begin(), str.end(), 'B'), 1);
        EXPECT_EQ(psl::count(str.begin(), str.end(), 'C'), 1);
    }

}

TEST(psl_algorithm, contains)
{
    // vector
    {
        psl::vector<psl::string> vec;
        EXPECT_EQ(psl::count(vec.begin(), vec.end(), "XXX"), 0);
    }
    {
        auto vec = psl::vector<psl::string>() << "AAA" << "AAA" << "BBB" << "CCC";
        EXPECT_EQ(psl::count(vec.begin(), vec.end(), "XXX"), 0);
        EXPECT_EQ(psl::count(vec.begin(), vec.end(), "AAA"), 2);
        EXPECT_EQ(psl::count(vec.begin(), vec.end(), "BBB"), 1);
        EXPECT_EQ(psl::count(vec.begin(), vec.end(), "CCC"), 1);
    }

    // string
    {
        psl::string str;
        EXPECT_FALSE(psl::contains(str.begin(), str.end(), 'X'));
    }
    {
        psl::string str("AABC");
        EXPECT_FALSE(psl::contains(str.begin(), str.end(), 'X'));
        EXPECT_TRUE(psl::contains(str.begin(), str.end(), 'A'));
        EXPECT_TRUE(psl::contains(str.begin(), str.end(), 'B'));
        EXPECT_TRUE(psl::contains(str.begin(), str.end(), 'C'));
    }
}

struct S
{
    S() {}
    explicit S(int i)
        : m_i(i)
    {}
    int m_i;
};

bool find_function(const S &s)
{
    return s.m_i == 333;
}

struct find_functor
{
    bool operator()(const S &s) const
    {
        return s.m_i == 333;
    }
};

TEST(psl_algorithm, find_if)
{
    auto vec = psl::vector<S>() << S(1) << S(22)<< S(333) << S(4444);

    auto it = vec.begin() + 2;

    // not found
    {
        auto it_f = psl::find_if(vec.begin(), vec.end(), [](const S &s) -> bool { return (s.m_i == 1234); });
        EXPECT_NE(it_f, it);
        EXPECT_EQ(it_f, vec.end());
    }
    // lambda
    {
        auto it_f = psl::find_if(vec.begin(), vec.end(), [](const S &s) -> bool { return (s.m_i == 333); });
        EXPECT_EQ(it_f, it);
        EXPECT_NE(it_f, vec.end());

    }
    // functor
    {
        auto it_f = psl::find_if(vec.begin(), vec.end(), find_functor());
        EXPECT_EQ(it_f, it);
        EXPECT_NE(it_f, vec.end());
    }
    // pass lambda
    {
        auto ll = [](const S &s) -> bool { return (s.m_i == 333); };
        auto it_f = psl::find_if(vec.begin(), vec.end(), ll);
        EXPECT_EQ(it_f, it);
        EXPECT_NE(it_f, vec.end());
    }
    // pass psl::function constructed from lambda
    {
        psl::function<bool(const S &val)> ff([](const S &s) -> bool { return (s.m_i == 333); });
        auto it_f = psl::find_if(vec.begin(), vec.end(), ff);
        EXPECT_EQ(it_f, it);
        EXPECT_NE(it_f, vec.end());
    }
    // pass psl::function constructed from function pointer
    {
        psl::function<bool(const S &val)> ff(find_function);
        auto it_f = psl::find_if(vec.begin(), vec.end(), ff);
        EXPECT_EQ(it_f, it);
        EXPECT_NE(it_f, vec.end());
    }
    {
        auto ll = [](const S &s) -> bool { return (s.m_i == 333); };
        psl::function<bool(const S &val)> ff(ll);
        auto it_f = psl::find_if(vec.begin(), vec.end(), ff);
        EXPECT_EQ(it_f, it);
        EXPECT_NE(it_f, vec.end());
    }
    // pass psl::function constructed from functor
    //    {
    //        psl::function<bool(const S &val)> ff((functor(const S &val)));
    //        auto it_f = psl::find_if(vec.begin(), vec.end(), ff);
    //        EXPECT_EQ(it_f, it);
    //    }
}

TEST(psl_algorithm, find_pos_if)
{
    auto vec = psl::vector<S>() << S(1) << S(22) << S(333) << S(4444);

    size_t pos = 2;

    // not found
    {
        size_t it_f = psl::find_pos_if(vec.begin(), vec.end(), [](const S &s) -> bool { return (s.m_i == 1234); });
        EXPECT_NE(it_f, pos);
        EXPECT_EQ(it_f, psl::npos);
    }
    // lambda
    {
        size_t it_f = psl::find_pos_if(vec.begin(), vec.end(), [](const S &s) -> bool { return (s.m_i == 333); });
        EXPECT_EQ(it_f, pos);
        EXPECT_NE(it_f, psl::npos);
    }
    // functor
    {
        auto it_f = psl::find_pos_if(vec.begin(), vec.end(), find_functor());
        EXPECT_EQ(it_f, pos);
        EXPECT_NE(it_f, psl::npos);
    }
    // pass lambda
    {
        auto ll = [](const S &s) -> bool { return (s.m_i == 333); };
        size_t it_f = psl::find_pos_if(vec.begin(), vec.end(), ll);
        EXPECT_EQ(it_f, pos);
        EXPECT_NE(it_f, psl::npos);
    }
    // pass psl::function constructed from lambda
    {
        psl::function<bool(const S &val)> ff([](const S &s) -> bool { return (s.m_i == 333); });
        size_t it_f = psl::find_pos_if(vec.begin(), vec.end(), ff);
        EXPECT_EQ(it_f, pos);
        EXPECT_NE(it_f, psl::npos);
    }
    // pass psl::function constructed from function pointer
    {
        psl::function<bool(const S &val)> ff(find_function);
        size_t it_f = psl::find_pos_if(vec.begin(), vec.end(), ff);
        EXPECT_EQ(it_f, pos);
        EXPECT_NE(it_f, psl::npos);
    }
    // pass psl::function constructed from functor
    //    {
    //        psl::function<bool(const S &val)> ff(find_functor);
    //        ssize_t it_f = psl::find_pos_if(vec.begin(), vec.end(), ff);
    //        EXPECT_EQ(it_f, pos);
    //        EXPECT_NE(it_f, 1);
    //    }
}

TEST(psl_algorithm, min_element_max_element)
{
    {
    auto vec = psl::vector<int>() << 333 << 1 << 4444 << 22;
    EXPECT_EQ(*psl::min_element(vec.begin(), vec.end()),  1);
    EXPECT_EQ(*psl::max_element(vec.begin(), vec.end()),  4444);
    }

    {
    auto vec = psl::vector<int>() << 1 << 1 << 1 << 1;
    EXPECT_EQ(*psl::min_element(vec.begin(), vec.end()),  1);
    EXPECT_EQ(*psl::max_element(vec.begin(), vec.end()),  1);
    }

    {
    auto vec = psl::vector<int>();
    EXPECT_EQ(psl::min_element(vec.begin(), vec.end()), vec.end());
    EXPECT_EQ(psl::max_element(vec.begin(), vec.end()), vec.end());
    }

}

TEST(psl_algorithm, min_max_element)
{

    {
    double a = 1.6, b = 13.5;
    auto pair = psl::min_max(a, b);
    EXPECT_EQ(&pair.first, &a);
    EXPECT_EQ(&pair.second, &b);
    }
    {
    double a = 13.5, b = 1.6;
    auto pair = psl::min_max(a, b);
    EXPECT_EQ(&pair.first, &b);
    EXPECT_EQ(&pair.second, &a);
    }
    {
    int a = 13, b = 13;
    auto pair = psl::min_max(a, b);
    EXPECT_EQ(&pair.first, &a);
    EXPECT_EQ(&pair.second, &b);
    }
    {
    int a = 13, b = 13;
    auto pair = psl::min_max(b, a);
    EXPECT_EQ(&pair.first, &b);
    EXPECT_EQ(&pair.second, &a);
    }

    {
        psl::string str_short("a"), str_long("aaaaaaaa");
        {
            auto pair = psl::min_max(str_short, str_long, [](const psl::string &a, const psl::string &b)
            {
                return a.size() < b.size();
            });
            EXPECT_EQ(&pair.first, &str_short);
            EXPECT_EQ(&pair.second, &str_long);
        }
        {
            auto pair = psl::min_max(str_long, str_short, [](const psl::string &a, const psl::string &b)
            {
                return a.size() < b.size();
            });
            EXPECT_EQ(&pair.first, &str_short);
            EXPECT_EQ(&pair.second, &str_long);
        }
    }

}

TEST(psl_algorithm, for_each)
{
    struct Sum
    {
        void operator()(int n) { sum += n; }
        int sum{0};
    };

    auto nums = psl::vector<int>() << 3 << 4 << 2 << 8 << 15 << 267;

    psl::for_each(nums.begin(), nums.end(), [](int &n){ n++; });

    // calls Sum::operator() for each number
    Sum s = psl::for_each(nums.begin(), nums.end(), Sum());

    EXPECT_EQ(s.sum, 305);
}

TEST(psl_algorithm, reverse)
{
    {
        auto nums1 = psl::vector<int>() << 1 << 2 << 3 << 4 << 5 << 6;
        psl::reverse(nums1.begin(), nums1.end());

        auto nums2 = psl::vector<int>() << 6 << 5 << 4 << 3 << 2 << 1;

        ASSERT_EQ(nums1.size(), nums2.size());

        for (size_t i = 0; i < nums1.size(); ++i)
        {
            EXPECT_EQ(nums1[i], nums2[i]);
        }
    }
    {
        //https://stackoverflow.com/a/8469002/5473659
        //auto nums1 = psl::vector<TestHelp::MoveOnlyObj>() << TestHelp::MoveOnlyObj(1) << TestHelp::MoveOnlyObj(2) << TestHelp::MoveOnlyObj(3) << TestHelp::MoveOnlyObj(4);

        //psl::vector<TestHelp::MoveOnlyObj> objs1;
        //objs1.emplace_back(1);
    }
}

TEST(psl_algorithm, count_if)
{
    auto nums = psl::vector<int>() << 1 << 2 << 3 << 4 << 5 << 6;

    size_t count = psl::count_if(nums.begin(), nums.end(), [](int i)
    {
        return (i > 3);
    });

    EXPECT_EQ(count, 3);
}

TEST(psl_algorithm, all_any_none)
{
    auto nums1 = psl::vector<int>() << 1 << 2 << 3 << 4 << 5 << 6;
    auto nums2 = psl::vector<int>() << 1 << 1 << 1 << 1 << 1 << 1;

    auto fn_greater_than_zero = [](int i){ return (i > 0); };
    auto fn_eq_zero = [](int i){ return (i == 0); };
    auto fn_eq_one = [](int i){ return (i == 1); };

    EXPECT_TRUE(psl::all_of(nums1.cbegin(), nums1.cend(), fn_greater_than_zero));
    EXPECT_FALSE(psl::all_of(nums1.cbegin(), nums1.cend(), fn_eq_one));
    EXPECT_TRUE(psl::all_of(nums2.cbegin(), nums2.cend(), fn_eq_one));

    EXPECT_TRUE(psl::any_of(nums1.cbegin(), nums1.cend(), fn_greater_than_zero));
    EXPECT_TRUE(psl::any_of(nums1.cbegin(), nums1.cend(), fn_eq_one));
    EXPECT_FALSE(psl::any_of(nums1.cbegin(), nums1.cend(), fn_eq_zero));

    EXPECT_TRUE(psl::none_of(nums1.cbegin(), nums1.cend(), fn_eq_zero));
    EXPECT_FALSE(psl::none_of(nums1.cbegin(), nums1.cend(), fn_eq_one));
    EXPECT_TRUE(psl::none_of(nums2.cbegin(), nums2.cend(), fn_eq_zero));
}

TEST(psl_algorithm, copy)
{
    {
        //auto v1 = psl::vector<int>() << 0 << 1 << 2 << 3 << 4;
        psl::vector<int> v1(psl::il_tag(), 0, 1, 2, 3, 4);
        psl::vector<int> v2(v1.size());

        psl::copy(v1.cbegin(), v1.cend(), v2.begin());

        EXPECT_EQ(v1, v2);
        EXPECT_EQ(v2[0], 0);
        EXPECT_EQ(v2[1], 1);
        EXPECT_EQ(v2[2], 2);
        EXPECT_EQ(v2[3], 3);
        EXPECT_EQ(v2[4], 4);
    }
    {
        auto v1 = psl::vector<int>() << 0 << 1 << 2 << 3 << 4;
        psl::vector<int> v2;

        psl::copy(v1.cbegin(), v1.cend(), psl::back_inserter(v2));

        EXPECT_EQ(v1, v2);
        EXPECT_EQ(v2[0], 0);
        EXPECT_EQ(v2[1], 1);
        EXPECT_EQ(v2[2], 2);
        EXPECT_EQ(v2[3], 3);
        EXPECT_EQ(v2[4], 4);
    }
}

TEST(psl_algorithm, copy_if)
{
    {
        auto v1 = psl::vector<int>() << 0 << 1 << 2 << 3 << 4;
        psl::vector<int> v2(v1.size());

        psl::copy_if(v1.cbegin(), v1.cend(), v2.begin(), [](int i){ return (i % 2 == 0); });

        EXPECT_EQ(v2[0], 0);
        EXPECT_EQ(v2[1], 2);
        EXPECT_EQ(v2[2], 4);
    }
    {
        auto v1 = psl::vector<int>() << 0 << 1 << 2 << 3 << 4;
        psl::vector<int> v2;

        psl::copy_if(v1.cbegin(), v1.cend(), psl::back_inserter(v2), [](int i){ return (i % 2 == 0); });

        EXPECT_EQ(v2[0], 0);
        EXPECT_EQ(v2[1], 2);
        EXPECT_EQ(v2[2], 4);
    }
}

TEST(psl_algorithm, sort)
{
    {
        auto vec = psl::vector<int>() << 42 << 2 << 1 << 5 << 2 << 4 << -13;
        EXPECT_EQ(vec.size(), 7);
        psl::sort(vec.begin(), vec.end());
        EXPECT_EQ(vec.size(), 7);
        EXPECT_EQ(vec[0], -13);
        EXPECT_EQ(vec[1], 1);
        EXPECT_EQ(vec[2], 2);
        EXPECT_EQ(vec[3], 2);
        EXPECT_EQ(vec[4], 4);
        EXPECT_EQ(vec[5], 5);
        EXPECT_EQ(vec[6], 42);
    }
    {
        auto vec = psl::vector<psl::string>() << "E" << "D" << "B" << "G" << "C" << "A" << "F";
        EXPECT_EQ(vec.size(), 7);
        psl::sort(vec.begin(), vec.end());
        EXPECT_EQ(vec.size(), 7);
        EXPECT_STREQ(vec[0].c_str(), "A");
        EXPECT_STREQ(vec[1].c_str(), "B");
        EXPECT_STREQ(vec[2].c_str(), "C");
        EXPECT_STREQ(vec[3].c_str(), "D");
        EXPECT_STREQ(vec[4].c_str(), "E");
        EXPECT_STREQ(vec[5].c_str(), "F");
    }
    {
        auto vec = psl::vector<psl::string>() << "AE" << "AD" << "AB" << "AG" << "AC" << "AA" << "AF";
        EXPECT_EQ(vec.size(), 7);
        psl::sort(vec.begin(), vec.end());
        EXPECT_EQ(vec.size(), 7);
        EXPECT_STREQ(vec[0].c_str(), "AA");
        EXPECT_STREQ(vec[1].c_str(), "AB");
        EXPECT_STREQ(vec[2].c_str(), "AC");
        EXPECT_STREQ(vec[3].c_str(), "AD");
        EXPECT_STREQ(vec[4].c_str(), "AE");
        EXPECT_STREQ(vec[5].c_str(), "AF");
    }
    {
        auto words = psl::vector<psl::string>() << "banana" << "apple" << "orange" << "grape";
        auto expected = psl::vector<psl::string>() << "apple" << "banana" << "grape" << "orange";
        psl::sort(words.begin(), words.end());
        EXPECT_EQ(words, expected);
    }
    {
        auto words = psl::vector<psl::string>() << "banana" << "apple" << "orange" << "grape";
        auto expected = psl::vector<psl::string>() << "orange" << "grape" << "banana" << "apple";
        psl::sort(words.begin(), words.end(), psl::greater<psl::string>());  // Sort in descending order
        EXPECT_EQ(words, expected);
    }
    {
        auto words = psl::vector<psl::string>() << "Banana" << "apple" << "Orange" << "grape";
        auto expected = psl::vector<psl::string>() << "apple" << "Banana" << "grape" << "Orange";
        psl::sort(words.begin(), words.end(),
                  [](const psl::string &a, const psl::string &b)
                  {
                      return psl::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end(), [](char c1, char c2) { return tolower(c1) < tolower(c2); });
                  });

        EXPECT_EQ(words, expected);
    }
    {
        auto vec = psl::vector<psl_tests::obj_int>() << psl_tests::obj_int(42)
                                                     << psl_tests::obj_int(2)
                                                     << psl_tests::obj_int(1)
                                                     << psl_tests::obj_int(5)
                                                     << psl_tests::obj_int(2)
                                                     << psl_tests::obj_int(4)
                                                     << psl_tests::obj_int(-13);
        EXPECT_EQ(vec.size(), 7);
        psl::sort(vec.begin(), vec.end(), [](psl_tests::obj_int lhs, psl_tests::obj_int rhs){ return lhs.get() < rhs.get(); });
        EXPECT_EQ(vec.size(), 7);
        EXPECT_EQ(vec[0].get(), -13);
        EXPECT_EQ(vec[1].get(), 1);
        EXPECT_EQ(vec[2].get(), 2);
        EXPECT_EQ(vec[3].get(), 2);
        EXPECT_EQ(vec[4].get(), 4);
        EXPECT_EQ(vec[5].get(), 5);
        EXPECT_EQ(vec[6].get(), 42);
    }
}
