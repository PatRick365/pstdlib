#include "psl_variant2.h"
//#include "psl_type_traits.h"

#include "ptest/ptest.h"


TEST(psl_variant2, basic_tests)
{
    psl::variant2 v;
    ASSERT_FALSE(v.is<int>());
    ASSERT_FALSE(v.valid());
    v.set<int>(23);
    ASSERT_TRUE(v.is<int>());
    ASSERT_FALSE(v.is<long>());
    ASSERT_TRUE(v.valid());
    ASSERT_EQ(v.get<int>(), 23);
}

TEST(psl_variant2, copy)
{
    psl::variant2 v1(13L);
    ASSERT_TRUE(v1.is<long>());
    ASSERT_EQ(v1.get<long>(), 13L);

    psl::variant2 v2(v1);
    ASSERT_TRUE(v2.is<long>());
    ASSERT_EQ(v2.get<long>(), 13L);

    psl::variant2 v3;
    v3 = v1;
    ASSERT_TRUE(v3.is<long>());
    ASSERT_EQ(v3.get<long>(), 13L);
}

TEST(psl_variant2, move)
{
    {
        psl::variant2 v1(13L);
        ASSERT_TRUE(v1.is<long>());
        ASSERT_EQ(v1.get<long>(), 13L);

        psl::variant2 v2(psl::move(v1));
        ASSERT_FALSE(v1.valid());
        ASSERT_TRUE(v2.is<long>());
        ASSERT_EQ(v2.get<long>(), 13L);
    }
    {
        psl::variant2 v1(13L);
        ASSERT_TRUE(v1.is<long>());
        ASSERT_EQ(v1.get<long>(), 13L);

        psl::variant2 v2;
        v2 = psl::move(v1);
        ASSERT_FALSE(v1.valid());
        ASSERT_TRUE(v2.is<long>());
        ASSERT_EQ(v2.get<long>(), 13L);
    }
}

TEST(psl_variant2, visit)
{
    {
        psl::variant2 v(13);
        auto fn = [](int i) -> long { return long(i * 2); };
        long l = psl::visit<long, int>(fn, v);
        EXPECT_EQ(l, 26L);
    }
    {
        psl::variant2 v(13);
        long l = 0;
        auto fn = [&l](int i) -> void { l = (i * 2); };
        psl::visit<void, int>(fn, v);
        EXPECT_EQ(l, 26L);
    }
}

TEST(psl_variant2, swap)
{
    psl::variant2 v1(1);
    psl::variant2 v2(2);
    ASSERT_TRUE(v1.is<int>());
    ASSERT_TRUE(v2.is<int>());
    EXPECT_EQ(v1.get<int>(), 1);
    EXPECT_EQ(v2.get<int>(), 2);

    v1.swap(v2);
    ASSERT_TRUE(v1.is<int>());
    ASSERT_TRUE(v2.is<int>());
    EXPECT_EQ(v1.get<int>(), 2);
    EXPECT_EQ(v2.get<int>(), 1);

    psl::swap(v1, v2);
    ASSERT_TRUE(v1.is<int>());
    ASSERT_TRUE(v2.is<int>());
    EXPECT_EQ(v1.get<int>(), 1);
    EXPECT_EQ(v2.get<int>(), 2);
}
