#include "psl_file.h"
#include "psl_file_info.h"

#include "ptest/ptest.h"
#include <stdio.h>


TEST(psl_file, constructors)
{

}

TEST(psl_file, file_info)
{
    EXPECT_FALSE(psl::file_info("nope.jpeg").exists());

    psl::file file;
    ASSERT_FALSE(file.is_open());
    ASSERT_TRUE(file.open("test_file_info", psl::open_mode::write_only));
    EXPECT_TRUE(file.is_open());

    psl::file_info info(file.name());

    EXPECT_TRUE(psl::file_info(file.name()).exists());

    EXPECT_TRUE(info.exists());
    EXPECT_TRUE(info.is_regular_file());
    EXPECT_FALSE(info.is_directory());
    EXPECT_FALSE(info.is_symlink());

    EXPECT_TRUE(info.is_type(psl::file_info::type::file));
    EXPECT_TRUE(info.is_regular_file());

    EXPECT_FALSE(info.is_type(psl::file_info::type::dir));
    EXPECT_FALSE(info.is_directory());

    EXPECT_EQ(info.size(), ssize_t(0));

    ASSERT_TRUE(file.write("afkmpsioegfjoguh"));
    //file.sync();

    // needs new file_info object, should it be auto updated on each call ? maybe by option
    EXPECT_EQ(psl::file_info(file.name()).size(), ssize_t(16));


    EXPECT_TRUE(file.remove());




}

TEST(psl_file, open_close)
{
    psl::file file;

    ASSERT_FALSE(file.is_open());

    ASSERT_TRUE(file.open("test_file_open", psl::open_mode::write_only));
    EXPECT_TRUE(file.is_open());

    EXPECT_TRUE(file.close());
    EXPECT_FALSE(file.is_open());

    EXPECT_TRUE(file.remove());
}

TEST(psl_file, write_read)
{
    psl::file file;
    ASSERT_FALSE(file.is_open());

    ASSERT_TRUE(file.open("test_file_write_read", psl::open_mode::read_write));

    ASSERT_TRUE(file.write("1234567890\nabc"));

    ASSERT_TRUE(file.seek_begin());

    EXPECT_STREQ(file.read(3).value_or("failed").c_str(), "123");
    EXPECT_STREQ(file.read_all().value_or("failed").c_str(), "4567890\nabc");

    EXPECT_TRUE(psl::file::remove(file.name()));
}

TEST(psl_file, csv_write_read)
{
    psl::file file;
    ASSERT_FALSE(file.is_open());

    ASSERT_TRUE(file.open("test_file_csv_write_read", psl::open_mode::read_write));

    ASSERT_TRUE(file.write("11;21;31\n"));
    ASSERT_TRUE(file.write("12;22;32\n"));
    ASSERT_TRUE(file.write("13;23;33\n"));

    ASSERT_TRUE(file.seek_begin());


    psl::optional<psl::string> csv_str = file.read_all();
    ASSERT_TRUE(csv_str.has_value());

    psl::vector<psl::string> rows = csv_str->split<psl::vector<psl::string>>('\n');

    psl::vector<psl::string> cells1 = rows[0].split<psl::vector<psl::string>>(';');
    EXPECT_STREQ(cells1[0].c_str(), "11");
    EXPECT_STREQ(cells1[1].c_str(), "21");
    EXPECT_STREQ(cells1[2].c_str(), "31");

    psl::vector<psl::string> cells2 = rows[1].split<psl::vector<psl::string>>(';');
    EXPECT_STREQ(cells2[0].c_str(), "12");
    EXPECT_STREQ(cells2[1].c_str(), "22");
    EXPECT_STREQ(cells2[2].c_str(), "32");

    psl::vector<psl::string> cells3 = rows[2].split<psl::vector<psl::string>>(';');
    EXPECT_STREQ(cells3[0].c_str(), "13");
    EXPECT_STREQ(cells3[1].c_str(), "23");
    EXPECT_STREQ(cells3[2].c_str(), "33");

    ASSERT_TRUE(file.close());
    EXPECT_TRUE(psl::file::remove(file.name()));
}

TEST(psl_file, csv_write_read2)
{
    psl::file file;
    ASSERT_FALSE(file.is_open());

    ASSERT_TRUE(file.open("test_file_csv_write_read", psl::open_mode::read_write));

    ASSERT_TRUE(file.write("11;21;31\n"));
    ASSERT_TRUE(file.write("12;22;32\n"));
    ASSERT_TRUE(file.write("13;23;33\n"));

    ASSERT_TRUE(file.seek_begin());

    psl::string line;

    {
        ASSERT_TRUE(file.read_line(line));
        psl::vector<psl::string> cells = line.split<psl::vector<psl::string>>(';');
        EXPECT_STREQ(cells[0].c_str(), "11");
        EXPECT_STREQ(cells[1].c_str(), "21");
        EXPECT_STREQ(cells[2].c_str(), "31\n");
    }
    {
        ASSERT_TRUE(file.read_line(line));
        psl::vector<psl::string> cells = line.split<psl::vector<psl::string>>(';');
        EXPECT_STREQ(cells[0].c_str(), "12");
        EXPECT_STREQ(cells[1].c_str(), "22");
        EXPECT_STREQ(cells[2].c_str(), "32\n");
    }
    {
        ASSERT_TRUE(file.read_line(line));
        psl::vector<psl::string> cells = line.split<psl::vector<psl::string>>(';');
        EXPECT_STREQ(cells[0].c_str(), "13");
        EXPECT_STREQ(cells[1].c_str(), "23");
        EXPECT_STREQ(cells[2].c_str(), "33\n");
    }

    ASSERT_FALSE(file.read_line(line));
    EXPECT_TRUE(line.empty());

    ASSERT_TRUE(file.close());
    EXPECT_TRUE(psl::file::remove(file.name()));
}

TEST(psl_file, csv_write_read_stream_ops)
{
    psl::file file;
    ASSERT_FALSE(file.is_open());

    ASSERT_TRUE(file.open("test_file_csv_write_read", psl::open_mode::read_write));

    try
    {
        file << "11;21;31\n";
        file << "12;22;32\n";
        file << "13;23;33\n";
    }
    catch (psl::io_device_write_error &)
    {
        FAIL();
    }

    ASSERT_TRUE(file.seek_begin());


    psl::string csv_str;
    try
    {
        file >> csv_str;
    }
    catch (psl::io_device_read_error &)
    {
        FAIL();
    }

    ASSERT_FALSE(csv_str.empty());

    psl::vector<psl::string> rows = csv_str.split<psl::vector<psl::string>>('\n');

    psl::vector<psl::string> cells1 = rows[0].split<psl::vector<psl::string>>(';');
    EXPECT_STREQ(cells1[0].c_str(), "11");
    EXPECT_STREQ(cells1[1].c_str(), "21");
    EXPECT_STREQ(cells1[2].c_str(), "31");

    psl::vector<psl::string> cells2 = rows[1].split<psl::vector<psl::string>>(';');
    EXPECT_STREQ(cells2[0].c_str(), "12");
    EXPECT_STREQ(cells2[1].c_str(), "22");
    EXPECT_STREQ(cells2[2].c_str(), "32");

    psl::vector<psl::string> cells3 = rows[2].split<psl::vector<psl::string>>(';');
    EXPECT_STREQ(cells3[0].c_str(), "13");
    EXPECT_STREQ(cells3[1].c_str(), "23");
    EXPECT_STREQ(cells3[2].c_str(), "33");

    ASSERT_TRUE(file.close());
    EXPECT_TRUE(psl::file::remove(file.name()));

    {
        bool exception_thrown = false;
        try
        {
            file >> csv_str;
        }
        catch (psl::io_device_read_error &)
        {
            exception_thrown = true;
        }
        EXPECT_TRUE(exception_thrown);
    }

    {
        bool exception_thrown = false;
        try
        {
            file << "text123";
        }
        catch (psl::io_device_write_error &)
        {
            exception_thrown = true;
        }
        EXPECT_TRUE(exception_thrown);
    }
}
