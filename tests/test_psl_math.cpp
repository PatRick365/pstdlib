#include "psl_math.h"

#include "ptest/ptest.h"


TEST(psl_math, pow)
{
    EXPECT_EQ(psl::pow(0, 0), 1);
    EXPECT_EQ(psl::pow(0, 1), 0);
    EXPECT_EQ(psl::pow(2, 0), 1);
    EXPECT_EQ(psl::pow(2, 1), 2);
    EXPECT_EQ(psl::pow(2, 2), 4);
    EXPECT_EQ(psl::pow(2, 3), 8);
    EXPECT_EQ(psl::pow(2, 4), 16);
    EXPECT_EQ(psl::pow(2, 5), 32);

    EXPECT_FLOAT_EQ(psl::pow(1.2F, 2), 1.44F);
    EXPECT_FLOAT_EQ(psl::pow(2.5F, 2), 6.25F);
    EXPECT_FLOAT_EQ(psl::pow(1.2, 5), 2.48832);

    EXPECT_FLOAT_EQ(psl::pow(1, -1), 1);
    EXPECT_FLOAT_EQ(psl::pow(10.0, -1), 0.1);

//    EXPECT_EQ(psl::pow(1.2F, 5), 2.48832);
//    EXPECT_EQ(psl::pow(2.5F, 2), 6.25);
}

TEST(psl_math, ceil)
{
    EXPECT_FLOAT_EQ(psl::ceil(0), 0);
    EXPECT_FLOAT_EQ(psl::ceil(0.00001), 1);
    EXPECT_FLOAT_EQ(psl::ceil(0.0001), 1);
    EXPECT_FLOAT_EQ(psl::ceil(0.001), 1);
    EXPECT_FLOAT_EQ(psl::ceil(0.01), 1);
    EXPECT_FLOAT_EQ(psl::ceil(0.1), 1);

    EXPECT_FLOAT_EQ(psl::ceil(-0.1), 0);
    EXPECT_FLOAT_EQ(psl::ceil(-1.1), -1);

    EXPECT_FLOAT_EQ(psl::ceil(1.1), 2);
    EXPECT_FLOAT_EQ(psl::ceil(2.1), 3);
}
