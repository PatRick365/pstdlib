#include "psl_type_traits.h"

/// is_integral
static_assert(psl::is_integral<char>());
static_assert(psl::is_integral<bool>());
static_assert(psl::is_integral<int>());
static_assert(psl::is_integral<long>());
static_assert(psl::is_integral<long long>());

static_assert(psl::is_integral<unsigned char>());
static_assert(psl::is_integral<unsigned short>());
static_assert(psl::is_integral<unsigned int>());
static_assert(psl::is_integral<unsigned long>());
static_assert(psl::is_integral<unsigned long long>());
static_assert(psl::is_integral<unsigned short>());

static_assert(psl::is_integral<const int>());
static_assert(psl::is_integral<volatile int>());
static_assert(psl::is_integral<const volatile int>());

static_assert( ! psl::is_integral<float>());
static_assert( ! psl::is_integral<double>());


/// is_floating_point
static_assert( ! psl::is_floating_point<int>());

static_assert(psl::is_floating_point<float>());
static_assert(psl::is_floating_point<double>());
static_assert(psl::is_floating_point<long double>());
