#include "psl_string_view.h"

#include "ptest/ptest.h"
#include "test_helpers.hpp"


TEST(psl_string_view, basic_tests)
{
    {
        psl::string_view sv;
        EXPECT_EQ(sv.length(), 0);
        EXPECT_EQ(sv.data(), nullptr);
    }
    {
        psl::string_view sv("ABC");
        EXPECT_EQ(sv.length(), 3);
        EXPECT_EQ(sv[0], 'A');
        EXPECT_EQ(sv[1], 'B');
        EXPECT_EQ(sv[2], 'C');
    }

}

TEST(psl_string_view, contains)
{
    {
        psl::string_view str("wasd");
        for (auto c : str)
        {
            EXPECT_TRUE(str.contains(c));
        }
    }

    {
        psl::string_view str("123456789");
        EXPECT_TRUE(str.contains("1"));
        EXPECT_TRUE(str.contains("12"));
        EXPECT_TRUE(str.contains("123"));
        EXPECT_TRUE(str.contains("1234"));
        EXPECT_TRUE(str.contains("12345"));
        EXPECT_TRUE(str.contains("123456"));
        EXPECT_TRUE(str.contains("1234567"));
        EXPECT_TRUE(str.contains("12345678"));
        EXPECT_TRUE(str.contains("123456789"));
        EXPECT_TRUE(str.contains("23456789"));
        EXPECT_TRUE(str.contains("3456789"));
        EXPECT_TRUE(str.contains("456789"));
        EXPECT_TRUE(str.contains("56789"));
        EXPECT_TRUE(str.contains("6789"));
        EXPECT_TRUE(str.contains("789"));
        EXPECT_TRUE(str.contains("89"));
        EXPECT_TRUE(str.contains("9"));
        EXPECT_TRUE(str.contains("234"));
        EXPECT_TRUE(str.contains("345"));
        EXPECT_TRUE(str.contains("456"));
        EXPECT_TRUE(str.contains("567"));
        EXPECT_TRUE(str.contains("678"));

        EXPECT_FALSE(str.contains("a"));
        EXPECT_FALSE(str.contains("ab"));
        EXPECT_FALSE(str.contains("abc"));

        EXPECT_FALSE(str.contains("13"));
        EXPECT_FALSE(str.contains("135"));
    }
}

TEST(psl_string_view, find)
{
    {
        psl::string_view str("0123210");
        EXPECT_EQ(str.find("0"), 0);
        EXPECT_EQ(str.find('0'), 0);
        EXPECT_EQ(str.find("1"), 1);
        EXPECT_EQ(str.find('1'), 1);
        EXPECT_EQ(str.find("2"), 2);
        EXPECT_EQ(str.find('2'), 2);
        EXPECT_EQ(str.find("3"), 3);
        EXPECT_EQ(str.find('3'), 3);

        /// @todo implement rfind(basic_string)
        //EXPECT_EQ(str.rfind("0"), 6);
        EXPECT_EQ(str.rfind('0'), 6);
        //EXPECT_EQ(str.rfind("1"), 5);
        EXPECT_EQ(str.rfind('1'), 5);
        //EXPECT_EQ(str.rfind("2"), 4);
        EXPECT_EQ(str.rfind('2'), 4);
        //EXPECT_EQ(str.rfind("3"), 3);
        EXPECT_EQ(str.rfind('3'), 3);

    }

    {
        psl::string_view str("123456789");
        EXPECT_EQ(str.find("1"), 0);
        EXPECT_EQ(str.find("12"), 0);
        EXPECT_EQ(str.find("123"), 0);
        EXPECT_EQ(str.find("1234"), 0);
        EXPECT_EQ(str.find("12345"), 0);
        EXPECT_EQ(str.find("123456"), 0);
        EXPECT_EQ(str.find("1234567"), 0);
        EXPECT_EQ(str.find("12345678"), 0);
        EXPECT_EQ(str.find("123456789"), 0);
        EXPECT_EQ(str.find("23456789"), 1);
        EXPECT_EQ(str.find("3456789"), 2);
        EXPECT_EQ(str.find("456789"), 3);
        EXPECT_EQ(str.find("56789"), 4);
        EXPECT_EQ(str.find("6789"), 5);
        EXPECT_EQ(str.find("789"), 6);
        EXPECT_EQ(str.find("89"), 7);
        EXPECT_EQ(str.find("9"), 8);
        EXPECT_EQ(str.find("234"), 1);
        EXPECT_EQ(str.find("345"), 2);
        EXPECT_EQ(str.find("456"), 3);
        EXPECT_EQ(str.find("567"), 4);
        EXPECT_EQ(str.find("678"), 5);

        EXPECT_EQ(str.find("a"), psl::string_view::npos);
        EXPECT_EQ(str.find("ab"), psl::string_view::npos);
        EXPECT_EQ(str.find("abc"), psl::string_view::npos);

        EXPECT_EQ(str.find("13"), psl::string_view::npos);
        EXPECT_EQ(str.find("135"), psl::string_view::npos);
    }

}



///// DOCTEST PORT:

TEST(psl_string_view, default_constructible_iterator)
{
    psl::string_view::const_iterator b;
}

TEST(psl_string_view, copy_constructible_iterator)
{
    psl::string_view v("12");
    psl::string_view::const_iterator a{v.begin()};
    psl::string_view::const_iterator b{a};
    EXPECT_EQ(a, b);
}

TEST(psl_string_view, copy_assignable_iterator)
{
    psl::string_view v("12");
    psl::string_view::const_iterator a{v.begin()};
    psl::string_view::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    a = b;
    EXPECT_EQ(a, b);
}

TEST(psl_string_view, pre_increment_iterator)
{
    psl::string_view v("1");
    psl::string_view::const_iterator a{v.begin()};
    psl::string_view::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_EQ(++a, b);
}

TEST(psl_string_view, post_increment_iterator)
{
    psl::string_view v("1");
    psl::string_view::const_iterator a{v.begin()};
    psl::string_view::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_NE(a++, b);
    EXPECT_EQ(a, b);
}

TEST(psl_string_view, dereference)
{
    psl::string_view v("01234");
    const char * c_str = "01234";
    {
        size_t cnt = 0;
        for(auto it = v.begin(); it != v.end(); ++it)
        {
            EXPECT_EQ(*it, c_str[cnt++]);
        }
        EXPECT_EQ(cnt, 5);
    }
    auto it = v.begin();
    EXPECT_EQ(*it, '0');
}

TEST(psl_string_view, arrow_operator_iterator)
{
    psl::string_view v("01234");
    const char * c_str = "01234";

    psl::string_view::const_iterator a{v.begin()};
    psl::string_view::const_iterator b{v.end()};
    (void)a;
    (void)b;

    {
        size_t cnt = 0;
        for(auto it = v.begin(); it != v.end(); ++it) {
            EXPECT_EQ(*(it.operator->()), c_str[cnt++]);
        }
        EXPECT_EQ(cnt, 5);
    }
}

/**
 * ConstIterator
 */
TEST(psl_string_view, copy_constructible_const_iterator)
{
    psl::string_view v("1,2");
    psl::string_view::const_iterator a{v.begin()};
    psl::string_view::const_iterator b{a};
    EXPECT_EQ(a, b);
}

TEST(psl_string_view, copy_assignable_const_iterator)
{
    psl::string_view v("1,2");
    psl::string_view::const_iterator a{v.begin()};
    psl::string_view::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    a = b;
    EXPECT_EQ(a, b);
}

TEST(psl_string_view, pre_increment_const_iterator)
{
    psl::string_view v("1");
    psl::string_view::const_iterator a{v.begin()};
    psl::string_view::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_EQ(++a, b);
}

TEST(psl_string_view, post_increment_const_iterator)
{
    psl::string_view v("1");
    psl::string_view::const_iterator a{v.begin()};
    psl::string_view::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_NE(a++, b);
    EXPECT_EQ(a, b);
}

TEST(psl_string_view, dereference_const_iterator)
{
    psl::string_view v("01234");
    psl::string_view::const_iterator beg{v.begin()};
    psl::string_view::const_iterator end{v.end()};
    {
        size_t cnt = 0;
        for(; beg != end; ++beg) {
            EXPECT_EQ(*beg, v[cnt++]);
        }
        EXPECT_EQ(cnt, 5);
    }
}

TEST(psl_string_view, substr)
{
    psl::string_view str("0123456789");
    EXPECT_EQ(str.substr(0, 3).size(), 3);
    EXPECT_EQ(str.substr(0, 3), psl::string_view("012"));
    EXPECT_EQ(str.substr(1, 3), psl::string_view("123"));
    EXPECT_EQ(str.substr(2, 3), psl::string_view("234"));
    EXPECT_EQ(str.substr(3, 3), psl::string_view("345"));
    EXPECT_EQ(str.substr(4, 3), psl::string_view("456"));
    EXPECT_EQ(str.substr(5, 3), psl::string_view("567"));
    EXPECT_EQ(str.substr(6, 3), psl::string_view("678"));
    EXPECT_EQ(str.substr(7, 3), psl::string_view("789"));

    EXPECT_EQ(str.substr(0), psl::string_view("0123456789"));
    EXPECT_EQ(str.substr(1), psl::string_view("123456789"));
    EXPECT_EQ(str.substr(2), psl::string_view("23456789"));
    EXPECT_EQ(str.substr(3), psl::string_view("3456789"));
    EXPECT_EQ(str.substr(4), psl::string_view("456789"));
    EXPECT_EQ(str.substr(5), psl::string_view("56789"));
    EXPECT_EQ(str.substr(6), psl::string_view("6789"));
    EXPECT_EQ(str.substr(7), psl::string_view("789"));
    EXPECT_EQ(str.substr(8), psl::string_view("89"));
    EXPECT_EQ(str.substr(9), psl::string_view("9"));

    EXPECT_EQ(str.substr(0).size(), 10);
    EXPECT_EQ(str.substr(1).size(), 9);
    EXPECT_EQ(str.substr(2).size(), 8);
    EXPECT_EQ(str.substr(3).size(), 7);
    EXPECT_EQ(str.substr(4).size(), 6);
    EXPECT_EQ(str.substr(5).size(), 5);
    EXPECT_EQ(str.substr(6).size(), 4);
    EXPECT_EQ(str.substr(7).size(), 3);
    EXPECT_EQ(str.substr(8).size(), 2);
    EXPECT_EQ(str.substr(9).size(), 1);
}

TEST(psl_string_view, back_front)
{
    EXPECT_EQ(psl::string_view("A").back(), 'A');
    EXPECT_EQ(psl::string_view("AB").back(), 'B');
    EXPECT_EQ(psl::string_view("ABC").back(), 'C');

    EXPECT_EQ(psl::string_view("Z").front(), 'Z');
    EXPECT_EQ(psl::string_view("YZ").front(), 'Y');
    EXPECT_EQ(psl::string_view("XYZ").front(), 'X');
}

TEST(psl_string_view, operators)
{
    EXPECT_TRUE(psl::string_view("A") == "A");
    EXPECT_TRUE(psl::string_view("AB") == "AB");
    EXPECT_TRUE(psl::string_view("ABC") == "ABC");

    EXPECT_TRUE(psl::string_view(".") == ".");
    EXPECT_TRUE(psl::string_view("..") == "..");

    EXPECT_FALSE(psl::string_view(".") == "..");
    EXPECT_FALSE(psl::string_view("..") == ".");
}

TEST(psl_string_view, size)
{
    EXPECT_EQ(psl::string_view("A").size(), 1);
    EXPECT_EQ(psl::string_view("AB").size(), 2);
    EXPECT_EQ(psl::string_view("ABC").size(), 3);
}

TEST(psl_string_view, wchar)
{
    {
        psl::wstring_view str;
        EXPECT_EQ(str.length(), 0);
        EXPECT_EQ(str, psl::wstring_view(L""));
    }
    {
        psl::wstring_view str(L"abc");
        EXPECT_EQ(str, psl::wstring_view(L"abc"));
        EXPECT_EQ(str, str);
    }

}

//TEST(psl_string_view, arrow_operator_const_iterator)
//{
//    pstd::string v({'0','1','2','3','4'});
//    pstd::string::const_iterator beg{v.begin()};
//    pstd::string::const_iterator end{v.end()};
//    {
//        size_t cnt = 0;
//        for(; beg != end; ++beg) {
//            EXPECT_EQ(*(beg.operator->()), cnt++);
//        }
//        EXPECT_EQ(cnt, 5);
//    }
//}

//TEST(psl_string_view, type_conversion_iterator)
//{
//    pstd::string v({1});
//    pstd::string::iterator it{v.begin()};
//    pstd::string::const_iterator cit{it};

//    //call pstd::string::const_iterator's operators
//    ///*CHECK*/EXPECT_NE(!cit, it));
//    /*CHECK*/EXPECT_EQ(cit, it);

//    //call pstd::string::iterator's operators
//    ///*CHECK*/EXPECT_EQ(!(it != cit));
//    /*CHECK*/EXPECT_EQ(it, cit);
//}
