#include "psl_tcp_server.h"
#include "psl_tcp_client.h"
#include "psl_thread.h"

#include "test_helpers.hpp"
#include "ptest/ptest.h"

#include <unistd.h>


using namespace psl::literals::units_time;

static constexpr int PORT = 3000;
static constexpr size_t BUFF_SZ = 64;


TEST(psl_tcp_server, read_write)
{
    return; // disable as the socket won't connect on gitlab for some reason

    psl::tcp_client client;

    psl::thread thread([]
    {
        psl::tcp_server server;

        if (!server.open(PORT))
        {
            psl::print(stderr, "psl_tcp_server: failed to open server\n");
            return;
        }

        uint8_t data[BUFF_SZ]{};
        ssize_t n = server.read(data, sizeof(data));

        if (n < 1)
        {
            psl::print(stderr, "psl_tcp_server: server failed to read\n");
            return;
        }

        if (!server.write(data, n))
        {
            psl::print(stderr, "psl_tcp_server: server failed to write\n");
            return;
        }
    });

    psl_tests::auto_do_on_destruct joiner([&thread] { thread.join(); });

    ASSERT_TRUE(client.open("127.0.0.1", PORT));

    ASSERT_TRUE(client.write("ABC\n"));
    char data[BUFF_SZ]{};

    auto bytes = client.read(&data[0], sizeof(data));

    ASSERT_EQ(bytes, 4);

    ASSERT_EQ(data[0], 'A');
    ASSERT_EQ(data[1], 'B');
    ASSERT_EQ(data[2], 'C');
    ASSERT_EQ(data[3], '\n');

}
