#include "psl_bitset.h"

#include "ptest/ptest.h"


template<size_t S>
static constexpr void test(ptest::fn_base::results &__res)
{
    psl::bitset<S> bs;

    for (size_t i = 0; i < S; ++i)
        EXPECT_FALSE(bs.get(i));

    for (size_t i = 0; i < S; ++i)
        bs.set(i, true);
    for (size_t i = 0; i < S; ++i)
        EXPECT_TRUE(bs.get(i));

    for (size_t i = 0; i < S; ++i)
        bs.set(i, (i % 2) == 0);
    for (size_t i = 0; i < S; ++i)
        EXPECT_EQ(bs.get(i), (i % 2) == 0);

}

TEST(psl_bitset, basic_tests)
{
    test<1>(__res);
    test<2>(__res);
    test<3>(__res);
    test<4>(__res);
    test<5>(__res);
    test<6>(__res);
    test<7>(__res);
    test<8>(__res);
    test<9>(__res);
    test<10>(__res);
    test<11>(__res);
    test<12>(__res);
    test<13>(__res);
    test<14>(__res);
    test<15>(__res);
    test<15>(__res);
    test<16>(__res);
    test<17>(__res);
    test<31>(__res);
    test<32>(__res);
    test<33>(__res);
    test<64>(__res);
    test<128>(__res);
    test<256>(__res);
}
