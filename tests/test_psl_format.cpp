#include "psl_format.h"
#include "psl_vector.h"

#include "ptest/ptest.h"
#include <stdio.h>

TEST(psl_format, basic_tests)
{
    {
        constexpr const char *expect = "5";
        psl::string result = psl::format("{}", 5);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "A";
        psl::string result = psl::format("{}", 'A');
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "ABC";
        psl::string result = psl::format("{}", "ABC");
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "1234";
        psl::string result = psl::format("{}", 1234);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "-111-222-";
        psl::string result = psl::format("-{}-{}-", 111, 222);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "-A-B-";
        psl::string result = psl::format("-{}-{}-", 'A', 'B');
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "-ABC-XYZ-";
        psl::string result = psl::format("-{}-{}-", "ABC", "XYZ");
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "-123-456-";
        psl::string result = psl::format("-{}-{}-", 123L, 456L);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "AB";
        psl::string result = psl::format("{}{}", 'A', 'B');
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "123456";
        psl::string result = psl::format("{}{}", 123, 456);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "A1";
        psl::string result = psl::format("{}{}", 'A', 1);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "AB";
        psl::string result = psl::format("{}{}", 'A', 'B');
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "ABC111";
        psl::string result = psl::format("{}{}", "ABC", 111);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "hey world!";
        psl::string result = psl::format("hey {}!", "world");
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "ABC30012";
        psl::string result = psl::format("{}{}{}{}{}", 'A', "BC", 300L, 1, 2);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "ABC";
        psl::string result = psl::format("ABC");
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "-ABC-";
        char arr[] = {'A', 'B', 'C'};
        psl::string result = psl::format("-{}-", arr);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "-ABC-";
        psl::string str = "ABC";
        psl::string result = psl::format("-{}-", str);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "-ABC-";
        psl::string_view str = "ABC";
        psl::string result = psl::format("-{}-", str);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "[1, 2, 3]";
        auto vec = psl::vector<int>() << 1 << 2 << 3;
        psl::string result = psl::format("{}", vec);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "[[11, 12, 13], [21, 22, 23], [31, 32, 33]]";
        psl::vector<psl::vector<int>> vec(3);vec.resize(3);
        vec[0].push_back(11);vec[0].push_back(12);vec[0].push_back(13);
        vec[1].push_back(21);vec[1].push_back(22);vec[1].push_back(23);
        vec[2].push_back(31);vec[2].push_back(32);vec[2].push_back(33);
        psl::string result = psl::format("{}", vec);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        constexpr const char *expect = "[[AA, AB, AC], [BA, BB, BC], [CA, CB, CC]]";
        auto vec = psl::vector<psl::vector<psl::string>>()
            << (psl::vector<psl::string>() << "AA" << "AB" << "AC")
            << (psl::vector<psl::string>() << "BA" << "BB" << "BC")
            << (psl::vector<psl::string>() << "CA" << "CB" << "CC");
        psl::string result = psl::format("{}", vec);
        EXPECT_STREQ(expect, result.c_str());
    }
    {
        bool except = false;
        try
        {
            psl::string result = psl::format("{}{}", 1, 2, 3);
        }
        catch (psl::format_error &e)
        {
            except = true;
            EXPECT_STREQ(e.what().c_str(), "too many format arguments");
        }
        ASSERT_TRUE(except);
    }

    {
        bool except = false;
        try
        {
            psl::string result = psl::format("{}");
        }
        catch (psl::format_error &e)
        {
            except = true;
            EXPECT_STREQ(e.what().c_str(), "not enough format arguments");
        }
        ASSERT_TRUE(except);
    }
    {
        bool except = false;
        try
        {
            psl::string result = psl::format("{}");
        }
        catch (psl::format_error &e)
        {
            except = true;
            EXPECT_STREQ(e.what().c_str(), "not enough format arguments");
        }
        ASSERT_TRUE(except);
    }
    {
        bool except = false;
        try
        {
            psl::string result = psl::format("{}{}{}", 1, 2);
        }
        catch (psl::format_error &e)
        {
            except = true;
            EXPECT_STREQ(e.what().c_str(), "not enough format arguments");
        }
        ASSERT_TRUE(except);
    }

    {   // different ref types
        constexpr const char *expect = "123456789";
        int i1 = 123, i2 = 456, i3 = 789;

        int &r1 = i1;
        const int &r2 = i2;
        int &&r3 = psl::move(i3);

        psl::string result = psl::format("{}{}{}", r1, r2, r3);
        EXPECT_STREQ(expect, result.c_str());
    }


//    {
//        psl::print(psl::color::black, "black\n");
//        psl::print(psl::color::blue, "blue\n");
//        psl::print(psl::color::cyan, "cyan\n");
//        psl::print(psl::color::green, "green\n");
//        psl::print(psl::color::magenta, "magenta\n");
//        psl::print(psl::color::red, "red\n");
//        psl::print(psl::color::white, "white\n");
//        psl::print(psl::color::yellow, "yellow\n");
//    }
}

struct Point
{
    int x = 0;
    int y = 0;
};

namespace psl
{
template<> struct formatter<Point>
{
    static bool write(psl::format_writer &writer, Point value)
    {
        writer.write(value.x);
        writer.write(", ");
        writer.write(value.y);
        return true;
    }
};
}

TEST(psl_format, custom_formatter)
{
    {
        constexpr const char *expect = "1, 2";
        Point p{1, 2};
        psl::string result = psl::format("{}", p);
        EXPECT_STREQ(expect, result.c_str());
    }
}



