#include "ptest/ptest.h"

#include "psl_process.h"


TEST(psl_process, read)
{
    psl::process proc;

    ASSERT_TRUE(proc.open("echo", psl::vector<psl::string>() << "ABC"));

    char data[200]{};

    auto bytes = proc.read(&data[0], sizeof(data));

    ASSERT_EQ(bytes, 4);

    ASSERT_EQ(data[0], 'A');
    ASSERT_EQ(data[1], 'B');
    ASSERT_EQ(data[2], 'C');
    ASSERT_EQ(data[3], '\n');

    ASSERT_TRUE(proc.close());
}

TEST(psl_process, read_all)
{
    psl::process proc;

    psl::string arg("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, met");

    ASSERT_TRUE(proc.open("echo", psl::vector<psl::string>() << arg));

    psl::vector<char> data;

    ASSERT_TRUE(proc.read_all(data));

    ASSERT_EQ(data.size(), arg.length() + 1);

    for (size_t i = 0; i < arg.length(); ++i)
    {
        ASSERT_EQ(data[i], arg[i]);
    }
}


TEST(psl_process, read_write)
{
    psl::process proc;

    ASSERT_TRUE(proc.open("cat", {}));

    ASSERT_TRUE(proc.write("ABC\n"));
    char data[200]{};

    auto bytes = proc.read(&data[0], sizeof(data));

    ASSERT_EQ(bytes, 4);

    ASSERT_EQ(data[0], 'A');
    ASSERT_EQ(data[1], 'B');
    ASSERT_EQ(data[2], 'C');
    ASSERT_EQ(data[3], '\n');
}

