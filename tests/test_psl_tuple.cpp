#include "psl_tuple.h"
#include "psl_string.h"

#include "ptest/ptest.h"
#include "test_helpers.hpp"


struct S1 {};
struct S2 {};

TEST(psl_tuple, basic_tests)
{

    {
        psl::tuple<int, double> t;
        EXPECT_EQ((t.get<0, int>()), 0);
        EXPECT_EQ((t.get<1, double>()), 0);
    }
    {
        psl::tuple<int, double> t(3, 4);
        EXPECT_EQ((t.get<0, int>()), 3);
        EXPECT_EQ((t.get<1, double>()), 4);
    }
    {
        psl::tuple<int, int, int> t(1, 2, 3);
        EXPECT_EQ((t.get<0, int>()), 1);
        EXPECT_EQ((t.get<1, int>()), 2);
        EXPECT_EQ((t.get<2, int>()), 3);
    }
    {
        psl::tuple<int, psl::string, bool> t;
        EXPECT_EQ((t.get<0, int>()), 0);
        EXPECT_EQ((t.get<1, psl::string>()), "");
        EXPECT_EQ((t.get<2, bool>()), false);

        t.set<0>(13);
        EXPECT_EQ((t.get<0, int>()), 13);
        EXPECT_EQ((t.get<1, psl::string>()), "");
        EXPECT_EQ((t.get<2, bool>()), false);

        t.set<1>("ABC");
        EXPECT_EQ((t.get<0, int>()), 13);
        EXPECT_EQ((t.get<1, psl::string>()), "ABC");
        EXPECT_EQ((t.get<2, bool>()), false);

        t.set<2>(true);
        EXPECT_EQ((t.get<0, int>()), 13);
        EXPECT_EQ((t.get<1, psl::string>()), "ABC");
        EXPECT_EQ((t.get<2, bool>()), true);
    }

    {
        psl::tuple<S1, S2> t1;
        t1.get<0, S1>();

        //t1.get<0, S2>();
    }

}
