#include "psl_tcp_client.h"

#include "test_helpers.hpp"
#include "ptest/ptest.h"
#include "psl_process.h"

#include <unistd.h>

using namespace psl::literals::units_time;


TEST(psl_tcp_client, read_write)
{
    psl::process proc;
    //auto args = psl::vector<psl::string>() << "-l" << "2000" << "-k" << "-c" << "\"xargs -n1 echo\""; // quoted strings seem to cause problems...
    auto args = psl::vector<psl::string>() << "-l" << "2000" << "--keep-open" << "--exec" << "/bin/cat";
    ASSERT_TRUE(proc.open("ncat", args));

    psl::this_thread::sleep_for(10_ms);

//    psl::string str;
//    proc.read_all(str);
//    ::puts(str.c_str());

    {
        psl::tcp_client client;
        ASSERT_TRUE(client.open("127.0.0.1", 2000));

        ASSERT_TRUE(client.write("ABC\n"));
        char data[200]{};

        auto bytes = client.read(&data[0], sizeof(data));

        client.write("uzfttf");

        ASSERT_EQ(bytes, 4);

        ASSERT_EQ(data[0], 'A');
        ASSERT_EQ(data[1], 'B');
        ASSERT_EQ(data[2], 'C');
        ASSERT_EQ(data[3], '\n');
    }
}
