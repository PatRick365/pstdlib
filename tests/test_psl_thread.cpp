#include "psl_thread.h"
#include "psl_string.h"
//#include "psl_array.h"
#include "psl_vector.h"
#include "psl_atomic.h"

#include "ptest/ptest.h"


psl::mutex mut;

//static unsigned long counter = 0;

unsigned long ix = 0;
//constexpr const  char *c = "ABC";
//static psl::string c[3] = {"A", "B", "C"};

psl::vector<psl::string> c = psl::vector<psl::string>() << "A" << "B" << "C";

psl::string str;


void foo()
{
    //printf("about to take\n");

    mut.take();

    //printf("taken\n");

    for (unsigned long i = 0; i < 1000; ++i)
    {
        str.append(c[ix]);
    }

    ++ix;

    mut.give();
    //printf("given\n");
}

TEST(psl_thread, basic_tests)
{
    int j = 0;
    psl::thread t([&j](int i)
    {
        j = i;
    }, 13);

    t.join();
    EXPECT_EQ(j, 13);
}

TEST(psl_thread, id)
{
    {
        psl::thread::id id_other;
        psl::thread t([&id_other] { id_other = psl::this_thread::get_id(); });
        psl::thread::id id_other_local = t.get_id();
        t.join();

        EXPECT_EQ(id_other_local, id_other);
        EXPECT_NE(id_other_local, psl::this_thread::get_id());
        EXPECT_NE(id_other_local, psl::thread::id());
        EXPECT_NE(psl::this_thread::get_id(), psl::thread::id());
    }
    {
        constexpr size_t num_threads = 10;
        psl::barrier barrier(num_threads + 1);

        psl::vector<psl::thread> threads;
        psl::vector<psl::thread::id> ids_other(num_threads);
        psl::vector<psl::thread::id> ids_other_local(num_threads);

        for (size_t i = 0; i < num_threads; ++i)
        {
            threads.emplace_back(
                [&barrier, &ids_other, i]
                {
                    barrier.wait();
                    ids_other[i] = psl::this_thread::get_id();
                });
            ids_other_local[i] = threads.back().get_id();
        }

        barrier.wait();

        for (psl::thread &thread : threads)
        {
            EXPECT_TRUE(thread.join());
        }

        EXPECT_NE(psl::this_thread::get_id(), psl::thread::id());

        for (size_t i = 0; i < num_threads; ++i)
        {
            EXPECT_EQ(ids_other_local[i], ids_other[i]);
            EXPECT_NE(ids_other_local[i], psl::this_thread::get_id());
            EXPECT_NE(ids_other_local[i], psl::thread::id());
            for (size_t j = 0; j < num_threads; ++j)
            {
                if (i != j)
                {
                    EXPECT_NE(ids_other_local[i], ids_other[j]);
                }
            }
        }

    }
}


/*TEST(psl_thread, paralell_string)
{
    psl::thread t1("my_thread1", &foo);
    psl::thread t2("my_thread2", &foo);
    psl::thread t3("my_thread3", &foo);

    t1.join();
    t2.join();
    t3.join();

    printf("%s", str.c_str());

    EXPECT_TRUE(true);
}*/


TEST(psl_thread, paralell_num)
{
    unsigned long long counter = 0;
    static constexpr unsigned long long top = 1000 * 1000 * 1;

    constexpr unsigned int threadcount = 10;

    auto fn_foo = [&counter]
    {
        //printf("about to take\n");

        mut.take();

        //printf("taken\n");

        for (unsigned long i = 0; i < top; ++i)
        {
            ++counter;
        }

        mut.give();
        //printf("given\n");
    };

    psl::vector<psl::thread> threads;
    for (unsigned int i = 0; i < threadcount; ++i)
    {
        threads.emplace_back(fn_foo);
    }
    for (psl::thread &thread : threads)
    {
        EXPECT_TRUE(thread.join());
    }

    EXPECT_EQ(counter, (top * threadcount));
}

TEST(psl_thread, paralell_atomic)
{
    psl::atomic<int> counter;
    constexpr unsigned long long top = 10000;
    constexpr unsigned int threadcount = 10;

    psl::vector<psl::thread> threads;
    for (unsigned int i = 0; i < threadcount; ++i)
    {
        threads.emplace_back(
            [&counter]
            {
                for (unsigned long i = 0; i < top; ++i)
                {
                    counter.fetch_add(1);
                }
            });
    }
    for (psl::thread &thread : threads)
    {
        EXPECT_TRUE(thread.join());
    }

    EXPECT_EQ(counter.load(), (top * threadcount));
}
