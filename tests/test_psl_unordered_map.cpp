#include "psl_unordered_map.h"
#include "test_helpers.hpp"

#include "ptest/ptest.h"


TEST(psl_unordered_map, basic_tests)
{
    {
        psl::unordered_map<int, psl_tests::obj_int> map;
        ASSERT_EQ(map.size(), (decltype(map.size())(0)));

        map.insert(5, psl_tests::obj_int(1));
        ASSERT_EQ(map.size(), (decltype(map.size())(1)));
        map.insert(15, psl_tests::obj_int(2));
        ASSERT_EQ(map.size(), ((decltype(map.size())(2))));

        map.insert(50, psl_tests::obj_int(3));
        ASSERT_EQ(map.size(), ((decltype(map.size())(3))));

        psl::unordered_map<int, psl_tests::obj_int>::iterator it2;
        psl::unordered_map<int, psl_tests::obj_int>::iterator it1;

        it1 = map.begin();
        it2 = map.find(5);
        ASSERT_EQ(it1, it2);
        ASSERT_EQ(it1->first, it2->first);
        ASSERT_EQ(it1->second.get(), it2->second.get());

        ++it1;
        it2 = map.find(15);
        ASSERT_EQ(it1, it2);
        ASSERT_EQ(it1->first, it2->first);
        ASSERT_EQ(it1->second.get(), it2->second.get());

        ++it1;
        it2 = map.find(50);
        ASSERT_EQ(it1, it2);
        ASSERT_EQ(it1->first, it2->first);
        ASSERT_EQ(it1->second.get(), it2->second.get());

        ++it1;
        ASSERT_EQ(it1, map.end());
    }

    {
        psl::unordered_map<int, int> map;
        ASSERT_EQ(map.size(), (decltype(map.size())(0)));
        map[50] = 3;
        ASSERT_EQ(map.size(), (decltype(map.size())(1)));
        ASSERT_EQ(map[50], 3);

        {
            psl::unordered_map<int, int> map2 = map;
            ASSERT_EQ(map2.size(), (decltype(map2.size())(1)));
            ASSERT_EQ(map2[50], 3);
            map2.erase(50);
            ASSERT_EQ(map2.size(), (decltype(map2.size())(0)));
        }
        {
            psl::unordered_map<int, int> map2 = map;
            ASSERT_EQ(map2.size(), (decltype(map2.size())(1)));
            ASSERT_EQ(map2[50], 3);
            map2.erase(map2.begin());
            ASSERT_EQ(map2.size(), (decltype(map2.size())(0)));

            bool exept = false;

            try
            {
                map2.erase(map2.begin());
            }  catch (...)
            {
                exept = true;
            }
            ASSERT_TRUE(exept);
        }
    }
}


