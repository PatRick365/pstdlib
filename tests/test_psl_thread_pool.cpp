#include "psl_thread_pool.h"

#include "ptest/ptest.h"


TEST(psl_thread_pool, basic_tests)
{
    psl::thread_pool pool(5);

    psl::vector<psl::future<int>> futures;

    for (int i = 0; i < 20; ++i)
    {
        futures.push_back(pool.queue_work(
            [i]
            {
                psl::this_thread::sleep_for(psl::time<psl::time_t::ms>(i * 10 % 50));
                return i;
            }));
    }

    for (size_t i = 0; i < futures.size(); ++i)
    {
        EXPECT_EQ(futures[i].get(), i);
    }
}
