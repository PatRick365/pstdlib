#include "ptest/ptest.h"

#include "psl_pml.h"


TEST(psl_pml, parse)
{
    {
        psl::string str = "<outer><inner></inner></outer>";

        try
        {
            psl::pml_node::ptr node = psl::pml::parse(str);
            SUCCEED();
        }
        catch (psl::pml_parse_error &err)
        {
            printf("error: '%s'\n", err.what().c_str());
            ADD_FAILURE();
        }
    }

    {

        psl::string str = R"(
<person>
    <name>Mark</name>
    <age>18</age>
    <properties>
        <0>Left</0>
        <1>Right</1>
        <2>13</2>
        <3>true</3>
        <4>false</4>
    </properties>
</person>
)";

        try
        {
            psl::pml_node::ptr root = psl::pml::parse(str);
            ASSERT_TRUE(root != nullptr);

            auto node_person = root->get_child("person");
            ASSERT_EQ(node_person->get_children_size(), 3);

            {
                auto node_name = node_person->get_child("name");
                ASSERT_TRUE(node_name != nullptr);
                EXPECT_TRUE(node_name->has_value());
                EXPECT_TRUE(node_name->is_str());
                ASSERT_EQ(node_name->get_type(), psl::pml_node_type::str);
                EXPECT_STREQ(node_name->to_str().c_str(), "Mark");
            }
            {
                auto node_name = (*node_person)["name"];
                ASSERT_TRUE(node_name != nullptr);
                EXPECT_TRUE(node_name->has_value());
                EXPECT_TRUE(node_name->is_str());
                ASSERT_EQ(node_name->get_type(), psl::pml_node_type::str);
                EXPECT_STREQ(node_name->to_str().c_str(), "Mark");
            }
            {
                auto node_name = (*node_person)[0];
                ASSERT_TRUE(node_name != nullptr);
                EXPECT_TRUE(node_name->has_value());
                EXPECT_TRUE(node_name->is_str());
                ASSERT_EQ(node_name->get_type(), psl::pml_node_type::str);
                EXPECT_STREQ(node_name->to_str().c_str(), "Mark");
            }

            {
                auto node_properties = node_person->get_child("properties");
                ASSERT_TRUE(node_properties != nullptr);
                EXPECT_FALSE(node_properties->has_value());
                ASSERT_EQ(node_properties->get_type(), psl::pml_node_type::value_less);
                ASSERT_EQ(node_properties->get_children_size(), 5);

                {
                    auto node_0 = node_properties->get_child(0);
                    ASSERT_TRUE(node_0 != nullptr);
                    EXPECT_TRUE(node_0->has_value());
                    ASSERT_EQ(node_0->get_type(), psl::pml_node_type::str);
                    EXPECT_STREQ(node_0->to_str().c_str(), "Left");
                }
                {
                    auto node_1 = node_properties->get_child(1);
                    ASSERT_TRUE(node_1 != nullptr);
                    EXPECT_TRUE(node_1->has_value());
                    ASSERT_EQ(node_1->get_type(), psl::pml_node_type::str);
                    EXPECT_STREQ(node_1->to_str().c_str(), "Right");
                }
                {
                    auto node_2 = node_properties->get_child(2);
                    ASSERT_TRUE(node_2 != nullptr);
                    EXPECT_TRUE(node_2->has_value());
                    ASSERT_EQ(node_2->get_type(), psl::pml_node_type::num);
                    EXPECT_FLOAT_EQ(node_2->to_num(), 13.0);
                }
                {
                    auto node_3 = node_properties->get_child(3);
                    ASSERT_TRUE(node_3 != nullptr);
                    EXPECT_TRUE(node_3->has_value());
                    ASSERT_EQ(node_3->get_type(), psl::pml_node_type::bool_);
                    EXPECT_TRUE(node_3->to_bool());
                }
                {
                    auto node_4 = node_properties->get_child(4);
                    ASSERT_TRUE(node_4 != nullptr);
                    EXPECT_TRUE(node_4->has_value());
                    ASSERT_EQ(node_4->get_type(), psl::pml_node_type::bool_);
                    EXPECT_FALSE(node_4->to_bool());
                }
            }
            //printf("[%s]", psl::pml::to_string(root.get()).data());
            SUCCEED();
        }
        catch (psl::pml_parse_error &err)
        {
            printf("error: '%s'\n", err.what().c_str());
            ADD_FAILURE();
        }
    }
}

TEST(psl_pml, parse_error)
{
    {
        psl::string str = "aaaa";

        try
        {
            psl::pml_node::ptr node = psl::pml::parse(str);
            ADD_FAILURE();
        }
        catch (psl::pml_parse_error &err)
        {
            SUCCEED();
        }
    }
    {
        psl::string str = "<outer></inner>";

        try
        {
            psl::pml_node::ptr node = psl::pml::parse(str);
            ADD_FAILURE();
        }
        catch (psl::pml_parse_error &err)
        {
            SUCCEED();
        }
    }
}
