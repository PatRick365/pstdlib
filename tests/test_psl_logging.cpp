#include "psl_logging.h"

#include "ptest/ptest.h"


class logger_string : public psl::i_logger
{
public:
    void log(const psl::string &str) override
    {
        m_result = str;
    }

    const psl::string &str()
    {
        return m_result;
    }

    void clear()
    {
        m_result.clear();
    }

private:
    psl::string m_result;
};

TEST(psl_logging, basic_tests)
{
    //psl::logger_stderr logg;

    logger_string log;
    log.set_level(psl::lvl::base);

    {
        log.clear();
        psl::string str("ABC");
        LOG_PURE(str);
        ASSERT_STREQ(log.str().c_str(), str.c_str());
    }
    {
        log.clear();
        psl::string_view str("ABC");
        LOG_PURE(str);
        ASSERT_STREQ(log.str().c_str(), str.data());
    }
    {
        log.clear();
        const char *str("ABC");
        LOG_PURE(str);
        ASSERT_STREQ(log.str().c_str(), str);
    }
    {
        log.clear();
        psl::string str("ABC");
        LOG(str);
        ASSERT_TRUE(log.str().contains(str));
    }
    {
        log.clear();
        psl::string str("ABC");
        LOG_LEVEL(psl::lvl::debug, str);
        ASSERT_FALSE(log.str().contains(str));
    }
    {
        log.clear();
        LOG_PURE("hey {}!", "world");
        ASSERT_STREQ(log.str().c_str(), "hey world!");
    }
}

TEST(psl_logging, file_logger)
{
    psl::string file_name("test.log");
    {
        psl::logger_o_device<psl::file> file_logger(file_name);
        ASSERT_TRUE(file_logger.o_device.is_open());
        LOG_PURE("hey {}!", "world");
        ASSERT_TRUE(file_logger.o_device.close());
    }

    psl::file file(file_name);
    psl::optional<psl::string> str = file.read_all();
    ASSERT_TRUE(psl::file::remove(file_name));
    ASSERT_TRUE(str.has_value());
    ASSERT_STREQ(str->c_str(), "hey world!\n");
    ASSERT_TRUE(file.close());
}

