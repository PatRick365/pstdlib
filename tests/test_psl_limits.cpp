#include "psl_limits.h"

#include "ptest/ptest.h"

#include <stdio.h>


TEST(psl_limits, is_signed)
{
    EXPECT_TRUE(psl::numeric_limits<long>::is_signed);
    EXPECT_FALSE(psl::numeric_limits<unsigned long>::is_signed);
}





























