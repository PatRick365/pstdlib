#include "psl_memory.h"
#include "psl_utility.h"

#include "ptest/ptest.h"
#include "test_helpers.hpp"

struct A { virtual ~A() = default; };
struct B : public A {};


TEST(psl_memory, unique_ptr_constructors)
{
    {
        psl::unique_ptr<int> ptr;
        EXPECT_EQ(ptr.get(), nullptr);
        EXPECT_EQ(ptr, nullptr);
        ptr = new int(42);
        EXPECT_EQ(*ptr, 42);
        EXPECT_EQ(*ptr, *ptr.get());

        psl::unique_ptr<int> ptr2(psl::move(ptr));
        EXPECT_EQ(ptr.get(), nullptr);
        EXPECT_EQ(ptr, nullptr);
        EXPECT_EQ(*ptr2, 42);
    }
    {
        int i = 69;
        auto ptr = psl::make_unique<int>(psl::move(i));
        ASSERT_EQ(*ptr, 69);
    }
    {
        psl::unique_ptr<B> ptr1;
        psl::unique_ptr<A> ptr2 = psl::move(ptr1);
    }
}

TEST(psl_memory, unique_ptr_release)
{
    {
        psl::unique_ptr<int> ptr(new int(123));
        EXPECT_NE(ptr, nullptr);
        auto raw_ptr = ptr.release();
        psl_tests::auto_do_on_destruct dle([raw_ptr]{ delete raw_ptr; });
        EXPECT_EQ(ptr, nullptr);
        ASSERT_NE(raw_ptr, nullptr);
        EXPECT_EQ(*raw_ptr, 123);
    }
}

TEST(psl_memory, shared_ptr_constructors)
{
    {   // copy construct default constructed
        psl::shared_ptr<int> ptr1;
        EXPECT_EQ(ptr1.use_count(), 0);
        EXPECT_EQ(ptr1, nullptr);
        psl::shared_ptr<int> ptr2(ptr1);
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 0);
        ASSERT_EQ(ptr2, nullptr);
    }
    {   // move construct default constructed
        psl::shared_ptr<int> ptr1;
        EXPECT_EQ(ptr1.use_count(), 0);
        EXPECT_EQ(ptr1, nullptr);
        psl::shared_ptr<int> ptr2(psl::move(ptr1));
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 0);
        ASSERT_EQ(ptr2, nullptr);
    }
    {   // copy constuctor
        psl::shared_ptr<int> ptr1(new int(123));
        EXPECT_EQ(ptr1.use_count(), 1);
        ASSERT_NE(ptr1, nullptr);
        psl::shared_ptr<int> ptr2(ptr1);
        EXPECT_EQ(ptr1.use_count(), 2);
        ASSERT_NE(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 2);
        ASSERT_NE(ptr2, nullptr);
    }
    {   // move constructor
        psl::shared_ptr<int> ptr1(new int(123));
        EXPECT_EQ(ptr1.use_count(), 1);
        ASSERT_NE(ptr1, nullptr);
        psl::shared_ptr<int> ptr2(psl::move(ptr1));
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 1);
        ASSERT_NE(ptr2, nullptr);
    }

    // polymorphism
    {   // construct
        psl::shared_ptr<A> ptr1(new B());
        EXPECT_EQ(ptr1.use_count(), 1);
        EXPECT_NE(ptr1, nullptr);
    }
    {   // copy construct default constructed
        psl::shared_ptr<A> ptr1;
        EXPECT_EQ(ptr1.use_count(), 0);
        EXPECT_EQ(ptr1, nullptr);
        psl::shared_ptr<B> ptr2(ptr1);
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 0);
        ASSERT_EQ(ptr2, nullptr);
    }
    {   // move construct default constructed
        psl::shared_ptr<A> ptr1;
        EXPECT_EQ(ptr1.use_count(), 0);
        EXPECT_EQ(ptr1, nullptr);
        psl::shared_ptr<B> ptr2(psl::move(ptr1));
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 0);
        ASSERT_EQ(ptr2, nullptr);
    }
    {   // copy constuctor
        psl::shared_ptr<A> ptr1(new A());
        EXPECT_EQ(ptr1.use_count(), 1);
        ASSERT_NE(ptr1, nullptr);
        psl::shared_ptr<B> ptr2(ptr1);
        EXPECT_EQ(ptr1.use_count(), 2);
        ASSERT_NE(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 2);
        ASSERT_NE(ptr2, nullptr);
    }
    {   // move constructor
        psl::shared_ptr<A> ptr1(new A());
        EXPECT_EQ(ptr1.use_count(), 1);
        ASSERT_NE(ptr1, nullptr);
        psl::shared_ptr<B> ptr2(psl::move(ptr1));
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 1);
        ASSERT_NE(ptr2, nullptr);
    }
}

TEST(psl_memory, shared_ptr_assign)
{
    {   // copy assign default constructed
        psl::shared_ptr<int> ptr1;
        EXPECT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr1.use_count(), 0);
        psl::shared_ptr<int> ptr2;
        ptr2 = ptr1;
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 0);
        ASSERT_EQ(ptr2, nullptr);
    }
    {   // move assign default constructed
        psl::shared_ptr<int> ptr1;
        EXPECT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr1.use_count(), 0);
        psl::shared_ptr<int> ptr2;
        ptr2 = psl::move(ptr1);
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 0);
        ASSERT_EQ(ptr2, nullptr);
    }
    {   // copy assignment
        psl::shared_ptr<int> ptr1(new int(123));
        EXPECT_EQ(ptr1.use_count(), 1);
        EXPECT_NE(ptr1, nullptr);
        psl::shared_ptr<int> ptr2;
        EXPECT_EQ(ptr2, nullptr);
        ptr2 = ptr1;
        EXPECT_EQ(ptr1.use_count(), 2);
        ASSERT_NE(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 2);
        ASSERT_NE(ptr2, nullptr);
    }
    {   // move asignment
        psl::shared_ptr<int> ptr1(new int(123));
        EXPECT_EQ(ptr1.use_count(), 1);
        ASSERT_NE(ptr1, nullptr);
        psl::shared_ptr<int> ptr2;
        EXPECT_EQ(ptr2.use_count(), 0);
        ASSERT_EQ(ptr2, nullptr);
        ptr2 = psl::move(ptr1);
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 1);
        ASSERT_NE(ptr2, nullptr);
    }

    // polymorphism
    {   // copy assign default constructed
        psl::shared_ptr<A> ptr1;
        EXPECT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr1.use_count(), 0);
        psl::shared_ptr<B> ptr2;
        ptr2 = ptr1;
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 0);
        ASSERT_EQ(ptr2, nullptr);
    }
    {   // move assign default constructed
        psl::shared_ptr<A> ptr1;
        EXPECT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr1.use_count(), 0);
        psl::shared_ptr<B> ptr2;
        ptr2 = psl::move(ptr1);
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 0);
        ASSERT_EQ(ptr2, nullptr);
    }
    {   // copy assignment
        psl::shared_ptr<A> ptr1(new A());
        EXPECT_EQ(ptr1.use_count(), 1);
        EXPECT_NE(ptr1, nullptr);
        psl::shared_ptr<B> ptr2;
        EXPECT_EQ(ptr2, nullptr);
        ptr2 = ptr1;
        EXPECT_EQ(ptr1.use_count(), 2);
        ASSERT_NE(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 2);
        ASSERT_NE(ptr2, nullptr);
    }
    {   // move asignment
        psl::shared_ptr<A> ptr1(new A());
        EXPECT_EQ(ptr1.use_count(), 1);
        ASSERT_NE(ptr1, nullptr);
        psl::shared_ptr<B> ptr2;
        EXPECT_EQ(ptr2.use_count(), 0);
        ASSERT_EQ(ptr2, nullptr);
        ptr2 = psl::move(ptr1);
        EXPECT_EQ(ptr1.use_count(), 0);
        ASSERT_EQ(ptr1, nullptr);
        EXPECT_EQ(ptr2.use_count(), 1);
        ASSERT_NE(ptr2, nullptr);
    }
}

TEST(psl_memory, shared_ptr_reset)
{
    {
        psl::shared_ptr<int> ptr;
        EXPECT_EQ(ptr.use_count(), 0);
        ASSERT_EQ(ptr, nullptr);
        ptr.reset();
        EXPECT_EQ(ptr.use_count(), 0);
        ASSERT_EQ(ptr, nullptr);
    }
    {
        psl::shared_ptr<int> ptr(new int(123));
        EXPECT_EQ(ptr.use_count(), 1);
        ASSERT_NE(ptr, nullptr);
        ptr.reset();
        EXPECT_EQ(ptr.use_count(), 0);
        ASSERT_EQ(ptr, nullptr);
    }
    {
        psl::shared_ptr<int> ptr(new int(123));
        EXPECT_EQ(ptr.use_count(), 1);
        ASSERT_NE(ptr, nullptr);
        EXPECT_EQ(*ptr, 123);

        ptr.reset(new int(456));
        EXPECT_EQ(ptr.use_count(), 1);
        ASSERT_NE(ptr, nullptr);
        EXPECT_EQ(*ptr, 456);
    }
    {
        psl::shared_ptr<int> ptr(new int(123));
        EXPECT_EQ(ptr.use_count(), 1);
        ASSERT_NE(ptr, nullptr);
        EXPECT_EQ(*ptr, 123);

        ptr.reset(new int(456), psl::default_delete<int>());
        EXPECT_EQ(ptr.use_count(), 1);
        ASSERT_NE(ptr, nullptr);
        EXPECT_EQ(*ptr, 456);
    }
}

TEST(psl_memory, shared_ptr_release)
{
    {
        psl::shared_ptr<int> ptr;
        EXPECT_EQ(ptr.use_count(), 0);
        ASSERT_EQ(ptr, nullptr);
        EXPECT_EQ(ptr.release(), nullptr);
        EXPECT_EQ(ptr.use_count(), 0);
        ASSERT_EQ(ptr, nullptr);
    }
    {
        psl::shared_ptr<int> ptr(new int(123));
        EXPECT_EQ(ptr.use_count(), 1);
        ASSERT_NE(ptr, nullptr);
        int *p = ptr.release();
        ASSERT_EQ(*p, 123);
        delete p;
        EXPECT_EQ(ptr.use_count(), 0);
        ASSERT_EQ(ptr, nullptr);
    }
}

TEST(psl_memory, shared_ptr_scope)
{
    {
        psl::shared_ptr<int> ptr(new int(123));
        {
            EXPECT_EQ(ptr.use_count(), 1);
            psl::shared_ptr<int> ptr2(ptr);
            EXPECT_EQ(ptr.use_count(), 2);
            {
                EXPECT_EQ(ptr.use_count(), 2);
                psl::shared_ptr<int> ptr3(ptr);
                EXPECT_EQ(ptr.use_count(), 3);
            }
        }
        EXPECT_EQ(ptr.use_count(), 1);
        EXPECT_EQ(*ptr, 123);
    }
}

TEST(psl_memory, make_shared)
{
    psl::shared_ptr<int> ptr = psl::make_shared<int>(123);
    EXPECT_EQ(ptr.use_count(), 1);
    ASSERT_NE(ptr, nullptr);
    EXPECT_EQ(*ptr, 123);
}


static int cstrct = 0;
static int dstr = 0;

struct C42
{
    C42()
    {
        ++cstrct;
    }
    ~C42()
    {
        ++dstr;
    }
};

TEST(psl_memory, placement_new)
{
    psl::allocator<C42> a;

    C42 *ptr = psl::allocator_traits<psl::allocator<C42>>::allocate(a, 1);

    EXPECT_EQ(cstrct, 0);
    psl::construct_at(ptr);
    EXPECT_EQ(cstrct, 1);

    EXPECT_EQ(dstr, 0);
    psl::destroy_at(ptr);
    EXPECT_EQ(dstr, 1);


    EXPECT_EQ(cstrct, 1);
    EXPECT_EQ(dstr, 1);
    psl::construct_at(ptr);
    EXPECT_EQ(cstrct, 2);
    *ptr = C42();
    EXPECT_EQ(dstr, 2);
    EXPECT_EQ(cstrct, 3);
    psl::destroy_at(ptr);
    EXPECT_EQ(dstr, 3);


    psl::allocator_traits<psl::allocator<C42>>::deallocate(a, ptr, 1);

}
