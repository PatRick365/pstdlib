#include "psl_filesystem.h"

#include "ptest/ptest.h"

namespace fs = psl::filesystem;

TEST(psl_filesystem, constructors)
{
    fs::path p("/root/bin/aaa.txt");
    EXPECT_STREQ(p.c_str(), "/root/bin/aaa.txt");
}

TEST(psl_filesystem, path_filename)
{
    EXPECT_STREQ(fs::path("/foo/bar.txt" ).filename().c_str(), "bar.txt");
    EXPECT_STREQ(fs::path("/foo/.bar"    ).filename().c_str(), ".bar");
    EXPECT_STREQ(fs::path("/foo/bar/"    ).filename().c_str(), "");
    EXPECT_STREQ(fs::path("/foo/."       ).filename().c_str(), ".");
    EXPECT_STREQ(fs::path("/foo/.."      ).filename().c_str(), "..");
    EXPECT_STREQ(fs::path("."            ).filename().c_str(), ".");
    EXPECT_STREQ(fs::path(".."           ).filename().c_str(), "..");
    EXPECT_STREQ(fs::path("/"            ).filename().c_str(), "");
    EXPECT_STREQ(fs::path("//host"       ).filename().c_str(), "host");
}

TEST(psl_filesystem, path_parent_path)
{
    EXPECT_STREQ(fs::path("/var/tmp/example.txt").parent_path().c_str(), "/var/tmp");
    EXPECT_STREQ(fs::path("/").parent_path().c_str(), "/");
    EXPECT_STREQ(fs::path("/var/tmp/.").parent_path().c_str(), "/var/tmp");
}

TEST(psl_filesystem, path_stem)
{
    EXPECT_STREQ(fs::path("/foo/bar.txt").stem().c_str(), "bar");
    EXPECT_STREQ(fs::path("/foo/.bar").stem().c_str(), ".bar");
    EXPECT_STREQ(fs::path("foo.bar.baz.tar").stem().c_str(), "foo.bar.baz");
}

TEST(psl_filesystem, path_extension)
{
    EXPECT_STREQ(fs::path("foo.bar.baz.tar").extension().c_str(), ".tar");
    EXPECT_STREQ(fs::path("foo.bar.baz").extension().c_str(), ".baz");
    EXPECT_STREQ(fs::path("foo.bar").extension().c_str(), ".bar");
    EXPECT_STREQ(fs::path("/foo/bar.txt").extension().c_str(), ".txt");
    EXPECT_STREQ(fs::path("/foo/bar.").extension().c_str(), ".");
    EXPECT_STREQ(fs::path("/foo/bar").extension().c_str(), "");
    EXPECT_STREQ(fs::path("/foo/bar.txt/bar.cc").extension().c_str(), ".cc");
    EXPECT_STREQ(fs::path("/foo/bar.txt/bar.").extension().c_str(), ".");
    EXPECT_STREQ(fs::path("/foo/bar.txt/bar").extension().c_str(), "");
    EXPECT_STREQ(fs::path("/foo/.").extension().c_str(), "");
    EXPECT_STREQ(fs::path("/foo/..").extension().c_str(), "");
    EXPECT_STREQ(fs::path("/foo/.hidden").extension().c_str(), "");
    EXPECT_STREQ(fs::path("/foo/..bar").extension().c_str(), ".bar");
}

TEST(psl_filesystem, operators)
{
    {
        fs::path path1("/foo/bar");
        fs::path path2("bar.txt");
        fs::path path3 = path1 / path2;
        EXPECT_STREQ(path3.c_str(), "/foo/bar" "/" "bar.txt");
    }
}
