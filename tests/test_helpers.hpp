#pragma once

#include "ptest/ptest.h"
#include "psl_string.h"
#include "psl_string_view.h"
#include "psl_vector.h"
#include "psl_format.h"

#include <cstddef>


template<> struct ptest::conv<psl::string> { static void to_str(str_buffer &buffer, const psl::string &value) { psl::strcpy(buffer.data, value.c_str()); } };
template<> struct ptest::conv<psl::string_view> { static void to_str(str_buffer &buffer, const psl::string_view &value) { ::memcpy(buffer.data, value.data(), value.length()); } };

template <typename T> struct ptest::conv<psl::vector<T>>
{
    static void to_str(str_buffer &buffer, const psl::vector<T> &value)
    {
        auto str = psl::format("{}", value);
        psl::strcpy(buffer.data, str.c_str());
    }
};

namespace psl_tests
{

template<typename Fn>
struct auto_do_on_destruct
{
    ~auto_do_on_destruct() { fn(); }
    Fn fn;
};

class obj_int
{
public:
    explicit obj_int(int i)
        : m_i(i)
    {}

    bool operator==(const obj_int& rhs) const { return (m_i == rhs.m_i); }
    bool operator!=(const obj_int& rhs) const { return (m_i != rhs.m_i); }

    int get() const { return m_i; }

private:
    int m_i;
};

class obj_move_only
{
public:
    explicit obj_move_only(int i)
        : m_i(i)
    {}

    obj_move_only(const obj_move_only &) = delete;
    obj_move_only& operator=(const obj_move_only &) = delete;
    obj_move_only(obj_move_only &&other)
        : m_i(other.m_i)
    {
        other.m_i = 0;
    }
    obj_move_only & operator=(obj_move_only &&other)
    {
        m_i = other.m_i;
        other.m_i = 0;
        return *this;
    }

    bool operator==(const obj_move_only& rhs) const { return (m_i == rhs.m_i); }
    bool operator!=(const obj_move_only& rhs) const { return (m_i != rhs.m_i); }

    int get() const { return m_i; }

private:
    int m_i;
};

class obj_unique
{
public:
    obj_unique()
        : m_id(make_id())
    {}

    bool operator==(const obj_unique& rhs) const { return (m_id == rhs.m_id); }
    bool operator!=(const obj_unique& rhs) const { return (m_id != rhs.m_id); }

    size_t get_id() const { return m_id; }

private:
    static size_t make_id()
    {
        static size_t id = 0;
        return ++id;
    }

private:
    size_t m_id;
};


class obj_side_effects
{
public:
    struct values
    {
        size_t n_construct = 0;
        size_t n_copyconstruct = 0;
        size_t n_moveconstruct = 0;
        size_t n_destruct = 0;
    };

    obj_side_effects(values &v)
        : m_v(v)
    {
        ++v.n_construct;
    }
    obj_side_effects(const obj_side_effects &other)
        : m_v(other.m_v)
    {
        ++m_v.n_copyconstruct;
    }
    obj_side_effects(obj_side_effects &&other)
        : m_v(other.m_v)
    {
        ++m_v.n_moveconstruct;
    }
    ~obj_side_effects()
    {
        ++m_v.n_destruct;
    }

private:
    values &m_v;
};

template<typename T>
class allocator_with_id
{
public:
    using base = psl::allocator<T>;

    allocator_with_id()
        : m_id(make_id())
    {}

    bool operator==(const allocator_with_id& rhs) const { return (get_id() == rhs.get_id()); }
    bool operator!=(const allocator_with_id& rhs) const { return (get_id() != rhs.get_id()); }

    size_t get_id() const { return m_id; }

    using allocator_type = base;
    using value_type = base::value_type;
    using pointer = base::pointer;
    using const_pointer = base::const_pointer;
    using void_pointer = base::void_pointer;
    using const_void_pointer = base::const_void_pointer;
    using difference_type = base::difference_type;
    using size_type = base::size_type;


    pointer allocate(size_t n)
    {
        return m_allocator.allocate(n);
    }

    void deallocate(pointer p, size_t n)
    {
        m_allocator.deallocate(p, n);
    }

private:
    static size_t make_id()
    {
        static size_t id = 0;
        return ++id;
    }

private:
    size_t m_id;
    psl::allocator<T> m_allocator;

};

}
