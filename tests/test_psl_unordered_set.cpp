#include "psl_unordered_set.h"
#include "test_helpers.hpp"

#include "ptest/ptest.h"


TEST(psl_unordered_set, basic_tests)
{
    psl::unordered_set<int> s;
    EXPECT_FALSE(s.contains(13));
    s.insert(13);
    EXPECT_TRUE(s.contains(13));

    EXPECT_FALSE(s.contains(42));
    s.insert(42);
    EXPECT_TRUE(s.contains(13));
    EXPECT_TRUE(s.contains(42));

    //psl::vector<psl::vector<int>> v(1);
}

TEST(psl_unordered_set, basic_tests_iterator)
{
  /*  {
        psl::unordered_set<int> s;
        psl::unordered_set<int>::iterator it = s.begin();
        ASSERT_EQ(it, s.end());
    }*/
    {
        psl::unordered_set<size_t> s;
        s.insert(1);
        s.insert(2);
        s.insert(3);

        psl::unordered_set<size_t>::iterator it;

        it = s.begin();
        ASSERT_NE(it, s.end());
        EXPECT_EQ(*it, 1);

        ++it;
        ASSERT_NE(it, s.end());
        EXPECT_EQ(*it, 2);

        ++it;
        ASSERT_NE(it, s.end());
        EXPECT_EQ(*it, 3);

        ++it;
        ASSERT_EQ(it, s.end());

        //for (auto it = s.begin(); it != s.end(); ++it)
    }

    {
        constexpr size_t r = 10;
        psl::unordered_set<size_t> s;
        for (size_t i = 0; i < r; ++i)
        {
            s.insert(i);
        }

        auto it = s.begin();
        for (size_t i = 0; i < r; ++i)
        {
            EXPECT_EQ(i, *it);
            ++it;
        }
    }

    constexpr size_t r = 1000;
    psl::vector<size_t> vec;
    vec.reserve(r);
    psl::unordered_set<size_t> s;
    for (size_t i = 0; i < r; ++i)
    {
        vec.push_back(i);
        s.insert(i);
    }

    EXPECT_EQ(vec.size(), s.size());

    for (auto it = s.begin(); it != s.end(); ++it)
    {
        auto found_it = psl::find(vec.begin(), vec.end(), *it);
        EXPECT_TRUE(found_it != vec.end());
        vec.erase(found_it);
    }
    EXPECT_EQ(vec.size(), 0);
}

TEST(psl_unordered_set, load_factor)
{
    psl::unordered_set<int> s;
    EXPECT_FLOAT_EQ(s.load_factor(), 0);

    s.insert(1);
    EXPECT_FLOAT_EQ(s.load_factor(), 0.1f);

    s.insert(2);
    EXPECT_FLOAT_EQ(s.load_factor(), 0.2f);

    s.insert(3);
    EXPECT_FLOAT_EQ(s.load_factor(), 0.3f);

    s.insert(4);
    s.insert(5);
    s.insert(6);
    EXPECT_FLOAT_EQ(s.load_factor(), 0.6f);
}


//TEST(psl_unordered_set, basic_tests_iterator)
//{
//    psl::vector<int> vec(psl::il_tag(), 1, 22, 33);

//    psl::unordered_set<int> s;
//    for (int i : vec)
//    {
//        s.insert(i);
//    }

//    psl::unordered_set<int>::iterator it = s.begin();

//    {
//        ASSERT_TRUE(it != s.end());
//        int val = *it;
//        ASSERT_TRUE();
//    }



//}
