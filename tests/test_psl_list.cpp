#include "psl_list.h"
#include "psl_string.h"

#include "ptest/ptest.h"

//TEST(psl_list, hmm)
//{
//    psl::list<int> list;
//    list.push_back(1);
//    list.push_back(2);
//    list.push_back(3);



//}

TEST(psl_list, initializer_list)
{
    psl::list<int> list(psl::il_tag(), 1, 22, 333);

    EXPECT_EQ(list.size(), 3);

    auto it = list.begin();

    EXPECT_EQ(*it, 1);
    it++;
    EXPECT_EQ(*it, 22);
    it++;
    EXPECT_EQ(*it, 333);
    it++;
    EXPECT_EQ(it, list.end());
}

TEST(psl_list, push_back)
{
    psl::list<int> list;

    EXPECT_EQ(list.size(), 0);

    list.push_back(12);
    EXPECT_EQ(list.size(), 1);
    EXPECT_EQ(list.front(), 12);
    EXPECT_EQ(list.back(), 12);
    EXPECT_EQ(*list.begin(), 12);

    list.push_back(42);
    EXPECT_EQ(list.size(), 2);
    EXPECT_EQ(list.front(), 12);
    EXPECT_EQ(list.back(), 42);

    {
        auto it = list.begin();
        EXPECT_EQ(*it, 12);
        EXPECT_EQ(*(++it), 42);
    }
}

TEST(psl_list, push_front)
{
    psl::list<int> list;

    EXPECT_EQ(list.size(), 0);

    list.push_front(12);
    EXPECT_EQ(list.size(), 1);
    EXPECT_EQ(list.front(), 12);
    EXPECT_EQ(list.back(), 12);
    EXPECT_EQ(*list.begin(), 12);

    list.push_front(42);
    EXPECT_EQ(list.size(), 2);
    EXPECT_EQ(list.front(), 42);
    EXPECT_EQ(list.back(), 12);

    {
        auto it = list.begin();
        EXPECT_EQ(*it, 42);
        EXPECT_EQ(*(++it), 12);
    }
}

