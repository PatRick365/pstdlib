#include "psl_future.h"
#include "psl_message_queue.h"

#include "ptest/ptest.h"


using namespace psl::literals::units_time;

TEST(psl_message_queue, sync)
{
    {   // single value
        psl::message_queue<int> q;
        q.send(1);
        int result = 0;
        EXPECT_TRUE(q.receive(result));
        EXPECT_EQ(result, 1);
    }
}


TEST(psl_message_queue, async)
{
    {   // single value prestored
        psl::message_queue<int> q;
        psl::future<void> fut = psl::async([&q] { q.send(1); });
        fut.wait();
        int result = 0;
        EXPECT_TRUE(q.receive(result));
        EXPECT_EQ(result, 1);
    }
    {   // multiple values prestored
        psl::message_queue<psl::string> q;
        psl::future<void> fut = psl::async(
            [&q]
            {
                q.send("A");
                q.send("B");
                q.send("C");
            });
        fut.wait();
        psl::string result;

        EXPECT_TRUE(q.receive(result));
        EXPECT_STREQ(result.c_str(), "A");

        EXPECT_TRUE(q.receive(result));
        EXPECT_STREQ(result.c_str(), "B");

        EXPECT_TRUE(q.receive(result));
        EXPECT_STREQ(result.c_str(), "C");
    }

    ///@todo find way to proof that receive before any send works without using arbitrary delay
    {   // single value wait
        psl::message_queue<int> q;
        psl::future<void> fut = psl::async(
            [&q]
            {
                psl::this_thread::sleep_for(50_ms);
                //printf("2) send\n");
                q.send(1);
            });
        int result = 0;
        //printf("1) receive. hopefully this comes first\n");
        EXPECT_TRUE(q.receive(result));
        EXPECT_EQ(result, 1);
        fut.wait();
    }
}
