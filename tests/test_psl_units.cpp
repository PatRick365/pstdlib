#include "psl_units.h"
#include "test_helpers.hpp"
#include "ptest/ptest.h"


using namespace psl::literals::units_distance;
using namespace psl::literals::units_area;
using namespace psl::literals::units_time;
using namespace psl::literals::units_weight;

static_assert(1_m == 100_cm);
static_assert(1_m + 50_cm > 1000_mm);


TEST(psl_units, basic_tests)
{
    EXPECT_FLOAT_EQ(psl::distance<psl::distance_t::m>(1).to_unit<psl::distance_t::km>().get(), 0.001);
    EXPECT_FLOAT_EQ(psl::distance<psl::distance_t::m>(1).to_unit<psl::distance_t::m>().get(), 1);
    EXPECT_FLOAT_EQ(psl::distance<psl::distance_t::m>(1).to_unit<psl::distance_t::dm>().get(), 10);
    EXPECT_FLOAT_EQ(psl::distance<psl::distance_t::m>(1).to_unit<psl::distance_t::cm>().get(), 100);
    EXPECT_FLOAT_EQ(psl::distance<psl::distance_t::m>(1).to_unit<psl::distance_t::mm>().get(), 1000);
    EXPECT_FLOAT_EQ(psl::distance<psl::distance_t::m>(1).to_unit<psl::distance_t::um>().get(), 1000 * 1000);
    EXPECT_FLOAT_EQ(psl::distance<psl::distance_t::m>(1).to_unit<psl::distance_t::nm>().get(), 1000 * 1000 * 1000);

    EXPECT_FLOAT_EQ(-psl::distance<psl::distance_t::m>(1).get(), -1);

    EXPECT_FLOAT_EQ((1_m2).get(), (1000000_mm2).to_unit<psl::area_t::m2>().get());

    {
        psl::distance<psl::distance_t::m> m(1);
        psl::distance<psl::distance_t::cm> cm(100);
        psl::distance<psl::distance_t::m> result = m + cm;
        EXPECT_FLOAT_EQ(result.get(), 2);
    }
    {
        auto result = 1_m + 100_cm - 1000_mm;
        EXPECT_FLOAT_EQ(result.get(), 1);
    }
    {
        auto result = 0.333_mm + 333_um;
        EXPECT_FLOAT_EQ(result.get(), 0.666);
        EXPECT_EQ(result, 666_um);
        EXPECT_NE(result, 1_km);
    }

    EXPECT_TRUE(1_m < 1_km);
    EXPECT_FALSE(1_m < 1_mm);
    EXPECT_FALSE(1_m > 1_km);
    EXPECT_TRUE(1_m > 1_mm);

    EXPECT_FLOAT_EQ(psl::time<psl::time_t::s>(1).to_unit<psl::time_t::s>().get(), 1);
    EXPECT_FLOAT_EQ(psl::time<psl::time_t::s>(1).to_unit<psl::time_t::ms>().get(), 1000);
    EXPECT_FLOAT_EQ(psl::time<psl::time_t::s>(1).to_unit<psl::time_t::us>().get(), 1000 * 1000);
    //fails float compare EXPECT_FLOAT_EQ(psl::time<psl::time_t::s>(1).to_unit<psl::time_t::ns>().get(), 1000 * 1000 * 1000);
    EXPECT_EQ(psl::time<psl::time_t::s>(1).to_unit<psl::time_t::ns>(), 1000000000_ns);

    EXPECT_EQ(1_h, 60_min);
    EXPECT_EQ(1_h, 3600_s);
    EXPECT_EQ(1_h, 3600000_ms);
    EXPECT_EQ(60_min, 1_h);
    EXPECT_EQ(3600_s, 1_h);
    EXPECT_EQ(3600000_ms, 1_h);

    EXPECT_EQ(-1_s, psl::time<psl::time_t::s>(-1));

    {
        auto area = 2_m * 2_m;
        EXPECT_FLOAT_EQ(area.get(), (4_m2).get());
        EXPECT_EQ(area, 4_m2);
    }
//    {
//        auto volume = 2_m2 * 2_m;
//        EXPECT_FLOAT_EQ(volume.get(), (4_m3).get());
//        EXPECT_EQ(volume, 4_m3);
//    }

    EXPECT_FLOAT_EQ((10_km / 2).get(), (5_km).get());
    EXPECT_FLOAT_EQ((10_km * 2).get(), (20_km).get());
    //EXPECT_FLOAT_EQ((10 / 10_km).get(), (1_km).get());

}

TEST(psl_units, fmt)
{
    EXPECT_EQ(psl::format("<{}>", 100_ns), "<100.000ns>");
    EXPECT_EQ(psl::format("<{}>", 10_ms), "<10.000ms>");
    EXPECT_EQ(psl::format("<{}>", 1_s), "<1.000s>");

    EXPECT_EQ(psl::format("<{}>", 333_nm), "<333.000nm>");
    EXPECT_EQ(psl::format("<{}>", 33_m), "<33.000m>");
    EXPECT_EQ(psl::format("<{}>", 3_km), "<3.000km>");
}


/*

TEST(psl_units, basic_tests)
{
    using namespace psl::distance;

    EXPECT_FLOAT_EQ(psl::unit<m>(1).to_unit<m>().get(), 1);
    EXPECT_FLOAT_EQ(psl::unit<m>(1).to_unit<dm>().get(), 10);
    EXPECT_FLOAT_EQ(psl::unit<m>(1).to_unit<cm>().get(), 100);
    EXPECT_FLOAT_EQ(psl::unit<m>(1).to_unit<mm>().get(), 1000);
    EXPECT_FLOAT_EQ(psl::unit<m>(1).to_unit<um>().get(), 1000 * 1000);
    EXPECT_FLOAT_EQ(psl::unit<m>(1).to_unit<nm>().get(), 1000 * 1000 * 1000);

    EXPECT_FLOAT_EQ(psl::unit<dm>(10).to_unit<m>().get(), 1);
    EXPECT_FLOAT_EQ(psl::unit<dm>(10).to_unit<dm>().get(), 10);
    EXPECT_FLOAT_EQ(psl::unit<dm>(10).to_unit<cm>().get(), 100);
    EXPECT_FLOAT_EQ(psl::unit<dm>(10).to_unit<mm>().get(), 1000);
    EXPECT_FLOAT_EQ(psl::unit<dm>(10).to_unit<um>().get(), 1000 * 1000);
    EXPECT_FLOAT_EQ(psl::unit<dm>(10).to_unit<nm>().get(), 1000 * 1000 * 1000);

    EXPECT_FLOAT_EQ(psl::unit<cm>(100).to_unit<m>().get(), 1);
    EXPECT_FLOAT_EQ(psl::unit<cm>(100).to_unit<dm>().get(), 10);
    EXPECT_FLOAT_EQ(psl::unit<cm>(100).to_unit<cm>().get(), 100);
    EXPECT_FLOAT_EQ(psl::unit<cm>(100).to_unit<mm>().get(), 1000);
    EXPECT_FLOAT_EQ(psl::unit<cm>(100).to_unit<um>().get(), 1000 * 1000);
    EXPECT_FLOAT_EQ(psl::unit<cm>(100).to_unit<nm>().get(), 1000 * 1000 * 1000);

    EXPECT_FLOAT_EQ(psl::unit<mm>(1000).to_unit<m>().get(), 1);
    EXPECT_FLOAT_EQ(psl::unit<mm>(1000).to_unit<dm>().get(), 10);
    EXPECT_FLOAT_EQ(psl::unit<mm>(1000).to_unit<cm>().get(), 100);
    EXPECT_FLOAT_EQ(psl::unit<mm>(1000).to_unit<mm>().get(), 1000);
    EXPECT_FLOAT_EQ(psl::unit<mm>(1000).to_unit<um>().get(), 1000 * 1000);
    EXPECT_FLOAT_EQ(psl::unit<mm>(1000).to_unit<nm>().get(), 1000 * 1000 * 1000);

    EXPECT_FLOAT_EQ(psl::unit<um>(1000 * 1000).to_unit<m>().get(), 1);
    EXPECT_FLOAT_EQ(psl::unit<um>(1000 * 1000).to_unit<dm>().get(), 10);
    EXPECT_FLOAT_EQ(psl::unit<um>(1000 * 1000).to_unit<cm>().get(), 100);
    EXPECT_FLOAT_EQ(psl::unit<um>(1000 * 1000).to_unit<mm>().get(), 1000);
    EXPECT_FLOAT_EQ(psl::unit<um>(1000 * 1000).to_unit<um>().get(), 1000 * 1000);
    EXPECT_FLOAT_EQ(psl::unit<um>(1000 * 1000).to_unit<nm>().get(), 1000 * 1000 * 1000);

    EXPECT_FLOAT_EQ(psl::unit<nm>(1000 * 1000 * 1000).to_unit<m>().get(), 1);
    EXPECT_FLOAT_EQ(psl::unit<nm>(1000 * 1000 * 1000).to_unit<dm>().get(), 10);
    EXPECT_FLOAT_EQ(psl::unit<nm>(1000 * 1000 * 1000).to_unit<cm>().get(), 100);
    EXPECT_FLOAT_EQ(psl::unit<nm>(1000 * 1000 * 1000).to_unit<mm>().get(), 1000);
    EXPECT_FLOAT_EQ(psl::unit<nm>(1000 * 1000 * 1000).to_unit<um>().get(), 1000 * 1000);
    EXPECT_FLOAT_EQ(psl::unit<nm>(1000 * 1000 * 1000).to_unit<nm>().get(), 1000 * 1000 * 1000);

    EXPECT_FLOAT_EQ(psl::unit<mm>(1).to_unit<cm>().get(), 0.1);


    using namespace psl::time;

    EXPECT_FLOAT_EQ(psl::unit<s>(1).to_unit<s>().get(), 1);
    EXPECT_FLOAT_EQ(psl::unit<s>(1).to_unit<ms>().get(), 1000);
    EXPECT_FLOAT_EQ(psl::unit<ms>(1).to_unit<ms>().get(), 1);
    EXPECT_FLOAT_EQ(psl::unit<ms>(1000).to_unit<s>().get(), 1);

    {
        using namespace psl::distance::literals;
        EXPECT_TRUE(psl::unit<km>(1) == 1_km);
        EXPECT_TRUE(psl::unit<m>(1) == 1_m);
        EXPECT_TRUE(psl::unit<dm>(1) == 1_dm);
        EXPECT_TRUE(psl::unit<cm>(1) == 1_cm);
        EXPECT_TRUE(psl::unit<mm>(1) == 1_mm);
        EXPECT_TRUE(psl::unit<um>(1) == 1_um);
        EXPECT_TRUE(psl::unit<nm>(1) == 1_nm);
    }
    {
        using namespace psl::time::literals;
        EXPECT_TRUE(psl::unit<ns>(1) == 1_ns);
        EXPECT_TRUE(psl::unit<us>(1) == 1_us);
        EXPECT_TRUE(psl::unit<ms>(1) == 1_ms);
        EXPECT_TRUE(psl::unit<s>(1) == 1_s);
    }
}

TEST(psl_units, operators)
{
    using namespace psl::distance;

    EXPECT_TRUE(psl::unit<m>(1) == psl::unit<cm>(100));
    EXPECT_TRUE(psl::unit<cm>(100) == psl::unit<m>(1));
    EXPECT_FALSE(psl::unit<m>(1) == psl::unit<cm>(13));
    EXPECT_FALSE(psl::unit<cm>(13) == psl::unit<m>(1));

    EXPECT_FALSE(psl::unit<m>(1) != psl::unit<cm>(100));
    EXPECT_FALSE(psl::unit<cm>(100) != psl::unit<m>(1));
    EXPECT_TRUE(psl::unit<m>(1) != psl::unit<cm>(50));
    EXPECT_TRUE(psl::unit<cm>(50) != psl::unit<m>(1));

    EXPECT_TRUE(psl::unit<m>(1) < psl::unit<cm>(101));
    EXPECT_TRUE(psl::unit<m>(1) > psl::unit<cm>(99));

    EXPECT_FALSE(psl::unit<m>(1) < psl::unit<cm>(100));
    EXPECT_FALSE(psl::unit<m>(1) > psl::unit<cm>(100));

    EXPECT_FALSE(psl::unit<m>(1) <= psl::unit<cm>(50));
    EXPECT_TRUE(psl::unit<m>(1) <= psl::unit<cm>(100));
    EXPECT_TRUE(psl::unit<m>(1) <= psl::unit<cm>(300));

    EXPECT_TRUE(psl::unit<m>(1) >= psl::unit<cm>(50));
    EXPECT_TRUE(psl::unit<m>(1) >= psl::unit<cm>(100));
    EXPECT_FALSE(psl::unit<m>(1) >= psl::unit<cm>(300));
}
*/
