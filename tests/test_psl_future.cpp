#include "psl_future.h"

#include "ptest/ptest.h"

using namespace psl::literals::units_time;


TEST(psl_future, invalid_future)
{
    psl::packaged_task<int()> task;
    psl::future<int> fut = task.get_future();
    EXPECT_FALSE(fut.valid());
}

TEST(psl_future, packaged_task)
{
    {   // task_lambda
        psl::packaged_task<int(int,int)> task([](int a, int b)
        {
            return a * b;
        });
        psl::future<int> result = task.get_future();

        EXPECT_FALSE(result.valid());
        task(2, 9);
        ASSERT_TRUE(result.valid());

        EXPECT_EQ(result.get(), 18);
    }
    {
        psl::packaged_task<int()> task([]()
        {
            psl::this_thread::sleep_for(10_ms);
            return 18;
        });

        psl::future<int> result = task.get_future();

        psl::thread t(psl::move(task));

        EXPECT_EQ(result.get(), 18);

        t.join();
    }
    {
        psl::packaged_task<int(int,int)> task([](int a, int b)
        {
            return a * b;
        });

        psl::future<int> result = task.get_future();

        psl::thread t([&task]
        {
            psl::this_thread::sleep_for(10_ms);
            task(2, 9);
        });

        EXPECT_EQ(result.get(), 18);

        t.join();
    }
    {
        psl::packaged_task<int(int,int)> task([](int a, int b)
        {
            psl::this_thread::sleep_for(10_ms);
            return a * b;
        });

        psl::future<int> result = task.get_future();

        auto fn = [](void *arg) -> void *
        {
            auto *t = reinterpret_cast<psl::packaged_task<int(int,int)> *>(arg);
            (*t)(2, 9);
            return nullptr;
        };

        psl::thread t(fn, &task);

        EXPECT_EQ(result.get(), 18);

        t.join();
    }
}

TEST(psl_future, promise)
{
    {   // set
        psl::promise<psl::string> prom;
        psl::future<psl::string> result = prom.get_future();

        EXPECT_FALSE(result.valid());
        prom.set_value("18");
        ASSERT_TRUE(result.valid());

        EXPECT_EQ(result.get(), "18");
    }
    {   // set move
        psl::promise<psl::string> prom;
        psl::future<psl::string> result = prom.get_future();

        EXPECT_FALSE(result.valid());
        psl::string str("18");
        prom.set_value(psl::move(str));
        ASSERT_TRUE(str.empty());
        ASSERT_TRUE(result.valid());

        EXPECT_EQ(result.get(), "18");
    }
}

TEST(psl_future, async)
{
    {   // void() function
        int i = 0;
        psl::future<void> result = psl::async([&i] { i = 9 * 2; });
        EXPECT_TRUE(result.wait());
        EXPECT_EQ(i, 18);
    }
    {   // void(int, int) function
        int i = 0;
        psl::future<void> result = psl::async([&i](int a, int b) { i = a * b; }, 9, 2);
        EXPECT_TRUE(result.wait());
        EXPECT_EQ(i, 18);
    }
    {   // int() function
        psl::future<int> result = psl::async([] { return 9 * 2; });
        EXPECT_EQ(result.get(), 18);
    }
    {   // int(int, int) function
        psl::future<int> result = psl::async([](int a, int b) { return a * b; }, 9, 2);
        EXPECT_EQ(result.get(), 18);
    }
}

/*
void task_lambda()
{
    std::packaged_task<int(int,int)> task([](int a, int b) {
        return std::pow(a, b);
    });
    std::future<int> result = task.get_future();

    task(2, 9);

    std::cout << "task_lambda:\t" << result.get() << '\n';
}

void task_bind()
{
    std::packaged_task<int()> task(std::bind(f, 2, 11));
    std::future<int> result = task.get_future();

    task();

    std::cout << "task_bind:\t" << result.get() << '\n';
}

void task_thread()
{
    std::packaged_task<int(int,int)> task(f);
    std::future<int> result = task.get_future();

    std::thread task_td(std::move(task), 2, 10);
    task_td.join();

    std::cout << "task_thread:\t" << result.get() << '\n';
}
*/
