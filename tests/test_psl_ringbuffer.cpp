#include "psl_ringbuffer.h"

#include "ptest/ptest.h"


TEST(psl_ringbuffer, read_write)
{
    {
        psl::ringbuffer<5> buffer;
        EXPECT_EQ(buffer.available_to_write(), 5);
        ASSERT_TRUE(buffer.write("012"));
        EXPECT_EQ(buffer.available_to_write(), 2);
        EXPECT_EQ(buffer.available_to_read(), 3);
        char data[5]{};
        auto bytes = buffer.read(&data[0], sizeof(data));
        ASSERT_EQ(bytes, 3);
        EXPECT_EQ(buffer.available_to_read(), 0);
        EXPECT_EQ(data[0], '0');
        EXPECT_EQ(data[1], '1');
        EXPECT_EQ(data[2], '2');
    }
    {
        psl::ringbuffer<5> buffer;
        EXPECT_EQ(buffer.available_to_write(), 5);
        ASSERT_TRUE(buffer.write("012"));
        EXPECT_EQ(buffer.available_to_write(), 2);
        EXPECT_EQ(buffer.available_to_read(), 3);
        char data;

        auto bytes = buffer.read(&data, 1);
        ASSERT_EQ(bytes, 1);
        EXPECT_EQ(buffer.available_to_read(), 2);
        EXPECT_EQ(data, '0');

        bytes = buffer.read(&data, 1);
        ASSERT_EQ(bytes, 1);
        EXPECT_EQ(buffer.available_to_read(), 1);
        EXPECT_EQ(data, '1');

        bytes = buffer.read(&data, 1);
        ASSERT_EQ(bytes, 1);
        EXPECT_EQ(buffer.available_to_read(), 0);
        EXPECT_EQ(data, '2');

        bytes = buffer.read(&data, 1);
        ASSERT_EQ(bytes, 0);
        EXPECT_EQ(buffer.available_to_read(), 0);
    }
    {
        psl::ringbuffer<5> buffer;
        {
            ASSERT_TRUE(buffer.write("012"));
            char data[5]{};
            auto bytes = buffer.read(&data[0], sizeof(data));
            ASSERT_EQ(bytes, 3);
            EXPECT_EQ(data[0], '0');
            EXPECT_EQ(data[1], '1');
            EXPECT_EQ(data[2], '2');
        }
        {
            ASSERT_TRUE(buffer.write("345"));
            char data[5]{};
            auto bytes = buffer.read(&data[0], sizeof(data));
            ASSERT_EQ(bytes, 3);
            EXPECT_EQ(data[0], '3');
            EXPECT_EQ(data[1], '4');
            EXPECT_EQ(data[2], '5');
        }
        {
            ASSERT_TRUE(buffer.write("678"));
            char data[5]{};
            auto bytes = buffer.read(&data[0], sizeof(data));
            ASSERT_EQ(bytes, 3);
            EXPECT_EQ(data[0], '6');
            EXPECT_EQ(data[1], '7');
            EXPECT_EQ(data[2], '8');
        }
    }
}
