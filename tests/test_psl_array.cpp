#include "psl_array.h"

#include "ptest/ptest.h"

TEST(psl_array, front)
{
    psl::array<int, 1> v;
    v[0] = 456;

    EXPECT_EQ(v.front(), 456);

}

TEST(psl_array, reverse_iterator)
{
    psl::array<int, 5> v = { 0, 1, 2, 3, 4 };
    {
        int cnt = 4;
        for(auto it = v.rbegin(); it != v.rend(); ++it)
        {
            EXPECT_EQ(*it, cnt--);
        }
        EXPECT_EQ(cnt, -1);
    }
    auto it = v.rbegin();
    EXPECT_EQ(*it, 4);
    *it = 999;
    EXPECT_EQ(*it, 999);
}


TEST(psl_array, sort)
{
    psl::array<int, 7> arr = { 42, 2, 1, 5, 2, 4, -13 };
    ASSERT_EQ(arr.size(), 7);
    EXPECT_EQ(arr[0], 42);
    EXPECT_EQ(arr[1], 2);
    EXPECT_EQ(arr[2], 1);
    EXPECT_EQ(arr[3], 5);
    EXPECT_EQ(arr[4], 2);
    EXPECT_EQ(arr[5], 4);
    EXPECT_EQ(arr[6], -13);
}

TEST(psl_array, default_constructible_iterator)
{
    psl::array<double, 1>::iterator a;
    psl::array<double, 1>::const_iterator b;
}

TEST(psl_array, copy_constructible_iterator)
{
    psl::array<int, 2> arr = { 1, 2 };
    psl::array<int, 2>::iterator a{arr.begin()};
    psl::array<int, 2>::iterator b{a};
    EXPECT_EQ(a, b);
}

TEST(psl_array, copy_assignable_iterator)
{
    psl::array<int, 2> arr = { 1, 2 };
    psl::array<int, 2>::iterator a{arr.begin()};
    psl::array<int, 2>::iterator b{arr.end()};
    EXPECT_NE(a, b);
    a = b;
    EXPECT_EQ(a, b);
}

TEST(psl_array, pre_increment_iterator)
{
    psl::array<int, 1> arr = { 1 };
    psl::array<int, 1>::iterator a{arr.begin()};
    psl::array<int, 1>::iterator b{arr.end()};
    EXPECT_NE(a, b);
    EXPECT_EQ(++a, b);
}

TEST(psl_array, post_increment_iterator)
{
    psl::array<int, 1> arr = { 1 };
    psl::array<int, 1>::iterator a{arr.begin()};
    psl::array<int, 1>::iterator b{arr.end()};
    EXPECT_NE(a, b);
    EXPECT_NE(a++, b);
    EXPECT_EQ(a, b);
}

TEST(psl_array, pre_decrement_iterator)
{
    psl::array<int, 1> arr = { 1 };
    psl::array<int, 1>::iterator a{arr.begin()};
    psl::array<int, 1>::iterator b{arr.end()};
    EXPECT_NE(a, b);
    /// @todo fix
    //EXPECT_EQ(a, --b);
}

TEST(psl_array, post_decrement_iterator)
{
    psl::array<int, 1> arr = { 1 };
    psl::array<int, 1>::iterator a{arr.begin()};
    psl::array<int, 1>::iterator b{arr.end()};
    EXPECT_NE(a, b);

    /// @todo fix
    //EXPECT_NE(a, b--);
    //EXPECT_EQ(a, b);
}

TEST(psl_array, dereference)
{
    psl::array<int, 5> arr = { 0, 1, 2, 3, 4 };
    {
        size_t cnt = 0;
        for(auto it = arr.begin(); it != arr.end(); ++it)
        {
            EXPECT_EQ(*it, cnt++);
        }
        EXPECT_EQ(cnt, 5);
    }
    auto it = arr.begin();
    EXPECT_EQ(*it, 0);
    *it = 999;
    EXPECT_EQ(*it, 999);
}

TEST(psl_array, arrow_operator_iterator)
{
    psl::array<int, 5> arr = { 0, 1, 2, 3, 4 };
    psl::array<int, 5>::iterator a{arr.begin()};
    psl::array<int, 5>::iterator b{arr.end()};
    (void)a;
    (void)b;

    {
        size_t cnt = 0;
        for(auto it = arr.begin(); it != arr.end(); ++it) {
            EXPECT_EQ(*(it.operator->()), cnt++);
        }
        EXPECT_EQ(cnt, 5);
    }
}

/**
 * ConstIterator
 */
TEST(psl_array, copy_constructible_const_iterator)
{
    psl::array<int, 2> arr = { 1, 2 };
    psl::array<int, 2>::const_iterator a{arr.begin()};
    psl::array<int, 2>::const_iterator b{a};
    EXPECT_EQ(a, b);
}

TEST(psl_array, copy_assignable_const_iterator)
{
    psl::array<int, 2> arr = { 1, 2 };
    psl::array<int, 2>::const_iterator a{arr.begin()};
    psl::array<int, 2>::const_iterator b{arr.end()};
    EXPECT_NE(a, b);
    a = b;
    EXPECT_EQ(a, b);
}

TEST(psl_array, pre_increment_const_iterator)
{
    psl::array<int, 1> arr = { 1 };
    psl::array<int, 1>::const_iterator a{arr.begin()};
    psl::array<int, 1>::const_iterator b{arr.end()};
    EXPECT_NE(a, b);
    EXPECT_EQ(++a, b);
}

TEST(psl_array, post_increment_const_iterator)
{
    psl::array<int, 1> arr = { 1 };
    psl::array<int, 1>::const_iterator a{arr.begin()};
    psl::array<int, 1>::const_iterator b{arr.end()};
    EXPECT_NE(a, b);
    EXPECT_NE(a++, b);
    EXPECT_EQ(a, b);
}

TEST(psl_array, pre_decrement_const_iterator)
{
    psl::array<int, 1> arr = { 1 };
    psl::array<int, 1>::const_iterator a{arr.begin()};
    psl::array<int, 1>::const_iterator b{arr.end()};
    EXPECT_NE(a, b);

    /// @todo fix
    //EXPECT_EQ(a, --b);
}

TEST(psl_array, post_decrement_const_iterator)
{
    psl::array<int, 1> arr = { 1 };
    psl::array<int, 1>::const_iterator a{arr.begin()};
    psl::array<int, 1>::const_iterator b{arr.end()};
    EXPECT_NE(a, b);

    /// @todo fix
    //EXPECT_NE(a, b--);
    //EXPECT_EQ(a, b);
}

TEST(psl_array, dereference_const_iterator)
{
    psl::array<int, 5> arr = { 0, 1, 2, 3, 4 };
    psl::array<int, 5>::const_iterator beg{arr.begin()};
    psl::array<int, 5>::const_iterator end{arr.end()};
    {
        size_t cnt = 0;
        for(; beg != end; ++beg) {
            EXPECT_EQ(*beg, cnt++);
        }
        EXPECT_EQ(cnt, 5);
    }
}

TEST(psl_array, arrow_operator_const_iterator)
{
    psl::array<int, 5> arr = { 0, 1, 2, 3, 4 };
    psl::array<int, 5>::const_iterator beg{arr.begin()};
    psl::array<int, 5>::const_iterator end{arr.end()};
    {
        size_t cnt = 0;
        for(; beg != end; ++beg) {
            EXPECT_EQ(*(beg.operator->()), cnt++);
        }
        EXPECT_EQ(cnt, 5);
    }
}

TEST(psl_array, type_conversion_iterator)
{
    psl::array<int, 5> arr = { 0, 1, 2, 3, 4 };
    psl::array<int, 5>::iterator it{arr.begin()};
    psl::array<int, 5>::const_iterator cit{it};

    //call pstd::vector<double>::const_iterator's operators
    ///*CHECK*/EXPECT_NE(!cit, it));
    /*CHECK*/EXPECT_EQ(cit, it);

    //call pstd::vector<double>::iterator's operators
    ///*CHECK*/EXPECT_EQ(!(it != cit));
    /*CHECK*/EXPECT_EQ(it, cit);
}

TEST(psl_array, fill)
{
    psl::array<int, 5> arr = { 0, 1, 2, 3, 4 };

    arr.fill(666);

    for (int val : arr)
    {
        EXPECT_EQ(val, 666);
    }
}

TEST(psl_array, swap)
{
    psl::array<int, 5> arr1 = { 0, 1, 2, 3, 4 };
    psl::array<int, 5> arr2 = { 5, 6, 7, 8, 9 };

    psl::array<int, 5> arr_copy1 = arr1;
    psl::array<int, 5> arr_copy2 = arr2;

    EXPECT_NE(arr1, arr2);
    EXPECT_EQ(arr1, arr_copy1);
    EXPECT_EQ(arr2, arr_copy2);

    arr_copy1.swap(arr_copy2);
    EXPECT_EQ(arr1, arr_copy2);
    EXPECT_EQ(arr2, arr_copy1);


}

