#include "psl_vector.h"
#include "psl_string.h"


#include "ptest/ptest.h"
#include "test_helpers.hpp"


constexpr const char *STR   = "abcdefghijklmnopqrstuvwxyz01234567890";
constexpr const char *STR_1 = "1111111111111111111111111111111111111";
constexpr const char *STR_2 = "2222222222222222222222222222222222222";
constexpr const char *STR_3 = "3333333333333333333333333333333333333";

using StrAlloc = psl_tests::allocator_with_id<psl::string>;

TEST(psl_vector, constructors)
{
    {   // 1
        psl::vector<psl::string> v;
        EXPECT_EQ(v.capacity(), 0);
        EXPECT_EQ(v.size(), 0);
        EXPECT_TRUE(v.empty());
        EXPECT_EQ(v.data(), nullptr);
    }
    {   // 3
        psl::vector<psl::string> v(3, STR);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR);
        EXPECT_STREQ(v[1].c_str(), STR);
        EXPECT_STREQ(v[2].c_str(), STR);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 4
        psl::vector<psl::string> v(3);
        ASSERT_EQ(v.size(), 3);
        EXPECT_EQ(v[0], psl::string());
        EXPECT_EQ(v[1], psl::string());
        EXPECT_EQ(v[2], psl::string());
        EXPECT_STREQ(v[0].c_str(), "");
        EXPECT_STREQ(v[1].c_str(), "");
        EXPECT_STREQ(v[2].c_str(), "");
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 5
        psl::vector<psl::string> v_src(psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string> v(v_src.begin(), v_src.end());
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 6
        psl::vector<psl::string> v_src( psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string> v(v_src);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 8
        psl::vector<psl::string> v_src(psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string> v(psl::move(v_src));
        EXPECT_EQ(v_src.size(), 0);
        EXPECT_EQ(v_src.data(), nullptr);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 10
        psl::vector<psl::string> v(psl::il_tag(), STR_1, STR_2, STR_3);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
}

TEST(psl_vector, constructors_custom_allocator)
{
    {   // 2
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v(allocator);
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        EXPECT_EQ(v.capacity(), 0);
        EXPECT_EQ(v.size(), 0);
        EXPECT_TRUE(v.empty());
        EXPECT_EQ(v.data(), nullptr);
    }
    {   // 3
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v(3, STR, allocator);
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR);
        EXPECT_STREQ(v[1].c_str(), STR);
        EXPECT_STREQ(v[2].c_str(), STR);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 4
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v(3, allocator);
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        ASSERT_EQ(v.size(), 3);
        EXPECT_EQ(v[0], psl::string());
        EXPECT_EQ(v[1], psl::string());
        EXPECT_EQ(v[2], psl::string());
        EXPECT_STREQ(v[0].c_str(), "");
        EXPECT_STREQ(v[1].c_str(), "");
        EXPECT_STREQ(v[2].c_str(), "");
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 5
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v_src(allocator, psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string, StrAlloc> v(v_src.begin(), v_src.end(), allocator);
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        EXPECT_EQ(v.get_allocator().get_id(), v_src.get_allocator().get_id());
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 6
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v_src(allocator, psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string, StrAlloc> v(v_src);
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        EXPECT_EQ(v.get_allocator().get_id(), v_src.get_allocator().get_id());
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 7
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v_src(allocator, psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string, StrAlloc> v(v_src, allocator);
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        EXPECT_EQ(v.get_allocator().get_id(), v_src.get_allocator().get_id());
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 8
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v_src(allocator, psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string, StrAlloc> v(psl::move(v_src));
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        //EXPECT_EQ(v.get_allocator().get_id(), v_src.get_allocator().get_id());
        EXPECT_EQ(v_src.size(), 0);
        EXPECT_EQ(v_src.data(), nullptr);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 9
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v_src(allocator, psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string, StrAlloc> v(psl::move(v_src), allocator);
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        //EXPECT_EQ(v.get_allocator().get_id(), v_src.get_allocator().get_id());
        EXPECT_EQ(v_src.size(), 0);
        EXPECT_EQ(v_src.data(), nullptr);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // 11
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v(allocator, psl::il_tag(), STR_1, STR_2, STR_3);
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
}

TEST(psl_vector, assignment_operators)
{
    {   // copy
        psl::vector<psl::string> v_src(psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string> v;
        v = v_src;
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // move
        psl::vector<psl::string> v_src(psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string> v;
        v = psl::move(v_src);
        EXPECT_EQ(v_src.size(), 0);
        EXPECT_EQ(v_src.data(), nullptr);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // self copy
        psl::vector<psl::string> v(psl::il_tag(), STR_1, STR_2, STR_3);
        v = v;
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // self move
        psl::vector<psl::string> v(psl::il_tag(), STR_1, STR_2, STR_3);
        v = psl::move(v);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
}

TEST(psl_vector, assignment_operators_custom_allocator)
{
    {   // copy
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v_src(allocator, psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string, StrAlloc> v;
        v = v_src;
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        EXPECT_EQ(v.get_allocator().get_id(), v_src.get_allocator().get_id());
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // move
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v_src(allocator, psl::il_tag(), STR_1, STR_2, STR_3);
        psl::vector<psl::string, StrAlloc> v;
        v = psl::move(v_src);
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        //EXPECT_EQ(v.get_allocator().get_id(), v_src.get_allocator().get_id());
        EXPECT_EQ(v_src.size(), 0);
        EXPECT_EQ(v_src.data(), nullptr);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // self copy
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v(allocator, psl::il_tag(), STR_1, STR_2, STR_3);
        v = v;
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        EXPECT_EQ(v.get_allocator().get_id(), v.get_allocator().get_id());
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
    {   // self move
        StrAlloc allocator;
        psl::vector<psl::string, StrAlloc> v(allocator, psl::il_tag(), STR_1, STR_2, STR_3);
        v = psl::move(v);
        EXPECT_EQ(v.get_allocator().get_id(), allocator.get_id());
        EXPECT_EQ(v.get_allocator().get_id(), v.get_allocator().get_id());
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), STR_1);
        EXPECT_STREQ(v[1].c_str(), STR_2);
        EXPECT_STREQ(v[2].c_str(), STR_3);
        EXPECT_NE(v.data(), nullptr);
    }
}


TEST(psl_vector, shrink_to_fit)
{
    {
        psl::vector<psl::string> v;
        EXPECT_EQ(v.capacity(), 0);
        EXPECT_EQ(v.size(), 0);

        v.shrink_to_fit();
        EXPECT_EQ(v.size(), 0);
        EXPECT_EQ(v.capacity(), 0);

        v.resize(10, STR);
        EXPECT_EQ(v.size(), 10);

        v.shrink_to_fit();
        EXPECT_EQ(v.capacity(), 10);

        v.clear();
        v.shrink_to_fit();
        EXPECT_EQ(v.size(), 0);
        EXPECT_EQ(v.capacity(), 0);
    }
    {
        psl::vector<psl::string> v(psl::il_tag(), STR, STR, STR);
        EXPECT_EQ(v.size(), 3);

        v.shrink_to_fit();
        EXPECT_EQ(v.capacity(), 3);
        EXPECT_EQ(v.size(), 3);

        v.clear();
        v.shrink_to_fit();
        EXPECT_EQ(v.size(), 0);
        EXPECT_EQ(v.capacity(), 0);
    }
}


TEST(psl_vector, reserve)
{
    psl::vector<int> v;
    EXPECT_EQ(v.capacity(), 0);
    v.reserve(100);
    EXPECT_EQ(v.capacity(), 100);
    v.shrink_to_fit();
    EXPECT_EQ(v.capacity(), 0);
}

TEST(psl_vector, reverse_iterator)
{
    auto v = psl::vector<int>() << 0 << 1 << 2 << 3 << 4;
    {
        int cnt = 4;
        for(auto it = v.rbegin(); it != v.rend(); ++it)
        {
            EXPECT_EQ(*it, cnt--);
        }
        EXPECT_EQ(cnt, -1);
    }
    auto it = v.rbegin();
    EXPECT_EQ(*it, 4);
    *it = 999;
    EXPECT_EQ(*it, 999);
}

// won’t compile...
//TEST(psl_vector, vector_of_vectors)
//{
//    psl::vector<psl::vector<int>> vv;
//    {
//        psl::vector<int> v(psl::il_tag(), 1, 2, 3);
//        vv.push_back(v);
//    }
//}

TEST(psl_vector, assign)
{
    {
        psl::vector<char> v(psl::il_tag(), '1', '2', '3', '4');
        EXPECT_EQ(v.size(), 4);
        v.assign(3, '0');
        ASSERT_EQ(v.size(), 3);
        EXPECT_EQ(v[0], '0');
        EXPECT_EQ(v[1], '0');
        EXPECT_EQ(v[2], '0');
    }
    {
        psl::vector<char> v1(psl::il_tag(), '1', '2', '3', '4');
        psl::vector<char> v2(psl::il_tag(), '5', '6', '7');
        EXPECT_EQ(v1.size(), 4);
        v1.assign(v2.cbegin(), v2.cend());
        EXPECT_EQ(v1.size(), 3);
        EXPECT_EQ(v1[0], '5');
        EXPECT_EQ(v1[1], '6');
        EXPECT_EQ(v1[2], '7');
    }
    {
        psl::vector<char> v(psl::il_tag(), '1', '2', '3', '4');
        EXPECT_EQ(v.size(), 4);
        v.assign(psl::il_tag(), '5', '6', '7');
        EXPECT_EQ(v.size(), 3);
        EXPECT_EQ(v[0], '5');
        EXPECT_EQ(v[1], '6');
        EXPECT_EQ(v[2], '7');
    }
}

TEST(psl_vector, insert)
{
    {
        psl::vector<psl::string> v;
        auto it = v.insert(v.cbegin(), "0");
        EXPECT_EQ(*it, "0");
        EXPECT_EQ(v[0], "0");
    }
    {
        psl::vector<psl::string> v;
        auto it = v.insert(v.cend(), "0");
        EXPECT_EQ(*it, "0");
        EXPECT_EQ(v[0], "0");
    }
    {
        psl::vector<psl::string> v(psl::il_tag(), "1", "2");
        auto it = v.insert(v.cbegin(), "0");
        EXPECT_EQ(*it, "0");
        EXPECT_EQ(v[0], "0");
        EXPECT_EQ(v[1], "1");
        EXPECT_EQ(v[2], "2");
    }
    {
        psl::vector<psl::string> v(psl::il_tag(), "0", "2");
        auto it = v.insert(v.cbegin() + 1, "1");
        EXPECT_EQ(*it, "1");
        EXPECT_EQ(v[0], "0");
        EXPECT_EQ(v[1], "1");
        EXPECT_EQ(v[2], "2");
    }
    {
        psl::vector<psl::string> v(psl::il_tag(), "0", "1");
        auto it = v.insert(v.cend(), "2");
        EXPECT_EQ(*it, "2");
        EXPECT_EQ(v[0], "0");
        EXPECT_EQ(v[1], "1");
        EXPECT_EQ(v[2], "2");
    }
}

TEST(psl_vector, emplace)
{
    {
        psl::vector<psl::string> v;
        auto it = v.emplace(v.cbegin(), "0");
        EXPECT_EQ(*it, "0");
        EXPECT_EQ(v[0], "0");
    }
    {
        psl::vector<psl::string> v;
        auto it = v.emplace(v.cend(), "0");
        EXPECT_EQ(*it, "0");
        EXPECT_EQ(v[0], "0");
    }
    {
        psl::vector<psl::string> v(psl::il_tag(), "1", "2");
        auto it = v.emplace(v.cbegin(), "0");
        EXPECT_EQ(*it, "0");
        EXPECT_EQ(v[0], "0");
        EXPECT_EQ(v[1], "1");
        EXPECT_EQ(v[2], "2");
    }
    {
        psl::vector<psl::string> v(psl::il_tag(), "0", "2");
        auto it = v.emplace(v.cbegin() + 1, "1");
        EXPECT_EQ(*it, "1");
        EXPECT_EQ(v[0], "0");
        EXPECT_EQ(v[1], "1");
        EXPECT_EQ(v[2], "2");
    }
    {
        psl::vector<psl::string> v(psl::il_tag(), "0", "1");
        auto it = v.emplace(v.cend(), "2");
        EXPECT_EQ(*it, "2");
        EXPECT_EQ(v[0], "0");
        EXPECT_EQ(v[1], "1");
        EXPECT_EQ(v[2], "2");
    }
}

TEST(psl_vector, emplace_back)
{
    {
        psl::vector<int> v;

        v.emplace_back(0);
        EXPECT_EQ(v.size(), 1);
        EXPECT_EQ(v.front(), 0);
        EXPECT_EQ(v.back(), 0);

        v.emplace_back(1);
        EXPECT_EQ(v.size(), 2);
        EXPECT_EQ(v.front(), 0);
        EXPECT_EQ(v.back(), 1);
    }
    {
        psl::vector<psl_tests::obj_move_only> v;
        v.emplace_back(psl_tests::obj_move_only(0));
        v.emplace_back(psl_tests::obj_move_only(1));
        v.emplace_back(psl_tests::obj_move_only(2));
        EXPECT_EQ(v.size(), 3);
        EXPECT_EQ(v[0].get(), 0);
        EXPECT_EQ(v[1].get(), 1);
        EXPECT_EQ(v[2].get(), 2);
    }
}

TEST(psl_vector, erase)
{
    {   // index based
        psl::vector<psl::string> v;
        v.push_back("0");
        v.push_back("1");
        v.push_back("2");
        v.push_back("3");
        EXPECT_EQ(v.size(), 4);

        v.erase(1);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), "0");
        EXPECT_STREQ(v[1].c_str(), "2");
        EXPECT_STREQ(v[2].c_str(), "3");

        v.erase(0);
        EXPECT_EQ(v.size(), 2);
        EXPECT_STREQ(v[0].c_str(), "2");
        EXPECT_STREQ(v[1].c_str(), "3");

        v.erase(1);
        EXPECT_EQ(v.size(), 1);
        EXPECT_STREQ(v[0].c_str(), "2");

        v.erase(0);
        EXPECT_EQ(v.size(), 0);
    }
    {   // iterator based
        psl::vector<psl::string> v;
        v.push_back("0");
        v.push_back("1");
        v.push_back("2");
        v.push_back("3");
        EXPECT_EQ(v.size(), 4);

        v.erase(v.begin() + 1);
        ASSERT_EQ(v.size(), 3);
        EXPECT_STREQ(v[0].c_str(), "0");
        EXPECT_STREQ(v[1].c_str(), "2");
        EXPECT_STREQ(v[2].c_str(), "3");

        v.erase(v.begin());
        EXPECT_EQ(v.size(), 2);
        EXPECT_STREQ(v[0].c_str(), "2");
        EXPECT_STREQ(v[1].c_str(), "3");

        v.erase(--v.end());
        EXPECT_EQ(v.size(), 1);
        EXPECT_STREQ(v[0].c_str(), "2");

        v.erase(v.begin());
        EXPECT_EQ(v.size(), 0);
    }
    {   // move only type
        psl::vector<psl::unique_ptr<int>> v;
        v.push_back(psl::make_unique<int>(0));
        v.push_back(psl::make_unique<int>(1));
        v.push_back(psl::make_unique<int>(2));
        v.push_back(psl::make_unique<int>(3));
        EXPECT_EQ(v.size(), 4);

        v.erase(1);
        ASSERT_EQ(v.size(), 3);
        EXPECT_EQ(*v[0], 0);
        EXPECT_EQ(*v[1], 2);
        EXPECT_EQ(*v[2], 3);

        v.erase(0);
        EXPECT_EQ(v.size(), 2);
        EXPECT_EQ(*v[0], 2);
        EXPECT_EQ(*v[1], 3);

        v.erase(1);
        EXPECT_EQ(v.size(), 1);
        EXPECT_EQ(*v[0], 2);

        v.erase(0);
        EXPECT_EQ(v.size(), 0);
    }
    {   // iterator range
        psl::vector<psl::string> v_org;
        v_org.push_back("0");
        v_org.push_back("1");
        v_org.push_back("2");
        v_org.push_back("3");
        v_org.push_back("4");
        EXPECT_EQ(v_org.size(), 5);
        {   // at beginning
            psl::vector<psl::string> v = v_org;
            auto it = v.erase(v.cbegin(), v.cbegin() + 3);
            EXPECT_EQ(v.size(), 2);
            EXPECT_STREQ(it->c_str(), "3");
            EXPECT_STREQ(v[0].c_str(), "3");
            EXPECT_STREQ(v[1].c_str(), "4");
        }
        {   // at middle
            psl::vector<psl::string> v = v_org;
            auto it = v.erase(v.cbegin() + 1, v.cbegin() + 4);
            EXPECT_EQ(v.size(), 2);
            EXPECT_STREQ(it->c_str(), "4");
            EXPECT_STREQ(v[0].c_str(), "0");
            EXPECT_STREQ(v[1].c_str(), "4");
        }
        {   // at end
            psl::vector<psl::string> v = v_org;
            auto it = v.erase(v.cbegin() + 2, v.cbegin() + 5);
            EXPECT_EQ(v.size(), 2);
            EXPECT_EQ(it, v.end());
            EXPECT_STREQ(v[0].c_str(), "0");
            EXPECT_STREQ(v[1].c_str(), "1");
        }
    }
}

TEST(psl_vector, pop_back)
{
    {   // basic test
        psl::vector<psl::string> v;
        v.push_back("4");
        EXPECT_EQ(v.size(), 1);
        v.emplace_back("4");
        EXPECT_EQ(v.size(), 2);
        v.pop_back();
        EXPECT_EQ(v.size(), 1);
        v.pop_back();
        EXPECT_EQ(v.size(), 0);
    }
    {   // move only
        psl::vector<psl_tests::obj_move_only> v;
        v.push_back(psl_tests::obj_move_only(0));
        v.push_back(psl_tests::obj_move_only(1));
        v.push_back(psl_tests::obj_move_only(2));
        EXPECT_EQ(v.size(), 3);

        v.pop_back();
        ASSERT_EQ(v.size(), 2);
        EXPECT_EQ(v[0].get(), 0);
        EXPECT_EQ(v[1].get(), 1);

        v.pop_back();
        ASSERT_EQ(v.size(), 1);
        EXPECT_EQ(v[0].get(), 0);

        v.pop_back();
        ASSERT_EQ(v.size(), 0);
    }
    {   // move only
        psl::vector<psl::unique_ptr<psl::string>> v;
        v.push_back(psl::make_unique<psl::string>("0"));
        v.push_back(psl::make_unique<psl::string>("1"));
        v.push_back(psl::make_unique<psl::string>("2"));
        EXPECT_EQ(v.size(), 3);

        v.pop_back();
        ASSERT_EQ(v.size(), 2);
        EXPECT_STREQ(v[0]->c_str(), "0");
        EXPECT_STREQ(v[1]->c_str(), "1");

        v.pop_back();
        ASSERT_EQ(v.size(), 1);
        EXPECT_STREQ(v[0]->c_str(), "0");

        v.pop_back();
        ASSERT_EQ(v.size(), 0);
    }
}

TEST(psl_vector, empty)
{
    psl::vector<double> x(0);
    psl::vector<double> y(0);

    EXPECT_EQ(x.size(),0);
    for(size_t i{0};i<100;++i){
        x.insert(x.begin(),5);
        y.push_back(3);
    }
    EXPECT_EQ(x.size(),y.size());
    EXPECT_EQ(x.size(),100);
}

TEST(psl_vector, default_constructible_iterator)
{
    psl::vector<double>::iterator a;
    psl::vector<double>::const_iterator b;
}

TEST(psl_vector, copy_constructible_iterator)
{
    auto v = psl::vector<double>() << 1 << 2;
    psl::vector<double>::iterator a{v.begin()};
    psl::vector<double>::iterator b{a};
    EXPECT_EQ(a, b);
}

TEST(psl_vector, copy_assignable_iterator)
{
    auto v = psl::vector<double>() << 1 << 2;
    psl::vector<double>::iterator a{v.begin()};
    psl::vector<double>::iterator b{v.end()};
    EXPECT_NE(a, b);
    a = b;
    EXPECT_EQ(a, b);
}

TEST(psl_vector, pre_increment_iterator)
{
    auto v = psl::vector<double>() << 1;
    psl::vector<double>::iterator a{v.begin()};
    psl::vector<double>::iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_EQ(++a, b);
}

TEST(psl_vector, post_increment_iterator)
{
    auto v = psl::vector<double>() << 1;
    psl::vector<double>::iterator a{v.begin()};
    psl::vector<double>::iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_NE(a++, b);
    EXPECT_EQ(a, b);
}

TEST(psl_vector, pre_decrement_iterator)
{
    auto v = psl::vector<double>() << 1;
    psl::vector<double>::iterator a{v.begin()};
    psl::vector<double>::iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_EQ(a, --b);
}

TEST(psl_vector, post_decrement_iterator)
{
    auto v = psl::vector<double>() << 1;
    psl::vector<double>::iterator a{v.begin()};
    psl::vector<double>::iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_NE(a, b--);
    EXPECT_EQ(a, b);
}

TEST(psl_vector, dereference)
{
    auto v = psl::vector<double>() << 0 << 1 << 2 << 3 << 4;
    {
        size_t cnt = 0;
        for(auto it = v.begin(); it != v.end(); ++it)
        {
            EXPECT_EQ(*it, cnt++);
        }
        EXPECT_EQ(cnt, 5);
    }
    auto it = v.begin();
    EXPECT_EQ(*it, 0);
    *it = 999;
    EXPECT_EQ(*it, 999);
}

TEST(psl_vector, arrow_operator_iterator)
{
    auto v = psl::vector<double>() << 0 << 1 << 2 << 3 << 4;
    psl::vector<double>::iterator a{v.begin()};
    psl::vector<double>::iterator b{v.end()};
    (void)a;
    (void)b;

    {
        size_t cnt = 0;
        for(auto it = v.begin(); it != v.end(); ++it) {
            EXPECT_EQ(*(it.operator->()), cnt++);
        }
        EXPECT_EQ(cnt, 5);
    }
}

/**
 * ConstIterator
 */
TEST(psl_vector, copy_constructible_const_iterator)
{
    auto v = psl::vector<double>() << 1 << 2;
    psl::vector<double>::const_iterator a{v.begin()};
    psl::vector<double>::const_iterator b{a};
    EXPECT_EQ(a, b);
}

TEST(psl_vector, copy_assignable_const_iterator)
{
    auto v = psl::vector<double>() << 1 << 2;
    psl::vector<double>::const_iterator a{v.begin()};
    psl::vector<double>::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    a = b;
    EXPECT_EQ(a, b);
}

TEST(psl_vector, pre_increment_const_iterator)
{
    auto v = psl::vector<double>() << 1;
    psl::vector<double>::const_iterator a{v.begin()};
    psl::vector<double>::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_EQ(++a, b);
}

TEST(psl_vector, post_increment_const_iterator)
{
    auto v = psl::vector<double>() << 1;
    psl::vector<double>::const_iterator a{v.begin()};
    psl::vector<double>::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_NE(a++, b);
    EXPECT_EQ(a, b);
}

TEST(psl_vector, pre_decrement_const_iterator)
{
    auto v = psl::vector<double>() << 1;
    psl::vector<double>::const_iterator a{v.begin()};
    psl::vector<double>::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_EQ(a, --b);
}

TEST(psl_vector, post_decrement_const_iterator)
{
    auto v = psl::vector<double>() << 1;
    psl::vector<double>::const_iterator a{v.begin()};
    psl::vector<double>::const_iterator b{v.end()};
    EXPECT_NE(a, b);
    EXPECT_NE(a, b--);
    EXPECT_EQ(a, b);
}

TEST(psl_vector, dereference_const_iterator)
{
    auto v = psl::vector<double>() << 0 << 1 << 2 << 3 << 4;
    psl::vector<double>::const_iterator beg{v.begin()};
    psl::vector<double>::const_iterator end{v.end()};
    {
        size_t cnt = 0;
        for(; beg != end; ++beg) {
            EXPECT_EQ(*beg, cnt++);
        }
        EXPECT_EQ(cnt, 5);
    }
}

TEST(psl_vector, arrow_operator_const_iterator)
{
    auto v = psl::vector<double>() << 0 << 1 << 2 << 3 << 4;
    psl::vector<double>::const_iterator beg{v.begin()};
    psl::vector<double>::const_iterator end{v.end()};
    {
        size_t cnt = 0;
        for(; beg != end; ++beg) {
            EXPECT_EQ(*(beg.operator->()), cnt++);
        }
        EXPECT_EQ(cnt, 5);
    }
}

TEST(psl_vector, type_conversion_iterator)
{
    auto v = psl::vector<double>() << 1;
    psl::vector<double>::iterator it{v.begin()};
    psl::vector<double>::const_iterator cit{it};

    //call pstd::vector<double>::const_iterator's operators
    ///*CHECK*/EXPECT_NE(!cit, it));
    /*CHECK*/EXPECT_EQ(cit, it);

    //call pstd::vector<double>::iterator's operators
    ///*CHECK*/EXPECT_EQ(!(it != cit));
    /*CHECK*/EXPECT_EQ(it, cit);
}


