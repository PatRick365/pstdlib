#include "psl_char_manip.h"

#include "ptest/ptest.h"


TEST(psl_char_manip, strlen)
{
    const char *str = "abc";

    EXPECT_EQ(psl::strlen(str), 3);
    EXPECT_EQ(psl::strlen(""), 0);
    EXPECT_EQ(psl::strlen("a"), 1);
    EXPECT_EQ(psl::strlen("ab"), 2);
    EXPECT_EQ(psl::strlen("abc"), 3);
    EXPECT_EQ(psl::strlen("abcfxthxfth xfthxft h 54545z4z5  \n"), 34);
}

TEST(psl_char_manip, strcpy)
{
    char str[4] = {'a', 'b', 'c', '\0'};

    char arr[4];

    auto ret = psl::strcpy(arr, str);

    EXPECT_EQ(arr[0], str[0]);
    EXPECT_EQ(arr[1], str[1]);
    EXPECT_EQ(arr[2], str[2]);

    EXPECT_STREQ(str, arr);
    EXPECT_STREQ(str, ret);


}
