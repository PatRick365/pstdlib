#include "psl_functional.h"
#include "psl_string.h"

#include "ptest/ptest.h"

psl::string string_stringint(const psl::string &str, int num)
{
    return str + psl::string().set_num(num);
}

int s_counter = 0;
void void_void()
{
    ++s_counter;
}

int int_void()
{
    return 42;
}

TEST(psl_functional, void_void)
{
    psl::function<void()> func(void_void);
    EXPECT_EQ(s_counter, 0);
    func();
    EXPECT_EQ(s_counter, 1);


    psl::function<void()> func2;
    func2 = void_void;
    EXPECT_EQ(s_counter, 1);
    func2();
    EXPECT_EQ(s_counter, 2);

    psl::function<void()> func3 = func2;
    EXPECT_EQ(s_counter, 2);
    func2();
    EXPECT_EQ(s_counter, 3);
}

TEST(psl_functional, int_void)
{
    psl::function<int()> func(int_void);
    EXPECT_EQ(func(), 42);

    psl::function<int()> func2 = int_void;
    EXPECT_EQ(func2(), 42);

}

TEST(psl_functional, string_stringint)
{
    psl::function<psl::string(const psl::string &, int)> func(string_stringint);
    EXPECT_STREQ(func("1+2=", 3).c_str(), "1+2=3");
    EXPECT_STRNE(func("1+2=", 3).c_str(), "1+2=4");

    psl::function<psl::string(const psl::string &, int)> func2 = string_stringint;
    EXPECT_STREQ(func2("1+2=", 3).c_str(), "1+2=3");
    EXPECT_STRNE(func2("1+2=", 3).c_str(), "1+2=4");
}

TEST(psl_functional, lambda)
{
    int count = 0;
    psl::function<void()> func([&count](){ ++count; });
    EXPECT_EQ(count, 0);
    func();
    EXPECT_EQ(count, 1);

    // assign to lambda with different captures
    psl::function<void()> func2([i = 1, j = 3](){});
    func2 = func;
    EXPECT_EQ(count, 1);
    func();
    EXPECT_EQ(count, 2);
}

TEST(psl_functional, hash)
{
    EXPECT_EQ(psl::hash<bool>()(false), psl::hash<bool>()(false));
    EXPECT_EQ(psl::hash<bool>()(true), psl::hash<bool>()(true));
    EXPECT_NE(psl::hash<bool>()(true), psl::hash<bool>()(false));

    EXPECT_EQ(psl::hash<float>()(0.0f), psl::hash<float>()(0.0f));
    EXPECT_EQ(psl::hash<float>()(0.0f), psl::hash<float>()(-0.0f));
    EXPECT_EQ(psl::hash<float>()(-0.0f), psl::hash<float>()(-0.0f));

    EXPECT_EQ(psl::hash<double>()(0.0), psl::hash<double>()(0.0));
    EXPECT_EQ(psl::hash<double>()(0.0), psl::hash<double>()(-0.0));
    EXPECT_EQ(psl::hash<double>()(-0.0), psl::hash<double>()(-0.0));

    EXPECT_EQ(psl::hash<long double>()(0.0l), psl::hash<long double>()(0.0l));
    EXPECT_EQ(psl::hash<long double>()(0.0l), psl::hash<long double>()(-0.0l));
    EXPECT_EQ(psl::hash<long double>()(-0.0l), psl::hash<long double>()(-0.0l));

    EXPECT_EQ(psl::hash<psl::nullptr_t>()(nullptr), psl::hash<bool *>()(nullptr));
}
