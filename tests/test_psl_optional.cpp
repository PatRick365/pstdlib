#include "psl_optional.h"

#include "test_helpers.hpp"

#include "ptest/ptest.h"

struct C
{
    int i = 42;
};

TEST(psl_optional, basic_tests)
{
    {
        psl::optional<C> opt;
        EXPECT_FALSE(opt.has_value());
        EXPECT_EQ(opt, psl::nullopt);
    }
    {
        psl::optional<C> opt(C{});
        ASSERT_TRUE(opt.has_value());
        EXPECT_EQ(opt.value().i, 42);
        EXPECT_EQ(opt->i, 42);
        opt = psl::nullopt;
        EXPECT_FALSE(opt.has_value());
        EXPECT_EQ(opt, psl::nullopt);
        opt = C{};
        ASSERT_TRUE(opt.has_value());
        EXPECT_EQ(opt.value().i, 42);
        EXPECT_EQ(opt->i, 42);
    }

    {
        psl_tests::obj_side_effects::values v;
        {
            psl_tests::obj_side_effects obj(v);
            EXPECT_EQ(v.n_construct, 1);
            EXPECT_EQ(v.n_copyconstruct, 0);
            EXPECT_EQ(v.n_moveconstruct, 0);
            EXPECT_EQ(v.n_destruct, 0);
            psl::optional<psl_tests::obj_side_effects> opt(obj);
            ASSERT_TRUE(opt.has_value());
            EXPECT_EQ(v.n_construct, 1);
            EXPECT_EQ(v.n_copyconstruct, 1);
            EXPECT_EQ(v.n_destruct, 0);
        }
        EXPECT_EQ(v.n_construct, 1);
        EXPECT_EQ(v.n_copyconstruct, 1);
        EXPECT_EQ(v.n_moveconstruct, 0);
        EXPECT_EQ(v.n_destruct, 2);
    }
}
