#include "psl_iterator.h"
#include "psl_vector.h"

#include "ptest/ptest.h"
#include <stdio.h>


TEST(psl_iterator, advance)
{
    psl::vector<int> v(psl::il_tag(), 0, 1, 2, 3, 4);

    auto it = v.begin();
    EXPECT_EQ(*it, 0);

    psl::advance(it, 1);
    EXPECT_EQ(*it, 1);

    psl::advance(it, -1);
    EXPECT_EQ(*it, 0);

    psl::advance(it, 4);
    EXPECT_EQ(*it, 4);

    psl::advance(it, 1);
    EXPECT_EQ(it, v.end());
}
