#pragma once

#include "psl_iodevice.h"
#include "psl_string.h"
#include "psl_memory.h"


namespace psl
{

struct process_impl;
class process : public psl::io_device
{
public:
    process();
    process(const psl::string &program, const psl::vector<psl::string> &args);
    ~process();

    bool open(const psl::string &program, const psl::vector<psl::string> &args);

    bool is_open() const override;
    bool close() override;

protected:
    bool write_intern(const uint8_t *data, size_t len) override;
    ssize_t read_intern(uint8_t *data, size_t len) override;


private:
    psl::unique_ptr<psl::process_impl> m_impl;


};

}
