#pragma once

#include <string.h>
#include <unistd.h>

#include "psl_limits.h"
#include "psl_utility.h"
#include "psl_functional.h"


namespace psl
{

constexpr size_t npos = psl::numeric_limits<size_t>::max();

template<typename ForwardIt1, typename ForwardIt2>
void iter_swap(ForwardIt1 a, ForwardIt2 b)
{
   using psl::swap;
   swap(*a, *b);
}

template<typename It, typename T>
size_t find_pos(It begin, It end, const T &element)
{
    ssize_t i = 0;
    while(begin != end)
    {
        if (*begin == element)
            return i;

        ++begin;
        ++i;
    }
    return npos;
}

template<typename It, typename Callable>
size_t find_pos_if(It begin, It end, Callable &&func)
{
    size_t i = 0;
    while(begin != end)
    {
        if (func(*begin))
            return i;

        ++begin;
        ++i;
    }
    return npos;
}

template<typename It, typename T>
constexpr It find(It begin, It end, const T &element)
{
    while(begin != end)
    {
        if (*begin == element)
            break;

        ++begin;
    }
    return begin;
}

template<typename It, typename Callable>
It find_if(It begin, It end, Callable &&func)
{
    while(begin != end)
    {
        if (func(*begin))
            break;

        ++begin;
    }

    return begin;
}

template<typename It, typename T>
size_t count(It begin, It end, const T &element)
{
    size_t count = 0;
    for (;begin != end; ++begin)
    {
        if (*begin == element)
        {
            ++count;
        }
    }
    return count;
}

template<typename It, typename Callable>
size_t count_if(It begin, It end, Callable &&func)
{
    size_t count = 0;
    for (;begin != end; ++begin)
    {
        if (func(*begin))
        {
            ++count;
        }
    }
    return count;
}

template<typename It, typename T>
bool contains(It begin, It end, const T &element)
{
    for (;begin != end; ++begin)
    {
        if (*begin == element)
        {
            return true;
        }
    }
    return false;
}

template<typename T>
constexpr void copy(const T *src, T *dest, size_t len)
{
    for (size_t i = 0; i < len; ++i)
    {
        dest[i] = src[i];
    }
}

template<typename InputIt, typename OutputIt>
constexpr void copy(InputIt first, InputIt last, OutputIt d_first)
{
    // does not work. if the type is not trivially copyconstructibe it would be wrong anyway. what about alignment?
//    if constexpr (psl::is_same<typename psl::iterator_traits<OutputIt>::iterator_category, psl::contiguous_iterator_tag>()
//            && psl::is_same<typename psl::iterator_traits<OutputIt>::iterator_category, psl::contiguous_iterator_tag>())
//    {
//        memcpy(&(*d_first), &(*first), last - first);
//        return;
//    }

    for (; first != last; ++first)
    {
        (*d_first) = *first;
        ++d_first;
    }
}

template<typename InputIt, typename OutputIt, typename Callable>
constexpr void copy_if(InputIt first, InputIt last, OutputIt d_first, Callable &&func)
{
    for (; first != last; ++first)
    {
        if (func(*first))
        {
            (*d_first) = *first;
            ++d_first;
        }
    }
}

template<typename T>
T min(const T &a, const T &b)
{
    if (a <= b)
        return a;
    return b;
}

//namespace detail
//{
//template<typename T>
//T min(const T &a)
//{
//    return a;
//}
//template<typename T, typename... Args>
//T min(Args&&... args)
//{
//    //return static_cast<T>((args)...);
//    return min(psl::forward<Args>(args)...);
//}
//template<typename T, typename... Args>
//T min(const T &a, Args&&... args)
//{
//    return min(a, psl::forward<Args>(args)...);
//}

//}

//template<typename T, typename... Args>
//T min(const T &a, Args&&... args)
//{
//    return detail::min(a, (args)...);
//}

template<typename T>
T max(const T &a, const T &b)
{
    if (a >= b)
        return a;
    return b;
}

//template<typename T>
//constexpr psl::pair<const T&, const T&> min_max(const T &a, const T &b)
//{
//    if (a <= b)
//        return psl::make_pair<const T&, const T&>(a, b);
//    return psl::make_pair<const T&, const T&>(b, a);
//}

template<typename T>
constexpr psl::pair<const T&, const T&> min_max(const T &a, const T &b)
{
    if (a <= b)
        return psl::make_pair(a, b);
    return psl::make_pair(b, a);
}

template<typename T, typename Compare>
constexpr psl::pair<const T &, const T &> min_max(const T &a, const T &b, Compare comp)
{
    if (comp(a, b))
        return psl::make_pair(a, b);
    return psl::make_pair(b, a);
}

template<typename It>
It min_element(It begin, It end)
{
    if (begin == end)
        return end;

    auto it = begin;

    while(++begin != end)
    {
        if (*begin < *it)
            it = begin;
    }
    return it;
}

template<typename It>
It max_element(It begin, It end)
{
    if (begin == end)
        return end;

    auto it = begin;

    while(++begin != end)
    {
        if (*begin > *it)
            it = begin;
    }
    return it;
}

template<typename T>
constexpr T abs(const T &a)
{
    if (a < 0)
        return a * -1;

    return a;
}

template<typename It, typename UnaryFunction>
UnaryFunction for_each(It begin, It end, UnaryFunction func)
{
    for (;begin != end; ++begin)
    {
        func(*begin);
    }
    return func;
}

template<typename BidirIt>
void reverse(BidirIt first, BidirIt last)
{
    while ((first != last) && (first != --last))
    {
        psl::iter_swap(first++, last);
    }

}

template<typename InputIt, typename UnaryPredicate>
bool all_of(InputIt first, InputIt last, UnaryPredicate p)
{
    for (; first != last; ++first)
    {
        if (!p(*first))
        {
            return false;
        }
    }
    return true;
}

template<typename InputIt, typename UnaryPredicate>
bool any_of(InputIt first, InputIt last, UnaryPredicate p)
{
    for (; first != last; ++first)
    {
        if (p(*first))
        {
            return true;
        }
    }
    return false;
}

template<typename InputIt, typename UnaryPredicate>
bool none_of(InputIt first, InputIt last, UnaryPredicate p)
{
    for (; first != last; ++first)
    {
        if (p(*first))
        {
            return false;
        }
    }
    return true;
}

template<typename ForwardIt, typename T>
void fill(ForwardIt first, ForwardIt last, const T &value)
{
    for (; first != last; ++first)
    {
        *first = value;
    }
}

template<typename RandomIt, typename Compare>
void sort(RandomIt first, RandomIt last, Compare comp)
{
    auto sz = last - first;
    for (size_t i = 0; i < sz; ++i)
    {
        bool swapped = false;         //flag to detect any swap is there or not
        for (size_t j = 0; j < sz - i - 1; ++j)
        {
            if (comp(*(first + j + 1), *(first + j))) //when the current item is smaller than next
            {
                psl::swap(*(first + j), *(first + j + 1));
                swapped = true;    //set swap flag
            }
        }
        if (!swapped)
            break;       // No swap in this pass, so array is sorted
    }
}

template<typename RandomIt>
void sort(RandomIt first, RandomIt last)
{
    psl::sort(first, last, psl::less<decltype(*first)>());
}

template<class InputIt1, class InputIt2>
bool lexicographical_compare(InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2)
{
    for (; (first1 != last1) && (first2 != last2); ++first1, (void) ++first2)
    {
        if (*first1 < *first2)
            return true;
        if (*first2 < *first1)
            return false;
    }
    return (first1 == last1) && (first2 != last2);
}
template<class InputIt1, class InputIt2, class Compare>
bool lexicographical_compare(InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2, Compare comp)
{
    for (; (first1 != last1) && (first2 != last2); ++first1, (void) ++first2)
    {
        if (comp(*first1, *first2))
            return true;
        if (comp(*first2, *first1))
            return false;
    }
    return (first1 == last1) && (first2 != last2);
}

}
