#pragma once

#include "psl_char_manip.h"
#include "psl_algorithm.h"
#include "psl_utility.h"
#include "psl_limits.h"
#include "psl_throw.h"

#include <cstdio>
#include <unistd.h>
#include <cassert>
#include <cwchar>

#include <cstdlib>


namespace psl
{

template<typename T> struct char_traits
{
    static bool eq(T a, T b) { return a == b; }
    static bool lt(T a, T b) { return a < b; }
};
template<> struct char_traits<char>
{
    static bool eq(char a, char b) { return static_cast<unsigned char>(a) == static_cast<unsigned char>(b); }
    static bool lt(char a, char b) { return static_cast<unsigned char>(a) < static_cast<unsigned char>(b); }
};

namespace internal
{
template<typename CharType, typename... Args> int typed_snprintf(CharType *s, size_t maxlen, const char *format, Args &&...args);
template<typename... Args> int typed_snprintf(char *s, size_t maxlen, const char *format, Args &&...args) { return ::snprintf(s, maxlen ,format, psl::forward<Args>(args)...); }
template<typename... Args> int typed_snprintf(wchar_t *s, size_t maxlen, const wchar_t *format, Args &&...args) { return ::swprintf(s, maxlen ,format, psl::forward<Args>(args)...); }
}

template<typename CharT,
         typename Traits = psl::char_traits<CharT>,
         typename Allocator = psl::allocator<CharT>>
class basic_string
{
public:
    class const_iterator;
    class iterator;
    using traits_type = Traits;
    using value_type = CharT;
    using allocator_type = Allocator;
    //using size_type = psl::allocator_traits<Allocator>::size_type; ///@todo whatdoto with this as i have implemented size_type as signed type...?
    using size_type = ssize_t;
    using difference_type = psl::allocator_traits<Allocator>::difference_type; //__PTRDIFF_TYPE__;
    using reference = value_type &;
    using const_reference = const value_type &;
    using pointer = psl::allocator_traits<Allocator>::pointer;
    using const_pointer = psl::allocator_traits<Allocator>::const_pointer;
    using iterator = basic_string::iterator;
    using const_iterator = basic_string::const_iterator;

private:
    static constexpr unsigned int SPRINT_BUFF_SZ = 50;
    static constexpr size_type SSO_SZ = 8;

public:
    static constexpr size_type npos = psl::numeric_limits<size_type>::max();

/// constructors, destructor, assign
    basic_string() = default;

    basic_string(const value_type *c_str)
        : basic_string(c_str, strlen(c_str))
    {}

    basic_string(value_type c, size_type len)
    {
        resize(len);

        for (size_type i = 0; i < len; ++i)
        {
            m_values[i] = c;
        }
    }

    basic_string(const value_type *str, size_type len)
    {
        resize(len);

        psl::strcpy(m_values, str);
    }

    basic_string(const basic_string<value_type> &other)
        : basic_string(other.data(), other.length())
    {}

//    basic_string(const std::initializer_list<char_type> &list)
//    {
//        /// @todo check logic
//        alloc_empty(list.size() + 1);

//        size_type i = 0;
//        for (const auto &val : list)
//        {
//            m_values[i] = val;
//            ++i;
//        }
//        m_sz = list.size() + 1;
//        set_null_term();

//    }

    basic_string(basic_string<value_type> &&other)
    {
        m_sz = other.m_sz;
        m_cap = other.m_cap;

        if (other.is_sso_in_use())
        {
            psl::strcpy(m_values, other.m_values);
        }
        else
        {
            m_values = other.m_values;
        }

        other.m_cap = SSO_SZ;
        other.m_sz = 0;
        other.m_sso[0] = value_type();
        other.m_values = other.m_sso;
    }

    /// reserves size
    explicit basic_string(size_type size)
    {
        reserve(size);
    }

    basic_string(const_iterator it1, const_iterator it2)
    {
        size_type sz = (it2 - it1);
        resize(sz);

        for (auto it_me = begin(); (it_me != end() && it1 != it2); )
        {
            *it_me = *it1;
            ++it_me;
            ++it1;
        }
    }

    ~basic_string()
    {
        destroy();
    }

    basic_string &append(const value_type *s, size_type count)
    {
        ssize_t sz_before = psl::max(m_sz - 1, size_type(0));
        ssize_t sz_after = sz_before  + count;
        ssize_t sz_other = count;

        /// @todo careful with < 0 - hmm whats this comments meaning ?
        resize(sz_after);

        //printf("sz_before:%i - sz_after:%i - str.size():%i\n", sz_before, sz_after, str.size());

        for (ssize_t i = 0; i < sz_other; ++i)
        {
            //printf("i:%i\n", i + sz_before);
            m_values[i + sz_before] = s[i];
        }

        return *this;
    }

    basic_string &append(const basic_string &str)
    {
        return append(str.c_str(), str.length());
    }

    basic_string &append(value_type c)
    {
        ssize_t sz_before = psl::max(m_sz - 1, size_type(0));
        ssize_t sz_after = sz_before + 1;

        resize(sz_after);

        m_values[sz_before] = c;

        return *this;
    }

    basic_string &operator<<(const_reference value)
    {
        return append(value);
    }

    basic_string &operator=(const basic_string &other)
    {
        resize(other.length());

        for (size_type i = 0; i < other.m_sz; ++i)
            m_values[i] = other.m_values[i];

        return *this;
    }

    basic_string &operator=(basic_string &&other)
    {
        destroy();

        m_sz = other.m_sz;
        m_cap = other.m_cap;

        if (other.is_sso_in_use())
        {
            m_values = m_sso;
            psl::strcpy(m_values, other.m_values);
        }
        else
        {
            m_values = other.m_values;
        }

        other.m_cap = SSO_SZ;
        other.m_sz = 0;
        other.m_sso[0] = value_type();
        other.m_values = &other.m_sso[0];

        return *this;
    }

/// element access
    reference operator[](size_type position)
    {
        if (position >= m_sz)
            psl::internal::throw_out_of_range("operator[]", position, m_sz);
        return m_values[position];
    }

    const_reference operator[](size_type position) const
    {
        if (position >= m_sz)
            psl::internal::throw_out_of_range("operator[]", position, m_sz);
        return m_values[position];
    }

    basic_string& operator+=(const basic_string& rhs)
    {
        return append(rhs);
    }

//    basic_string& operator+=(const char_type *c_str)
//    {
//        append(c_str);
//    }

    friend basic_string operator+(basic_string lhs, const basic_string& rhs)
    {
        return lhs.append(rhs);
    }
    /// @todo bugged
    friend basic_string operator+(basic_string lhs, const value_type *c_str)
    {
        return lhs.append(basic_string<value_type>(c_str));
    }

    friend basic_string operator+(basic_string lhs, value_type c)
    {
        return lhs.append(c);
    }

    // friend bool operator<(const basic_string& lhs, const basic_string& rhs)
    // {
    //     return true;
    // }

    friend bool operator==(const basic_string& lhs, const value_type *c_str)
    {
        size_type i;
        for (i = 0; i < lhs.size(); ++i)
        {
            if (!char_traits<value_type>::eq(lhs[i], c_str[i]) || c_str[i] == value_type())
                return false;
        }

        if (i != lhs.size())
            return false;

        if (!char_traits<value_type>::eq(c_str[i], value_type()))
            return false;

        return true;
    }
    friend bool operator!=(const basic_string& lhs, const value_type *c_str)
    {
        return !(lhs == c_str);
    }

    const_reference front() const
    {
        return m_values[0];
    }

    const_reference back() const
    {
        return m_values[m_sz - 2];
    }

    pointer data()
    {
        return m_values;
    }
    const_pointer data() const
    {
        return m_values;
    }

    const_pointer c_str() const
    {
        // @todo: is dumb
        if (m_sz > 0)
            return m_values;

        static constexpr const value_type null_char = 0;
        return &null_char;
    }

/// capacity
    size_type size() const
    {
        return psl::max(m_sz - 1, size_type(0));
    }

    size_type length() const
    {
        return size();
    }

    size_type capacity() const
    {
        return psl::max(m_cap - 1, size_type(0));
    }

    bool empty() const
    {
        return (m_sz == 0);
    }

    void reserve(size_type size)
    {
        ++size; // for null term

        size = psl::max(SSO_SZ, size);

        //::printf("reserve %zu -> %zu sso: %d\n", m_cap, size, is_sso_in_use());

        if (size <= m_cap)
            return;

        value_type *new_array = psl::allocator_traits<Allocator>::allocate(m_alloc, size);

        for (size_type i = 0; i < m_sz; ++i)
            new_array[i] = m_values[i];

        destroy();

        m_values = new_array;
        m_cap = size;
    }

    // broken after sso impl, but who needs this anyway???
//    void shrink_to_fit()
//    {
//        size_type newSize = (m_sz >= MIN_SZ) ? m_sz : MIN_SZ;
//        char_type *newArray = new char_type[newSize];

//        for (size_type i = 0; i < m_sz; ++i)
//            newArray[i] = m_values[i];

//        if (!is_sso_in_use())
//            delete[] m_values;

//        m_values = newArray;
//        m_cap = newSize;
//    }

/// modifiers
    void push_back(value_type value)
    {
        resize(length() + 1);
        m_values[m_sz - 2] = value;
    }

    void resize(size_type count)
    {
        if (count <= 0)
        {
            m_sz = 0;
            return;
        }

        reserve(count);

        for (size_type i = m_sz; i < count; ++i)
        {
            m_values[i] = value_type();
        }

        m_sz = count + 1; // null terminator
        set_null_term();
    }

    void resize(size_type count, const value_type &value)
    {
        ssize_t size_before = m_sz;
        resize(count);

        for (size_type i = size_before; i < count; ++i)
            m_values[i] = value;
    }

    void insert(size_type position, const value_type &value)
    {
        if (position > m_sz)
            psl::internal::throw_out_of_range("insert", position, m_sz);

        if (m_sz >= m_cap)
            reserve(m_cap * 2);

        for (size_type i = m_sz; i > position; --i)
            m_values[i] = m_values[i - 1];

        m_values[position] = value;

        inc_and_set_null_term();
    }

    iterator insert(const_iterator pos, const_reference val)
    {
        auto diff = pos - begin();
        if (diff < 0 || static_cast<size_type>(diff) > m_sz)
            psl::internal::throw_out_of_range("insert", pos - 0, m_sz);

        size_type current = static_cast<size_type>(diff);
        if (m_sz >= m_cap)
            reserve(m_cap * 2);

        for (size_type i = m_sz; i-->current;)
            m_values[i + 1] = m_values[i];

        m_values[current] = val;

        inc_and_set_null_term();

        return iterator(m_values + current);
    }

    void erase(size_type position)
    {
        if (position >= m_sz)
            psl::internal::throw_out_of_range("erase", position, m_sz);

        for (size_type i = position; i < m_sz - 1; ++i)
            m_values[i] = m_values[i + 1];

        dec_and_set_null_term();
    }

    iterator erase(const_iterator pos)
    {
        auto diff = pos - begin();
        if (diff < 0 || static_cast<size_type>(diff) >= m_sz)
            psl::internal::throw_out_of_range("erase", pos - 0, m_sz);

        size_type current = static_cast<size_type>(diff);
        for (size_type i = current; i < m_sz - 1; ++i)
            m_values[i] = m_values[i + 1];

        dec_and_set_null_term();

        return iterator(m_values + current);
    }

    void pop_back()
    {
        if (m_sz == 0)
            psl::internal::throw_exception("pop_back on empty container");
        dec_and_set_null_term();
    }

    void clear()
    {
        m_sz = 0;
    }

    basic_string &set_num(float num, int precision)
    { return set_num_internal<double>(num, precision, "%.*f"); }

    basic_string &set_num(double num, int precision)
    { return set_num_internal<double>(num, precision, "%.*lf"); }

    basic_string &set_num(int num)
    { return set_num_internal<long>(num, "%d"); }

    basic_string &set_num(long num)
    { return set_num_internal<long>(num, "%ld"); }

    template<typename... Args>
    basic_string &sprintf(const value_type *fmt, Args &&...args)
    {
        // get length without null terminator
        int len = internal::typed_snprintf(0, 0, fmt, psl::forward<Args>(args)...);
        if (len <= 0)
        {
            clear();
            return *this;
        }

        resize(len);

        internal::typed_snprintf(data(), len + 1U, fmt, psl::forward<Args>(args)...);
        return *this;
    }

    bool contains(value_type c) const
    {
        return psl::find(begin(), end(), c) != end();
    }

    bool contains(const basic_string &str) const
    {
        if (str.empty() || size() < str.size())
            return false;

        size_type j = 0;
        for (size_type i = 0; i < m_sz - 1; ++i)
        {
//            PSL_ASSERT(i < size());
//            PSL_ASSERT(j < str.size());

            if (char_traits<value_type>::eq(m_values[i], str[j]))
            {
                if (++j == str.size())
                    return true;
            }
            else
            {
                j = 0;
            }
        }
        return false;
    }

    size_type find(const basic_string &str, size_type pos = 0) const
    {
        return find(str.data(), pos, str.length());
    }

    size_type find(const value_type *s, size_type pos, size_type count) const
    {
        if (count == 0 || empty() || size() < count || pos >= size())
            return npos;

        size_type j = 0;
        for (size_type i = pos; i < size(); ++i)
        {
            if (char_traits<value_type>::eq(m_values[i], s[j]))
            {
                ++j;
                if (j == count)
                    return (i - count + 1);
            }
            else
            {
                j = 0;
            }
        }
        return npos;
    }

    size_type find(value_type c, size_type pos = 0) const
    {
        if (empty() || pos >= size())
            return npos;

        for (size_type i = pos; i < size(); ++i)
        {
            if (char_traits<value_type>::eq(m_values[i], c))
            {
                return i;
            }
        }
        return npos;
    }

    // template<typename StringViewLike>
    // size_type find(const StringViewLike& t, size_type pos = 0)
    // {
    //     return find(t.data(), pos, t.length());
    // }

    size_type rfind(value_type c) const
    {
        if (empty())
            return npos;

        for (size_type i = m_sz; i --> 0; )
        {
            if (char_traits<value_type>::eq(m_values[i], c))
            {
                return i;
            }
        }
        return npos;
    }

    /// @todo make basic_string::substr less lame
//    basic_string substr(size_type pos = 0, size_type count = npos) const
//    {
//        /// @todo can still overflow
//        size_type end = (count == npos) ? npos : pos + count;

//        basic_string str;
//        for (size_type i = pos; ((i < end) && (i < m_sz)); ++i)
//        {
//            str.push_back(m_values[i]);
//        }
//        return str;
//    }

    basic_string substr(size_type pos = 0, size_type count = npos) const
    {
        /// @todo can still overflow
        size_type end = (count == npos) ? npos : pos + count;

        basic_string str;
        for (size_type i = pos; ((i < end) && (i < m_sz - 1)); ++i)
        {
            str.push_back(m_values[i]);
        }
        return str;
    }

    template<typename Container>
    Container split(value_type delimit, bool skip_empty = false) const
    {
        Container vec;

        auto it_start = begin();

        while(it_start != end())
        {
            auto it_found = psl::find(it_start, end(), delimit);

            if (it_found == end())
                break;

            //printf("range: %d - char: %c\n", (it_found - it_start), *it_found);

            if (!skip_empty || (it_start != it_found))
                vec << basic_string(it_start, it_found);

            it_start = it_found;
            ++it_start;
        }

        if (!skip_empty || (it_start != end()))
            vec << basic_string(it_start, end());

        return vec;

    }
//    psl::vector<basic_string> split(char_type delimit) const
//    {
//        psl::vector<basic_string> vec;

//        auto start_it = begin();

//        for (const_iterator it = begin(); it != end(); ++it)
//        {
//            if ((*it) == delimit)
//            {
//                if (start_it != it)
//                    vec.push_back(basic_string(start_it, it));

//                ++it;   // skip delimit
//                start_it = it;

//                if (it == end())
//                    break;
//            }
//        }

//        if (start_it != end())
//            vec.push_back(basic_string(start_it, end()));

//        return vec;
//    }

private:
    template<typename U, typename = psl::enable_if_t<psl::is_arithmetic_v<U>>>
    basic_string &set_num_internal(U num, const value_type *format)
    {
        reserve(SPRINT_BUFF_SZ);
        m_sz = 1 + ::sprintf(m_values, format, num);
        return *this;
    }

    template<typename U, typename = psl::enable_if_t<psl::is_arithmetic_v<U>>>
    basic_string &set_num_internal(U num, int precision, const value_type *format)
    {
        reserve(SPRINT_BUFF_SZ);
        m_sz = 1 + ::sprintf(m_values, format, precision, num);
        return *this;
    }

    void destroy()
    {
        if (!is_sso_in_use())
            psl::allocator_traits<Allocator>::deallocate(m_alloc, m_values, m_sz);
    }

public:
    void dump() const
    {
        assert(m_values != nullptr);

        printf(">DUMP-v--------<\n");
        printf("  size     : %ld\n", m_sz);
        printf("  max size : %ld\n", m_cap);
        printf("  c_str    : \"%s\"\n", c_str());
        printf(">-----^--------<\n\n");
    }

/// iterators
    iterator begin()
    {
        if (m_sz == 0)
            return end();

        return iterator(m_values);
    }
    iterator end()
    {
        size_type pos = 0;

        if (m_sz > 0)
            pos += m_sz - 1;

        return iterator(m_values + pos);
    }

    const_iterator begin() const
    {
        if (m_sz == 0)
            return end();

        return const_iterator(m_values);
    }
    const_iterator end() const
    {
        size_type pos = 0;

        if (m_sz > 0)
            pos += m_sz - 1;

        return const_iterator(m_values + pos);
    }

    const_iterator cbegin() const
    {
        return begin();
    }
    const_iterator cend() const
    {
        return end();
    }

/// private section
private:
    void set_null_term()
    {
        if (m_sz > 1)
            m_values[m_sz - 1] = value_type();
    }

    // @todo: is dumb
    void inc_and_set_null_term()
    {
        ++m_sz;

        if (m_sz == 1)
            m_sz = 2;

        m_values[m_sz - 1] = value_type();
    }

    // @todo: is dumb
    void dec_and_set_null_term()
    {
        --m_sz;

        if (m_sz < 2)
            m_sz = 0;
        else
            m_values[m_sz - 1] = value_type();
    }

    bool is_sso_in_use() const
    {
        return (m_values == m_sso);
    }

private:
    allocator_type m_alloc;
    value_type *m_values = m_sso;
    value_type m_sso[SSO_SZ] = {};

    size_type m_sz = 0;
    size_type m_cap = SSO_SZ;

/// iterator implementations
public:
    class iterator
    {
    public:
        using value_type = basic_string::value_type;
        using reference = basic_string::reference;
        using pointer = basic_string::pointer;
        using difference_type = basic_string::difference_type;

    public:
        iterator()
        {
            ptr = nullptr;
        }

        iterator(pointer ptr)
        {
            this->ptr = ptr;
        }

        reference operator*() const
        {
            return *ptr;
        }

        pointer operator->() const
        {
            return ptr;
        }

        iterator& operator++()
        {
            ++ptr;
            return *this;
        }

        iterator operator++(int)
        {
            iterator it = *this;
            ++ptr;
            return it;
        }

        iterator operator+ (difference_type difference) const
        {
            return iterator(ptr + difference);
        }

        iterator operator- (difference_type difference) const
        {
            return iterator(ptr - difference);
        }

        bool operator==(const const_iterator &it) const
        {
            return it == ptr;
        }

        bool operator!=(const const_iterator &it) const
        {
            return it != ptr;
        }

        operator const_iterator() const
        {
            return const_iterator(ptr);
        }

    private:
        pointer ptr;
    };

    class const_iterator
    {
    public:
        using value_type = basic_string::value_type;
        using reference = basic_string::const_reference;
        using pointer = basic_string::const_pointer;
        using difference_type = basic_string::difference_type;

    public:
        const_iterator()
        {
            ptr = nullptr;
        }

        const_iterator(pointer ptr)
        {
            this->ptr = ptr;
        }

        reference operator*() const
        {
            return *ptr;
        }

        pointer operator->() const
        {
            return ptr;
        }

        const_iterator& operator++()
        {
            ++ptr;
            return *this;
        }

        const_iterator operator++(int)
        {
            const_iterator it = *this;
            ++ptr;
            return it;
        }

        bool operator==(const const_iterator &it) const
        {
            return it.ptr == ptr;
        }

        bool operator!=(const const_iterator &it) const
        {
            return it.ptr != ptr;
        }

        // basic_string::difference_type operator+(const const_iterator &rop) ? should or not?
        // {
        //     return ptr + rop.ptr;
        // }
        basic_string::difference_type operator-(const const_iterator &rop)
        {
            return ptr - rop.ptr;
        }

    private:
        pointer ptr;
    };

};

template<class CharT>
void swap(psl::basic_string<CharT> &lhs, psl::basic_string<CharT> &rhs)
{
    psl::basic_string<CharT> temp = psl::move(lhs);
    lhs = psl::move(rhs);
    rhs = psl::move(temp);
}

template<class CharT, class Traits, class Alloc>
bool operator==(const psl::basic_string<CharT, Traits, Alloc> &lhs, const psl::basic_string<CharT, Traits, Alloc> &rhs)
{
    using size_type = psl::basic_string<CharT, Traits, Alloc>::size_type;

    if (lhs.size() != rhs.size())
        return false;

    for (size_type i = 0; i < lhs.size(); ++i)
    {
        if (!Traits::eq(lhs[i], rhs[i]))
            return false;
    }
    return true;
}
template<class CharT, class Traits, class Alloc>
bool operator!=(const psl::basic_string<CharT, Traits, Alloc> &lhs, const psl::basic_string<CharT, Traits, Alloc> &rhs)
{
    return !(lhs == rhs);
}
template<class CharT, class Traits, class Alloc>
bool operator<(const psl::basic_string<CharT, Traits, Alloc> &lhs, const psl::basic_string<CharT, Traits, Alloc> &rhs)
{
    return ::strcmp(lhs.c_str(), rhs.c_str()) < 0;
}
template<class CharT, class Traits, class Alloc>
bool operator<=(const psl::basic_string<CharT, Traits, Alloc> &lhs, const psl::basic_string<CharT, Traits, Alloc> &rhs)
{
    return ::strcmp(lhs.c_str(), rhs.c_str()) <= 0;
}
template<class CharT, class Traits, class Alloc>
bool operator>(const psl::basic_string<CharT, Traits, Alloc> &lhs, const psl::basic_string<CharT, Traits, Alloc> &rhs)
{
    return ::strcmp(lhs.c_str(), rhs.c_str()) > 0;
}
template<class CharT, class Traits, class Alloc>
bool operator>=(const psl::basic_string<CharT, Traits, Alloc> &lhs, const psl::basic_string<CharT, Traits, Alloc> &rhs)
{
    return ::strcmp(lhs.c_str(), rhs.c_str()) >= 0;
}

using string = basic_string<char> ;
using wstring = basic_string<wchar_t> ;

}
