#pragma once

#include "psl_iodevice.h"


namespace psl
{

class stdcout : public psl::io_device
{
public:
    bool is_open() const override { return true; }
    bool close() override { return true; }

    bool write_stderr(const char *data, size_t len);

protected:
    bool write_intern(const uint8_t *data, size_t len) override;
    ssize_t read_intern(uint8_t *data, size_t len) override;
};

class stdcerr : public psl::io_device
{
public:
    bool is_open() const override { return true; }
    bool close() override { return true; }

    bool write_stdout(const char *data, size_t len);

protected:
    bool write_intern(const uint8_t *data, size_t len) override;
    ssize_t read_intern(uint8_t *data, size_t len) override;
};

}
