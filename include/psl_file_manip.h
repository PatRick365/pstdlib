#pragma once

#include <unistd.h>
#include <sys/stat.h>


namespace psl
{

extern int file_open(const char *path, int file_open_mode);
extern ssize_t file_write(int fd, const void *buf, size_t count);
extern ssize_t file_read(int fd, void *buf, size_t count);
extern int file_fsync(int fd);
extern int file_close(int fd);
extern int file_unlink(const char *path);
extern off_t file_lseek(int fd, off_t offset, int whence);

extern int file_fstat(int fd, struct stat &s);
extern int file_stat(const char *path, struct stat &s);

}
