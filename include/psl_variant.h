#pragma once

#include "psl_memory.h"

#include "psl_string.h"
#include "psl_string_vec.h"
#include "psl_type_traits.h"

#include <unistd.h>


namespace psl
{

struct content_base
{
    virtual ~content_base() = default;
};

template<typename T>
struct content
{
    virtual ~content() = default;

    T value;
};

class variant
{
public:
    enum val_type { t_null, t_int, t_uint, t_long, t_ulong, t_float, t_double, t_string, t_string_vector };

    variant() = default;

    static variant null;

    variant(int v)                       { set_int(v); }
    variant(unsigned int v)              { set_uint(v); }
    variant(long v)                      { set_long(v); }
    variant(unsigned long v)             { set_ulong(v); }
    variant(float v)                     { set_float(v); }
    variant(double v)                    { set_double(v); }
    variant(const psl::string &v)        { set_string(v); }
    variant(const psl::string_vector &v) { set_string_vector(v); }

//    variant(pstd::string &&v)                    : m_string(pstd::make_unique<pstd::string>(pstd::move(v))) {}
//    variant(pstd::string_vec &&v)      : m_string_vec(pstd::make_unique<pstd::string_vec>(pstd::move(v))) {}

    variant(const variant &other)
    {
        *this = other;
    }

    variant& operator=(const psl::variant &other)
    {
        if (&other == this)
            return *this;

        reset();

        switch (other.m_type)
        {
        case t_null: break;
        case t_int: set_int(other.get_int()); break;
        case t_uint: set_uint(other.get_uint()); break;
        case t_long: set_long(other.get_long()); break;
        case t_ulong: set_ulong(other.get_ulong()); break;
        case t_float: set_float(other.get_float()); break;
        case t_double: set_double(other.get_double()); break;
        case t_string: set_string(other.get_string()); break;
        case t_string_vector: set_string_vector(other.get_string_vector()); break;
        }
        return *this;
    }

    variant &operator=(int v)                       { set_int(v); return *this; }
    variant &operator=(unsigned int v)              { set_uint(v); return *this; }
    variant &operator=(long v)                      { set_long(v); return *this; }
    variant &operator=(unsigned long v)             { set_ulong(v); return *this; }
    variant &operator=(float v)                     { set_float(v); return *this; }
    variant &operator=(double v)                    { set_double(v); return *this; }
    variant &operator=(const psl::string &v)        { set_string(v); return *this; }
    variant &operator=(const psl::string_vector &v) { set_string_vector(v); return *this; }

    void set_int(int v)                                 { reset(); m_type = t_int;    m_int = psl::move(psl::make_unique<int>(v)); }
    void set_uint(unsigned int v)                       { reset(); m_type = t_uint;   m_uint = psl::make_unique<unsigned int>(v); }
    void set_long(long v)                               { reset(); m_type = t_long;   m_long = psl::make_unique<long>(v); }
    void set_ulong(unsigned long v)                     { reset(); m_type = t_ulong;  m_ulong = psl::make_unique<unsigned long>(v); }
    void set_float(float v)                             { reset(); m_type = t_float;  m_float = psl::make_unique<float>(v); }
    void set_double(double v)                           { reset(); m_type = t_double; m_double = psl::make_unique<double>(v); }
    void set_string(const psl::string &v)               { reset(); m_type = t_string; m_string = psl::make_unique<psl::string>(v); }
    void set_string_vector(const psl::string_vector &v) { reset(); m_type = t_string_vector; m_string_vector = new psl::string_vector(); *m_string_vector = v; }
    //void set_string_vec(const pstd::string_vec &v) { if (m_init) { invalidate_all(); } m_init = true; m_type = t_string_vec; m_string_vec = pstd::make_unique<pstd::string_vec>(v); }

    bool is_null()          const { return m_type == t_null; }
    bool is_int()           const { return m_type == t_int; }
    bool is_uint()          const { return m_type == t_uint; }
    bool is_long()          const { return m_type == t_long; }
    bool is_ulong()         const { return m_type == t_ulong; }
    bool is_float()         const { return m_type == t_float; }
    bool is_double()        const { return m_type == t_double; }
    bool is_string()        const { return m_type == t_string; }
    bool is_string_vector() const { return m_type == t_string_vector; }

    bool is(val_type t) const { return t == m_type; }

                   int &get_int()           { return *m_int; }
          unsigned int &get_uint()          { return *m_uint; }
                  long &get_long()          { return *m_long; }
         unsigned long &get_ulong()         { return *m_ulong; }
                 float &get_float()         { return *m_float; }
                double &get_double()        { return *m_double; }
           psl::string &get_string()        { return *m_string; }
    psl::string_vector &get_string_vector() { return *m_string_vector; }

                   int &get_int() const           { return *m_int; }
          unsigned int &get_uint() const          { return *m_uint; }
                  long &get_long() const          { return *m_long; }
         unsigned long &get_ulong() const         { return *m_ulong; }
                 float &get_float() const         { return *m_float; }
                double &get_double() const        { return *m_double; }
           psl::string &get_string() const        { return *m_string; }
    psl::string_vector &get_string_vector() const { return *m_string_vector; }


private:
    void reset()
    {
        if (m_init)
        {
            m_int.reset(nullptr);
            m_uint.reset(nullptr);
            m_long.reset(nullptr);
            m_ulong.reset(nullptr);
            m_float.reset(nullptr);
            m_double.reset(nullptr);
            m_string.reset(nullptr);
            m_string_vector.reset(nullptr);
        }
        m_init = true;
    }

//    template <typename T>
//    T get(const pstd::variant &/*var*/) { return T(); }


private:
    val_type m_type = t_null;
    bool m_init = false;

                   psl::unique_ptr<int> m_int;
          psl::unique_ptr<unsigned int> m_uint;
                  psl::unique_ptr<long> m_long;
         psl::unique_ptr<unsigned long> m_ulong;
                 psl::unique_ptr<float> m_float;
                psl::unique_ptr<double> m_double;
           psl::unique_ptr<psl::string> m_string;
    psl::unique_ptr<psl::string_vector> m_string_vector;


};

typedef psl::vector<psl::variant> variant_vector;


template<typename T>        T get(const psl::variant &)                        { return psl::variant::null; }
template<>                int get<int>(const psl::variant &var)                { return var.get_int(); }
template<>       unsigned int get<unsigned int>(const psl::variant &var)       { return var.get_uint(); }
template<>               long get<long>(const psl::variant &var)               { return var.get_long(); }
template<>      unsigned long get<unsigned long>(const psl::variant &var)      { return var.get_ulong(); }
template<>              float get<float>(const psl::variant &var)              { return var.get_float(); }
template<>             double get<double>(const psl::variant &var)             { return var.get_double(); }
template<>        psl::string get<psl::string>(const psl::variant &var)        { return var.get_string(); }
template<> psl::string_vector get<psl::string_vector>(const psl::variant &var) { return var.get_string_vector(); }


template<typename R, typename T, typename Visitor>
R visit(Visitor &&vis, const psl::variant &variant)
{
    return static_cast<R>(vis(psl::get<T>(variant)));
}

}


