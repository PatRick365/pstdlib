#pragma once

// this include is not allowed here...
//#include "psl_algorithm.h"

#include "psl_type_traits.h"

#include <unistd.h>


namespace psl
{

constexpr double log10(int)
{
    // log10 2
    return 0.30102999566;
}

template <typename T>
constexpr T pi = T(3.141592653589793238462643383279);

template <typename T, typename psl::enable_if<psl::is_integral<T>::value>::type * = nullptr>
ssize_t num_digits(T number)
{
    ssize_t digits = 0;

    if (number < 0)
        digits = 1;

    while (number)
    {
        number /= 10;
        digits++;
    }
    return digits;
}

template <typename T, typename psl::enable_if<psl::is_arithmetic<T>::value>::type * = nullptr>
constexpr T pow(T n, int e)
{
    if (e == 0)
        return 1;

    if (e < 0)
        return 1.0 / psl::pow(n, e * -1);

    T result = n;

    for (int i = 0; i < e - 1; ++i)
    {
        result *= n;
    }
    return result;
}

template <typename T, typename psl::enable_if<psl::is_arithmetic<T>::value>::type * = nullptr>
constexpr double fpow(T n, int e)
{
    return pow<double>(n, e);
}

float ceil(float f);

}
