#pragma once


namespace psl
{

typedef decltype(nullptr) nullptr_t; // Defined in header <cstddef>


template <class...>
using void_t = void;

template<class T, T v>
struct integral_constant
{
    static constexpr T value = v;
    using value_type = T;
    using type = integral_constant; // using injected-class-name
    constexpr operator value_type() const noexcept { return value; }
    constexpr value_type operator()() const noexcept { return value; }
};


typedef integral_constant<bool, true>  true_type;
typedef integral_constant<bool, false> false_type;


template<class T> struct is_lvalue_reference     : psl::false_type {};
template<class T> struct is_lvalue_reference<T&> : psl::true_type {};


template< class T > struct remove_const          { typedef T type; };
template< class T > struct remove_const<const T> { typedef T type; };

template< class T > struct remove_volatile             { typedef T type; };
template< class T > struct remove_volatile<volatile T> { typedef T type; };

template< class T >
struct remove_cv
{
    typedef typename psl::remove_volatile<typename psl::remove_const<T>::type>::type type;
};

template <class T> using remove_cv_t = typename remove_cv<T>::type;
template <class T> using remove_const_t = typename remove_const<T>::type;
template <class T> using remove_volatile_t = typename remove_volatile<T>::type;

/// is_same
template<class T, class U>
struct is_same : psl::false_type {};

template<class T>
struct is_same<T, T> : psl::true_type {};


/// is_integral
template<typename> struct is_integral_base : psl::false_type {};
template<typename T> struct is_integral : public is_integral_base<typename psl::remove_cv<T>::type>::type {};

template<> struct is_integral_base<bool> : psl::true_type {};
template<> struct is_integral_base<char> : psl::true_type {};
template<> struct is_integral_base<short> : psl::true_type {};
template<> struct is_integral_base<int> : psl::true_type {};
template<> struct is_integral_base<long> : psl::true_type {};
template<> struct is_integral_base<long long> : psl::true_type {};

template<> struct is_integral_base<unsigned char> : psl::true_type {};
template<> struct is_integral_base<unsigned short> : psl::true_type {};
template<> struct is_integral_base<unsigned int> : psl::true_type {};
template<> struct is_integral_base<unsigned long> : psl::true_type {};
template<> struct is_integral_base<unsigned long long> : psl::true_type {};


/// is_floating_point
template<typename> struct is_floating_point_base : psl::false_type {};
template<typename T> struct is_floating_point : public is_floating_point_base<typename psl::remove_cv<T>::type>::type {};

template<> struct is_floating_point<float> : psl::true_type {};
template<> struct is_floating_point<double> : psl::true_type {};
template<> struct is_floating_point<long double> : psl::true_type {};


namespace detail
{

template <class T>
struct type_identity { using type = T; }; // or use psl::type_identity (since C++20)

template <class T> // Note that `cv void&` is a substitution failure
auto try_add_lvalue_reference(int) -> type_identity<T &>;
template <class T> // Handle T = cv void case
auto try_add_lvalue_reference(...) -> type_identity<T>;

template <class T>
auto try_add_rvalue_reference(int) -> type_identity<T &&>;
template <class T>
auto try_add_rvalue_reference(...) -> type_identity<T>;

} // namespace detail

template <class T>
struct add_lvalue_reference : decltype(detail::try_add_lvalue_reference<T>(0)) {};

template <class T>
struct add_rvalue_reference : decltype(detail::try_add_rvalue_reference<T>(0)) {};

template<class T>
typename psl::add_rvalue_reference<T>::type declval() noexcept;


template<bool B, class T = void>
struct enable_if {};

template<class T>
struct enable_if<true, T> { typedef T type; };

template <bool B, class T = void>
using enable_if_t = typename enable_if<B, T>::type;

template<class T> struct add_cv { typedef const volatile T type; };
template<class T> struct add_const { typedef const T type; };
template<class T> struct add_volatile { typedef volatile T type; };

template <class T> using add_cv_t = typename add_cv<T>::type;
template <class T> using add_const_t = typename add_const<T>::type;
template <class T> using add_volatile_t = typename add_volatile<T>::type;


// this crap from stackoverflow here does not work....
//template <class, class T, class... Args>
//struct is_constructible : psl::false_type
//{};

//template <class T, class... Args>
//struct is_constructible<void_t<decltype(T(psl::declval<Args>()...))>, T, Args...> : psl::true_type
//{};

//template <class T, class... Args>
//struct is_constructible_<void_t<decltype(::new T(psl::declval<Args>()...))>, T, Args...> : psl::true_type {};

// todo
template <class T, class... Args>
struct is_trivially_constructible;

// todo
template <class T, class... Args>
struct is_nothrow_constructible;

// nneds is_constructible
//template <class T, class... Args>
//inline constexpr bool is_constructible_v = is_constructible<T, Args...>::value;

//template <class T, class... Args>
//inline constexpr bool is_trivially_constructible_v = is_trivially_constructible<T, Args...>::value;

//template <class T, class... Args>
//inline constexpr bool is_nothrow_constructible_v = is_nothrow_constructible<T, Args...>::value;


//template <class T>
//struct is_copy_constructible
//    : psl::is_constructible<T, typename psl::add_lvalue_reference<typename psl::add_const<T>::type>::type>
//{};
//template <class T>
//inline constexpr bool is_copy_constructible_v = is_copy_constructible<T>::value;

template <class T>
struct is_trivially_copy_constructible
    : psl::is_trivially_constructible<T, typename psl::add_lvalue_reference<typename psl::add_const<T>::type>::type>
{};
template <class T>
inline constexpr bool is_trivially_copy_constructible_v = is_trivially_copy_constructible<T>::value;

template <class T>
struct is_nothrow_copy_constructible
    : psl::is_nothrow_constructible<T, typename psl::add_lvalue_reference<typename psl::add_const<T>::type>::type>
{};
template <class T>
inline constexpr bool is_nothrow_copy_constructible_v = is_nothrow_copy_constructible<T>::value;


template <class T> struct remove_reference { typedef T type; };
template <class T> struct remove_reference<T &> { typedef T type; };
template <class T> struct remove_reference<T &&> { typedef T type; };

template <class T>
using remove_reference_t = typename remove_reference<T>::type;

template <bool B, class T, class F> struct conditional { typedef T type; };
template <class T, class F> struct conditional<false, T, F> { typedef F type; };
template <bool B, class T, class F> using conditional_t = typename conditional<B, T, F>::type;


template<class T>
struct is_array : psl::false_type {};

template<class T>
struct is_array<T[]> : psl::true_type {};

template<class T, unsigned long N>
struct is_array<T[N]> : psl::true_type {};

template <class T>
inline constexpr bool is_array_v = is_array<T>::value;


template<class T>
struct remove_extent { typedef T type; };

template<class T>
struct remove_extent<T[]> { typedef T type; };

template<class T, unsigned long N>
struct remove_extent<T[N]> { typedef T type; };

template<class T>
using remove_extent_t = typename remove_extent<T>::type;


namespace detail
{

template <class T>
auto try_add_pointer(int) -> type_identity<typename psl::remove_reference<T>::type*>;
template <class T>
auto try_add_pointer(...) -> type_identity<T>;

} // namespace detail

template <class T>
struct add_pointer : decltype(detail::try_add_pointer<T>(0)) {};


template <class T> struct is_reference : psl::false_type {};
template <class T> struct is_reference<T &> : psl::true_type {};
template <class T> struct is_reference<T &&> : psl::true_type {};
template <class T> inline constexpr bool is_reference_v = is_reference<T>::value;

template<class T> struct is_const          : psl::false_type {};
template<class T> struct is_const<const T> : psl::true_type {};
template<class T> inline constexpr bool is_const_v = is_const<T>::value;

template<class T>
struct is_function : psl::integral_constant<
    bool,
    !psl::is_const<const T>::value && !psl::is_reference<T>::value
> {};
template <class T> inline constexpr bool is_function_v = is_function<T>::value;

template<class T>
struct decay
{
private:
    typedef typename psl::remove_reference<T>::type U;
public:
    typedef typename psl::conditional<
        psl::is_array<U>::value,
        typename psl::remove_extent<U>::type*,
        typename psl::conditional<
            psl::is_function<U>::value,
            typename psl::add_pointer<U>::type,
            typename psl::remove_cv<U>::type
        >::type
    >::type type;
};
template<class T>
using decay_t = typename decay<T>::type;

template <class T> struct is_pointer : psl::false_type{};
template <class T> struct is_pointer<T *> : psl::true_type{};
template <class T> struct is_pointer<T *const> : psl::true_type{};
template <class T> struct is_pointer<T *volatile> : psl::true_type{};
template <class T> struct is_pointer<T *const volatile> : psl::true_type{};

template< class T >
struct is_member_pointer_helper         : psl::false_type {};

template< class T, class U >
struct is_member_pointer_helper<T U::*> : psl::true_type {};

template< class T >
struct is_member_pointer :
    is_member_pointer_helper<typename psl::remove_cv<T>::type> {};

template< class T >
inline constexpr bool is_member_pointer_v = is_member_pointer<T>::value;

template< class T >
inline constexpr bool is_pointer_v = is_pointer<T>::value;

template< class T >
struct is_arithmetic : psl::integral_constant<bool,
        psl::is_integral<T>::value ||
        psl::is_floating_point<T>::value> {};
template< class T >
inline constexpr bool is_arithmetic_v = is_arithmetic<T>::value;

template <class _Tp> struct is_enum : public integral_constant<bool, __is_enum(_Tp)> {};

template< class T >
struct is_null_pointer : psl::is_same<psl::nullptr_t, psl::remove_cv_t<T>> {};

template< class T >
inline constexpr bool is_null_pointer_v = is_null_pointer<T>::value;

template<class T>
struct is_scalar : psl::integral_constant<bool,
                     psl::is_arithmetic<T>::value     ||
                     psl::is_enum<T>::value           ||
                     psl::is_pointer<T>::value        ||
                     psl::is_member_pointer<T>::value ||
                     psl::is_null_pointer<T>::value> {};

template< class T >
inline constexpr bool is_scalar_v = is_scalar<T>::value;

template <class _Tp> struct __libcpp_union : public false_type{};
template <class _Tp> struct is_union : public __libcpp_union<typename remove_cv<_Tp>::type>{};
template <class T> inline constexpr bool is_union_v = is_union<T>::value;

namespace detail {
template <class T>
psl::integral_constant<bool, !psl::is_union<T>::value> test(int T::*);

template <class>
psl::false_type test(...);
}

template <class T>
struct is_class : decltype(detail::test<T>(nullptr))
{};

template< class T>
struct is_object : psl::integral_constant<bool,
                     psl::is_scalar<T>::value ||
                     psl::is_array<T>::value  ||
                     psl::is_union<T>::value  ||
                     psl::is_class<T>::value> {};

namespace details {
    template <typename B>
    psl::true_type test_pre_ptr_convertible(const volatile B*);
    template <typename>
    psl::false_type test_pre_ptr_convertible(const volatile void*);

    template <typename, typename>
    auto test_pre_is_base_of(...) -> psl::true_type;
    template <typename B, typename D>
    auto test_pre_is_base_of(int) ->
        decltype(test_pre_ptr_convertible<B>(static_cast<D*>(nullptr)));
}

template <typename Base, typename Derived>
struct is_base_of :
    psl::integral_constant<
        bool,
        psl::is_class<Base>::value && psl::is_class<Derived>::value &&
        decltype(details::test_pre_is_base_of<Base, Derived>(0))::value
    > {};

template< class Base, class Derived >
inline constexpr bool is_base_of_v = is_base_of<Base, Derived>::value;


namespace detail
{

template <class T>
inline T&& forward(typename remove_reference<T>::type& t) noexcept
{
    return static_cast<T&&>(t);
}

template <class T>
inline T&& forward(typename remove_reference<T>::type&& t) noexcept
{
    static_assert(!is_lvalue_reference<T>::value, "Can not forward an rvalue as an lvalue.");
    return static_cast<T&&>(t);
}

template<class T>
struct invoke_impl
{
    template<class F, class... Args>
    static auto call(F&& f, Args&&... args)
        -> decltype(forward<F>(f)(forward<Args>(args)...));
};

template<class B, class MT>
struct invoke_impl<MT B::*>
{
    template<class T, class Td = typename psl::decay<T>::type,
             class = typename psl::enable_if<psl::is_base_of<B, Td>::value>::type>
    static auto get(T&& t) -> T&&;

    template<class T, class Td = typename psl::decay<T>::type/*,
             class = typename psl::enable_if<is_reference_wrapper<Td>::value>::type*/>
    static auto get(T&& t) -> decltype(t.get());

    template<class T, class Td = typename psl::decay<T>::type,
             class = typename psl::enable_if<!psl::is_base_of<B, Td>::value>::type/*,
             class = typename psl::enable_if<!is_reference_wrapper<Td>::value>::type*/>
    static auto get(T&& t) -> decltype(*forward<T>(t));

    template<class T, class... Args, class MT1,
             class = typename psl::enable_if<psl::is_function<MT1>::value>::type>
    static auto call(MT1 B::*pmf, T&& t, Args&&... args)
        -> decltype((invoke_impl::get(
                         forward<T>(t)).*pmf)(forward<Args>(args)...));

    template<class T>
    static auto call(MT B::*pmd, T&& t)
        -> decltype(invoke_impl::get(forward<T>(t)).*pmd);
};

template<class F, class... Args, class Fd = typename psl::decay<F>::type>
auto INVOKE(F&& f, Args&&... args)
    -> decltype(invoke_impl<Fd>::call(forward<F>(f),
                                      forward<Args>(args)...));
} // namespace detail

// Minimal C++11 implementation:
// template<class> struct result_of;
// template<class F, class... ArgTypes>
// struct result_of<F(ArgTypes...)>
// {
//     using type = decltype(detail::INVOKE(psl::declval<F>(), psl::declval<ArgTypes>()...));
// };

// Conforming C++14 implementation (is also a valid C++11 implementation):
namespace detail
{
template<typename AlwaysVoid, typename, typename...>
struct invoke_result {};
template<typename F, typename...Args>
struct invoke_result<
    decltype(void(detail::INVOKE(psl::declval<F>(), psl::declval<Args>()...))),
    F, Args...>
{
    using type = decltype(detail::INVOKE(psl::declval<F>(), psl::declval<Args>()...));
};
} // namespace detail

template<class> struct result_of;
template<class F, class... ArgTypes>
struct result_of<F(ArgTypes...)> : detail::invoke_result<void, F, ArgTypes...> {};

template<class F, class... ArgTypes>
struct invoke_result : detail::invoke_result<void, F, ArgTypes...> {};

template< class F, class... ArgTypes >
using invoke_result_t = typename invoke_result<F, ArgTypes...>::type;

}
