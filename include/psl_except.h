#pragma once

#include "psl_string.h"

#define PSL_DEFINE_EXCEPTION(name, base)        \
    class name : public base                    \
    {                                           \
    public:                                     \
        using base::base;                       \
    }


namespace psl
{

[[noreturn]] void terminate() noexcept;

class exception
{
public:
    exception();
    explicit exception(const psl::string &what);
    virtual ~exception() = default;

    const psl::string &what() const;

protected:
    psl::string m_what;
    //bool m_caught = false;
};


PSL_DEFINE_EXCEPTION(logic_error, exception);
PSL_DEFINE_EXCEPTION(out_of_range, logic_error);
PSL_DEFINE_EXCEPTION(runtime_error, exception);
PSL_DEFINE_EXCEPTION(system_error, runtime_error);

}
