#pragma once

#include "psl_string.h"


namespace psl
{
namespace filesystem
{

class path
{
public:
    using value_type = char;
    using string_type = psl::basic_string<value_type>;
    using const_iterator = char;
    using iterator = char;

    enum format { native_format, generic_format, auto_format };

    static constexpr value_type preferred_separator = '/';

    path() noexcept;
    path(const path &p);
    path(path &&p) noexcept;
    path(string_type &&source, format fmt = auto_format);
    template <class Source>
    path(const Source &source, format fmt = auto_format)
        : m_str(source)
    {(void)fmt;}

    const value_type *c_str() const noexcept;
    const string_type &native() const noexcept;
    operator string_type() const;

    void clear();

    path root_name() const;
    path root_directory() const;
    path root_path() const;
    path relative_path() const;
    path parent_path() const;
    path filename() const;
    path stem() const;
    path extension() const;

    bool is_absolute() const;
    bool is_relative() const;

    friend path operator/(const path &lhs, const path &rhs);

private:
    string_type m_str;

};

}
}
