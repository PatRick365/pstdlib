#pragma once

#include "psl_optional.h"
#include "psl_string_view.h"
#include "psl_vector.h"
#include "psl_string.h"

namespace psl
{

class backtrace
{
public:
    backtrace();
    ~backtrace();

    backtrace(const backtrace &) = delete;
    backtrace(backtrace &&) = delete;
    backtrace &operator=(const backtrace &) = delete;
    backtrace &operator=(backtrace &&) = delete;

    psl::vector<psl::string_view> frames() const;

private:
    char **m_strs = nullptr;
    int m_frames = 0;
    //psl::vector<string_view> m_frames;
};

psl::optional<psl::string> demangle(const string &name);
}

