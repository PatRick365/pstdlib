#pragma once

#include "psl_iodevice.h"
#include "psl_memory.h"
#include "psl_string.h"


namespace psl
{

class tcp_client_impl;
class tcp_client : public psl::io_device
{
public:
    tcp_client();
    ~tcp_client();

    bool is_open() const override;

    bool open(const psl::string &hostname, int port);
    bool close() override;

protected:
    bool write_intern(const uint8_t *data, size_t len) override;
    ssize_t read_intern(uint8_t *data, size_t len) override;

private:
    psl::unique_ptr<tcp_client_impl> m_impl;
};

}
