﻿#pragma once

#include "psl_string.h"
#include "psl_time.h"
#include "psl_units.h"
#include "psl_except.h"
#include "psl_functional.h"
#include "psl_mutex.h"
#include "psl_format.h"

#include <stdio.h>
#include <pthread.h>


namespace psl
{

class thread_error : public runtime_error
{
public:
    using runtime_error::runtime_error;
};


class condition_variable
{
public:
    condition_variable();
    condition_variable(const condition_variable &) = delete;

    void notify_one() noexcept;
    void notify_all() noexcept;

    void wait(psl::mutex &lock);
    template<typename Predicate>
    void wait(psl::mutex &lock, Predicate pred)
    {
        while (!pred())
            wait(lock);
    }

private:
    pthread_cond_t m_condition;
};

class barrier
{
public:
    explicit barrier(size_t num_threads/*, CompletionFunction f = CompletionFunction()*/);
    barrier(const barrier &) = delete;
    ~barrier();

    void wait();
private:
    pthread_barrier_t m_barrier{};
};

class thread
{
public:
    class id
    {
        friend class thread;
    public:
        id() noexcept = default;
        explicit id(pthread_t handle)
            : m_handle(handle)
        {}

        bool operator==(psl::thread::id rhs) const noexcept
        {
            return pthread_equal(m_handle, rhs.m_handle) != 0;
        }
        bool operator!=(psl::thread::id rhs) const noexcept
        {
            return pthread_equal(m_handle, rhs.m_handle) == 0;
        }

    private:
        pthread_t m_handle = 0;
    };

    thread() = default;

    thread(const thread &) = delete;
    thread& operator=(const thread &) = delete;

    thread(thread &&other);
    thread &operator=(thread &&other);

    template<class Function, class ...Args>
    thread(Function &&fn, Args &&...args)
    {
        psl::barrier barrier(2);

        psl::function<void()> *fn_capture = new psl::function<void()>(
            [&barrier,
             fn = psl::move(psl::forward<Function>(fn)),
             ... args = psl::forward<Args>(args)]() mutable
            {
                barrier.wait();
                fn(psl::forward<Args>(args)...);
            });

        auto start_routine = [](void *arg) -> void *
        {
            psl::function<void()> *fn_capture = reinterpret_cast<psl::function<void()> *>(arg);
            (*fn_capture)();
            delete fn_capture;
            return nullptr;
        };

        if (int ret = ::pthread_create(&m_id.m_handle, nullptr, start_routine, fn_capture))
        {
            psl::print(stderr, "pthread_create has failed. error {}: {}\n", ret, ::strerror(ret));
            return;
        }

        barrier.wait();
    }

    ~thread();

    bool join();
    bool try_join();

    bool joinable() const;

    pthread_t native_handle() const;
    id get_id() const;

    void detach();
private:
    id m_id;
};

namespace this_thread
{

template<time_t TimeDim>
void sleep_for(const psl::time<TimeDim> &sleep_duration)
{
    if (sleep_duration.get() < 0)
        throw psl::logic_error("sleep_for does not support negative time.");

    psl::time<time_t::s> s = sleep_duration.template to_unit<time_t::s>();
    long n_sec = s.get();

    psl::time<time_t::ns> ns = sleep_duration.template to_unit<time_t::ns>() -  psl::time<time_t::s>(n_sec);

    struct timespec ts {n_sec, long(ns.get())};

    ::nanosleep(&ts, nullptr);
}

inline void sleep_until(const psl::time_point &sleep_time)
{
    psl::time<psl::time_t::s> sleep_duration = sleep_time - psl::time_point::now();
    sleep_for(sleep_duration);
}

inline thread::id get_id() noexcept
{
    return thread::id{pthread_self()};
}
}

}
