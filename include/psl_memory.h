#pragma once

#include "psl_utility.h"
#include "psl_new.h"
#include "psl_type_traits.h"
#include "psl_mutex.h"

#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <cassert>


namespace psl
{

template<class T>
struct default_delete
{
    constexpr default_delete() noexcept = default;

    template<class U>
    default_delete(const default_delete<U> &/*d*/) noexcept
    {}

    void operator()(T *ptr) const
    {
        delete ptr;
    }

    template<class U>
    void operator()(U *ptr) const
    {
        delete ptr;
    }
};

template<typename T, typename Deleter = psl::default_delete<T>>
class unique_ptr
{
public:
    using element_type = T;
    using pointer = element_type *;
    using deleter_type = Deleter;


    unique_ptr(unique_ptr &) = delete;
    unique_ptr &operator=(unique_ptr &) = delete;

    unique_ptr() noexcept
        : m_ptr(nullptr)
    {}

    constexpr unique_ptr(psl::nullptr_t) noexcept
        : m_ptr(nullptr)
    {}

    explicit unique_ptr(pointer p) noexcept
        : m_ptr(p)
    {}

    unique_ptr(unique_ptr &&other) noexcept
        : m_ptr(other.release()),
          m_deleter(other.get_deleter())
    {}

    unique_ptr &operator=(unique_ptr &&other) noexcept
    {
        get_deleter()(get());
        m_ptr = other.release();
        return *this;
    }

    template <class U, class E>
    unique_ptr(unique_ptr<U, E> &&other) noexcept
        : m_ptr(other.release()),
          m_deleter(other.get_deleter())
    {}

//    template <typename A, psl::enable_if_t<std::is_copy_constructible_v<A>> = true>
//    unique_ptr(pointer p, const A &d)
//    {
//    }

    ~unique_ptr()
    {
        get_deleter()(get());
    }

    T &operator=(T *ptr)
    {
        get_deleter()(get());
        m_ptr = ptr;
        return *ptr;
    }

    typename psl::add_lvalue_reference<T>::type operator*() const noexcept(noexcept(*psl::declval<pointer>()))
    {
        return *m_ptr;
    }

    T *operator->() const noexcept
    {
        return m_ptr;
    }

    operator bool() const
    {
        return (m_ptr != nullptr);
    }

    friend bool operator==(const unique_ptr& lhs, const unique_ptr& rhs)
    {
        return lhs.m_ptr == rhs.m_ptr;
    }
    friend bool operator!=(const unique_ptr& lhs, const unique_ptr& rhs)
    {
        return !(lhs.m_ptr == rhs.m_ptr);
    }

    friend bool operator==(const unique_ptr& lhs, const T *rhs)
    {
        return (lhs.m_ptr == rhs);
    }
    friend bool operator!=(const unique_ptr& lhs, const T *rhs)
    {
        return !(lhs.m_ptr == rhs);
    }

    T *get() const
    {
        return m_ptr;
    }

    Deleter& get_deleter() noexcept
    {
        return m_deleter;
    }

    const Deleter& get_deleter() const noexcept
    {
        return m_deleter;
    }

    T *release()
    {
        T *ptr = m_ptr;
        m_ptr = nullptr;
        return ptr;
    }

    void reset(pointer ptr = pointer())
    {
        get_deleter()(get());
        m_ptr = ptr;
    }

    void swap(unique_ptr &other) noexcept
    {
        psl::swap(m_ptr, other.m_ptr);
    }

private:
    T *m_ptr;
    deleter_type m_deleter;
};

template<typename T, typename D>
void swap(psl::unique_ptr<T, D> &lhs, psl::unique_ptr<T, D> &rhs) noexcept
{
    lhs.swap(rhs);
}

namespace internal
{

struct reference_count
{
    long m_count = 1;
    psl::mutex m_mut;
};

}


template <typename T>
class shared_ptr
{
    template <typename Y> friend class shared_ptr;
public:
    using element_type = T;
    using pointer = element_type *;
    using deleter_type = psl::default_delete<T>;

    shared_ptr() noexcept = default;

    constexpr shared_ptr(psl::nullptr_t) noexcept {}

    template<class Y>
    explicit shared_ptr(Y *ptr)
        : m_ptr(ptr)
    {
        if (m_ptr)
        {
            make_reference_count();
        }
    }

    template<class Y, class Deleter>
    shared_ptr(Y *ptr, Deleter d)
        : m_ptr(ptr),
          m_deleter(d)
    {
        if (m_ptr)
        {
            make_reference_count();
        }
    }

    template<class Deleter>
    shared_ptr(psl::nullptr_t ptr, Deleter d)
        : m_ptr(ptr),
          m_deleter(d)
    {}

    template<class Y, class Deleter, class Alloc>
    shared_ptr(Y *ptr, Deleter d, Alloc alloc);

    template<class Deleter, class Alloc>
    shared_ptr(psl::nullptr_t ptr, Deleter d, Alloc alloc);

    template<class Y>
    shared_ptr(const shared_ptr<Y> &r, element_type *ptr) noexcept;

    template<class Y>
    shared_ptr(shared_ptr<Y> &&r, element_type *ptr) noexcept;

    shared_ptr(const shared_ptr &r)
    {
        if (r && &r != this)
        {
            psl::region_lock lock(r.m_reference_count->m_mut);
            m_ptr = r.m_ptr;
            m_reference_count = r.m_reference_count;
            ++m_reference_count->m_count;
        }
    }

    template<class Y>
    shared_ptr(const shared_ptr<Y> &r) noexcept
    {
        if (r)
        {
            psl::region_lock lock(r.m_reference_count->m_mut);
            m_ptr = static_cast<element_type *>(r.get());
            m_reference_count = r.m_reference_count;
            ++m_reference_count->m_count;
        }
    }

    shared_ptr(shared_ptr &&r)
    {
        if (r && &r != this)
        {
            psl::region_lock lock(r.m_reference_count->m_mut);
            m_ptr = r.m_ptr;
            r.m_ptr = nullptr;

            m_reference_count = r.m_reference_count;
            r.m_reference_count = nullptr;
        }
    }

    template<class Y>
    shared_ptr(shared_ptr<Y> &&r) noexcept
    {
        if (r)
        {
            psl::region_lock lock(r.m_reference_count->m_mut);
            m_ptr = static_cast<element_type *>(r.get());
            r.m_ptr = nullptr;

            m_reference_count = r.m_reference_count;
            r.m_reference_count = nullptr;
        }
    }

    // template<class Y>
    // explicit shared_ptr(const std::weak_ptr<Y> &r);

    template<class Y, class Deleter>
    shared_ptr(psl::unique_ptr<Y, Deleter> &&r)
        : shared_ptr(r.release(), r.get_deleter())
    {}

    ~shared_ptr()
    {
        destroy();
    }

    shared_ptr &operator=(const shared_ptr &r) noexcept
    {
        destroy();
        if (r)
        {
            psl::region_lock lock(r.m_reference_count->m_mut);
            m_ptr = r.m_ptr;
            m_reference_count = r.m_reference_count;
            ++m_reference_count->m_count;
        }
        return *this;
    }

    template<class Y>
    shared_ptr &operator=(const shared_ptr<Y> &r) noexcept
    {
        destroy();
        if (r)
        {
            psl::region_lock lock(r.m_reference_count->m_mut);
            m_ptr = static_cast<element_type *>(r.get());
            m_reference_count = r.m_reference_count;
            ++m_reference_count->m_count;
        }
        return *this;
    }

    shared_ptr &operator=(shared_ptr &&r) noexcept
    {
        destroy();
        if (r && &r != this)
        {
            psl::region_lock lock(r.m_reference_count->m_mut);
            m_ptr = r.m_ptr;
            m_reference_count = r.m_reference_count;
            r.m_ptr = nullptr;
            r.m_reference_count = nullptr;
        }
        return *this;
    }

    template<class Y>
    shared_ptr &operator=(shared_ptr<Y> &&r) noexcept
    {
        destroy();
        if (r)
        {
            psl::region_lock lock(r.m_reference_count->m_mut);
            m_ptr = static_cast<element_type *>(r.get());
            m_reference_count = r.m_reference_count;
            r.m_ptr = nullptr;
            r.m_reference_count = nullptr;
        }
        return *this;
    }

    template<class Y, class Deleter>
    shared_ptr &operator=(psl::unique_ptr<Y, Deleter> &&r)
    {
        destroy();
        if (r)
        {
            make_reference_count();
            m_ptr = r.release();
        }
        return *this;
    }

    T *operator->() noexcept
    {
        return m_ptr;
    }

    T &operator*() noexcept
    {
        return *m_ptr;
    }

    operator bool() const
    {
        return (m_ptr != nullptr);
    }

    friend bool operator==(const shared_ptr& lhs, const shared_ptr& rhs)
    {
        return lhs.m_ptr == rhs.m_ptr;
    }
    friend bool operator!=(const shared_ptr& lhs, const shared_ptr& rhs)
    {
        return !(lhs.m_ptr == rhs.m_ptr);
    }

    friend bool operator==(const shared_ptr& lhs, const T *rhs)
    {
        return lhs.m_ptr == rhs;
    }
    friend bool operator!=(const shared_ptr& lhs, const T *rhs)
    {
        return !(lhs.m_ptr == rhs);
    }

    element_type *get() const noexcept
    {
        return m_ptr;
    }

    T *release()
    {
        if (!m_ptr)
        {
            return nullptr;
        }

        T *ptr = m_ptr;
        m_ptr = nullptr;

        dec_reference_count();

        return ptr;
    }

    void reset() noexcept
    {
        destroy();
    }

    template<class Y>
    void reset(Y *ptr)
    {
        destroy();
        if (ptr)
        {
            make_reference_count();
            m_ptr = static_cast<element_type *>(ptr);
        }
    }

    template<class Y, class Deleter>
    void reset(Y *ptr, Deleter d)
    {
        reset(ptr);
        m_deleter = d;
    }

    template<class Y, class Deleter, class Alloc>
    void reset(Y *ptr, Deleter d, Alloc alloc);

    void swap(shared_ptr &r) noexcept
    {
        ///@todo thread safety?
        psl::swap(m_ptr, r.m_ptr);
        psl::swap(m_reference_count, r.m_reference_count);
        psl::swap(m_deleter, r.m_deleter);
    }

    long use_count() const noexcept
    {
        if (m_reference_count)
        {
            psl::region_lock lock(m_reference_count->m_mut);
            return m_reference_count->m_count;
        }
        return 0;
    }

private:
    void make_reference_count()
    {
        assert(m_reference_count == nullptr);
        m_reference_count = new internal::reference_count;
    }

    void dec_reference_count()
    {        
        assert(m_reference_count);

        long reference_count = 0;
        {
            psl::region_lock lock(m_reference_count->m_mut);
            reference_count = --m_reference_count->m_count;
        }

        assert(reference_count >= 0);

        if (reference_count <= 0)
        {
            delete m_reference_count;
            m_reference_count = nullptr;
            m_deleter(m_ptr);
            m_ptr = nullptr;
        }
    }

    void destroy()
    {
        if (m_ptr)
        {
            dec_reference_count();
        }
        m_ptr = nullptr;
        m_reference_count = nullptr;
    }

private:
    T *m_ptr = nullptr;
    deleter_type m_deleter;
    internal::reference_count *m_reference_count = nullptr;
};





//template<typename T, typename... Args>
//pstd::unique_ptr<T> make_unique(Args&&... args)
//{
//    return pstd::unique_ptr<T>(new T(pstd::forward<Args>(args)...));
//}

template<class T> struct _Unique_if {
    typedef psl::unique_ptr<T> _Single_object;
};

//template<class T> struct _Unique_if<T[]> {
//    typedef unique_ptr<T[]> _Unknown_bound;
//};

//template<class T, size_t N> struct _Unique_if<T[N]> {
//    typedef void _Known_bound;
//};

/// disabled
template<class T, class... Args>
    typename _Unique_if<T>::_Single_object
    make_unique(Args&&... args) {
        return psl::unique_ptr<T>(new T(psl::forward<Args>(args)...));
    }
template<class T, class... Args>
    shared_ptr<T>
    make_shared(Args&&... args) {
        return psl::shared_ptr<T>(new T(psl::forward<Args>(args)...));
    }



//#include <utility>
//template<class T, class... Args>
//    typename _Unique_if<T>::_Single_object
//    make_unique(Args&&... args) {
//        return pstd::unique_ptr<T>(new T(std::forward<Args>(args)...));
//    }


//template<class T>
//    typename _Unique_if<T>::_Unknown_bound
//    make_unique(size_t n) {
//        typedef typename remove_extent<T>::type U;
//        return unique_ptr<T>(new U[n]());
//    }

template<class T, class... Args>
    typename _Unique_if<T>::_Known_bound
    make_unique(Args&&...) = delete;

//#include <memory>

template<class Alloc>
struct allocator_traits
{
    using allocator_type = Alloc;
    using value_type = Alloc::value_type;
    using pointer = Alloc::pointer;
    using const_pointer = Alloc::const_pointer;
    using void_pointer = Alloc::void_pointer;
    using const_void_pointer = Alloc::const_void_pointer;
    using difference_type = Alloc::difference_type;
    using size_type = Alloc::size_type;
//    using propagate_on_container_copy_assignment = Alloc::propagate_on_container_copy_assignment;// if present, otherwise std::false_type
//    using propagate_on_container_move_assignment = Alloc::propagate_on_container_move_assignment;// if present, otherwise std::false_type
//    using propagate_on_container_swap = Alloc::propagate_on_container_swap;// if present, otherwise std::false_type
//    using is_always_equal = Alloc::is_always_equal;// if present, otherwise std::is_empty<Alloc>::type


    static pointer allocate(Alloc &a, size_type n)
    {
        return a.allocate(n);
    }

    static void deallocate(Alloc& a, pointer p, size_type n)
    {
        return a.deallocate(p, n);
    }
};

template<class T>
struct allocator
{
    using value_type = T;
    using pointer = T *;
    using const_pointer = const T *;
    using void_pointer = void *;
    using const_void_pointer = const void *;
    using difference_type = long int;
    using size_type = size_t;

    pointer allocate(size_t n)
    {
        return static_cast<pointer>(malloc(sizeof(value_type) * n));
    }

    void deallocate(pointer p, size_t n)
    {
        (void)n;
        free(p);
    }
};


template<class T, class... Args>
T* construct_at(T *p, Args &&...args)
{
    return new (const_cast<void *>(static_cast<const volatile void *>(p))) T(psl::forward<Args>(args)...);
}

template<class T>
void destroy_at(T* p)
{
    p->~T();
}

template<typename T>
typename psl::enable_if<psl::is_object<T>::value, T*>::type  addressof(T& arg) noexcept
{
    return reinterpret_cast<T*>(
               &const_cast<char&>(
                   reinterpret_cast<const volatile char&>(arg)));
}

template<typename T>
typename psl::enable_if<!psl::is_object<T>::value, T*>::type addressof(T& arg) noexcept
{
    return &arg;
}

}


