#pragma once

#include "psl_iodevice.h"
#include "psl_string.h"


namespace psl
{

class string_builder : public o_device
{
public:
    string_builder();

    bool is_open() const override;
    bool close() override;

    const psl::string &get_string() const;
    psl::string &get_string();

private:
    bool write_intern(const uint8_t *data, size_t len) override;

private:
    psl::string m_str;
};

}
