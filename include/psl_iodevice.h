#pragma once

#include "psl_atomic.h"
#include "psl_except.h"
#include "psl_optional.h"
#include "psl_string.h"
#include "psl_thread.h"
#include "psl_vector.h"

#include <cstddef>


namespace psl
{

PSL_DEFINE_EXCEPTION(io_device_error, runtime_error);
PSL_DEFINE_EXCEPTION(io_device_write_error, io_device_error);
PSL_DEFINE_EXCEPTION(io_device_read_error, io_device_error);

enum class open_mode : int
{
    create = 0100,
    read_only = 00,
    write_only = create | 01,
    read_write = create | 02
};

class i_device
{
public:
    i_device() = default;
    virtual ~i_device() = default;

    i_device(const i_device &) = delete;
    i_device &operator=(const i_device &) = delete;
    i_device(i_device &&) = delete;
    i_device &operator=(i_device &&) = delete;

    virtual bool is_open() const = 0;
    virtual bool close() = 0;

    ssize_t read(uint8_t *data, size_t len);
    ssize_t read(char *data, size_t len);
    bool read(psl::vector<uint8_t> &data, size_t len);
    bool read(psl::string &data, size_t len);
    bool read_all(psl::vector<char> &data);
    bool read_all(psl::string &data);
    bool read_line(psl::string &data);

    virtual psl::optional<psl::string> read_all();

    psl::i_device &operator>>(psl::vector<char> &data)
    {
        if (!read_all(data))
            throw io_device_read_error("failed to read from input device...");
        return *this;
    }
    psl::i_device &operator>>(psl::string &data)
    {
        if (!read_all(data))
            throw io_device_read_error("failed to read from input device...");
        return *this;
    }

protected:
    virtual ssize_t read_intern(uint8_t *data, size_t len) = 0;
};


class o_device
{
public:
    o_device() = default;
    virtual ~o_device() = default;

    o_device(const o_device &) = delete;
    o_device &operator=(const o_device &) = delete;
    o_device(o_device &&) = delete;
    o_device &operator=(o_device &&) = delete;

    virtual bool is_open() const = 0;
    virtual bool close() = 0;

    bool write(const uint8_t *data, size_t len);
    bool write(const char *str, size_t len);
    bool write(const char *str);
    bool write(const psl::vector<uint8_t> &data);
    bool write(const psl::string &data);

    // untested
    template<size_t S>
    psl::o_device &operator<<(const char data[S])
    {
        if (!write(data, S))
            throw io_device_write_error("failed to write to output device...");
        return *this;
    }
    template<size_t S>
    psl::o_device &operator<<(const uint8_t data[S])
    {
        if (!write(data, S))
            throw io_device_write_error("failed to write to output device...");
        return *this;
    }

    psl::o_device &operator<<(const psl::vector<uint8_t> &data)
    {
        if (!write(data))
            throw io_device_write_error("failed to write to output device...");
        return *this;
    }
    psl::o_device &operator<<(const psl::string &data)
    {
        if (!write(data))
            throw io_device_write_error("failed to write to output device...");
        return *this;
    }
protected:
    virtual bool write_intern(const uint8_t *data, size_t len) = 0;
};

class io_device : public i_device, public o_device
{};

// does not work. segaults on write, also on mutex take of the atomic...
class io_pipe
{
public:
    io_pipe() = default;
    io_pipe(i_device *from, o_device *to);
    ~io_pipe();

    void stop();

private:
    psl::thread m_thread;
    psl::atomic<bool> m_stopToken = false;
    psl::i_device *m_from = nullptr;
    psl::o_device *m_to = nullptr;
};

}
