#pragma once

#include "psl_string.h"
#include "psl_string_view.h"
#include "psl_except.h"

#include <cstring>
#include <unistd.h>
#include <cstdio>


namespace psl
{

PSL_DEFINE_EXCEPTION(format_error, runtime_error);

template<typename T> struct formatter;

class format_writer
{
public:
    template<typename String>
    explicit format_writer(const String &fmt)
        : m_fmt(psl::move(fmt))
    {}

    template<typename T>
    void write(const T &value)
    {
        formatter<T>::write(*this, value);
    }

    template<typename... Args>
    bool sprintf(const char *fmt, Args&&... args)
    {
        // get length without null terminator
        int len = ::snprintf(0, 0, fmt, psl::forward<Args>(args)...);
        if (len <= 0)
        {
            return false;
        }

        ssize_t move_len = m_fmt.length() - m_pos - m_chars_to_overwrite;
        ssize_t new_size = m_fmt.length() + len - m_chars_to_overwrite;

        ssize_t move_from = m_pos + m_chars_to_overwrite;
        ssize_t move_to = m_pos + len;

        if (new_size > m_fmt.length())
            m_fmt.resize(new_size);

        ::memmove(m_fmt.data() + move_to, m_fmt.data() + move_from, move_len);

        {
            char char_overwritten_by_null = m_fmt[move_to];
            ::snprintf(m_fmt.data() + m_pos, len + 1U, fmt, psl::forward<Args>(args)...);
            m_fmt[move_to] = char_overwritten_by_null;
        }

        if (new_size < m_fmt.length())
            m_fmt.resize(new_size);

        m_chars_to_overwrite = psl::max(ssize_t(0), m_chars_to_overwrite - ssize_t(len * 2));
        m_pos += len;
        return true;
    }

    void write(const char *str, size_t len)
    {
        ssize_t move_len = m_fmt.length() - m_pos - m_chars_to_overwrite;
        ssize_t new_size = m_fmt.length() + len - m_chars_to_overwrite;

        ssize_t move_from = m_pos + m_chars_to_overwrite;
        ssize_t move_to = m_pos + len;

        if (new_size > m_fmt.length())
            m_fmt.resize(new_size);

        ::memmove(m_fmt.data() + move_to, m_fmt.data() + move_from, move_len);

        ::memcpy(m_fmt.data() + m_pos, str, len);

        if (new_size < m_fmt.length())
            m_fmt.resize(new_size);

        m_chars_to_overwrite = psl::max(ssize_t(0), m_chars_to_overwrite - ssize_t(len * 2));
        m_pos += len;
    }

protected:
    psl::string m_fmt;
    ssize_t m_pos = 0;
    ssize_t m_chars_to_overwrite = 0;
};

namespace internal
{

class format_writer_accessor : public format_writer
{
public:
    using format_writer::format_writer;

    ssize_t advance()
    {
        m_chars_to_overwrite = 2;
        m_pos = m_fmt.find("{}", 0);
        return m_pos;
    }

    psl::string &str() { return m_fmt; }
};

}

#define GEN_FORMATTER(type, fmt) \
    template<> struct formatter<type> { static bool write(format_writer &writer, type value) { return writer.sprintf(fmt, value); }}
GEN_FORMATTER(char, "%c");
GEN_FORMATTER(unsigned char, "%c");
GEN_FORMATTER(int, "%d");
GEN_FORMATTER(unsigned int, "%u");
GEN_FORMATTER(long, "%ld");
GEN_FORMATTER(unsigned long, "%lu");
GEN_FORMATTER(long long, "%lld");
GEN_FORMATTER(unsigned long long, "%llu");
GEN_FORMATTER(float, "%f");
GEN_FORMATTER(double, "%f");
GEN_FORMATTER(long double, "%Lf");
#undef GEN_FORMATTER

template<> struct formatter<bool> { static bool write(format_writer &writer, bool value) { value ? writer.write("true", 4) : writer.write("false", 5); return true; }};
template<> struct formatter<char *> { static bool write(format_writer &writer, const char *value) { writer.write(value, psl::strlen(value)); return true; }};
template<> struct formatter<const char *> { static bool write(format_writer &writer, const char *value) { writer.write(value, psl::strlen(value)); return true; }};
template<> struct formatter<psl::string_view> { static bool write(format_writer &writer, const psl::string_view &value) { writer.write(value.data(), value.length()); return true; }};
template<> struct formatter<psl::string> { static bool write(format_writer &writer, const psl::string &value) { writer.write(value.c_str(), value.length()); return true; }};

template<size_t S>
struct formatter<char[S]>
{
    static bool write(format_writer &writer, const char value[S])
    {
        size_t len = (value[S - 1] == '\0') ? (S - 1) : S;    // adjust for null term
        writer.write(value, len);
        return true;
    }
};

namespace internal
{

[[maybe_unused]] static void replace(format_writer_accessor &writer)
{
    if (writer.advance() != psl::string::npos)
    {
        throw format_error("not enough format arguments");
    }
}

template<typename T, typename ...Args>
static void replace(format_writer_accessor &writer, const T &first, Args &&...args)
{
    if (writer.advance() == psl::string::npos)
    {
        throw format_error("too many format arguments");
    }

    formatter<T>::write(writer, first);

    return replace(writer, psl::forward<Args>(args)...);
}

}

template<typename ...Args>
psl::string format(const char *fmt, Args &&...args)
{
    internal::format_writer_accessor writer(fmt);
    internal::replace(writer, psl::forward<Args>(args)...);
    return psl::move(writer.str());
}
template<typename ...Args>
psl::string format(const psl::string_view &fmt, Args &&...args)
{
    internal::format_writer_accessor writer(psl::string(fmt.data(), fmt.length()));
    internal::replace(writer, psl::forward<Args>(args)...);
    return psl::move(writer.str());
}
template<typename ...Args>
psl::string format(const psl::string &fmt, Args &&...args)
{
    internal::format_writer_accessor writer(fmt);
    internal::replace(writer, psl::forward<Args>(args)...);
    return psl::move(writer.str());
}

/// print
inline void print(const char *fmt) { ::fputs(fmt, stdout); }
inline void print(const psl::string_view &fmt) { ::fprintf(stdout, "%.*s", int(fmt.length()), fmt.data()); }
inline void print(const psl::string &fmt) { ::fputs(fmt.c_str(), stdout); }
template<typename ...Args>
void print(const char *fmt, Args &&...args) { ::fputs(psl::format(fmt, psl::forward<Args>(args)...).c_str(), stdout); }
template<typename ...Args>
void print(const psl::string_view &fmt, Args &&...args) { ::fputs(psl::format(fmt, psl::forward<Args>(args)...).c_str(), stdout); }
template<typename ...Args>
void print(const psl::string &fmt, Args &&...args) { ::fputs(psl::format(fmt, psl::forward<Args>(args)...).c_str(), stdout); }
/// print FILE
inline void print(FILE *file, const char *fmt) { ::fputs(fmt, file); }
inline void print(FILE *file, const psl::string_view &fmt) { ::fprintf(file, "%.*s", int(fmt.length()), fmt.data()); }
inline void print(FILE *file, const psl::string &fmt) { ::fputs(fmt.c_str(), file); }
template<typename ...Args>
void print(FILE *file, const char *fmt, Args &&...args) { ::fputs(psl::format(fmt, psl::forward<Args>(args)...).c_str(), file); }
template<typename ...Args>
void print(FILE *file, const psl::string_view &fmt, Args &&...args) { ::fputs(psl::format(fmt, psl::forward<Args>(args)...).c_str(), file); }
template<typename ...Args>
void print(FILE *file, const psl::string &fmt, Args &&...args) { ::fputs(psl::format(fmt, psl::forward<Args>(args)...).c_str(), file); }

/// println
inline void println(const char *fmt) { ::puts(fmt); }
inline void println(const psl::string_view &fmt) { ::printf("%.*s\n", int(fmt.length()), fmt.data()); }
inline void println(const psl::string &fmt) { ::puts(fmt.c_str()); }
template<typename ...Args>
void println(const char *fmt, Args &&...args) { ::puts(psl::format(fmt, psl::forward<Args>(args)...).c_str()); }
template<typename ...Args>
void println(const psl::string_view &fmt, Args &&...args) { ::puts(psl::format(fmt, psl::forward<Args>(args)...).c_str()); }
template<typename ...Args>
void println(const psl::string &fmt, Args &&...args) { ::puts(psl::format(fmt, psl::forward<Args>(args)...).c_str()); }

enum class color
{
    red,
    green,
    yellow,
    blue,
    magenta,
    cyan,
    white,
    black
};

template<typename ...Args>
void print(color col, const psl::string_view &fmt, Args &&...args)
{
    const char *str_color = nullptr;

    switch (col)
    {
    case color::red:
        str_color = "\x1B[31m"; break;
    case color::green:
        str_color = "\x1B[32m"; break;
    case color::yellow:
        str_color = "\x1B[33m"; break;
    case color::blue:
        str_color = "\x1B[34m"; break;
    case color::magenta:
        str_color = "\x1B[35m"; break;
    case color::cyan:
        str_color = "\x1B[36m"; break;
    case color::white:
        str_color = "\x1B[37m"; break;
    case color::black:
        str_color = "\x1B[30m"; break;
    }

    psl::string str = psl::format(fmt, psl::forward<Args>(args)...);
    psl::string str_colored = psl::format("{}{}{}", str_color, str, "\x1B[0m");
    fputs(str_colored.c_str(), stdout);

}
template<typename ...Args>
void print(color col, const char *fmt, Args &&...args)
{
    print(col, psl::string_view(fmt, psl::strlen(fmt)), psl::forward<Args>(args)...);
}
template<typename ...Args>
void print(color col, const psl::string &fmt, Args &&...args)
{
    print(col, psl::string_view(fmt.c_str(), fmt.length()), psl::forward<Args>(args)...);
}

/// @todo add other versions of print
}
