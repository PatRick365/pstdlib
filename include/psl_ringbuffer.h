#pragma once

#include "psl_array.h"
#include "psl_iodevice.h"


namespace psl
{

template<size_t S>
class ringbuffer : public psl::io_device
{
public:
    bool is_open() const override { return true; }
    bool close() override { return true; }

    size_t available_to_read() const
    {
        return m_data_avail;
    }

    size_t available_to_write() const
    {
        return S - m_data_avail;
    }

protected:
    bool write_intern(const uint8_t *data, size_t len) override
    {
        size_t avail = available_to_write();

        if (avail < len)
        {
            //psl::print(stderr, "ringbuffer tried to write {} bytes but only {} available.\n", len, avail);
            return false;
        }

        for (size_t i = 0; i < len; ++i)
        {
            size_t pos = (m_pos_read + m_data_avail + i) % S;
            m_data[pos] = data[i];
        }

        m_data_avail += len;

        return true;
    }
    ssize_t read_intern(uint8_t *data, size_t len) override
    {
        size_t avail = available_to_read();

        len = psl::min(len, avail);

        for (size_t i = 0; i < len; ++i)
        {
            size_t pos = (m_pos_read + i) % S;
            data[i] = m_data[pos];
        }

        m_pos_read = (m_pos_read + len + 0) % S;
        m_data_avail -= len;

        return len;
    }

private:
    psl::array<char, S> m_data;
    size_t m_pos_read = 0;
    size_t m_data_avail = 0;
};

}
