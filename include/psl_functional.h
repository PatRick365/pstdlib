#pragma once

#include "psl_utility.h"
#include "psl_memory.h"

#include <stdio.h>


namespace psl
{


template <typename T>
class function;

template <typename R, typename... Args>
class function<R(Args...)>
{
public:
    function() = default;

    // construct from any functor type
    template <typename Functor>
    function(Functor f)
            // specialize functions and erase their type info by casting
        : m_invoke_fn(reinterpret_cast<invoke_fn_t>(invoke_fn<Functor>)),
          m_construct_fn(reinterpret_cast<construct_fn_t>(construct_fn<Functor>)),
          m_destroy_fn(reinterpret_cast<destroy_fn_t>(destroy_fn<Functor>)),
          m_data_ptr(new char[sizeof(Functor)]),
          m_data_size(sizeof(Functor))
    {
        // copy the functor to internal storage
        m_construct_fn(m_data_ptr, reinterpret_cast<char*>(&f));
    }

    // copy constructor
    function(const function &other)
        : m_invoke_fn(other.m_invoke_fn),
          m_construct_fn(other.m_construct_fn),
          m_destroy_fn(other.m_destroy_fn),
          m_data_size(other.m_data_size)
    {
        if (m_invoke_fn)
        {
            // when the source is not a null function, copy its internal functor
            delete[] m_data_ptr;
            m_data_ptr = (new char[m_data_size]);
            m_construct_fn(m_data_ptr, other.m_data_ptr);
        }
    }

    function &operator=(const function &other)
    {
        if (&other == this)
            return *this;
        m_invoke_fn = other.m_invoke_fn;
        m_construct_fn = other.m_construct_fn;
        m_destroy_fn = other.m_destroy_fn;
        m_data_size = other.m_data_size;

        if (m_invoke_fn)
        {
            // when the source is not a null function, copy its internal functor
            delete[] m_data_ptr;
            m_data_ptr = (new char[m_data_size]);
            m_construct_fn(m_data_ptr, other.m_data_ptr);
        }
        return *this;
    }

    ~function()
    {
        if (m_data_ptr != nullptr)
        {
            m_destroy_fn(m_data_ptr);
            delete[] m_data_ptr;
        }
    }

    // other constructors, from nullptr, from function pointers

    R operator()(Args&&... args)
    {
        return m_invoke_fn(m_data_ptr, psl::forward<Args>(args)...);
    }

    explicit operator bool() const noexcept
    {
        return (m_data_ptr != nullptr);
    }
private:
    // function pointer types for the type-erasure behaviors
    // all these char* parameters are actually casted from some functor type
    typedef R (*invoke_fn_t)(char*, Args&&...);
    typedef void (*construct_fn_t)(char*, char*);
    typedef void (*destroy_fn_t)(char*);

    // type-aware generic functions for invoking
    // the specialization of these functions won't be capable with
    //   the above function pointer types, so we need some cast
    template <typename Functor>
    static R invoke_fn(Functor* fn, Args&&... args)
    {
        return (*fn)(psl::forward<Args>(args)...);
    }

    template <typename Functor>
    static void construct_fn(Functor* construct_dst, Functor* construct_src)
    {
        psl::construct_at(construct_dst, psl::move(*construct_src));
    }

    template <typename Functor>
    static void destroy_fn(Functor* f)
    {
        psl::destroy_at(f);
    }

private:
    // these pointers are storing behaviors
    invoke_fn_t m_invoke_fn = nullptr;
    construct_fn_t m_construct_fn = nullptr;
    destroy_fn_t m_destroy_fn = nullptr;

    // erase the type of any functor and store it into a char*
    // so the storage size should be obtained as well
    char *m_data_ptr = nullptr;
    size_t m_data_size = 0;

};

//template<typename R, typename F, typename ...Args >
//psl::function<R(Args...)> bind(F &&f, Args &&...args)
//{

//}

template <typename T = void>
struct equal_to
{
    constexpr bool operator()(const T &lhs, const T &rhs) const
    {
        return (lhs == rhs);
    }
};

namespace internal
{
template <class T>
struct scalar_hash
{
    size_t operator()(T v) const
    {
        union
        {
            T t;
            size_t a;
        } u;
        u.a = 0;
        u.t = v;
        return u.a;
    }
};
}

static_assert((sizeof(size_t) == sizeof(long long)), "(sizeof(size_t) != sizeof(long long)");

template<typename Key> struct hash;

template<> struct hash<bool> { size_t operator()(bool v) const { return size_t(v); } };
template<> struct hash<char> { size_t operator()(char v) const { return size_t(v); } };
template<> struct hash<signed char> { size_t operator()(signed char v) const { return size_t(v); } };
template<> struct hash<unsigned char> { size_t operator()(bool v) const { return size_t(v); } };
template<> struct hash<char8_t> { size_t operator()(char8_t v) const { return size_t(v); } };
template<> struct hash<char16_t> { size_t operator()(char16_t v) const { return size_t(v); } };
template<> struct hash<char32_t> { size_t operator()(char32_t v) const { return size_t(v); } };
template<> struct hash<wchar_t> { size_t operator()(wchar_t v) const { return size_t(v); } };
template<> struct hash<short> { size_t operator()(short v) const { return size_t(v); } };
template<> struct hash<unsigned short> { size_t operator()(unsigned short v) const { return size_t(v); } };
template<> struct hash<int> { size_t operator()(int v) const { return size_t(v); } };
template<> struct hash<unsigned int> { size_t operator()(unsigned int v) const { return size_t(v); } };
template<> struct hash<long> { size_t operator()(long v) const { return size_t(v); } };
template<> struct hash<long long> { size_t operator()(long long v) const { return size_t(v); } };
template<> struct hash<unsigned long> { size_t operator()(unsigned long v) const { return size_t(v); } };
template<> struct hash<unsigned long long> { size_t operator()(unsigned long long v) const { return size_t(v); } };
template<> struct hash<float> { size_t operator()(float v) const { if (v == 0) return 0; return internal::scalar_hash<float>()(v); } };
template<> struct hash<double> { size_t operator()(double v) const { if (v == 0) return 0; return internal::scalar_hash<double>()(v); } };
template<> struct hash<long double> { size_t operator()(long double v) const { if (v == 0) return 0; return internal::scalar_hash<long double>()(v); } };
template<> struct hash<psl::nullptr_t> { size_t operator()(psl::nullptr_t) const { return size_t(0); } };
template<typename T> struct hash<T *> { size_t operator()(const T *v) const { return size_t(v); } };

template<class T = void>
struct less
{
    constexpr bool operator()(const T &lhs, const T &rhs) const
    {
        return lhs < rhs; // assumes that the implementation handles pointer total order
    }
};
template<class T = void>
struct greater
{
    constexpr bool operator()(const T &lhs, const T &rhs) const
    {
        return lhs > rhs; // assumes that the implementation handles pointer total order
    }
};

}
