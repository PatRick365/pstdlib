#pragma once

#include "psl_filesystem.h"
#include "psl_memory.h"
#include "psl_string.h"
#include "psl_vector.h"
#include "psl_iodevice.h"


namespace psl
{

PSL_DEFINE_EXCEPTION(pml_error, runtime_error);
PSL_DEFINE_EXCEPTION(pml_type_error, pml_error);
PSL_DEFINE_EXCEPTION(pml_parse_error, pml_error);

enum class pml_node_type
{
    num,
    str,
    bool_,
    value_less,
};

using value_type_num = long double;
using value_type_str = psl::string;
using value_type_bool = bool;

class pml_node
{
public:
    using ptr = psl::unique_ptr<pml_node>;
    using raw_ptr = pml_node *;

    pml_node(const psl::string &name);
    explicit pml_node(pml_node_type type, const psl::string &name);
    virtual ~pml_node() = default;

    pml_node_type get_type() const;
    bool is(pml_node_type type) const;
    bool has_value() const;
    bool is_num() const;
    bool is_str() const;
    bool is_bool() const;

    value_type_num to_num() const;
    const value_type_str &to_str() const;
    value_type_bool to_bool() const;

    const psl::string &get_name() const { return m_name; }

    void push_back(pml_node::ptr node);

    size_t get_children_size() const;
    const psl::vector<pml_node::ptr> &get_children() const;
    psl::vector<pml_node::ptr> &get_children();

    raw_ptr get_child(const psl::string_view &name) const;
    raw_ptr get_child(size_t ix) const;

    raw_ptr operator[](size_t ix) const;
    raw_ptr operator[](const psl::string_view &name) const;

protected:
    pml_node_type m_type;
    psl::string m_name;
    psl::vector<pml_node::ptr> m_lst_children;
};

namespace pml
{

pml_node::ptr parse(i_device &i_device);
pml_node::ptr parse(const psl::filesystem::path &path);
pml_node::ptr parse(const psl::string &data);
pml_node::ptr parse(const char *data, size_t len);

psl::string to_string(const pml_node::raw_ptr node, unsigned int indent = 2);
bool write(const pml_node::raw_ptr node, o_device &o_device, unsigned int indent = 2);

pml_node::ptr make(const psl::string &name = psl::string());
pml_node::ptr make_num(const psl::string &name = psl::string(), value_type_num value = value_type_num());
pml_node::ptr make_str(const psl::string &name = psl::string(), const value_type_str &value = value_type_str());
pml_node::ptr make_bool(const psl::string &name = psl::string(), value_type_bool value = value_type_bool(false));

}

}
