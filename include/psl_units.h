#pragma once

// Prefix name 	N/A 	deci- 	centi- 	milli- 	micro- 	nano- 	pico- 	femto- 	atto- 	zepto- 	yocto-
// Prefix symbol 		d- 	    c-   	m- 	    μ-  	n- 	    p- 	    f- 	    a- 	    z- 	    y-
// Factor 	100 	    10–1 	10–2 	10–3 	10–6 	10–9 	10–12 	10–15 	10–18 	10–21 	10–24

#include "psl_limits.h"
#include "psl_format.h"
#include "psl_math.h"
#include "psl_algorithm.h"


namespace psl
{

enum class unit_t : int
{
    distance,
    area,
    volume,
    time,
    weight
};

enum class distance_t : int
{
    nm = -9,
    um = -6,
    mm = -3,
    cm = -2,
    dm = -1,
     m =  0,
    km =  3
};
enum class area_t : int
{
    nm2 = -18,
    um2 = -12,
    mm2 = -6,
    cm2 = -4,
    dm2 = -2,
     m2 =  0,
    km2 =  6
};
enum class volume_t : int
{
    nm3 = -27,
    um3 = -18,
    mm3 = -9,
    cm3 = -6,
    dm3 = -3,
     m3 =  0,
    km3 =  9
};

enum class time_t : int
{
     ns,
     us,
     ms,
      s,
    min,
      h,
};

enum class weight_t : int
{
    ng = -9,
    ug = -6,
    mg = -3,
    cg = -2,
    dg = -1,
    g  =  0,
    kg =  3
};

using unit_value_t = double;

template<unit_t Type, typename EUnit, EUnit UnitLhs, EUnit UnitRhs>
struct unit_converter
{
    static constexpr unit_value_t convert(unit_value_t value)
    {
        int diff = static_cast<int>(UnitLhs) - static_cast<int>(UnitRhs);
        return value * psl::fpow(10, diff);
    }
};

template<typename EUnit, EUnit UnitLhs, EUnit UnitRhs>
struct unit_converter<unit_t::time, EUnit, UnitLhs, UnitRhs>
{
    static constexpr unit_value_t convert(unit_value_t value)
    {
        constexpr unit_value_t factors[] = { psl::fpow(10, -9), psl::fpow(10, -6), psl::fpow(10, -3), 1, 60, 3600 };
        value *= factors[static_cast<uint>(UnitLhs)];
        value /= factors[static_cast<uint>(UnitRhs)];
        return value;
    }
};

template<unit_t Type, typename EUnit, EUnit Unit>
class unit
{
public:
    constexpr unit() = default;
    explicit constexpr unit(unit_value_t value)
        : m_value(value)
    {}

    constexpr unit_value_t get() const { return m_value; }

    template<EUnit UnitRhs>
    constexpr unit<Type, EUnit, UnitRhs> to_unit() const
    {
        return unit<Type, EUnit, UnitRhs>(unit_converter<Type, EUnit, Unit, UnitRhs>::convert(m_value));
    }

    template<EUnit UnitRhs>
    constexpr unit operator+(unit<Type, EUnit, UnitRhs> rhs) const
    {
        unit_value_t result = m_value + rhs.template to_unit<Unit>().get();
        return unit(result);
    }
    template<EUnit UnitRhs>
    constexpr unit &operator+=(unit<Type, EUnit, UnitRhs> rhs)
    {
        m_value += rhs.template to_unit<Unit>().get();
        return *this;
    }

    template<EUnit UnitRhs>
    constexpr unit operator-(unit<Type, EUnit, UnitRhs> rhs) const
    {
        unit_value_t result = m_value - rhs.template to_unit<Unit>().get();
        return unit(result);
    }
    template<EUnit UnitRhs>
    constexpr unit &operator-=(unit<Type, EUnit, UnitRhs> rhs)
    {
        m_value -= rhs.template to_unit<UnitRhs>().get();
        return *this;
    }

    unit operator+() const
    {
        return unit(m_value);
    }
    unit operator-() const
    {
        return unit(-m_value);
    }

    constexpr unit operator*(unit_value_t factor) const
    {
        return unit(m_value * factor);
    }
    constexpr unit &operator*=(unit_value_t factor)
    {
        m_value *= factor;
        return *this;
    }

    constexpr unit operator/(unit_value_t factor) const
    {
        return unit(m_value / factor);
    }
    constexpr unit &operator/=(unit_value_t factor)
    {
        m_value /= factor;
        return *this;
    }

    template<EUnit UnitRhs>
    constexpr bool operator==(unit<Type, EUnit, UnitRhs> rhs) const
    {
        return psl::abs(m_value - rhs.template to_unit<Unit>().get()) < psl::numeric_limits<unit_value_t>::epsilon();
    }
    template<EUnit UnitRhs>
    constexpr bool operator!=(unit<Type, EUnit, UnitRhs> rhs) const
    {
        return !operator==(rhs);
    }

    template<EUnit UnitRhs>
    constexpr bool operator<(unit<Type, EUnit, UnitRhs> rhs) const
    {
        return (m_value < rhs.template to_unit<Unit>().get());
    }
    template<EUnit UnitRhs>
    constexpr bool operator>(unit<Type, EUnit, UnitRhs> rhs) const
    {
        return (m_value > rhs.template to_unit<Unit>().get());
    }

    template<EUnit UnitRhs>
    constexpr bool operator<=(unit<Type, EUnit, UnitRhs> rhs) const
    {
        return !operator>(rhs);
    }
    template<EUnit UnitRhs>
    constexpr bool operator>=(unit<Type, EUnit, UnitRhs> rhs) const
    {
        return !operator<(rhs);
    }

private:
    unit_value_t m_value = {};
};

template<time_t Unit>
using time = unit<unit_t::time, time_t, Unit>;

template<distance_t Unit>
using distance = unit<unit_t::distance, distance_t, Unit>;

template<area_t Unit>
using area = unit<unit_t::area, area_t, Unit>;

template<volume_t Unit>
using volume = unit<unit_t::volume, volume_t, Unit>;

template<weight_t Unit>
using weight = unit<unit_t::weight, weight_t, Unit>;

// this cast to distance_t -> int -> area_t does not make sense...
template<distance_t Unit>
constexpr area<static_cast<area_t>(static_cast<int>(Unit))> operator*(distance<Unit> lhs, distance<Unit> rhs)
{
    unit_value_t result = lhs.get() * rhs.get();
    return area<static_cast<area_t>(static_cast<int>(Unit))>(result);
}
//template<area_t UnitLhs, distance_t UnitRhs>
//constexpr volume<static_cast<volume_t>(static_cast<int>(UnitLhs))> operator*(area<UnitLhs> lhs, distance<UnitRhs> rhs)
//{
//    constexpr distance_t units_m[] = { distance_t::nm, distance_t::um, distance_t::mm, distance_t::cm, distance_t::dm, distance_t:: m, distance_t::km };
//    constexpr area_t units_m2[] = { area_t::nm2, area_t::um2, area_t::mm2, area_t::cm2, area_t::dm2, area_t:: m2, area_t::km2 };

//    //static_assert(static_cast<int>(UnitLhs) == static_cast<int>(UnitLhs));
//    unit_value_t result = lhs.get() * rhs.get();
//    return area<static_cast<area_t>(static_cast<int>(UnitLhs))>(result);
//}


#define GEN_LITERAL(type, unit)                                                                                   \
    inline constexpr auto operator "" _##unit(long double value)        { return type<type##_t::unit>(value); }   \
    inline constexpr auto operator "" _##unit(unsigned long long value) { return type<type##_t::unit>(value); }   \

namespace literals::units_distance
{
GEN_LITERAL(distance, nm);
GEN_LITERAL(distance, um);
GEN_LITERAL(distance, mm);
GEN_LITERAL(distance, cm);
GEN_LITERAL(distance, dm);
GEN_LITERAL(distance,  m);
GEN_LITERAL(distance, km);
}
namespace literals::units_area
{
GEN_LITERAL(area, nm2);
GEN_LITERAL(area, um2);
GEN_LITERAL(area, mm2);
GEN_LITERAL(area, dm2);
GEN_LITERAL(area, cm2);
GEN_LITERAL(area,  m2);
GEN_LITERAL(area, km2);
}
namespace literals::units_area
{
GEN_LITERAL(volume, nm3);
GEN_LITERAL(volume, um3);
GEN_LITERAL(volume, mm3);
GEN_LITERAL(volume, dm3);
GEN_LITERAL(volume, cm3);
GEN_LITERAL(volume,  m3);
GEN_LITERAL(volume, km3);
}
namespace literals::units_time
{
GEN_LITERAL(time,  ns);
GEN_LITERAL(time,  us);
GEN_LITERAL(time,  ms);
GEN_LITERAL(time,   s);
GEN_LITERAL(time, min);
GEN_LITERAL(time,   h);
}
namespace literals::units_weight
{
GEN_LITERAL(weight,  ng);
GEN_LITERAL(weight,  ug);
GEN_LITERAL(weight,  mg);
GEN_LITERAL(weight,  cg);
GEN_LITERAL(weight,  dg);
GEN_LITERAL(weight,   g);
GEN_LITERAL(weight,  kg);
}
#undef GEN_LITERAL

namespace internal
{
#define MATCH(a, b) case a::b: return #b
constexpr const char *to_string(distance_t e)
{
    switch (e)
    {
    MATCH(distance_t, nm);
    MATCH(distance_t, um);
    MATCH(distance_t, mm);
    MATCH(distance_t, cm);
    MATCH(distance_t, dm);
    MATCH(distance_t,  m);
    MATCH(distance_t, km);
    }
    return "?";
}
constexpr const char *to_string(area_t e)
{
    switch (e)
    {
    MATCH(area_t, nm2);
    MATCH(area_t, um2);
    MATCH(area_t, mm2);
    MATCH(area_t, cm2);
    MATCH(area_t, dm2);
    MATCH(area_t,  m2);
    MATCH(area_t, km2);
    }
    return "?";
}
constexpr const char *to_string(volume_t e)
{
    switch (e)
    {
    MATCH(volume_t, nm3);
    MATCH(volume_t, um3);
    MATCH(volume_t, mm3);
    MATCH(volume_t, cm3);
    MATCH(volume_t, dm3);
    MATCH(volume_t,  m3);
    MATCH(volume_t, km3);
    }
    return "?";
}
constexpr const char *to_string(time_t e)
{
    switch (e)
    {
    MATCH(time_t,  ns);
    MATCH(time_t,  us);
    MATCH(time_t,  ms);
    MATCH(time_t,   s);
    MATCH(time_t, min);
    MATCH(time_t,   h);
    }
    return "?";
}
constexpr const char *to_string(weight_t e)
{
    switch (e)
    {
    MATCH(weight_t, ng);
    MATCH(weight_t, ug);
    MATCH(weight_t, mg);
    MATCH(weight_t, cg);
    MATCH(weight_t, dg);
    MATCH(weight_t,  g);
    MATCH(weight_t, kg);
    }
    return "?";
}
#undef MATCH
}

template<unit_t Type, typename EUnit, EUnit Unit> struct formatter<psl::unit<Type, EUnit, Unit>>
{
    static bool write(format_writer &writer, psl::unit<Type, EUnit, Unit> value)
    {
        writer.sprintf("%.3f", value.get());
        writer.write(internal::to_string(Unit));
        return true;
    }
};

//template<distance_t Unit> struct formatter<psl::distance<Unit>>
//{
//    static bool write(format_writer &writer, psl::distance<Unit> value)
//    {
//        writer.sprintf("%.3f", value.get());
//        writer.write(internal::to_string(Unit));
//        return true;
//    }
//};
//template<area_t Unit> struct formatter<psl::area<Unit>>
//{
//    static bool write(format_writer &writer, psl::area<Unit> value)
//    {
//        writer.sprintf("%.3f", value.get());
//        writer.write(internal::to_string(Unit));
//        return true;
//    }
//};
//template<volume_t Unit> struct formatter<psl::volume<Unit>>
//{
//    static bool write(format_writer &writer, psl::volume<Unit> value)
//    {
//        writer.sprintf("%.3f", value.get());
//        writer.write(internal::to_string(Unit));
//        return true;
//    }
//};
//template<time_t Unit> struct formatter<psl::time<Unit>>
//{
//    static bool write(format_writer &writer, psl::time<Unit> value)
//    {
//        writer.sprintf("%.3f", value.get());
//        writer.write(internal::to_string(Unit));
//        return true;
//    }
//};
//template<weight_t Unit> struct formatter<psl::weight<Unit>>
//{
//    static bool write(format_writer &writer, psl::weight<Unit> value)
//    {
//        writer.sprintf("%.3f", value.get());
//        writer.write(internal::to_string(Unit));
//        return true;
//    }
//};

}
