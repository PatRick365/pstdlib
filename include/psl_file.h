#pragma once

#include "psl_string.h"
#include "psl_iodevice.h"
#include "psl_memory.h"
#include "psl_optional.h"


namespace psl
{

struct file_impl;
class file : public psl::io_device
{
public:
    file();
    ~file();

    explicit file(const psl::string &path, psl::open_mode mode = psl::open_mode::read_write);

    bool open(const psl::string &path, psl::open_mode mode = psl::open_mode::read_write);

    void sync();

    psl::optional<psl::string> read(ssize_t len);
    psl::optional<psl::string> read_all() override;

    bool remove();

    bool close() override;

    bool is_open() const override;

    bool at_end() const;
    ssize_t pos() const;
    bool seek(ssize_t pos);
    bool seek_begin();
    ssize_t size() const;

    psl::string name() const;

    void dump_info() const;

    static bool remove(const psl::string &path);

protected:
    bool write_intern(const uint8_t *data, size_t len) override;
    ssize_t read_intern(uint8_t *data, size_t len) override;

private:
    psl::unique_ptr<psl::file_impl> m_impl;

};

}
