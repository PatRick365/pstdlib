#pragma once

#include "psl_iodevice.h"
#include "psl_memory.h"


namespace psl
{
class tcp_server_impl;
class tcp_server : public psl::io_device
{
public:
    tcp_server();
    ~tcp_server();

    bool is_open() const override;

    bool open(int port);
    bool close() override;

protected:
    bool write_intern(const uint8_t *data, size_t len) override;
    ssize_t read_intern(uint8_t *data, size_t len) override;

private:
    psl::unique_ptr<tcp_server_impl> m_impl;
};

}
