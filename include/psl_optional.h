#pragma once

#include <unistd.h>
#include "psl_memory.h"


namespace psl
{

struct nullopt_t{};

inline constexpr nullopt_t nullopt {/*unspecified*/};


template <typename T>
class optional
{
public:
    constexpr optional() noexcept = default;
    constexpr optional(psl::nullopt_t) noexcept {}
    constexpr optional(const optional& other)
    {
        if (other.m_valid)
        {
            psl::construct_at(reinterpret_cast<T *>(m_storage), other.value());
            m_valid = true;
        }
    }
    constexpr optional(optional &&other) noexcept(true/* see below */)
    {
        if (other.m_valid)
        {
            (reinterpret_cast<T &>(m_storage)) = (reinterpret_cast<T &>(other.m_storage));
            other.m_valid = false;
            m_valid = false;
        }
    }

    template<class U = T>
    constexpr optional(U &&value)
    {
        psl::construct_at<T>(reinterpret_cast<T *>(m_storage), psl::forward<U>(value));
        m_valid = true;
    }

    ~optional()
    {
        if (m_valid)
        {
            psl::destroy_at<T>(reinterpret_cast<T *>(m_storage));
        }
    }

    constexpr optional& operator=(psl::nullopt_t) noexcept
    {
        if (m_valid)
        {
            psl::destroy_at<T>(reinterpret_cast<T *>(m_storage));
            m_valid = false;
        }
        return *this;
    }

    template< class U = T >
    constexpr optional &operator=(U &&value)
    {
        if (m_valid)
        {
            psl::destroy_at<T>(reinterpret_cast<T *>(m_storage));
            m_valid = false;
        }
        psl::construct_at<T>(reinterpret_cast<T *>(m_storage), psl::forward<U>(value));
        m_valid = true;
        return *this;
    }

    constexpr explicit operator bool() const noexcept{ return m_valid; }
    constexpr bool has_value() const noexcept { return m_valid; }

    constexpr const T* operator->() const { return reinterpret_cast<const T *>(m_storage); }
    //constexpr T* operator->() const { return reinterpret_cast<const T *>(m_storage); }
    constexpr const T& operator*() const& { return reinterpret_cast<const T &>(m_storage); }
    constexpr T& operator*() & { return reinterpret_cast<T &>(m_storage); }
    constexpr const T&& operator*() const&& { return reinterpret_cast<const T &>(m_storage); }
    constexpr T&& operator*() && { return reinterpret_cast<const T &>(m_storage); }

    constexpr T& value() & { return reinterpret_cast<T &>(m_storage); }
    constexpr const T& value() const & { return reinterpret_cast<const T &>(m_storage); }
    constexpr T&& value() && { return reinterpret_cast<T &&>(m_storage); }
    constexpr const T&& value() const && { return reinterpret_cast<const T &&>(m_storage); }

    template<class U>
    constexpr T value_or(U &&default_value ) &&
    {
        return bool(*this) ? psl::move(**this) : static_cast<T>(psl::forward<U>(default_value));
    }

private:
    alignas(T)unsigned char m_storage[sizeof(T)]{};
    bool m_valid = false;
};

template<class T>
constexpr bool operator==(psl::nullopt_t, const optional<T> &opt) noexcept
{
    return !opt.has_value();
}
template<class T>
constexpr bool operator==(const optional<T> &opt, psl::nullopt_t) noexcept
{
    return !opt.has_value();
}


}
