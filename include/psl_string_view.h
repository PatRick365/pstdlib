#pragma once

#include "psl_algorithm.h"
#include "psl_char_manip.h"
#include "psl_string.h"
#include "psl_except.h"

#include <cwchar>


namespace psl
{

template<typename T> class basic_string_view;
typedef basic_string_view<char> string_view;
typedef basic_string_view<wchar_t> wstring_view;

template<typename T>
class basic_string_view
{
public:
    class const_iterator;
    class iterator;
    using char_type = T;
    using size_type = ssize_t;
    using difference_type = long int; //__PTRDIFF_TYPE__;
    using reference = char_type&;
    using const_reference = const char_type &;
    using pointer = char_type *;
    using const_pointer = const char_type *;
    //using iterator = basic_string_view::const_iterator; /// hmm, no work
    using const_iterator = basic_string_view::const_iterator;

public:
    static constexpr size_type npos = psl::numeric_limits<size_type>::max();

/// constructors, destructor, assign
    constexpr basic_string_view() = default;

    constexpr basic_string_view(const char_type *c_str)
        : m_values(c_str),
          m_sz(psl::strlen(c_str))
    {}

    constexpr basic_string_view(const char_type *str, size_type len)
        : m_values(str),
          m_sz(len)
    {}

    constexpr basic_string_view(const basic_string_view<char_type> &other) = default;

    constexpr basic_string_view(basic_string_view<char_type> &&other) = default;

    constexpr basic_string_view(const_iterator it1, const_iterator it2)
    {
        m_values = &(*it1);

        while (it1 != it2)
        {
            ++m_sz;
        }
    }

    constexpr basic_string_view &operator=(const basic_string_view &other)
    {
        m_values = other.m_values;
        m_sz = other.m_sz;
        return *this;
    }

    constexpr basic_string_view &operator=(basic_string_view &&other)
    {
        m_sz = other.m_sz;
        m_values = other.m_values;

        other.m_sz = 0;
        other.m_values = nullptr;

        return *this;
    }

/// element access
    constexpr const_reference operator[](size_type position) const
    {
        if (position >= m_sz)
            throw psl::out_of_range(psl::string().sprintf("operator[] out of range: sz = %zu but position = %zu", m_sz, position));

        return m_values[position];
    }

    constexpr void remove_prefix(size_type n)
    {
        m_values += n;
    }
    constexpr void remove_suffix(size_type n)
    {
        m_sz -= n;
    }

    friend constexpr bool operator==(const basic_string_view& lhs, const basic_string_view& rhs)
    {
        if (lhs.length() != rhs.length())
            return false;

        for (size_type i = 0; i < lhs.size(); ++i)
        {
            if (lhs[i] != rhs[i])
                return false;
        }

        return true;
    }
    friend constexpr bool operator!=(const basic_string_view& lhs, const basic_string_view& rhs)
    {
        return !(lhs == rhs);
    }

    friend constexpr bool operator==(const basic_string_view<char_type>& lhs, const char_type *c_str)
    {
        return (lhs == psl::basic_string_view<char_type>(c_str));
    }
    friend constexpr bool operator!=(const basic_string_view<char_type>& lhs, const char_type *c_str)
    {
        return !(lhs == c_str);
    }

    constexpr const_reference front() const
    {
        if (m_sz == 0)
            throw psl::basic_string_view<char>("front called on empty container");  // nope

        return m_values[0];
    }

    constexpr const_reference back() const
    {
        if (m_sz == 0)
            throw psl::basic_string_view<char>("back called on empty container");  // nope

        return m_values[m_sz - 1];
    }

    constexpr const_pointer data() const
    {
        return m_values;
    }

/// capacity
    constexpr size_type size() const
    {
        return m_sz;
    }

    constexpr size_type length() const
    {
        return size();
    }

    constexpr bool empty() const
    {
        return (m_sz == 0);
    }

    constexpr bool contains(char_type c)
    {
        return psl::find(begin(), end(), c) != end();
    }

    constexpr bool contains(const basic_string_view &str)
    {
        if (str.empty() || size() < str.size())
            return false;

        size_type j = 0;
        for (size_type i = 0; i < m_sz; ++i)
        {
            if (m_values[i] == str[j])
            {
                if (++j == str.size())
                    return true;
            }
            else
            {
                j = 0;
            }
        }
        return false;
    }

    constexpr size_type find(const basic_string_view &str) const
    {
        if (str.empty() || size() < str.size())
            return npos;

        size_type j = 0;
        for (size_type i = 0; i < m_sz; ++i)
        {
            if (m_values[i] == str[j])
            {
                if (++j == str.size())
                    return (i - str.size() + 1);
            }
            else
            {
                j = 0;
            }
        }
        return npos;
    }

    constexpr size_type find(char_type c) const
    {
        if (empty())
            return npos;

        for (size_type i = 0; i < m_sz; ++i)
        {
            if (m_values[i] == c)
            {
                return i;
            }
        }
        return npos;
    }

    constexpr size_type rfind(char_type c) const
    {
        if (empty())
            return npos;

        for (size_type i = m_sz; i --> 0; )
        {
            if (m_values[i] == c)
            {
                return i;
            }
        }
        return npos;
    }

    constexpr basic_string_view substr(size_type pos = 0, size_type count = npos) const
    {
        size_type len = psl::min(count, length() - pos);
        return basic_string_view(m_values + pos, len); // might be 1 off
    }

    template<typename Container>
    Container split(char_type delimit, bool skip_empty = false) const
    {
        size_t count = psl::count(begin(), end(), delimit);

        Container vec(count);   // reserve

        auto it_start = begin();

        while(it_start != end())
        {
            auto it_found = psl::find(it_start, end(), delimit);

            if (it_found == end())
                break;

            //printf("range: %d - char: %c\n", (it_found - it_start), *it_found);

            if (!skip_empty || (it_start != it_found))
                vec.push_back(basic_string_view(it_start, it_found));

            it_start = it_found;
            ++it_start;
        }

        if (!skip_empty || (it_start != end()))
            vec.push_back(basic_string_view(it_start, end()));

        return vec;

    }

public:
/// iterators
    constexpr const_iterator begin()
    {
        return const_iterator(m_values);
    }
    constexpr const_iterator end()
    {
        return const_iterator(m_values + m_sz);
    }

    constexpr const_iterator begin() const
    {
        return const_iterator(m_values);
    }
    constexpr const_iterator end() const
    {
        return const_iterator(m_values + m_sz);
    }

    constexpr const_iterator cbegin() const
    {
        return begin();
    }
    constexpr const_iterator cend() const
    {
        return end();
    }

/// private section
private:
    const_pointer m_values = nullptr;
    size_type m_sz = 0;

/// iterator implementations
public:
    class const_iterator
    {
    public:
        using value_type = basic_string_view::char_type;
        using reference = basic_string_view::const_reference;
        using pointer = basic_string_view::const_pointer;
        using difference_type = basic_string_view::difference_type;

    public:
        constexpr const_iterator() = default;

        constexpr const_iterator(pointer ptr)
            : ptr(ptr)
        {}

        constexpr reference operator*() const
        {
            return *ptr;
        }

        constexpr pointer operator->() const
        {
            return ptr;
        }

        constexpr const_iterator& operator++()
        {
            ++ptr;
            return *this;
        }

        constexpr const_iterator operator++(int)
        {
            const_iterator it = *this;
            ++ptr;
            return it;
        }

        constexpr bool operator==(const const_iterator &it) const
        {
            return it.ptr == ptr;
        }

        constexpr bool operator!=(const const_iterator &it) const
        {
            return it.ptr != ptr;
        }

        constexpr basic_string_view::difference_type operator-(const const_iterator &rop)
        {
            return ptr - rop.ptr;
        }

    private:
        pointer ptr = nullptr;
    };

};

}
