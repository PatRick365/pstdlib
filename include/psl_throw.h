#pragma once

#include <stddef.h>


// to avoid include of psl_escept.h
namespace psl::internal
{

void throw_exception(const char *what);
void throw_out_of_range(const char *function, size_t position, size_t size);

}
