/***********************************************************************
* Author      : Patrick Falkensteiner
* Date        : 02.04.2019
* Description : Simple implementation of vector template with iterators
************************************************************************/

#pragma once

#include "psl_utility.h"
#include "psl_algorithm.h"
#include "psl_memory.h"
#include "psl_except.h"
#include "psl_iterator.h"
#include "psl_string.h"
#include "psl_format.h"
#include "psl_initializer_list.h"

#include <unistd.h>


namespace psl
{

template<typename T, typename Allocator = psl::allocator<T>>
class vector
{
public:
    class const_iterator;
    class iterator;
    using value_type = T;
    using allocator_type = Allocator;
    using size_type = size_t;
    using difference_type = long int;
    using reference = value_type&;
    using const_reference = const value_type &;
    using pointer = value_type *;
    using const_pointer = const value_type *;
    using iterator = vector::iterator;
    using const_iterator = vector::const_iterator;
    using reverse_iterator = psl::reverse_iterator<iterator>;
    using const_reverse_iterator = psl::reverse_iterator<const_iterator>;


/// constructors
    // 1
    vector() noexcept(noexcept(Allocator())) = default;
    // 2
    explicit vector(const Allocator &alloc) noexcept
        : m_alloc(alloc)
    {}
    // 3
    vector(size_type count, const T &value, const Allocator &alloc = Allocator())
        : m_alloc(alloc)
    {
        reserve(count);
        for (size_type i = 0; i < count; ++i)
        {
            push_back(value);
        }
    }
    // 4
    explicit vector(size_type count, const Allocator &alloc = Allocator())
        : m_alloc(alloc)
    {
        resize(count);
    }
    // 5
    template <class InputIt>
    vector(InputIt first, InputIt last, const Allocator &alloc = Allocator())
        : m_alloc(alloc)
    {
        while (first != last)
        {
            push_back(*first);
            ++first;
        }
    }
    // 6
    vector(const vector &other)
        : vector(other, other.m_alloc)
    {}
    // 7
    vector(const vector &other, const Allocator &alloc)
        : m_alloc(alloc)
    {
        reserve(other.m_sz);

        for (size_type i = 0; i < other.m_sz; ++i)
            psl::construct_at(m_values + i, other.m_values[i]);

        m_sz = other.m_sz;
    }
    // 8
    vector(vector &&other) noexcept
        : m_alloc(psl::move(other.m_alloc)),
          m_sz(other.m_sz),
          m_cap(other.m_cap),
          m_values(other.m_values)
    {
        other.m_sz = 0;
        other.m_cap = 0;
        other.m_values = nullptr;
    }
    // 9
    constexpr vector(vector &&other, const Allocator &alloc)
        : m_alloc(alloc),
          m_sz(other.m_sz),
          m_cap(other.m_cap),
          m_values(other.m_values)
    {
        other.m_sz = 0;
        other.m_cap = 0;
        other.m_values = nullptr;
    }
    // 10
    template<typename ...Args>
    vector(psl::il_tag, Args &&...args)
    {
        reserve(sizeof...(args));
        psl::initializer_list_push_back(*this, psl::forward<Args>(args)...);
    }
    // 11
    template<typename ...Args>
    vector(const Allocator &alloc, psl::il_tag, Args &&...args)
        : m_alloc(alloc)
    {
        reserve(sizeof...(args));
        psl::initializer_list_push_back(*this, psl::forward<Args>(args)...);
    }

    ~vector()
    {
        for (size_type i = 0; i < m_sz; ++i)
            psl::destroy_at(m_values + i);

        psl::allocator_traits<Allocator>::deallocate(m_alloc, m_values, m_cap);
    }

    ///@todo psl::allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value
    vector &operator=(const vector &other)
    {
        if (&other == this)
            return *this;

        m_alloc = other.m_alloc;

        reserve(other.m_sz);

        // copy values into already constructed space
        for (size_type i = 0; i < m_sz; ++i)
            m_values[i] = other.m_values[i];

        // newly construct into rest of space
        for (size_type i = m_sz; i < other.m_sz; ++i)
            psl::construct_at(m_values + i, other.m_values[i]);

        m_sz = other.m_sz;

        return *this;
    }

    ///@todo psl::allocator_traits<allocator_type>::propagate_on_container_move_assignment::value
    vector &operator=(vector &&other)
    {
        if (&other == this)
            return *this;

        m_alloc = other.m_alloc;
        m_sz = other.m_sz;
        m_cap = other.m_cap;
        m_values = other.m_values;

        other.m_sz = 0;
        other.m_cap = 0;
        other.m_values = nullptr;

        return *this;
    }

    void assign(size_type count, const T &value)
    {
        clear();
        resize(count, value);
    }
    template<typename InputIt>
    void assign(InputIt first, InputIt last)
    {
        clear();
        for (; first != last; ++first)
        {
            push_back(*first);
        }
    }
    template<typename ...Args>
    void assign(psl::il_tag, Args &&...args)
    {
        clear();
        reserve(sizeof...(Args));
        psl::initializer_list_push_back(*this, psl::forward<Args>(args)...);
    }

    allocator_type get_allocator() const noexcept
    {
        return m_alloc;
    }

/// element access
    reference operator[](size_type position)
    {
        assert(position < m_sz);
        return m_values[position];
    }

    const_reference operator[](size_type position) const
    {
        assert(position < m_sz);
        return m_values[position];
    }

    value_type at(size_type position) const
    {
        if (position >= m_sz)
            throw psl::out_of_range(psl::format("position({}) >= size({})", position, m_sz));

        return m_values[position];
    }

    const_reference front() const
    {
        assert(!empty());
        return m_values[0];
    }
    reference front()
    {
        assert(!empty());
        return m_values[0];
    }

    const_reference back() const
    {
        assert(!empty());
        return m_values[m_sz - 1];
    }
    reference back()
    {
        assert(!empty());
        return m_values[m_sz - 1];
    }
    pointer data()
    {
        return m_values;
    }

    const_pointer data() const
    {
        return m_values;
    }

/// capacity
    bool empty() const
    {
        return (m_sz == 0);
    }

    size_type size() const
    {
        return m_sz;
    }

    size_type max_size() const noexcept
    {
        return psl::numeric_limits<size_type>::max();
    }

    void reserve(size_type capacity)
    {
        capacity = psl::max(capacity, min_cap);

        if (capacity <= m_cap)
            return;

        value_type *newArray = psl::allocator_traits<Allocator>::allocate(m_alloc, capacity);

        for (size_type i = 0; i < m_sz; ++i)
            psl::construct_at(newArray + i, psl::move(m_values[i]));

        for (size_type i = 0; i < m_sz; ++i)
            psl::destroy_at(m_values + i);

        psl::allocator_traits<Allocator>::deallocate(m_alloc, m_values, m_cap);

        m_values = newArray;
        m_cap = capacity;
    }

    size_type capacity() const
    {
        return m_cap;
    }

    void shrink_to_fit()
    {
        value_type *newArray = psl::allocator_traits<Allocator>::allocate(m_alloc, m_sz);

        for (size_type i = 0; i < m_sz; ++i)
            psl::construct_at(newArray + i, m_values[i]);

        for (size_type i = 0; i < m_sz; ++i)
            psl::destroy_at(m_values + i);

        psl::allocator_traits<Allocator>::deallocate(m_alloc, m_values, m_cap);

        m_values = newArray;
        m_cap = m_sz;
    }

/// modifiers
    void clear()
    {
        for (size_type i = 0; i < m_sz; ++i)
        {
            psl::destroy_at(m_values + i);
        }

        m_sz = 0;
    }

    void insert(size_type position, const value_type &value)
    {
        if (position > m_sz)
            throw psl::out_of_range(psl::format("position({}) >= size({})", position, m_sz));

        grow_if_needed(1);

        psl::construct_at(m_values + m_sz, value);

        for (size_type i = m_sz; i > position; --i)
            m_values[i] = m_values[i - 1];

        m_values[position] = value;
        ++m_sz;
    }

    iterator insert(const_iterator pos, const_reference val)
    {
        difference_type diff = pos - cbegin();

        // first check iterator, only then grow array!
        grow_if_needed(1);

        size_type current = static_cast<size_type>(diff);

        psl::construct_at(m_values + m_sz, val);

        for (size_t i = m_sz; i-->current;)
            m_values[i + 1] = m_values[i];

        m_values[current] = val;
        ++m_sz;
        return iterator(m_values + current);
    }

    template <typename... Args>
    iterator emplace(const_iterator pos, Args &&...args)
    {
        if (empty())
        {
            assert(pos == cend());
            emplace_back(psl::forward<Args>(args)...);
            return begin();
        }

        grow_if_needed(1);

        difference_type current = pos - cbegin();

        psl::construct_at(m_values + m_sz, m_values[m_sz - 1]);

        ++m_sz;

        for (difference_type i = m_sz - 1; i > current; --i)
            m_values[i] = m_values[i - 1];

        psl::destroy_at(&(*pos));
        psl::construct_at(&(*pos), psl::forward<Args>(args)...);

        return begin() + current;
    }

    void erase(size_type position)
    {
        if (position >= m_sz)
            throw psl::out_of_range(psl::format("position({}) >= size({})", position, m_sz));

        for (size_type i = position; i < m_sz - 1; ++i)
            m_values[i] = psl::move(m_values[i + 1]);

        psl::destroy_at(m_values + m_sz - 1);

        --m_sz;
    }

    iterator erase(const_iterator pos)
    {
        auto diff = pos - begin();
        if (diff < 0 || static_cast<size_type>(diff) >= m_sz)
            throw psl::out_of_range("iterator out of bounds");

        size_type current = static_cast<size_type>(diff);
        for (size_type i = current; i < m_sz - 1; ++i)
            m_values[i] = psl::move(m_values[i + 1]);

        psl::destroy_at(m_values + m_sz - 1);

        --m_sz;
        return iterator(m_values + current);
    }

    ///@todo: implement with better performance
    iterator erase(const_iterator first, const_iterator last)
    {
        auto pos_first = first - begin();
        auto pos_last = last - begin();
        for (size_t i = pos_first; i < pos_last; ++i)
        {
            erase(pos_first);
        }
        return begin() + pos_first;
    }

    void push_back(const value_type &value)
    {
        grow_if_needed(1);
        psl::construct_at(m_values + m_sz, value);
        ++m_sz;
    }
    void push_back(value_type &&value)
    {
        grow_if_needed(1);
        psl::construct_at(m_values + m_sz, psl::forward<T>(value));
        ++m_sz;
    }

    vector &operator<<(const_reference value)
    {
        push_back(value);
        return *this;
    }
    vector &operator<<(value_type &&value)
    {
        emplace_back(value);
        return *this;
    }

    template <class... Args>
    reference emplace_back(Args &&...args)
    {
        grow_if_needed(1);

        psl::construct_at(m_values + m_sz, psl::forward<Args>(args)...);
        ++m_sz;
        return back();
    }

    void pop_back()
    {
        assert(!empty());
        psl::destroy_at(m_values + m_sz - 1);
        --m_sz;
    }

    void resize(size_type count)
    {
        resize(count, T());
    }

    void resize(size_type count, const value_type &value)
    {
        reserve(count);
        for (size_type i = m_sz; i < count; ++i)
            psl::construct_at(m_values + i, value);

        m_sz = count;
    }

    //noexcept(psl::allocator_traits<Allocator>::propagate_on_container_swap::value|| psl::allocator_traits<Allocator>::is_always_equal::value)
    void swap(vector &other)
    {
        psl::swap(m_values, other.m_values);
        psl::swap(m_sz, other.m_sz);
        psl::swap(m_cap, other.m_cap);
    }

/// iterators getters
    iterator begin()
    {
        return iterator(m_values);
    }
    iterator end()
    {
        return iterator(m_values + m_sz);
    }
    const_iterator begin() const
    {
        if (empty())
            return end();
        return const_iterator(m_values);
    }
    const_iterator end() const
    {
        return const_iterator(m_values + m_sz);
    }
    const_iterator cbegin() const
    {
        return begin();
    }
    const_iterator cend() const
    {
        return end();
    }

    reverse_iterator rbegin()
    {
        return reverse_iterator(end());
    }
    reverse_iterator rend()
    {
        return reverse_iterator(begin());
    }
    const_reverse_iterator rbegin() const
    {
        return const_reverse_iterator(cend());
    }
    const_reverse_iterator rend() const
    {
        return const_reverse_iterator(cbegin());
    }
    const_reverse_iterator crbegin() const
    {
        return const_reverse_iterator(cend());
    }
    const_reverse_iterator crend() const
    {
        return const_reverse_iterator(cbegin());
    }

/// private section
private:
    void grow_if_needed(size_type needed)
    {
        if (m_sz + needed > m_cap)
        {
            reserve(m_cap * grow_factor);
        }
    }
private:
    static constexpr size_type grow_factor = 2;
    static constexpr size_type min_cap = 5;

    Allocator m_alloc;

    size_type m_sz = 0;
    size_type m_cap = 0;
    value_type *m_values = nullptr;

/// iterator implementations
public:
    class iterator
    {
    public:
        using value_type = vector::value_type;
        using reference = vector::reference;
        using pointer = vector::pointer;
        using difference_type = vector::difference_type;
        using iterator_category = psl::contiguous_iterator_tag;
    public:
        iterator()
            : m_ptr(nullptr)
        {}

        explicit iterator(pointer ptr)
            : m_ptr(ptr)
        {}

        reference operator*() const
        {
            assert(m_ptr);
            return *m_ptr;
        }

        pointer operator->() const
        {
            assert(m_ptr);
            return m_ptr;
        }

        iterator& operator++()
        {
            assert(m_ptr);
            ++m_ptr;
            return *this;
        }

        iterator operator++(int)
        {
            assert(m_ptr);
            iterator it = *this;
            ++m_ptr;
            return it;
        }

        iterator& operator--()
        {
            --m_ptr;
            return *this;
        }

        iterator operator--(int)
        {
            iterator it = *this;
            --m_ptr;
            return it;
        }

        iterator operator+(difference_type difference) const
        {
            return iterator(m_ptr + difference);
        }
        iterator operator-(difference_type difference) const
        {
            return iterator(m_ptr - difference);
        }

        iterator &operator+=(difference_type difference)
        {
            m_ptr += difference;
            return *this;
        }
        iterator &operator-=(difference_type difference)
        {
            m_ptr -= difference;
            return *this;
        }

        difference_type operator-(iterator rhs) const
        {
            return m_ptr - rhs.m_ptr;
        }

        bool operator==(const const_iterator &it) const
        {
            return it == const_iterator(m_ptr);
        }
        bool operator!=(const const_iterator &it) const
        {
            return const_iterator(it) != const_iterator(m_ptr);
        }
        bool operator<(const const_iterator &it) const
        {
            return it.m_ptr < m_ptr;
        }
        bool operator>(const const_iterator &it) const
        {
            return it.m_ptr > m_ptr;
        }
        bool operator<=(const const_iterator &it) const
        {
            return it.m_ptr <= m_ptr;
        }
        bool operator>=(const const_iterator &it) const
        {
            return it.m_ptr > m_ptr;
        }

        operator const_iterator() const
        {
            return const_iterator(m_ptr);
        }
        
    private:
        pointer m_ptr;
    };

    class const_iterator
    {
    public:
        using value_type = vector::value_type;
        using reference = vector::const_reference;
        using pointer = vector::const_pointer;
        using difference_type = vector::difference_type;
        using iterator_category = psl::contiguous_iterator_tag;

    public:
        const_iterator()
            : m_ptr(nullptr)
        {}

        explicit const_iterator(pointer ptr)
            : m_ptr(ptr)
        {}

        const_reference operator*() const
        {
            assert(m_ptr);
            return *m_ptr;
        }

        const_pointer operator->() const
        {
            assert(m_ptr);
            return m_ptr;
        }

        const_iterator& operator++()
        {
            assert(m_ptr);
            ++m_ptr;
            return *this;
        }

        const_iterator operator++(int)
        {
            assert(m_ptr);
            const_iterator it = *this;
            ++m_ptr;
            return it;
        }

        const_iterator& operator--()
        {
            --m_ptr;
            return *this;
        }

        const_iterator operator--(int)
        {
            const_iterator it = *this;
            --m_ptr;
            return it;
        }

        const_iterator operator+(difference_type difference) const
        {
            return const_iterator(m_ptr + difference);
        }
        const_iterator operator-(difference_type difference) const
        {
            return const_iterator(m_ptr - difference);
        }

        const_iterator &operator+=(difference_type difference)
        {
            m_ptr += difference;
            return *this;
        }
        const_iterator &operator-=(difference_type difference)
        {
            m_ptr -= difference;
            return *this;
        }

        difference_type operator-(const_iterator rhs) const
        {
            return m_ptr - rhs.m_ptr;
        }

        bool operator==(const const_iterator &it) const
        {
            return it.m_ptr == m_ptr;
        }
        bool operator!=(const const_iterator &it) const
        {
            return it.m_ptr != m_ptr;
        }
        bool operator<(const const_iterator &it) const
        {
            return it.m_ptr < m_ptr;
        }
        bool operator>(const const_iterator &it) const
        {
            return it.m_ptr > m_ptr;
        }
        bool operator<=(const const_iterator &it) const
        {
            return it.m_ptr <= m_ptr;
        }
        bool operator>=(const const_iterator &it) const
        {
            return it.m_ptr > m_ptr;
        }
        
    private:
        const_pointer m_ptr;
    };
};

/// non-member functions
template<typename T>
bool operator==(const vector<T> &lop, const vector<T> &rop)
{
    if (lop.size() != rop.size())
        return false;

    for (size_t i = 0; i < lop.size(); ++i)
    {
        if (lop[i] != rop[i])
            return false;
    }
    return true;
}

template<typename T>
bool operator!=(const vector<T> &lop, const vector<T> &rop)
{
    return !(lop == rop);
}

template<typename T>
struct formatter<psl::vector<T>>
{
    static bool write(format_writer &writer, const psl::vector<T> &value)
    {
        writer.write("[");
        for (size_t i = 0; i < value.size(); ++i)
        {
            if (i > 0)
                writer.write(", ");
            writer.write(value[i]);
        }
        writer.write("]");
        return true;
    }
};

}
