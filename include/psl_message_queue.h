/**
 * @file message_queue.h
 * @brief A thread-safe message queue implementation.
 *
 * This header defines a templated message queue that supports
 * sending and receiving messages in a thread-safe manner using
 * a condition variable and a mutexes.
 */

#pragma once

#include "psl_thread.h"
#include "psl_vector.h"

namespace psl
{

/**
 * @class message_queue
 * @brief A thread-safe message queue.
 *
 * This class implements a message queue using a condition variable
 * and a mutex to ensure thread safety. Messages can be sent and
 * received in a synchronized manner.
 *
 * @tparam T The type of elements stored in the queue.
 */
template<typename T>
class message_queue
{
public:
    /**
     * @brief Default constructor.
     *
     * Initializes an empty message queue.
     */
    message_queue() = default;

    /**
     * @brief Destructor.
     *
     * Clears the queue and stops wakes up any waiting threads.
     */
    ~message_queue()
    {
        clear();
    }

    /**
     * @brief Receives a message from the queue.
     *
     * Waits until a message is available or the queue is stopped.
     *
     * @param[out] value Reference to store the received message.
     * @return `true` if a message was successfully received, `false` if the queue was stopped.
     */
    bool receive(T &value)
    {
        psl::region_lock lock_cv(m_mut_cv);
        m_cv.wait(m_mut_cv, [this] { return (m_stop || !m_queue.empty()); });

        if (m_stop)
            return false;

        assert(!m_queue.empty());
        value = psl::move(m_queue.front());
        m_queue.erase(0);

        return true;
    }

    /**
     * @brief Sends a message to the queue.
     *
     * Adds a message to the queue and notifies a waiting thread.
     *
     * @param[in] value The message to be added to the queue.
     * @return `true` if the message was successfully added.
     */
    bool send(const T &value)
    {
        {
            psl::region_lock lock(m_mut_cv);
            m_queue.push_back(value);
        }
        m_cv.notify_one();
        return true;
    }

    /**
     * @brief Clears the queue and stops any waiting threads.
     *
     * This function ensures that all threads waiting on `receive`
     * are notified and the queue is emptied.
     */
    void clear()
    {
        {
            psl::region_lock lock_data(m_mut_cv);
            m_stop = true;
            m_queue.clear();
        }
        m_cv.notify_all();
    }

private:
    psl::condition_variable m_cv; ///< Condition variable for synchronization.
    psl::mutex m_mut_cv;          ///< Mutex to protect access to the queue.
    psl::vector<T> m_queue;       ///< Container storing the messages.
    bool m_stop = false;          ///< Flag to indicate if the queue should stop.
};

} // namespace psl
