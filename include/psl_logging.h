#pragma once

#include "psl_string.h"
#include "psl_file.h"
#include "psl_vector.h"
#include "psl_format.h"


//#define eprintf(...) fprintf (stderr, __VA_ARGS__)

#define HERE __FILE__, __LINE__
#define HERE_FUNC HERE, __PRETTY_FUNCTION__

#define LOG_ENTRY psl::log_entry entry(HERE_FUNC)

#define LOG_PURE(str, ...) psl::logging::get().log_pure(str __VA_OPT__(,__VA_ARGS__))
#define LOG(str, ...) psl::logging::get().log(HERE, str __VA_OPT__(,__VA_ARGS__))

#define LOG_LEVEL_PURE(lvl, str, ...) psl::logging::get().log_level_pure(lvl, str, __VA_OPT__(,__VA_ARGS__))
#define LOG_LEVEL(lvl, str, ...) psl::logging::get().log_level(HERE, lvl, str __VA_OPT__(,__VA_ARGS__))

#define LOG_ERROR(str, ...) psl::logging::get().log(HERE, psl::string("(E) ") + str, __VA_OPT__(,__VA_ARGS__))
#define LOG_WARN(str, ...) psl::logging::get().log(HERE, psl::string("(W) ") + str, __VA_OPT__(,__VA_ARGS__))

#define LOG_EMPTY_LINES(n) psl::logging::get().log_empty_lines(n)
#define LOG_EMPTY_LINE psl::logging::get().log_empty_line()


namespace psl
{

enum class lvl
{
    base,
    info,
    debug,
    fullblown
};
extern psl::string to_string(lvl level);

class i_logger
{
public:
    i_logger();
    virtual ~i_logger();

    virtual void log(const psl::string &str) = 0;

    lvl level() const;
    void set_level(const lvl &level);

    bool is_active() const;
    void set_active(bool is_active);

    bool is_active_for_level(lvl level);

    bool is_registered() const;

private:
    bool m_active = true;
    lvl m_level = lvl::base;
};

class logger_stdout : public i_logger
{
public:
    void log(const psl::string &str) override;
};

class logger_stderr : public i_logger
{
public:
    void log(const psl::string &str) override;
};


///
/// \brief The logger_o_device class
///
template <typename ODevice>
class logger_o_device : public i_logger
{
public:
    template<typename ...Args>
    explicit logger_o_device(Args ...args)
        : o_device(psl::forward<Args>(args)...)
    {}

    void log(const psl::string &str) override
    {
        o_device.write(psl::string().sprintf("%s\n", str.c_str()).c_str());
    }

    ODevice o_device;
};


//class logger_iodevice : public i_logger
//{
//public:
//    logger_iodevice(psl::i_io_device &io_device)
//        : m_io_device(io_device)
//    {}

//    void log(const psl::string &str) override
//    {
//        m_io_device.write(psl::string::format("%s\n", str.c_str()).c_str());
//    }
//private:
//    psl::i_io_device &m_io_device;
//};


///
/// \brief The logging class
///
class logging
{
private:
    ///
    /// \brief logging - private constructor. this is a singleton. access with logging::get
    ///
    logging() = default;

public:
    ///
    /// \brief logging - deleted special member functions. copying not allowed for singleton.
    ///
    logging(const logging&) = delete;
    logging& operator=(const logging &) = delete;
    logging(logging &&) = delete;
    logging & operator=(logging &&) = delete;

    ///
    /// \brief get - access point for singleton
    /// \return singleton instance of class
    ///
    static logging& get();

    ///
    /// \brief register_logger - manually register a logger object with the logging singleton.
    ///                          alternatively use logger_base::set_auto_register
    ///
    /// \param          logger - object derived from logger_base to register
    ///
    void register_logger(i_logger *logger);

    ///
    /// \brief un_register_logger - manually unregister a logger object with the logging singleton.
    ///                             alternatively use logger_base::set_auto_register
    ///
    /// \param             logger - object derived from logger_base to unregister
    ///
    void un_register_logger(i_logger *logger);

    ///
    /// \brief is_registered
    /// \param logger
    /// \return
    ///
    bool is_registered(i_logger *logger) const;

    ///
    /// \brief set_level_all - set log level of all registered logger objects
    ///
    /// \param level         - log level to set as psl::lvl
    ///
    void set_level_all(lvl level);

    ///
    /// \brief log_pure - log line of text without extra info
    ///                   macro equivalent: LOG_PURE
    ///
    /// \param str      - string to log
    ///
    template<typename StringType, typename ...Args>
    void log_pure(const StringType &fmt, Args &&...args)
    {
        if (any_logger_active())
        {
            log_pure_unchecked(psl::format(fmt, args...));
        }
    }

    ///
    /// \brief log  - log text with file and line number info. pass HERE macro for ease of use.
    ///               macro equivalent: LOG
    ///
    /// \param file - filename as const char *
    ///
    /// \param line - line number as int
    ///
    /// \param fmt  - format string to log as string supported by psl::format
    ///
    /// \param args - format arguments
    ///
    template<typename StringType, typename ...Args>
    void log(const char *file, int line, const StringType &fmt, Args &&...args)
    {
        if (any_logger_active())
        {
            log_pure_unchecked(psl::format("[{}:{}] ", shorten_file_name(file), line) + psl::format(fmt, args...));
        }
    }

    ///
    /// \brief log   - if level is enabled for any logger, log text. pass HERE macro for ease of use.
    ///                macro equivalent: LOG_LEVEL_PURE
    ///
    /// \param level - level at which this line should be logged
    ///
    /// \param fmt   - format string to log as string supported by psl::format
    ///
    /// \param args  - format arguments
    ///
    template<typename StringType, typename ...Args>
    void log_level_pure(lvl level, const StringType &fmt, Args &&...args)
    {
        if (any_logger_for_lvl(level))
        {
            log_level_pure_unchecked(level, psl::format(fmt, args...));
        }
    }

    ///
    /// \brief log   - if level is enabled for any logger, log text with file and line number info. pass HERE macro for ease of use.
    ///                macro equivalent: LOG_LEVEL
    ///
    /// \param level - level at which this line should be logged
    ///
    /// \param file  - filename as const char *
    ///
    /// \param line  - line number as int
    ///
    /// \param fmt   - format string to log as string supported by psl::format
    ///
    /// \param args  - format arguments
    ///
    template<typename StringType, typename ...Args>
    void log_level(const char *file, int line, lvl level, const StringType &fmt, Args &&...args)
    {
        if (any_logger_for_lvl(level))
        {
            log_level_pure_unchecked(level, psl::format("[{}:{}] ", shorten_file_name(file), line) + psl::format(fmt, (args)...));
        }
    }

    void log_empty_line();
    void log_empty_lines(unsigned int n);

private:
    bool any_logger_active();
    bool any_logger_for_lvl(lvl level);

    void log_level_pure_unchecked(lvl level, const psl::string &str);
    void log_pure_unchecked(const psl::string &str);

    const char *shorten_file_name(const char *file);

private:
    psl::vector<i_logger *> m_loggers;
    psl::file m_log_file;

};

class log_entry
{
public:
    log_entry(const char *file, int line, const char *func, lvl level = lvl::fullblown);
    ~log_entry();

private:
    const char *m_file;
    int m_line;
    lvl m_level;
    psl::string m_str;
};

}
