#pragma once

#include <string.h>
#include <stddef.h>


namespace psl
{

template<size_t S>
class bitset
{
    using value_type = bool;
    using size_type = size_t;
    using difference_type = long int;
    using reference = value_type&;
    using const_reference = const value_type &;
    using pointer = value_type *;
    using const_pointer = const value_type *;

public:
    bitset() = default;

    void set(size_t n, value_type value)
    {
        size_t n_byte = n / 8;
        size_t n_bit = n % 8;

        char &byte = data[n_byte];

        if (value)
            byte |= 1U << n_bit;
        else
            byte &= ~(1UL << n_bit);
    }
    value_type get(size_t n) const
    {
        size_t n_byte = n / 8;
        size_t n_bit = n % 8;

        char byte = data[n_byte];

        value_type bit = (byte >> n_bit) & 1U;
        return bit;
    }

    void fill(value_type value)
    {
        char c = value ? 0xFF : 0x00;
        ::memset(&data[0], c, sz);
    }

    size_type size() const { return S; }

//    psl::bit_wrapper_thingy_ref operator[](size_t n)
//    {
//        size_t n_byte = n / 8;
//        size_t n_bit = n % 8;

//        char &byte = data[n_byte];
//    }


private:
    static constexpr size_t sz = (S / 8) + (((S % 8) != 0) ? 1 : 0);
    char data[sz] = {};
};

}
