/**
 * @file thread_pool.h
 * @brief Thread pool implementation for parallel task execution.
 */

#pragma once

#include "psl_atomic.h"
#include "psl_future.h"
#include "psl_thread.h"
#include "psl_vector.h"
#include "psl_message_queue.h"

namespace psl
{

namespace internal
{
struct thread_pool_data_base
{
    virtual ~thread_pool_data_base() = default;
    virtual void execute() = 0;
};

template<typename Ret>
struct thread_pool_data : public thread_pool_data_base
{
    template<typename Function>
    explicit thread_pool_data(Function &&f)
        : m_task(psl::forward<Function>(f))
    {}

    void execute() override
    {
        m_task();
    }

    psl::future<Ret> get_future() { return m_task.get_future(); }

    psl::packaged_task<Ret()> m_task; ///< Packaged task for deferred execution.
};
}

/**
 * @brief A thread pool for managing parallel task execution.
 */
class thread_pool
{
public:
    /**
     * @brief Default constructor.
     */
    thread_pool() = default;

    /**
     * @brief Constructs a thread pool with the specified number of threads.
     * @param num_threads The number of threads created.
     */
    explicit thread_pool(size_t num_threads);

    /**
     * @brief Destructor that cleans up resources.
     */
    ~thread_pool();

    /**
     * @brief Queues a task for execution.
     * @tparam Function Type of the callable function.
     * @param f The function to be executed asynchronously.
     * @return A future representing the result of the task.
     */
    template<typename Function>
    psl::future<psl::invoke_result_t<psl::decay_t<Function>>> queue_work(Function &&f)
    {
        auto data = new internal::thread_pool_data<psl::invoke_result_t<psl::decay_t<Function>>>(f);
        m_msg_queue.send(data);
        return data->get_future();
    }

private:
    size_t m_num_threads = 0; ///< Number of threads in the pool.
    psl::vector<psl::unique_ptr<psl::thread>> m_threads; ///< Container for worker threads.
    psl::message_queue<internal::thread_pool_data_base *> m_msg_queue; ///< Queue for storing tasks.
    psl::atomic<bool> m_stop = false; ///< Flag to signal thread pool shutdown.
};

}
