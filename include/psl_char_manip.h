#pragma once

#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <assert.h>


namespace psl
{

template<typename CharType>
size_t strlen(const CharType *str)
{
    size_t i = 0;
    while (str[i] != '\0'){ ++i; }
    return i;
}

template<typename CharType>
CharType *strcpy(CharType *dest, const CharType *src)
{
    if (dest == nullptr || src == nullptr)
        return nullptr;

    CharType *ptr = dest;

    while (*src != '\0')
    {
        *dest = *src;
        dest++;
        src++;
    }

    *dest = '\0';

    return ptr;
}

}
