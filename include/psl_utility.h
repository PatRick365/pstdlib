#pragma once

#include "psl_type_traits.h"


namespace psl
{

template <class T>
inline T&& forward(typename remove_reference<T>::type& t) noexcept
{
    return static_cast<T&&>(t);
}

template <class T>
inline T&& forward(typename remove_reference<T>::type&& t) noexcept
{
    static_assert(!is_lvalue_reference<T>::value, "Can not forward an rvalue as an lvalue.");
    return static_cast<T&&>(t);
}

template<class T>
typename remove_reference<T>::type &&move(T&& arg )
{
    return static_cast<typename psl::remove_reference<T>::type &&>(arg);
}

template<class T>
constexpr void swap(T& a, T& b)
{
    T temp = a;
    a = b;
    b = temp;
}

template <typename T1, typename T2>
class pair
{
public:
    /// constructors
    /// (1)
    constexpr pair()
        : first(T1()),
          second(T2())
    {}

    /// (2)
    constexpr pair(const T1 &first, const T2 &second)
        : first(first),
          second(second)
    {}

    /// (3)
    template<class U1, class U2>
    constexpr pair(U1 &&first, U2 &&second)
        : first(psl::move(first)),
          second(psl::move(second))
    {}

    /// (4)
    template<class U1, class U2>
    constexpr pair(const pair<U1, U2> &other)
        : first(other.first),
          second(other.second)
    {}

    /// (5)
    template<class U1, class U2>
    constexpr pair(const pair<U1, U2> &&other)
        : first(psl::move(other.first)),
          second(psl::move(other.second))
    {}

    /// (7)
    constexpr pair(const pair &other) = default;

    /// (8)
    constexpr pair(pair &&other) = default;

    /// assignment
    /// (1)
    constexpr pair &operator=(const pair &other)
    {
        first = other.first;
        second = other.second;
        return *this;
    }

    /// (2)
    template<class U1, class U2>
    constexpr pair &operator=(const pair<U1, U2> &other)
    {
        first = other.first;
        second = other.second;
        return *this;
    }

    /// (3)
    constexpr pair &operator=(const pair &&other)
    {
        first = psl::move(other.first);
        second = psl::move(other.second);
        return *this;
    }

    /// (4)
    template<class U1, class U2>
    constexpr pair &operator =(pair<U1, U2> &&other)
    {
        first = psl::move(other.first);
        second = psl::move(other.second);
        return *this;
    }

    T1 first;
    T2 second;
};

template <typename T1, typename T2>
constexpr pair<T1, T2> make_pair(T1 &&first, T2 &&second)
{
    //return pair<T1, T2>(psl::move(t1), psl::move(t2));
    return pair<T1, T2>(first, second);
}

}
