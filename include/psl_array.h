#pragma once

#include "psl_iterator.h"
#include "psl_except.h"
#include "psl_string.h"

#include <unistd.h>
#include <assert.h>


namespace psl
{

template<typename T, size_t S>
class array
{
public:
    class const_iterator;
    class iterator;
    using value_type = T;
    using size_type = size_t;
    using difference_type = long int; //__PTRDIFF_TYPE__;
    using reference = value_type&;
    using const_reference = const value_type &;
    using pointer = value_type *;
    using const_pointer = const value_type *;
    using iterator = array::iterator;
    using const_iterator = array::const_iterator;
    using reverse_iterator = psl::reverse_iterator<iterator>;
    using const_reverse_iterator = psl::reverse_iterator<const_iterator>;


/// element access
    reference operator[](size_type position)
    {
        assert(position < S);
        return values[position];
    }
    const_reference operator[](size_type position) const
    {
        assert(position < S);
        return values[position];
    }

    reference at(size_type position)
    {
        if (position >= S)
            throw psl::out_of_range(psl::string().sprintf("operator[] out of range: sz = %zu but position = %zu", S, position));
        return values[position];
    }
    const_reference at(size_type position) const
    {
        if (position >= S)
            throw psl::out_of_range(psl::string().sprintf("operator[] out of range: sz = %zu but position = %zu", S, position));
        return values[position];
    }

    const_reference front() const
    {
        assert(S != 0);
        return values[0];
    }

    const_reference back() const
    {
        assert(S != 0);
        return values[S - 1];
    }

    pointer data()
    {
        return values;
    }

    const_pointer data() const
    {
        return values;
    }

    void fill(const T &value)
    {
        psl::fill(begin(), end(), value);
    }

    constexpr void swap(array &other)
    {
        array arr = other;
        psl::copy(cbegin(), cend(), other.begin());
        psl::copy(arr.cbegin(), arr.cend(), begin());
    }


/// capacity
    size_type size() const
    {
        return S;
    }

    bool empty() const
    {
        return (S == 0);
    }

/// iterators
    iterator begin()
    {
        if (S == 0)
            return end();

        return iterator(values);
    }
    iterator end()
    {
        return iterator(values + S);
    }

    const_iterator begin() const
    {
        if (S == 0)
            return end();

        return iterator(values);
    }
    const_iterator end() const
    {
        return const_iterator(values + S);
    }

    const_iterator cbegin() const
    {
        if (S == 0)
            return end();

        return const_iterator(values);
    }
    const_iterator cend() const
    {
        return const_iterator(values + S);
    }

    reverse_iterator rbegin()
    {
        return reverse_iterator(end());
    }
    reverse_iterator rend()
    {
        return reverse_iterator(begin());
    }

    const_reverse_iterator crbegin() const
    {
        return const_reverse_iterator(cend());
    }
    const_reverse_iterator crend() const
    {
        return const_reverse_iterator(cbegin());
    }

public:
    value_type values[S];


/// iterator implementations
public:
    class iterator
    {
    public:
        using value_type = array::value_type;
        using reference = array::reference;
        using pointer = array::pointer;
        using difference_type = array::difference_type;
        using iterator_category = psl::contiguous_iterator_tag;

    public:
        iterator() = default;

        explicit iterator(pointer ptr)
        {
            this->ptr = ptr;
        }

        reference operator*() const
        {
            assert(ptr != nullptr);
            return *ptr;
        }

        pointer operator->() const
        {
            return ptr;
        }

        iterator& operator++()
        {
            assert(ptr != nullptr);
            ++ptr;
            return *this;
        }
        iterator operator++(int)
        {
            assert(ptr != nullptr);
            iterator it = *this;
            ++ptr;
            return it;
        }

        iterator& operator--()
        {
            assert(ptr != nullptr);
            --ptr;
            return *this;
        }
        iterator operator--(int)
        {
            assert(ptr != nullptr);
            iterator it = *this;
            --ptr;
            return it;
        }

        iterator operator+(difference_type difference) const
        {
            assert(ptr != nullptr);
            return iterator(ptr + difference);
        }
        iterator operator-(difference_type difference) const
        {
            assert(ptr != nullptr);
            return iterator(ptr - difference);
        }

        bool operator==(const const_iterator &it) const
        {
            return &(*it) == ptr;
        }
        bool operator!=(const const_iterator &it) const
        {
            return &(*it) != ptr;
        }

        operator const_iterator() const
        {
            return const_iterator(ptr);
        }

    private:
        pointer ptr = nullptr;
    };

    class const_iterator
    {
    public:
        using value_type = array::value_type;
        using reference = array::const_reference;
        using pointer = array::const_pointer;
        using difference_type = array::difference_type;
        using iterator_category = psl::contiguous_iterator_tag;

    public:
        const_iterator() = default;

        explicit const_iterator(pointer ptr)
            : ptr(ptr)
        {}

        const_reference operator*() const
        {
            assert(ptr != nullptr);
            return *ptr;
        }

        const_pointer operator->() const
        {
            return ptr;
        }

        const_iterator& operator++()
        {
            assert(ptr != nullptr);
            ++ptr;
            return *this;
        }
        const_iterator operator++(int)
        {
            assert(ptr != nullptr);
            const_iterator it = *this;
            ++ptr;
            return it;
        }

        const_iterator& operator--()
        {
            assert(ptr != nullptr);
            --ptr;
            return *this;
        }
        const_iterator operator--(int)
        {
            assert(ptr != nullptr);
            const_iterator it = *this;
            --ptr;
            return it;
        }

        bool operator==(const const_iterator &it) const
        {
            return it.ptr == ptr;
        }
        bool operator!=(const const_iterator &it) const
        {
            return it.ptr != ptr;
        }

        array::difference_type operator+(const const_iterator &rop)
        {
            return ptr + rop.ptr;
        }
        array::difference_type operator-(const const_iterator &rop)
        {
            return ptr - rop.ptr;
        }

    private:
        const_pointer ptr = nullptr;
    };
};

template< class T, size_t N >
bool operator==(const psl::array<T,N>& lhs,
                const psl::array<T,N>& rhs )
{
    for (size_t i = 0; i < N; ++i)
    {
        if (lhs[i] != rhs[i])
        {
            return false;
        }
    }
    return true;
}

template< class T, size_t N >
bool operator!=( const psl::array<T,N>& lhs,
                 const psl::array<T,N>& rhs )
{
    return !(lhs == rhs);
}

template <class T, size_t N>
constexpr void swap(array<T, N> &lhs, array<T, N> &rhs)
{
    lhs.swap(rhs);
}

//template< class T, size_t N >
//bool operator<( const psl::array<T,N>& lhs,
//                const psl::array<T,N>& rhs )
//{
//    for (size_t i = 0; i < N; ++i)
//    {
//        if (lhs[i] >= rhs[i])
//        {
//            return false;
//        }
//    }
//    return true;
//}

//template< class T, size_t N >
//bool operator<=( const psl::array<T,N>& lhs,
//                 const psl::array<T,N>& rhs )
//{
//    return !(lhs > rhs);
//}

//template< class T, size_t N >
//bool operator>( const psl::array<T,N>& lhs,
//                const psl::array<T,N>& rhs )
//{
//    return !(lhs <= rhs);
//}

//template< class T, size_t N >
//bool operator>=( const psl::array<T,N>& lhs,
//                 const psl::array<T,N>& rhs )
//{
//    return !(lhs < rhs);
//}

}
