#pragma once

#pragma once

#include "psl_utility.h"
#include "psl_format.h"
//#include "psl_exception.h"
#include "psl_initializer_list.h"

#include <unistd.h>
#include <stdio.h>


namespace psl
{
template<typename T>
class list
{
public:
    class const_iterator;
    class iterator;
    using value_type = T;
    using size_type = size_t;
    using difference_type = long int; //__PTRDIFF_TYPE__;
    using reference = value_type&;
    using const_reference = const value_type &;
    using pointer = value_type *;
    using const_pointer = const value_type *;
    using iterator = list::iterator;
    using const_iterator = list::const_iterator;
    //using node_ptr = node *;

private:
    struct node
    {
        node() = default;
        explicit node(const T &value)
            : m_value(value)
        {}
        explicit node(T &&value)
            : m_value(psl::move(value))
        {}

//        node(const node &) = default;
//        node &operator=(const node &) = default;
//        node(node &&) = default;
//        node &operator=(node &&) = default;

        T m_value;
        node *m_next = nullptr;
        node *m_prev = nullptr;

//        node *operator++()
//        {
//            // this = this->m_next; // hihi
//            return this;
//        }
    };

public:
    list() = default;

    explicit list(size_type count)
        : list(count, value_type())
    {}

    list(const list &other)
    {
        clear();

        for (const auto &val : other)
        {
            push_back(val);
        }
    }

    list(size_type count, const_reference value)
    {
        for (size_type i = 0; i < count; ++i)
            push_back(value);
    }

    template<typename ...Args>
    list(psl::il_tag, Args &&...args)
    {
        psl::initializer_list_push_back(*this, psl::forward<Args>(args)...);
    }

    list(list<const value_type> &other)
    {
        for (const auto &val : other)
        {
            push_back(val);
        }
    }

    list(list<value_type> &&other)
    {
        m_head = other.m_head;
        m_tail = other.m_tail;
        m_sz = other.m_sz;

        other.m_head = nullptr;
        other.m_tail = nullptr;
        other.m_sz = 0;
    }



    ~list()
    {
        clear();
    }

    list &operator=(const list &other)
    {
        clear();

        for (const auto &val : other)
        {
            push_back(val);
        }
        return *this;
    }

    void push_back(const_reference value)
    {
        if (m_head == nullptr)
        {
            m_head = new node(value);
            m_tail = m_head;
        }
        else
        {
            node *new_node = new node(value);
            m_tail->m_next = new_node;
            new_node->m_prev = m_tail;
            m_tail = new_node;
        }
        ++m_sz;
    }

    void push_back(value_type &&value)
    {
        if (m_head == nullptr)
        {
            m_head = new node(psl::forward<value_type>(value));
            m_tail = m_head;
        }
        else
        {
            node *new_node = new node(psl::forward<value_type>(value));
            m_tail->m_next = new_node;
            new_node->m_prev = m_tail;
            m_tail = new_node;
        }
        ++m_sz;
    }

    list &operator<<(const_reference value)
    {
        push_back(value);
        return *this;
    }

    void push_front(const_reference value)
    {
        if (m_head == nullptr)
        {
            m_head = new node(value);
            m_tail = m_head;
        }
        else
        {
            node *new_node = new node(value);
            m_head->m_prev = new_node;
            new_node->m_next = m_head;
            m_head = new_node;
        }
        ++m_sz;
    }

//    iterator insert(const_iterator pos, const_reference value)
//    {
//        if (m_sz == 0)
//        {
//            if (pos == end())
//            {
//                m_head = new node(value);
//                m_tail = m_head;
//            }
//            else
//            {
//                throw "OOOPS!!!";
//            }
//        }
//        else
//        {
//            if (pos.m_node->m_prev != nullptr)
//            {

//            }
//        }

//        node *new_node = new node(value);
//        //pos.m_node

//        ++m_sz;
//    }

    void clear()
    {
        node *trav = m_head;

        while (trav != nullptr)
        {
            if (trav->m_next != nullptr)
            {
                trav = trav->m_next;
                delete trav->m_prev;
            }
            else
            {
                delete trav;
                break;
            }
        }

        m_head = nullptr;
        m_tail = nullptr;
    }

    reference front()
    { return m_head->m_value; }

    reference back()
    { return m_tail->m_value; }

    size_type size() const
    { return m_sz; }

    bool empty() const
    {
        return (m_sz == 0) && (m_head == nullptr) && (m_tail == nullptr);
    }

    void dump()
    {
        node *trav = m_head;

        psl::print("-v-dump--\nsz: {}\nvalues: ", m_sz);

        while (trav != nullptr)
        {
            psl::print("%d ", trav->m_value);
            trav = trav->m_next;
        }
        psl::print("\n-^-------\n\n");
    }

    void rdump()
    {
        node *trav = m_tail;

        psl::print("-v-rdump-\nsz: {}\nvalues: ", m_sz);

        while (trav != nullptr)
        {
            psl::print("{} ", trav->m_value);
            trav = trav->m_prev;
        }
        psl::print("\n-^-------\n\n");
    }

    /// iterators
    iterator begin()
    {
        return iterator(m_head);
    }
    iterator end()
    {
        // wrong ??? but how do ???
        return iterator(nullptr);
    }

    const_iterator begin() const
    {
        return const_iterator(m_head);
    }
    const_iterator end() const
    {
        return const_iterator(nullptr);
    }

    const_iterator cbegin() const
    {
        return const_iterator(m_head);
    }
    const_iterator cend() const
    {
        return const_iterator(nullptr);
    }

    /// iterator implementations
public:
    class iterator
    {
    public:
        using value_type = list::value_type;
        using reference = list::reference;
        using pointer = list::pointer;
        using difference_type = list::difference_type;

    public:
        iterator()
        {
            m_node = nullptr;
        }

        explicit iterator(node *ptr)
        {
            m_node = ptr;
        }

        reference operator*() const
        {
            return m_node->m_value;
        }

        pointer operator->() const
        {
            return &(m_node->m_value);
        }

        iterator& operator++()
        {
            m_node = m_node->m_next;
            return *this;
        }

        iterator operator++(int)
        {
            iterator it = *this;
            m_node = m_node->m_next;
            return it;
        }

//        iterator operator+ (difference_type difference) const
//        {
//            return iterator(m_ptr + difference);
//        }

//        iterator operator- (difference_type difference) const
//        {
//            return iterator(m_ptr - difference);
//        }

        bool operator==(const const_iterator &it) const
        {
            return it == const_iterator(m_node);
        }

        bool operator!=(const const_iterator &it) const
        {
            return it != const_iterator(m_node);;
        }

        operator const_iterator() const
        {
            return const_iterator(m_node);
        }

    private:
        node *m_node;
    };

    class const_iterator
    {
    public:
        using value_type = list::value_type;
        using reference = list::reference;
        using pointer = list::pointer;
        using difference_type = list::difference_type;

    public:
        const_iterator()
        {
            m_node = nullptr;
        }

        explicit const_iterator(node *ptr)
        {
            m_node = ptr;
        }

        reference operator*() const
        {
            return m_node->m_value;
        }

        node *operator->() const
        {
            return m_node;
        }

        const_iterator& operator++()
        {
            m_node = m_node->m_next;
            return *this;
        }

        const_iterator operator++(int)
        {
            const_iterator it = *this;
            m_node = m_node->m_next;
            return it;
        }

        bool operator==(const const_iterator &it) const
        {
            return it.m_node == m_node;
        }

        bool operator!=(const const_iterator &it) const
        {
            return it.m_node != m_node;
        }

//        vector::difference_type operator-(const const_iterator &rop)
//		{
//			return ptr - rop.ptr;
//		}

        node *m_node;
    };

private:
    node *m_head = nullptr;
    node *m_tail = nullptr;

    size_t m_sz = 0;

};

}
