#pragma once

#include <pthread.h>


namespace psl
{

class mutex
{
public:
    mutex();
    ~mutex();

    bool take();
    bool try_take();

    bool give();

    pthread_mutex_t &native_handle();

private:
    pthread_mutexattr_t m_attr{};
    pthread_mutex_t m_pthread_mutex{};
};

class region_lock
{
public:
    explicit region_lock(mutex &mut);

    ~region_lock() noexcept(false);
    // nope, leads to double give in destructor.. a flag would not be thread safe..
    //    bool take() { return m_mut.take(); }
    //    bool give() { return m_mut.give(); }

private:
    mutex &m_mut;
};


}
