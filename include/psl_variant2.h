#pragma once

#include "psl_memory.h"
#include "psl_except.h"
#include "psl_string.h"


namespace psl
{

PSL_DEFINE_EXCEPTION(variant_exception, exception);
PSL_DEFINE_EXCEPTION(invalid_variant_exception, variant_exception);


class variant2
{
public:
    variant2() = default;

    static variant2 null;

    template<typename U>
    variant2(const U &value)
    {
        set<U>(value);
    }

    variant2(const variant2 &other)
    {
        if (other.m_value)
        {
            m_value = other.m_value->clone();
        }
    }

    variant2(variant2 &&other)
    {
        m_value.reset(other.m_value.release());
    }

    variant2& operator=(const psl::variant2 &other)
    {
        if (other.m_value)
        {
            m_value = other.m_value->clone();
        }
        else
        {
            m_value.reset();
        }
        return *this;
    }

    variant2& operator=(psl::variant2 &&other)
    {
        m_value.reset(other.m_value.release());
        return *this;
    }

    bool valid() const
    {
        return (m_value.get() != nullptr);
    }

    operator bool() const
    {
        return valid();
    }

    template<typename U>
    variant2 &operator=(const U &value)
    {
        set(value);
        return *this;
    }

    template<typename U>
    void set(const U &value)
    {
        m_value = psl::make_unique<content<U>>(value);
    }
    template<typename U>
    void set(U &&value)
    {
        m_value = psl::make_unique<content<U>>(psl::forward<U>(value));
    }

    template<typename U>
    const U &get() const
    {
        if (!valid())
            throw invalid_variant_exception("invalid variant");
        return dynamic_cast<content<U> *>(m_value.get())->value;
    }
    template<typename U>
    U &get()
    {
        if (!valid())
            throw invalid_variant_exception("invalid variant");
        return dynamic_cast<content<U> *>(m_value.get())->value;
    }

    template<typename U>
    bool is() const
    {
        return (dynamic_cast<content<U> *>(m_value.get()) != nullptr);
    }

    void reset()
    {
        m_value.reset();
    }

    void swap(variant2 &other)
    {
        m_value.swap(other.m_value);
    }

private:
    struct content_base
    {
        virtual ~content_base() = default;
        virtual content_base *clone() const = 0;
    };

    template<typename T>
    struct content : public content_base
    {
        content(const T &v)
            : value(v)
        {}
        content(T &&v)
            : value(psl::move(v))
        {}
        content *clone() const override
        {
           return new content(*this);
        }
        T value;
    };

    psl::unique_ptr<content_base> m_value;
};

void swap(variant2 &lhs, variant2 &rhs)
{
    lhs.swap(rhs);
}

template<typename T>
T get(const psl::variant2 &variant)
{
    return variant.get<T>();
}

template<typename R, typename T, typename Visitor>
R visit(Visitor &&vis, const psl::variant2 &variant)
{
    return static_cast<R>(vis(psl::get<T>(variant)));
}

}


