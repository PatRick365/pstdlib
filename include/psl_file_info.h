#pragma once

#include "psl_string.h"

#include <sys/stat.h>


namespace psl
{

class file_info
{
public:
    enum class type { invalid, file, dir, char_dev, block_dev, named_pipe, sym_link, socket };
    static psl::string to_string(type t);

    file_info() = default;
    explicit file_info(int file_descriptor);
    explicit file_info(const psl::string &path);

    bool is_type(type t) const;

    ssize_t size() const;

    bool exists() const;

    bool is_regular_file() const;
    bool is_directory() const;
    bool is_symlink() const;

private:
    bool m_is_init = false;
    bool m_exists = false;
    struct stat m_stat {};
};

}
