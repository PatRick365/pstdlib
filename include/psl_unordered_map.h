#pragma once

#include <unistd.h>
#include <stdlib.h>
#include <cstdio>

#include "psl_vector.h"
#include "psl_utility.h"
#include "psl_algorithm.h"


namespace psl
{

template<typename Key, typename Value, typename Alloc = psl::allocator<psl::pair<Key, Value>>>
class unordered_map
{
public:
    using size_type = typename psl::vector<psl::pair<Key, Value>>::size_type;
    using value_type = psl::pair<Key, Value>;
    using iterator = typename psl::vector<psl::pair<Key, Value>>::iterator;
    using const_iterator = typename psl::vector<psl::pair<Key, Value>>::const_iterator;
    using allocator_type = Alloc;
    using difference_type = long int;
    using reference = value_type &;
    using const_reference = const value_type &;
    using pointer = value_type *;
    using const_pointer = const value_type *;
    using reverse_iterator = psl::reverse_iterator<iterator>;
    using const_reverse_iterator = psl::reverse_iterator<const_iterator>;


public:
    unordered_map() = default;

    const_iterator find(const Key &key) const
    {
        return psl::find_if(m_values.cbegin(), m_values.cend(), [&key](const value_type &v)
        {
            return (v.first == key);
        });
    }
    iterator find(const Key &key)
    {
        return psl::find_if(m_values.begin(), m_values.end(), [&key](const value_type &v)
        {
            return (v.first == key);
        });
    }

    iterator insert(const Key &key, const Value &value)
    {
        iterator it = find(key);

        if (it != end())
        {
            it->first = key;
            it->second = value;
            return it;
        }

        m_values.emplace_back(key, value);
        return --m_values.end();
    }

    Value &operator[](const Key &key)
    {
        iterator it = find(key);

        if (it != end())
        {
            return it->second;
        }
        return insert(key, Value())->second;
    }

    void erase(iterator it)
    {
        m_values.erase(it);
    }

    void erase(const Key &key)
    {
        erase(find(key));
    }

    size_type size() const
    {
        return m_values.size();
    }

    iterator begin() { return m_values.begin(); }
    iterator end() { return m_values.end(); }
    const_iterator begin() const { return m_values.begin(); }
    const_iterator end() const { return m_values.end(); }
    const_iterator cbegin() const { return m_values.cbegin(); }
    const_iterator cend() const { return m_values.cend(); }

    reverse_iterator rbegin() { return m_values.rbegin(); }
    reverse_iterator rend() { return m_values.rend(); }
    const_reverse_iterator rbegin() const { return m_values.rbegin(); }
    const_reverse_iterator rend() const { return m_values.rend(); }
    const_reverse_iterator crbegin() const { return m_values.crbegin(); }
    const_reverse_iterator crend() const { return m_values.crend(); }

private:
    psl::vector<psl::pair<Key, Value>> m_values;
};

}
