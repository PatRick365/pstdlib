#pragma once

#include "psl_string.h"


namespace psl
{

template<typename T>
T from_string(const char *str, bool *ok = nullptr);

#define GEN_READER(type, fmt)                                                                                          \
    template<>                                                                                                         \
    type from_string<type>(const char *str, bool *ok)                                                                  \
    {                                                                                                                  \
        type value{};                                                                                                  \
        int ret = sscanf(str, fmt, &value);                                                                            \
        if (ok)                                                                                                        \
        {                                                                                                              \
            *ok = (ret == 1);                                                                                          \
        }                                                                                                              \
        return value;                                                                                                  \
    }

GEN_READER(char, "%c");
GEN_READER(unsigned char, "%hhu");
GEN_READER(int, "%d");
GEN_READER(unsigned int, "%u");
GEN_READER(long, "%ld");
GEN_READER(unsigned long, "%lu");
GEN_READER(long long, "%lld");
GEN_READER(unsigned long long, "%llu");
GEN_READER(float, "%f");
GEN_READER(double, "%lf");
GEN_READER(long double, "%Lf");
#undef GEN_READER

template<typename T>
T from_string(const psl::string &str, bool *ok = nullptr)
{
    return from_string<T>(str.data(), ok);
}

}
