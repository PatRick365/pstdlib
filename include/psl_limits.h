#pragma once

#include "psl_math.h"

#include <stdint.h>
#include <limits.h>
#include <stdint.h>
#include <float.h>


namespace psl
{

enum float_round_style
{
    round_indeterminate       = -1,
    round_toward_zero         = 0,
    round_to_nearest          = 1,
    round_toward_infinity     = 2,
    round_toward_neg_infinity = 3
};

enum float_denorm_style
{

    denorm_indeterminate = -1,
    denorm_absent        = 0,
    denorm_present       = 1
};

template <typename T>
class numeric_limits
{
public:
    //identifies types for which psl::numeric_limits is specialized
    static constexpr bool is_specialized = false;

    //identifies signed types
    static constexpr bool is_signed = false;

    // identifies integer types
    static constexpr bool is_integer = false;

    // identifies exact types
    static constexpr bool is_exact = false;

    // identifies floating-point types that can represent the special value "positive infinity"
    static constexpr bool has_infinity = false;

    // identifies floating-point types that can represent the special value "quiet not-a-number" (NaN)
    static constexpr bool has_quiet_NaN = false;

    // identifies floating-point types that can represent the special value "signaling not-a-number" (NaN)
    static constexpr bool has_signaling_NaN = false;

    // identifies the denormalization style used by the floating-point type
    static constexpr float_denorm_style has_denorm = denorm_absent;

    // identifies the floating-point types that detect loss of precision as denormalization loss rather than inexact result
    static constexpr bool has_denorm_loss = false;

    // identifies the rounding style used by the type
    static constexpr float_round_style round_style = round_toward_zero;

    // identifies the IEC 559/IEEE 754 floating-point types
    static constexpr bool is_iec559 = false;

    // identifies types that represent a finite set of values
    static constexpr bool is_bounded = false;

    // identifies types that handle overflows with modulo arithmetic
    static constexpr bool is_modulo = false;

    // number of radix digits that can be represented without change
    static constexpr int digits = 0;

    // number of decimal digits that can be represented without change
    static constexpr int digits10 = 0;

    // number of decimal digits necessary to differentiate all values of this type
    static constexpr int max_digits10 = 0;

    // the radix or integer base used by the representation of the given type
    static constexpr int radix = 0;

    // one more than the smallest negative power of the radix that is a valid normalized floating-point value
    static constexpr int min_exponent = 0;

    // the smallest negative power of ten that is a valid normalized floating-point value
    static constexpr int min_exponent10 = 0;

    // one more than the largest integer power of the radix that is a valid finite floating-point value
    static constexpr int max_exponent = 0;

    // the largest integer power of 10 that is a valid finite floating-point value
    static constexpr int max_exponent10 = 0;

    // identifies types which can cause arithmetic operations to trap
    static constexpr bool traps = false;

    // identifies floating-point types that detect tinyness before rounding
    static constexpr bool tinyness_before = false;

    // returns the smallest finite value of the given type
    static constexpr T min() { return T(); }

    // returns the lowest finite value of the given type
    static constexpr T lowest() { return T(); }

    // returns the largest finite value of the given type
    static constexpr T max() { return T(); }

    // returns the difference between 1.0 and the next representable value of the given floating-point type
    static constexpr T epsilon() { return T(); }

    // returns the maximum rounding error of the given floating-point type
    static constexpr T round_error() { return T(); }

    // returns the positive infinity value of the given floating-point type
    static constexpr T infinity() { return T(); }

    // returns a quiet NaN value of the given floating-point type
    static constexpr T quiet_NaN() { return T(); }

    // returns a signaling NaN value of the given floating-point type
    static constexpr T signaling_NaN() { return T(); }

    // returns the smallest positive subnormal value of the given floating-point type
    static constexpr T denorm_min() { return T(); }
};

/// specializations

template<> class numeric_limits<bool>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = false;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = 1;
    static constexpr int digits10 = 0;
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = false;
    static constexpr bool tinyness_before = false;

    static constexpr bool min() { return false; }
    static constexpr bool lowest() { return min(); }
    static constexpr bool max() { return true; }
    static constexpr bool epsilon() { return false; }
    static constexpr bool round_error() { return false; }
    static constexpr bool infinity() { return false; }
    static constexpr bool quiet_NaN() { return false; }
    static constexpr bool signaling_NaN() { return false; }
    static constexpr bool denorm_min() { return false; }
};

template<> class numeric_limits<char>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = true; // gcc
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = CHAR_BIT - numeric_limits<char>::is_signed;
    static constexpr int digits10 = numeric_limits<char>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr char min() { return CHAR_MIN; }
    static constexpr char lowest() { return min(); }
    static constexpr char max() { return CHAR_MAX; }
    static constexpr char epsilon() { return 0; }
    static constexpr char round_error() { return 0; }
    static constexpr char infinity() { return 0; }
    static constexpr char quiet_NaN() { return 0; }
    static constexpr char signaling_NaN() { return 0; }
    static constexpr char denorm_min() { return 0; }
};

template<> class numeric_limits<signed char>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = true;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = CHAR_BIT - 1;
    static constexpr int digits10 = numeric_limits<signed char>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr signed char min() { return SCHAR_MIN; }
    static constexpr signed char lowest() { return min(); }
    static constexpr signed char max() { return SCHAR_MAX; }
    static constexpr signed char epsilon() { return 0; }
    static constexpr signed char round_error() { return 0; }
    static constexpr signed char infinity() { return 0; }
    static constexpr signed char quiet_NaN() { return 0; }
    static constexpr signed char signaling_NaN() { return 0; }
    static constexpr signed char denorm_min() { return 0; }
};

template<> class numeric_limits<unsigned char>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = false;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = true;
    static constexpr int digits = CHAR_BIT;
    static constexpr int digits10 = numeric_limits<unsigned char>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr unsigned char min() { return 0; }
    static constexpr unsigned char lowest() { return min(); }
    static constexpr unsigned char max() { return UCHAR_MAX; }
    static constexpr unsigned char epsilon() { return 0; }
    static constexpr unsigned char round_error() { return 0; }
    static constexpr unsigned char infinity() { return 0; }
    static constexpr unsigned char quiet_NaN() { return 0; }
    static constexpr unsigned char signaling_NaN() { return 0; }
    static constexpr unsigned char denorm_min() { return 0; }
};

template<> class numeric_limits<wchar_t>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = true; // gcc
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = CHAR_BIT * sizeof(wchar_t) - numeric_limits<wchar_t>::is_signed;
    static constexpr int digits10 = numeric_limits<wchar_t>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr wchar_t min() { return WCHAR_MIN; }
    static constexpr wchar_t lowest() { return min(); }
    static constexpr wchar_t max() { return WCHAR_MAX; }
    static constexpr wchar_t epsilon() { return 0; }
    static constexpr wchar_t round_error() { return 0; }
    static constexpr wchar_t infinity() { return 0; }
    static constexpr wchar_t quiet_NaN() { return 0; }
    static constexpr wchar_t signaling_NaN() { return 0; }
    static constexpr wchar_t denorm_min() { return 0; }
};

template<> class numeric_limits<char8_t>    // C++20 feature
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = false;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = true;
    static constexpr int digits = CHAR_BIT;
    static constexpr int digits10 = numeric_limits<char8_t>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr char8_t min() { return 0; }
    static constexpr char8_t lowest() { return min(); }
    static constexpr char8_t max() { return UCHAR_MAX; }
    static constexpr char8_t epsilon() { return 0; }
    static constexpr char8_t round_error() { return 0; }
    static constexpr char8_t infinity() { return 0; }
    static constexpr char8_t quiet_NaN() { return 0; }
    static constexpr char8_t signaling_NaN() { return 0; }
    static constexpr char8_t denorm_min() { return 0; }
};

template<> class numeric_limits<char16_t>   // C++11 feature
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = false;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = true;
    static constexpr int digits = CHAR_BIT * sizeof(char16_t);
    static constexpr int digits10 = numeric_limits<char16_t>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr char16_t min() { return 0; }
    static constexpr char16_t lowest() { return min(); }
    static constexpr char16_t max() { return UINT_LEAST16_MAX; }
    static constexpr char16_t epsilon() { return 0; }
    static constexpr char16_t round_error() { return 0; }
    static constexpr char16_t infinity() { return 0; }
    static constexpr char16_t quiet_NaN() { return 0; }
    static constexpr char16_t signaling_NaN() { return 0; }
    static constexpr char16_t denorm_min() { return 0; }
};

template<> class numeric_limits<char32_t>   // C++11 feature
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = false;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = true;
    static constexpr int digits = CHAR_BIT * sizeof(char32_t);
    static constexpr int digits10 = numeric_limits<char32_t>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr char32_t min() { return 0; }
    static constexpr char32_t lowest() { return min(); }
    static constexpr char32_t max() { return UINT_LEAST32_MAX ; }
    static constexpr char32_t epsilon() { return 0; }
    static constexpr char32_t round_error() { return 0; }
    static constexpr char32_t infinity() { return 0; }
    static constexpr char32_t quiet_NaN() { return 0; }
    static constexpr char32_t signaling_NaN() { return 0; }
    static constexpr char32_t denorm_min() { return 0; }
};

template<> class numeric_limits<short>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = true;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = CHAR_BIT * sizeof(short) - 1;
    static constexpr int digits10 = numeric_limits<short>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr short min() { return SHRT_MIN; }
    static constexpr short lowest() { return min(); }
    static constexpr short max() { return SHRT_MAX; }
    static constexpr short epsilon() { return 0; }
    static constexpr short round_error() { return 0; }
    static constexpr short infinity() { return 0; }
    static constexpr short quiet_NaN() { return 0; }
    static constexpr short signaling_NaN() { return 0; }
    static constexpr short denorm_min() { return 0; }
};

template<> class numeric_limits<unsigned short>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = false;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = true;
    static constexpr int digits = CHAR_BIT * sizeof(short);
    static constexpr int digits10 = numeric_limits<unsigned short>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr unsigned short min() { return 0; }
    static constexpr unsigned short lowest() { return min(); }
    static constexpr unsigned short max() { return USHRT_MAX; }
    static constexpr unsigned short epsilon() { return 0; }
    static constexpr unsigned short round_error() { return 0; }
    static constexpr unsigned short infinity() { return 0; }
    static constexpr unsigned short quiet_NaN() { return 0; }
    static constexpr unsigned short signaling_NaN() { return 0; }
    static constexpr unsigned short denorm_min() { return 0; }
};

template<> class numeric_limits<int>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = true;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = CHAR_BIT * sizeof(int) - 1;
    static constexpr int digits10 = numeric_limits<int>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr int min() { return INT_MIN; }
    static constexpr int lowest() { return min(); }
    static constexpr int max() { return INT_MAX; }
    static constexpr int epsilon() { return 0; }
    static constexpr int round_error() { return 0; }
    static constexpr int infinity() { return 0; }
    static constexpr int quiet_NaN() { return 0; }
    static constexpr int signaling_NaN() { return 0; }
    static constexpr int denorm_min() { return 0; }
};

template<> class numeric_limits<unsigned int>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = false;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = true;
    static constexpr int digits = CHAR_BIT * sizeof(int);
    static constexpr int digits10 = numeric_limits<unsigned int>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr unsigned int min() { return 0; }
    static constexpr unsigned int lowest() { return min(); }
    static constexpr unsigned int max() { return UINT_MAX; }
    static constexpr unsigned int epsilon() { return 0; }
    static constexpr unsigned int round_error() { return 0; }
    static constexpr unsigned int infinity() { return 0; }
    static constexpr unsigned int quiet_NaN() { return 0; }
    static constexpr unsigned int signaling_NaN() { return 0; }
    static constexpr unsigned int denorm_min() { return 0; }
};

template<> class numeric_limits<long>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = true;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = CHAR_BIT * sizeof(long) - 1;
    static constexpr int digits10 = numeric_limits<long>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr long min() { return LONG_MIN; }
    static constexpr long lowest() { return min(); }
    static constexpr long max() { return LONG_MAX; }
    static constexpr long epsilon() { return 0; }
    static constexpr long round_error() { return 0; }
    static constexpr long infinity() { return 0; }
    static constexpr long quiet_NaN() { return 0; }
    static constexpr long signaling_NaN() { return 0; }
    static constexpr long denorm_min() { return 0; }
};

template<> class numeric_limits<unsigned long>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = false;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = true;
    static constexpr int digits = CHAR_BIT * sizeof(long);
    static constexpr int digits10 = numeric_limits<unsigned long>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr unsigned long min() { return 0; }
    static constexpr unsigned long lowest() { return min(); }
    static constexpr unsigned long max() { return ULONG_MAX; }
    static constexpr unsigned long epsilon() { return 0; }
    static constexpr unsigned long round_error() { return 0; }
    static constexpr unsigned long infinity() { return 0; }
    static constexpr unsigned long quiet_NaN() { return 0; }
    static constexpr unsigned long signaling_NaN() { return 0; }
    static constexpr unsigned long denorm_min() { return 0; }
};

template<> class numeric_limits<long long>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = true;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = CHAR_BIT * sizeof(long long) - 1;
    static constexpr int digits10 = numeric_limits<long long>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr long long min() { return LLONG_MIN; }
    static constexpr long long lowest() { return min(); }
    static constexpr long long max() { return LLONG_MAX; }
    static constexpr long long epsilon() { return 0; }
    static constexpr long long round_error() { return 0; }
    static constexpr long long infinity() { return 0; }
    static constexpr long long quiet_NaN() { return 0; }
    static constexpr long long signaling_NaN() { return 0; }
    static constexpr long long denorm_min() { return 0; }
};

template<> class numeric_limits<unsigned long long>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = false;
    static constexpr bool is_integer = true;
    static constexpr bool is_exact = true;
    static constexpr bool has_infinity = false;
    static constexpr bool has_quiet_NaN = false;
    static constexpr bool has_signaling_NaN = false;
    static constexpr float_denorm_style has_denorm = denorm_absent;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_toward_zero;
    static constexpr bool is_iec559 = false;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = true;
    static constexpr int digits = CHAR_BIT * sizeof(long long);
    static constexpr int digits10 = numeric_limits<unsigned long long>::digits * psl::log10(2);
    static constexpr int max_digits10 = 0;
    static constexpr int radix = 2;
    static constexpr int min_exponent = 0;
    static constexpr int min_exponent10 = 0;
    static constexpr int max_exponent = 0;
    static constexpr int max_exponent10 = 0;
    static constexpr bool traps = true;
    static constexpr bool tinyness_before = false;

    static constexpr unsigned long long min() { return 0; }
    static constexpr unsigned long long lowest() { return min(); }
    static constexpr unsigned long long max() { return ULLONG_MAX; }
    static constexpr unsigned long long epsilon() { return 0; }
    static constexpr unsigned long long round_error() { return 0; }
    static constexpr unsigned long long infinity() { return 0; }
    static constexpr unsigned long long quiet_NaN() { return 0; }
    static constexpr unsigned long long signaling_NaN() { return 0; }
    static constexpr unsigned long long denorm_min() { return 0; }
};

template<> class numeric_limits<float>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = true;
    static constexpr bool is_integer = false;
    static constexpr bool is_exact = false;
    static constexpr bool has_infinity = true;
    static constexpr bool has_quiet_NaN = true;
    static constexpr bool has_signaling_NaN = true;
    static constexpr float_denorm_style has_denorm = denorm_present;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_to_nearest;
    static constexpr bool is_iec559 = true;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = FLT_MANT_DIG;
    static constexpr int digits10 = FLT_DIG; /* 6 for IEEE float */
    static constexpr int max_digits10 = __FLT_DECIMAL_DIG__;
    static constexpr int radix = FLT_RADIX;
    static constexpr int min_exponent = FLT_MIN_EXP;
    static constexpr int min_exponent10 = FLT_MIN_10_EXP;
    static constexpr int max_exponent = FLT_MAX_EXP;
    static constexpr int max_exponent10 = FLT_MAX_10_EXP;
    static constexpr bool traps = false;
    static constexpr bool tinyness_before = false;

    static constexpr float min() { return FLT_MIN; }
    static constexpr float lowest() { return -FLT_MAX; }
    static constexpr float max() { return FLT_MAX; }
    static constexpr float epsilon() { return FLT_EPSILON; }
    static constexpr float round_error() { return 0.5F; }
    static constexpr float infinity() { return __builtin_huge_valf(); }
    static constexpr float quiet_NaN() { return __builtin_nanf(""); }
    static constexpr float signaling_NaN() { return __builtin_nansf(""); }
    static constexpr float denorm_min() { return __FLT_DENORM_MIN__; }
};

template<> class numeric_limits<double>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = true;
    static constexpr bool is_integer = false;
    static constexpr bool is_exact = false;
    static constexpr bool has_infinity = true;
    static constexpr bool has_quiet_NaN = true;
    static constexpr bool has_signaling_NaN = true;
    static constexpr float_denorm_style has_denorm = denorm_present;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_to_nearest;
    static constexpr bool is_iec559 = true;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = DBL_MANT_DIG;
    static constexpr int digits10 = DBL_DIG; /* 15 for IEEE double */
    static constexpr int max_digits10 = __DBL_DECIMAL_DIG__;
    static constexpr int radix = FLT_RADIX;
    static constexpr int min_exponent = DBL_MIN_EXP;
    static constexpr int min_exponent10 = DBL_MIN_10_EXP;
    static constexpr int max_exponent = DBL_MAX_EXP;
    static constexpr int max_exponent10 = DBL_MAX_10_EXP;
    static constexpr bool traps = false;
    static constexpr bool tinyness_before = false;

    static constexpr double min() { return DBL_MIN; }
    static constexpr double lowest() { return -DBL_MAX; }
    static constexpr double max() { return DBL_MAX; }
    static constexpr double epsilon() { return DBL_EPSILON; }
    static constexpr double round_error() { return 0.5; }
    static constexpr double infinity() { return __builtin_huge_val(); }
    static constexpr double quiet_NaN() { return __builtin_nan(""); }
    static constexpr double signaling_NaN() { return __builtin_nans(""); }
    static constexpr double denorm_min() { return __DBL_DENORM_MIN__; }
};

template<> class numeric_limits<long double>
{
public:
    static constexpr bool is_specialized = true;
    static constexpr bool is_signed = true;
    static constexpr bool is_integer = false;
    static constexpr bool is_exact = false;
    static constexpr bool has_infinity = true;
    static constexpr bool has_quiet_NaN = true;
    static constexpr bool has_signaling_NaN = true;
    static constexpr float_denorm_style has_denorm = denorm_present;
    static constexpr bool has_denorm_loss = false;
    static constexpr float_round_style round_style = round_to_nearest;
    static constexpr bool is_iec559 = true;
    static constexpr bool is_bounded = true;
    static constexpr bool is_modulo = false;
    static constexpr int digits = LDBL_MANT_DIG;
    static constexpr int digits10 = LDBL_DIG; /* 18 for 80-bit Intel long double */
    static constexpr int max_digits10 = DECIMAL_DIG;
    static constexpr int radix = FLT_RADIX;
    static constexpr int min_exponent = LDBL_MIN_EXP;
    static constexpr int min_exponent10 = LDBL_MIN_10_EXP;
    static constexpr int max_exponent = LDBL_MAX_EXP;
    static constexpr int max_exponent10 = LDBL_MAX_10_EXP;
    static constexpr bool traps = false;
    static constexpr bool tinyness_before = false;

    static constexpr long double min() { return LDBL_MIN; }
    static constexpr long double lowest() { return -LDBL_MAX; }
    static constexpr long double max() { return LDBL_MAX; }
    static constexpr long double epsilon() { return LDBL_EPSILON; }
    static constexpr long double round_error() { return 0.5L; }
    static constexpr long double infinity() { return __builtin_huge_vall(); }
    static constexpr long double quiet_NaN() { return __builtin_nanl(""); }
    static constexpr long double signaling_NaN() { return __builtin_nansl(""); }
    static constexpr long double denorm_min() { return __LDBL_DENORM_MIN__; }
};

}
