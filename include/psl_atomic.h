#pragma once

#include "psl_mutex.h"
#include "psl_type_traits.h"


namespace psl
{

namespace internal
{

template<typename T>
class atomic_base
{
public:
    atomic_base()
        : m_value(T())
    {}

    atomic_base(const T &value)
        : m_value(value)
    {}

    atomic_base(const atomic_base &) = delete;
    atomic_base &operator=(const atomic_base &) = delete;
    atomic_base(atomic_base &&) = delete;
    atomic_base &operator=(atomic_base &&) = delete;

    atomic_base &operator=(const T &value)
    {
        store(value);
        return *this;
    }

    operator T()
    {
        return load();
    }

    bool is_lock_free() const noexcept { return false; }

    T load() const
    {
        psl::region_lock lock(m_mut);
        return m_value;
    }
    void store(const T &value)
    {
        psl::region_lock lock(m_mut);
        m_value = value;
    }

protected:
    T m_value;
    mutable psl::mutex m_mut;
};

}

template<typename T, typename Enable = void>
class atomic : public internal::atomic_base<T>
{
    using base = internal::atomic_base<T>;
public:
    using base::atomic_base;
    using base::operator=;
};

template<typename T>
class atomic<T, typename psl::enable_if<psl::is_integral<T>::value>::type> : public internal::atomic_base<T>
{
    using base = internal::atomic_base<T>;
public:
    using base::atomic_base;
    using base::operator=;

    T operator++() noexcept
    {
        psl::region_lock lock(base::m_mut);
        return ++base::m_value;
    }
    T operator++() volatile noexcept
    {
        psl::region_lock lock(base::m_mut);
        return ++base::m_value;
    }
    T operator++(int) noexcept
    {
        psl::region_lock lock(base::m_mut);
        return base::m_value++;
    }
    T operator++(int) volatile noexcept
    {
        psl::region_lock lock(base::m_mut);
        return base::m_value++;
    }
    T operator--() noexcept
    {
        psl::region_lock lock(base::m_mut);
        return --base::m_value;
    }
    T operator--() volatile noexcept
    {
        psl::region_lock lock(base::m_mut);
        return --base::m_value;
    }
    T operator--(int) noexcept
    {
        psl::region_lock lock(base::m_mut);
        return base::m_value--;
    }
    T operator--(int) volatile noexcept
    {
        psl::region_lock lock(base::m_mut);
        return base::m_value--;
    }

    T fetch_add(T value)
    {
        psl::region_lock lock(base::m_mut);
        T ret = base::m_value;
        base::m_value += value;
        return ret;
    }
    T fetch_sub(T value)
    {
        psl::region_lock lock(base::m_mut);
        T ret = base::m_value;
        base::m_value -= value;
        return ret;
    }
    T fetch_and(T value)
    {
        psl::region_lock lock(base::m_mut);
        T ret = base::m_value;
        base::m_value &= value;
        return ret;
    }
    T fetch_or(T value)
    {
        psl::region_lock lock(base::m_mut);
        T ret = base::m_value;
        base::m_value |= value;
        return ret;
    }
    T fetch_xor(T value)
    {
        psl::region_lock lock(base::m_mut);
        T ret = base::m_value;
        base::m_value ^= value;
        return ret;
    }
};

}
