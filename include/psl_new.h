#pragma once

#include <stddef.h>


// Default placement versions of operator new.
inline void *operator new(size_t, void *p)
{
    return p;
}

inline void *operator new[](size_t, void *p)
{
    return p;
}
