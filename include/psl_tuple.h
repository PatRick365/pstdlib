#pragma once

#include "psl_utility.h"

#include <stddef.h>
#include <assert.h>


namespace psl
{

template<size_t N, typename ...Ts>
class tuple_leaf
{
public:
    tuple_leaf() = default;

    template<size_t I, typename U>
    U get() const
    {
        assert(false && "noooooooo!!!!");
        throw "damn1";
    }
    template<size_t I, typename U>
    void set(const U &)
    {
        assert(false && "noooooooo!!!!");
        throw "damn2";
    }
};

template<size_t N, typename T, typename ...Ts>
class tuple_leaf<N, T, Ts...> : public tuple_leaf<N + 1, Ts...>
{
    typedef T value_type;
    typedef tuple_leaf<N + 1, Ts...> base;
public:
    tuple_leaf() = default;

    template<typename U, typename ...Args>
    explicit tuple_leaf(const U &v, Args &&...args)
        : base(psl::forward<Args>(args)...),
          m_v(v)
    {}

    template<size_t I, typename U>
    U get() const
    {
        if constexpr (I == N)
            return m_v;
        return base::template get<I, U>();
    }
    template<size_t I, typename U>
    void set(const U &value)
    {
        if constexpr (I == N)
        {
            m_v = value;
            return;
        }
        base::template set<I, U>(value);
    }


private:
    T m_v = {};
};


template<typename ...Ts>
class tuple : public tuple_leaf<0UL, Ts...>
{
    typedef tuple_leaf<0UL, Ts...> base;
public:
    template<typename ...Args>
    explicit tuple(Args &&...args)
        : base(psl::forward<Args>(args)...)
    {}
    tuple(const tuple &t) = delete;
    tuple(tuple &&t) = delete;

    template<size_t I, typename U>
    U get() const
    {
        return base::template get<I, U>();
    };
    template<size_t I, typename U>
    void set(const U &value)
    {
        return base::template set<I, U>(value);
    }
};

}
