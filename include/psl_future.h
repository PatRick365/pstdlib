#pragma once

#include "psl_memory.h"
#include "psl_thread.h"


namespace psl
{

namespace internal
{
template<typename T>
struct shared_state
{
    bool ready()
    {
        psl::region_lock lock(m_mut_cv);
        return m_ready;
    }

    void wait_for_ready()
    {
        psl::region_lock lock(m_mut_cv);
        m_cv.wait(m_mut_cv, [this](){ return m_ready; });
    }

    void set_ready()
    {
        {
            psl::region_lock lock(m_mut_cv);
            m_ready = true;
        }
        m_cv.notify_all();
    }

public:
    psl::mutex m_mut_value;
    T m_value;

private:
    bool m_ready = false;
    psl::condition_variable m_cv;
    psl::mutex m_mut_cv;
};

template<>
struct shared_state<void>
{
    bool ready()
    {
        psl::region_lock lock(m_mut_cv);
        return m_ready;
    }

    void wait_for_ready()
    {
        psl::region_lock lock(m_mut_cv);
        m_cv.wait(m_mut_cv, [this](){ return m_ready; });
    }

    void set_ready()
    {
        {
            psl::region_lock lock(m_mut_cv);
            m_ready = true;
        }
        m_cv.notify_all();
    }
private:
    bool m_ready = false;
    psl::condition_variable m_cv;
    psl::mutex m_mut_cv;
};

}

template<typename T>
class future
{
public:
    future() noexcept {}
    future(future &&other) noexcept
        : m_shared_state(psl::move(other.m_shared_state))
    {}
    future(const future &other) = delete;
    ~future()
    {
        if (m_shared_state)
            wait();
    }

    future &operator=(future &&other) noexcept
    {
        m_shared_state = psl::move(other.m_shared_state);
    }
    future &operator=(const future &other) = delete;

    bool wait() const
    {
        if (!m_shared_state)
        {
            return false;
        }
        m_shared_state->wait_for_ready();
        return true;
    }

    T get()
    {
        if (!m_shared_state)
            throw psl::runtime_error("future has no state");

        wait();
        psl::region_lock lock(m_shared_state->m_mut_value);
        return psl::move(m_shared_state->m_value);
    }
    //T &get(); // (member only of future<T&> template specialization)

    bool valid() const noexcept
    {
        if (!m_shared_state)
            return false;

        return m_shared_state->ready();
    }

public:
    explicit future(const psl::shared_ptr<internal::shared_state<T>> &shared_state)
        : m_shared_state(shared_state)
    {}

private:
    mutable psl::shared_ptr<internal::shared_state<T>> m_shared_state;
};

template<>
class future<void>
{
public:
    future() noexcept = default;
    future(future &&other) noexcept
        : m_shared_state(psl::move(other.m_shared_state))
    {}
    future(const future &other) = delete;
    ~future() { wait(); }

    bool wait() const
    {
        if (!m_shared_state)
        {
            return false;
        }
        m_shared_state->wait_for_ready();
        return true;
    }

    bool valid() const noexcept
    {
        if (!m_shared_state)
            return false;

        return m_shared_state->ready();
    }

public:
    explicit future(const psl::shared_ptr<internal::shared_state<void>> &shared_state)
        : m_shared_state(shared_state)
    {}

private:
    mutable psl::shared_ptr<internal::shared_state<void>> m_shared_state;
};

template<typename T>
class promise
{
public:
    promise()
        : m_shared_state(psl::make_shared<internal::shared_state<T>>())
    {}
    void set_value(const T &value)
    {
        psl::region_lock lock(m_shared_state->m_mut_value);
        m_shared_state->m_value = value;
        m_shared_state->set_ready();
    }
    void set_value(T &&value)
    {
        psl::region_lock lock(m_shared_state->m_mut_value);
        m_shared_state->m_value = psl::move(value);
        m_shared_state->set_ready();
    }
    //void set_value(T &value); //(member only of promise<R&> template specialization)

    psl::future<T> get_future()
    {
        return psl::future<T>(m_shared_state);
    }

private:
    psl::shared_ptr<internal::shared_state<T>> m_shared_state;
};
template<>
class promise<void>
{
public:
    promise()
        : m_shared_state(psl::make_shared<internal::shared_state<void>>())
    {}
    void set_value()
    {
        m_shared_state->set_ready();
    }

    psl::future<void> get_future()
    {
        return psl::future<void>(m_shared_state);
    }

private:
    psl::shared_ptr<internal::shared_state<void>> m_shared_state;
};

template<typename> class packaged_task; //not defined

template<typename R, class ...ArgTypes>
class packaged_task<R(ArgTypes ...)>
{
public:
    packaged_task() noexcept = default;

    template <class F>
    explicit packaged_task(F &&f)
        : m_fn(psl::move(f))
    {}

    packaged_task(const packaged_task &) = delete;

    packaged_task(packaged_task &&rhs) noexcept = default;

    template<typename... Args>
    void operator()(Args &&... args)
    {
        if (m_fn)
        {
            if constexpr (psl::is_same<R, void>())
            {
                m_fn(psl::forward<Args>(args)...);
                m_promise.set_value();
            }
            else
            {
                m_promise.set_value(m_fn(psl::forward<Args>(args)...));
            }
        }
    }

    psl::future<R> get_future()
    {
        if (m_fn)
        {
            return m_promise.get_future();
        }
        else
        {
            return psl::future<R>();
        }
    }

private:
    psl::promise<R> m_promise;
    psl::function<R(ArgTypes ...)> m_fn;
};

template<typename Function, typename ...Args>
psl::future<psl::invoke_result_t<psl::decay_t<Function>, psl::decay_t<Args>...>> async(Function &&f, Args &&...args)
{
    using Ret = psl::invoke_result_t<psl::decay_t<Function>, psl::decay_t<Args>...>;
    psl::packaged_task<Ret(Args...)> task(psl::forward<Function>(f));
    psl::future<Ret> result = task.get_future();

    psl::thread thread(psl::move(task), psl::forward<Args>(args)...);
    thread.detach();
    return result;
}

}
