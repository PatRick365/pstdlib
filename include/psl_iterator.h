#pragma once

#include "psl_memory.h"
#include "psl_utility.h"

#include <unistd.h>


namespace psl
{

struct input_iterator_tag {};
struct output_iterator_tag {};
struct forward_iterator_tag : public input_iterator_tag {};
struct bidirectional_iterator_tag : public forward_iterator_tag {};
struct random_access_iterator_tag : public bidirectional_iterator_tag {};
struct contiguous_iterator_tag: public random_access_iterator_tag {};

template<class Iter>
struct iterator_traits
{
    using difference_type = typename Iter::difference_type;
    using value_type = typename Iter::value_type;
    using pointer = typename Iter::pointer;
    using reference = typename Iter::reference;
    using iterator_category = typename Iter::iterator_category;
};

//template<class T>
//struct iterator_traits<T *>
//{
//    using difference_type = typename T::difference_type;
//    using value_type = typename T::value_type;
//    using pointer = typename T::pointer;
//    using reference = typename T::reference;
//    using iterator_category = typename T::iterator_category;
//};

template<class T>
struct iterator_traits<T *>
{
    using difference_type = long long;
    using value_type = typename T::value_type;
    using pointer = typename T::pointer;
    using reference = typename T::reference;
    using iterator_category = typename T::iterator_category;
};

template<
    class Category,
    class T,
    class Distance = long long,
    class Pointer = T*,
    class Reference = T&
> struct iterator
{
    using iterator_category = Category;
    using value_type = T;
    //using difference_type = Distance;
    using pointer = Pointer;
    using reference = Reference;
};

template<class Iter>
class reverse_iterator
{
public:
    using iterator_type = Iter;
    using iterator_category = typename psl::iterator_traits<Iter>::iterator_category;
    using value_type = typename psl::iterator_traits<Iter>::value_type;
    using difference_type = typename psl::iterator_traits<Iter>::difference_type;
    using pointer = typename psl::iterator_traits<Iter>::pointer;
    using reference = typename psl::iterator_traits<Iter>::reference;

    constexpr reverse_iterator() = default;
    constexpr explicit reverse_iterator(iterator_type it)
        : m_it(it)
    {}
    template<class U>
    constexpr reverse_iterator(const reverse_iterator<U> &other)
        : m_it(other)
    {}

    template<class U>
    constexpr reverse_iterator &operator=(const reverse_iterator<U> &other)
    {
        m_it = other.m_it;
        return *this;
    }

    constexpr iterator_type base() const { return m_it; }

    constexpr reference operator*() const  {  return (--iterator_type(m_it)).operator*();  }
    constexpr pointer operator->() const { return (--iterator_type(m_it)).operator->(); }

    //constexpr /*unspecified*/ operator[](difference_type n) const;

    constexpr reverse_iterator &operator++() { m_it.operator--(); return *this; }
    constexpr reverse_iterator &operator--() { m_it.operator++(); return *this; }
    constexpr reverse_iterator operator++(int i) { m_it.operator--(i); return *this; }
    constexpr reverse_iterator operator--(int i) { m_it.operator++(i); return *this; }
    constexpr reverse_iterator operator+(difference_type n) const { m_it.operator-(n); return *this; }
    constexpr reverse_iterator operator-(difference_type n) const { m_it.operator+(n); return *this; }
    constexpr reverse_iterator &operator+=(difference_type n) { m_it.operator-=(n); return *this; }
    constexpr reverse_iterator &operator-=(difference_type n) { m_it.operator+=(n); return *this; }

private:
    iterator_type m_it;
};

template<class Iterator1, class Iterator2>
constexpr bool operator==(const psl::reverse_iterator<Iterator1> &lhs, const psl::reverse_iterator<Iterator2> &rhs)
{
    return lhs.base() == rhs.base();
}
template<class Iterator1, class Iterator2>
constexpr bool operator!=(const psl::reverse_iterator<Iterator1> &lhs, const psl::reverse_iterator<Iterator2> &rhs)
{
    return lhs.base() != rhs.base();
}


template<typename Container>
class back_insert_iterator
{
public:
    using iterator_category = psl::output_iterator_tag;
    using value_type = void;
    using difference_type = long;
    using pointer =	void;
    using reference = void;
    using container_type = Container;

    constexpr explicit back_insert_iterator(container_type &c)
        : m_c(psl::addressof(c))
    {}

    constexpr back_insert_iterator<Container> &operator=(const typename Container::value_type &value)
    {
        m_c->push_back(value);
        return *this;
    }

    constexpr back_insert_iterator<Container> &operator=(typename Container::value_type &&value)
    {
        m_c->push_back(psl::move(value));
        return *this;
    }

    constexpr back_insert_iterator&operator*() { return *this; }

    constexpr back_insert_iterator &operator++() { return *this; }
    constexpr back_insert_iterator operator++(int) { return *this; }

private:
    container_type *m_c;
};

template<typename Container>
psl::back_insert_iterator<Container> back_inserter(Container &c)
{
    return psl::back_insert_iterator<Container>(c);
}

// implementation via constexpr if, available in C++17
template<typename It, typename Distance>
constexpr void advance(It &it, Distance n)
{
    using category = typename psl::iterator_traits<It>::iterator_category;
    static_assert(psl::is_base_of_v<psl::input_iterator_tag, category>);

    auto dist = typename psl::iterator_traits<It>::difference_type(n);
    if constexpr (psl::is_base_of_v<psl::random_access_iterator_tag, category>)
    {
        it += dist;
    }
    else
    {
        while (dist > 0)
        {
            --dist;
            ++it;
        }
        if constexpr (psl::is_base_of_v<psl::bidirectional_iterator_tag, category>)
        {
            while (dist < 0)
            {
                ++dist;
                --it;
            }
        }
    }
}

template<typename InputIt>
constexpr InputIt next(InputIt it, typename psl::iterator_traits<InputIt>::difference_type n = 1)
{
    psl::advance(it, n);
    return it;
}

template<class BidirIt>
constexpr BidirIt prev(BidirIt it, typename psl::iterator_traits<BidirIt>::difference_type n = 1)
{
    psl::advance(it, -n);
    return it;
}

template<class Iterator1, class Iterator2>
constexpr bool operator<(const psl::reverse_iterator<Iterator1> &lhs, const psl::reverse_iterator<Iterator2> &rhs)
{
    return lhs.base() != rhs.base();
}

}

