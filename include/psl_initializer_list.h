#pragma once

#include "psl_utility.h"


namespace psl
{

struct il_tag{};

template<typename Container, typename First>
void initializer_list_push_back(Container &c, const First &first)
{
    c.push_back(first);
}


template<typename Container, typename First, typename Second, typename ...Args>
void initializer_list_push_back(Container &c, const First &first, const Second &second, Args &&...args)
{
    c.push_back(first);
    initializer_list_push_back<Container, Second, Args...>(c, second,  psl::forward<Args>(args)...);
}

}
