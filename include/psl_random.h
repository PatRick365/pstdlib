#pragma once

#include "psl_vector.h"
#include <cstddef>
#include <cstdint>

namespace psl
{

class seed_seq
{
public:
    using result_type = uint_least32_t;

    seed_seq() noexcept = default;
    seed_seq(const seed_seq &) = delete;
    template<class InputIt>
    seed_seq(InputIt begin, InputIt end)
        : v(begin, end)
    {}
    template<typename ...Args>
    seed_seq(psl::il_tag, Args &&...args)
        : v(psl::il_tag(), psl::forward<Args>(args)...)
    {}
    seed_seq &operator=(const seed_seq &) = delete;

    ///@todo seed_seq
    template<class RandomIt>
    void generate(RandomIt begin, RandomIt end);

private:
    psl::vector<result_type> v;
};

template<class UIntType, size_t w, size_t n, size_t m, size_t r,
         UIntType a, size_t u, UIntType d, size_t s,
         UIntType b, size_t t, UIntType c, size_t l, UIntType f>
class mersenne_twister_engine
{
public:
    static constexpr size_t word_size = w;
    static constexpr size_t state_size = n;
    static constexpr size_t shift_size = m;
    static constexpr size_t mask_bits = r;
    static constexpr UIntType xor_mask = a;
    static constexpr size_t tempering_u = u;
    static constexpr UIntType tempering_d = d;
    static constexpr size_t tempering_s = s;
    static constexpr UIntType tempering_b = b;
    static constexpr size_t tempering_t = t;
    static constexpr UIntType tempering_c = c;
    static constexpr size_t tempering_l = l;
    static constexpr UIntType initialization_multiplier = f;
    static constexpr UIntType default_seed = 5489u;

    using result_type = UIntType;

    mersenne_twister_engine()
        : mersenne_twister_engine(default_seed)
    {}
    explicit mersenne_twister_engine(result_type value)
        : m_state_index(0)
    {
        seed(value);
    }
    ///@todo seed_seq
    // template<class SeedSeq>
    // explicit mersenne_twister_engine(SeedSeq &seq);

    void seed(result_type value = default_seed)
    {
        m_state_array[0] = value;                          // suggested initial seed = 19650218UL

        for (int i = 1; i < n; i++)
        {
            value = f * (value ^ (value >> (w - 2))) + i;    // Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier.
            m_state_array[i] = value;
        }

        m_state_index = 0;
    }
    ///@todo seed_seq
    // template<class SeedSeq>
    // void seed(SeedSeq &seq);

    result_type operator()()
    {
        size_t k = m_state_index;      // point to current state location
            // 0 <= state_index <= n-1   always

        //  int k = k - n;                   // point to state n iterations before
        //  if (k < 0) k += n;               // modulo n circular indexing
        // the previous 2 lines actually do nothing
        //  for illustration only

        ssize_t j = k - (n - 1);         // point to state n-1 iterations before
        if (j < 0) j += n;               // modulo n circular indexing

        assert(k < state_size);
        result_type x = (m_state_array[k] & (0xffffffffUL << r)) | (m_state_array[j] & (0xffffffffUL >> (w-r)));

        result_type xA = x >> 1;
        if (x & 0x00000001UL) xA ^= a;

        j = k - (n-m);                   // point to state n-m iterations before
        if (j < 0) j += n;               // modulo n circular indexing

        assert(j < state_size);
        x = m_state_array[j] ^ xA;       // compute next value in the state
        assert(k < state_size);
        m_state_array[k++] = x;          // update new state value

        if (k >= n) k = 0;               // modulo n circular indexing
        m_state_index = k;

        result_type y = x ^ (x >> u);       // tempering
        y = y ^ ((y << s) & b);
        y = y ^ ((y << t) & c);
        result_type z = y ^ (y >> l);

        return z;
    }

    void discard(unsigned long long z)
    {
        for (unsigned long long i = 0; i < z; ++i)
        {
            operator()();
        }
    }

    static constexpr result_type min() { return 0u; }
    static constexpr result_type max() { return psl::pow(2, word_size) - 1; }

    // friend bool operator==(const mersenne_twister_engine &lhs, const mersenne_twister_engine &rhs);
    // friend bool operator!=(const mersenne_twister_engine &lhs, const mersenne_twister_engine &rhs);

private:
    result_type m_state_array[state_size]; // the array for the state vector
    size_t m_state_index = 0;              // index into state vector array, 0 <= state_index <= n-1   always
};

using mt19937 = psl::mersenne_twister_engine<uint_fast32_t,
                                             32, 624, 397, 31,
                                             0x9908b0df, 11,
                                             0xffffffff, 7,
                                             0x9d2c5680, 15,
                                             0xefc60000, 18, 1812433253>;

using mt19937_64 = psl::mersenne_twister_engine<uint_fast64_t,
                                               64, 312, 156, 31,
                                               0xb5026f5aa96619e9, 29,
                                               0x5555555555555555, 17,
                                               0x71d67fffeda60000, 37,
                                               0xfff7eee000000000, 43, 6364136223846793005>;


template<class UIntType>
class pcg_engine
{
public:
    static constexpr UIntType default_seed = 0u;

    using result_type = UIntType;

    pcg_engine()
        : pcg_engine(default_seed)
    {}
    explicit pcg_engine(result_type value)
        : m_state(0),
          m_inc(0)
    {
        seed(value);
    }
    ///@todo seed_seq
    // template<class SeedSeq>
    // explicit mersenne_twister_engine(SeedSeq &seq);

    void seed(result_type value = default_seed)
    {
        m_state = 0U;
        m_inc = (value << 1u) | 1u;
        pcg32_random_r();
        m_state += 0x853c49e6748fea9bULL;
        pcg32_random_r();
    }
    ///@todo seed_seq
    // template<class SeedSeq>
    // void seed(SeedSeq &seq);

    result_type operator()()
    {
        uint64_t oldstate = m_state;
        // Advance internal state
        m_state = oldstate * 6364136223846793005ULL + (m_inc|1);
        // Calculate output function (XSH RR), uses old state for max ILP
        uint32_t xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
        uint32_t rot = oldstate >> 59u;
        return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
    }

    void discard(unsigned long long z)
    {
        for (unsigned long long i = 0; i < z; ++i)
        {
            operator()();
        }
    }

    //static constexpr result_type min() { return 0u; }
    //static constexpr result_type max() { return psl::pow(2, word_size) - 1; }

    // friend bool operator==(const pcg_engine &lhs, const pcg_engine &rhs);
    // friend bool operator!=(const pcg_engine &lhs, const pcg_engine &rhs);

private:
    result_type pcg32_random_r()
    {
        uint64_t oldstate = m_state;
        m_state = oldstate * 6364136223846793005ULL + m_inc;
        uint32_t xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
        uint32_t rot = oldstate >> 59u;
        return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
    }

private:
    uint64_t m_state;
    result_type m_inc;
};

using pcg_32 = pcg_engine<uint_fast32_t>;
using pcg_64 = pcg_engine<uint_fast64_t>;

}
