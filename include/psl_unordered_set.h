#pragma once

#include "psl_iterator.h"
#include "psl_memory.h"
#include "psl_functional.h"
#include "psl_utility.h"
#include "psl_vector.h"
#include "psl_list.h"


namespace psl
{

template<typename Key,
         typename Hash = psl::hash<Key>,
         typename KeyEqual = psl::equal_to<Key>,
         typename Allocator = psl::allocator<Key>
         >
class unordered_set
{
public:
    static constexpr size_t DEFAULT_BUCKET_COUNT = 10;

    struct set_node
    {
        size_t hash;
        Key value;
    };
    using list_nodes = psl::list<set_node>;

//    template <class Iter, class NodeType>
//    struct insert_return_type_t
//    {
//        Iter     position;
//        bool     inserted;
//        NodeType node;
//    };

public:
    class const_iterator;
    class iterator;
    using key_type = Key;
    using value_type = Key;
    using size_type = size_t;
    using difference_type = long;
    using hasher = Hash;
    using key_equal = KeyEqual;
    using allocator_type = Allocator;
    using reference = value_type &;
    using const_reference = const value_type &;
    using pointer = value_type *;
    using const_pointer = const value_type *;
    using iterator = unordered_set::iterator;
//    using const_iterator = fff;
//    using local_iterator = fff;
//    using const_local_iterator = fff;
    using node_type = set_node;
//    using insert_return_type = insert_return_type_t<abc>;

// temp
    using it_vec_list_nodes = typename psl::vector<list_nodes/*, allocator_type*/>::iterator;
    using it_list_nodes = typename list_nodes::iterator;


public:
    unordered_set()
        : m_values(DEFAULT_BUCKET_COUNT)
    {}

    void clear() noexcept
    {
        m_values.clear();
        m_values.resize(DEFAULT_BUCKET_COUNT);
    }

    psl::pair<iterator, bool> insert(const value_type &value)
    {
        size_t hash = hasher()(value);
        size_t pos = calc_bucket_pos(hash);
        list_nodes &bucket = m_values[pos];

        auto it = psl::find_if(bucket.cbegin(), bucket.cend(), [hash](const set_node &n)
        {
            return (hash == n.hash);
        });

        if (it != bucket.cend())
        {
            return psl::make_pair(it_from_hash(hash), false);
        }

        bucket.push_back(set_node{hash, value});
        ++m_sz;

        rehash_if_needed();

        return psl::make_pair(it_from_hash(hash), true);
    }

//    template <class... Args>
//    psl::pair<iterator, bool> emplace(Args &&...args)
//    {
//    }

    bool contains(const value_type &value)
    {
        size_t hash = hasher()(value);
        size_t pos = calc_bucket_pos(hash);
        return !m_values[pos].empty();
    }

    size_type size() const
    {
        return m_sz;
    }

    bool empty() const
    {
        return (size() == 0);
    }

    // bucket interface
    size_type bucket_count() const
    {
        return m_values.size();
    }

    size_type max_bucket_count() const
    {
        return psl::numeric_limits<psl::vector<int>::size_type>::max();
    }

    // hash policy
    float load_factor() const
    {
        //typename bucket_type::size_type lf = 0;
        size_type lf = 0;
        size_type count_buckets = bucket_count();

        assert(count_buckets != 0);

        // @todo psl::accumulate
        for (const auto bucket : m_values)
        {
            lf += bucket.size();
        }

        return float(lf) / count_buckets;
    }

    float max_load_factor() const
    {
        return m_max_load_factor;
    }
    void max_load_factor(float ml)
    {
        m_max_load_factor = ml;
    }

    void rehash(size_type count)
    {
        m_values.resize(count);

        for (list_nodes &bucket : m_values)
        {
            list_nodes bucket2 = psl::move(bucket);
            bucket = list_nodes();
            for (set_node &node : bucket2)
            {
                size_t pos = calc_bucket_pos(node.hash);
                m_values[pos].push_back(psl::move(node));
            }
        }

//        for (size_t i = 0; i < m_values.size();++i)
//        {
//            psl::println("{}: {}", i, m_values[i].size());
//        }

    }

    // iterator getters
    iterator begin()
    {
        auto m_it_begin = m_values.begin();
        auto m_it_end = m_values.end();
        list_nodes *m_bucket = nullptr;
        typename list_nodes::iterator m_it_bucket;

        for (; m_it_begin != m_it_end; ++m_it_begin)
        {
            if (!m_it_begin->empty())
            {
                m_bucket = &(*m_it_begin);
                m_it_bucket = m_bucket->begin();
                break;
            }
        }


        return iterator(m_it_begin, m_it_end, m_bucket, m_it_bucket);
    }
    iterator end()
    {
        return iterator(m_values.end(), m_values.end(), nullptr, {});
    }

    /// iterator implementations
    public:
        class iterator
        {
//            using it_vec_list_nodes = typename psl::vector<list_nodes/*, allocator_type*/>::iterator;
//            using it_list_nodes = typename list_nodes::iterator;
        public:
            using value_type = unordered_set::value_type;
            using reference = unordered_set::reference;
            using pointer = unordered_set::pointer;
            using difference_type = unordered_set::difference_type;
            using iterator_category = psl::random_access_iterator_tag;
        public:
            iterator()
            {}

            explicit iterator(it_vec_list_nodes it_begin, it_vec_list_nodes it_end, list_nodes *bucket, it_list_nodes it_bucket)
                : m_it_begin(it_begin),
                  m_it_end(it_end),
                  m_bucket(bucket),
                  m_it_bucket(it_bucket)
            {}

            reference operator*() const
            {
                return m_it_bucket->value;
            }

            pointer operator->() const
            {
                return &m_it_bucket->value;
            }

            iterator& operator++()
            {
                for (; (m_it_begin != m_it_end); ++m_it_begin)
                {
                    if (m_bucket == nullptr)
                    {
                        m_bucket = &(*m_it_begin);
                        m_it_bucket = m_bucket->begin();

                        if (!m_bucket->empty())
                        {
                            return *this;
                        }

                    }
                    else
                    {
                        if (m_it_bucket == m_bucket->end())
                        {
                            m_bucket = nullptr;
                            continue;
                        }

                        ++m_it_bucket;

                        if (m_it_bucket == m_bucket->end())
                        {
                            m_bucket = nullptr;
                            continue;
                        }

                        return *this;
                    }
                }
                return *this;
            }

            iterator operator++(int)
            {
                iterator it = *this;
                ++it;
                return it;
            }

//            iterator& operator--()
//            {
//                --m_ptr;
//                return *this;
//            }

//            iterator operator--(int)
//            {
//                iterator it = *this;
//                --m_ptr;
//                return it;
//            }

//            bool operator==(const const_iterator &it) const
//            {
//                return it == const_iterator(m_ptr);
//            }

            bool operator==(const iterator &it) const
            {
                bool b1 = (m_it_begin == it.m_it_begin);
                bool b2 = (m_it_bucket == it.m_it_bucket);
                return b1 && b2;
//                return ((m_it_begin == it.m_it_begin) && (m_it_bucket == m_it_bucket));
            }

//            bool operator!=(const const_iterator &it) const
//            {
//                return const_iterator(*this) != const_iterator(it);
//            }

//            operator const_iterator() const
//            {
//                return const_iterator(m_ptr);
//            }

        private:
            it_vec_list_nodes m_it_begin;
            it_vec_list_nodes m_it_end;
            list_nodes *m_bucket = nullptr;
            it_list_nodes m_it_bucket;
        };

//        class const_iterator
//        {
//        public:
//            using value_type = vector::value_type;
//            using reference = vector::const_reference;
//            using pointer = vector::const_pointer;
//            using difference_type = vector::difference_type;
//            using iterator_category = psl::contiguous_iterator_tag;

//        public:
//            const_iterator()
//                : m_ptr(nullptr)
//            {}

//            explicit const_iterator(pointer ptr)
//                : m_ptr(ptr)
//            {}

//            const_reference operator*() const
//            {
//                assert(m_ptr);
//                return *m_ptr;
//            }

//            const_pointer operator->() const
//            {
//                assert(m_ptr);
//                return m_ptr;
//            }

//            const_iterator& operator++()
//            {
//                assert(m_ptr);
//                ++m_ptr;
//                return *this;
//            }

//            const_iterator operator++(int)
//            {
//                assert(m_ptr);
//                const_iterator it = *this;
//                ++m_ptr;
//                return it;
//            }

//            const_iterator& operator--()
//            {
//                --m_ptr;
//                return *this;
//            }

//            const_iterator operator--(int)
//            {
//                const_iterator it = *this;
//                --m_ptr;
//                return it;
//            }

//            const_iterator operator+(difference_type difference) const
//            {
//                return const_iterator(m_ptr + difference);
//            }
//            const_iterator operator-(difference_type difference) const
//            {
//                return const_iterator(m_ptr - difference);
//            }

//            const_iterator &operator+=(difference_type difference)
//            {
//                m_ptr += difference;
//                return *this;
//            }
//            const_iterator &operator-=(difference_type difference)
//            {
//                m_ptr -= difference;
//                return *this;
//            }

//            difference_type operator-(const_iterator rhs) const
//            {
//                return m_ptr  - rhs.m_ptr;
//            }

//            bool operator==(const const_iterator &it) const
//            {
//                return it.m_ptr == m_ptr;
//            }

//            bool operator!=(const const_iterator &it) const
//            {
//                return it.m_ptr != m_ptr;
//            }

//        private:
//            const_pointer m_ptr;
//        };
//    };


private:
    size_t calc_bucket_pos(size_t hash) const
    {
        return hash % bucket_count();
    }
    bool rehash_if_needed()
    {
        if (size() > max_load_factor() * bucket_count())
        {
            rehash(bucket_count() * 2);
            return true;
        }
        return false;
    }

    iterator it_from_hash(size_t hash)
    {
        auto vec_buckets_end = m_values.end();

        for (typename psl::vector<psl::list<set_node>>::iterator it_bucket = m_values.begin(); (it_bucket != vec_buckets_end); ++it_bucket)
        {
            psl::list<set_node> &lst = *it_bucket;

            for (typename psl::list<set_node>::iterator it_node = lst.begin(); (it_node != lst.end()); ++it_node)
            {
                set_node &node = *it_node;

                if (node.hash == hash)
                {
                    return iterator(it_bucket, vec_buckets_end, &(*it_bucket), it_node);
                }
            }
        }
        return end();
    }

private:
    psl::vector<psl::list<set_node>/*, allocator_type*/> m_values;
    size_type m_sz = 0;
    float m_max_load_factor = 1.0f;
};

}


