#pragma once

#include "psl_units.h"


namespace psl
{

class time_point
{
public:
    time_point();

    static time_point now();

    psl::time<psl::time_t::s> seconds() const { return m_seconds; }

    psl::time<psl::time_t::s> operator-(time_point rhs) const
    {
        return m_seconds - rhs.m_seconds;
    }
    psl::time<psl::time_t::s> operator+(time_point rhs) const
    {
        return m_seconds + rhs.m_seconds;
    }

private:
    psl::time<psl::time_t::s> m_seconds;
};

class timer
{
public:
    timer();

    void start();
    psl::time<psl::time_t::s> elapsed() const;

private:
    time_point m_timestamp;
};

}
