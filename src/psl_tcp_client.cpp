#include "psl_tcp_client.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <cerrno>
#include <cstring>


namespace psl
{

class tcp_client_impl
{
public:
    bool is_open() const { return m_is_open; }

    bool open(const psl::string &hostname, int port)
    {
        m_portno = port;
        m_sockfd = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

        if (m_sockfd < 0)
        {
            psl::print(stderr, "ERROR opening socket\n");
            return false;
        }

        m_server = ::gethostbyname(hostname.c_str());

        if (m_server == nullptr)
        {
            psl::print(stderr, "ERROR, no such host\n");
            return false;
        }

        ::memset(&serv_addr, 0, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;

        ::memcpy(&serv_addr.sin_addr.s_addr, m_server->h_addr, m_server->h_length);
        serv_addr.sin_port = htons(m_portno);

        if (::connect(m_sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        {
            psl::print(stderr, "ERROR connecting: {}\n", strerror(errno));
            return false;
        }

        m_is_open = true;
        return true;
    }
    bool close()
    {
        if (!m_is_open)
            return true;
        shutdown(m_sockfd, SHUT_RDWR);
        return (::close(m_sockfd) == 0);

    }

    bool write(const uint8_t *data, size_t size)
    {
        if (!m_is_open)
        {
            psl::print(stderr, "socket not open...\n");
            return false;
        }

        ssize_t bytes_written = ::write(m_sockfd, data, size);
        if (bytes_written < 0)
        {
            psl::print(stderr, "ERROR writing to socket\n");
            return false;
        }

        return true;
    }

    ssize_t read(uint8_t *data, size_t size)
    {
        if (!m_is_open)
        {
            psl::print(stderr, "socket not open...\n");
            return -1;
        }

        ssize_t bytes_read = ::read(m_sockfd, data, size);
        if (bytes_read < 0)
        {
            psl::print(stderr, "ERROR reading from socket");
            return -1;
        }

        return bytes_read;
    }

private:
    bool m_is_open = false;

    int m_sockfd = 0;
    int m_portno = 0;
    struct sockaddr_in serv_addr{};
    struct hostent *m_server = nullptr;
};

tcp_client::tcp_client()
    : m_impl(psl::make_unique<tcp_client_impl>())
{}

tcp_client::~tcp_client()
{
    m_impl->close();
}

bool tcp_client::is_open() const
{
    return m_impl->is_open();
}

bool tcp_client::open(const string &hostname, int port)
{
    return m_impl->open(hostname, port);
}

bool tcp_client::close()
{
    return m_impl->close();
}

bool tcp_client::write_intern(const uint8_t *data, size_t len)
{
    return m_impl->write(data, len);
}

ssize_t tcp_client::read_intern(uint8_t *data, size_t len)
{
    return m_impl->read(data, len);
}

}
