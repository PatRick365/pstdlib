#include "psl_process.h"
#include "psl_filesystem.h"


#include <sys/wait.h>
#include <sys/prctl.h>
#include <unistd.h>
#include <cstdio>
#include <cstring>


namespace psl
{

struct process_impl
{
    bool open(const psl::string &program, const psl::vector<psl::string> &args)
    {
        int inpipefds[2];
        int outpipefds[2];

        if (::pipe(inpipefds) == -1)
        {
            psl::print(stderr, "failed to setup in pipe for program \"{}\"", program);
            return false;
        }
        if (::pipe(outpipefds) == -1)
        {
            psl::print(stderr, "failed to setup out pipe for program \"{}\"", program);
            return false;
        }

        m_pid = ::fork();
        if (m_pid == 0)
        {
            // Child
            ::dup2(outpipefds[0], STDIN_FILENO);
            ::dup2(inpipefds[1], STDOUT_FILENO);
            ::dup2(inpipefds[1], STDERR_FILENO);

            ::close(inpipefds[0]);
            ::close(inpipefds[1]);

            //ask kernel to deliver SIGTERM in case the parent dies
            ::prctl(PR_SET_PDEATHSIG, SIGTERM);

            psl::vector<char *> pargs;
            pargs.reserve(args.size() + 2);

            auto fileName = psl::filesystem::path(program).filename().native();
            pargs << fileName.data();
            for (size_t i = 0; i < args.size(); ++i)
            {
                pargs << const_cast<char *>(args[i].c_str());
            }
            pargs << nullptr;

            ::execvp(program.c_str(), pargs.data());
            // Nothing below this line should be executed by child process. If so,
            // it means that the execl function wasn't successfull, so lets exit:
            psl::print(stderr, "failed to open program \"{}\"", program);
            return false;
        }

        //close unused pipe ends
        ::close(outpipefds[0]);
        ::close(inpipefds[1]);

        // save used pipes to members
        m_inpipefd = inpipefds[0];
        m_outpipefd = outpipefds[1];


        m_program = program;
        m_is_open = true;
        return true;
    }

    bool close()
    {
        if (!m_is_open)
            return true;

        ::kill(m_pid, SIGKILL); //send SIGKILL signal to the child process
        int status;
        ::waitpid(m_pid, &status, 0);

        m_program.clear();
        m_is_open = false;
        return true;
    }

    pid_t m_pid = 0;

    int m_inpipefd = 0;
    int m_outpipefd = 0;

    psl::string m_program;
    bool m_is_open = false;
};


process::process()
    : m_impl(psl::make_unique<process_impl>())
{}

process::process(const psl::string &program, const psl::vector<string> &args)
    : m_impl(psl::make_unique<process_impl>())
{
    m_impl->open(program, args);
}

process::~process()
{
    if (m_impl->m_is_open)
    {
        m_impl->close();
    }
}

bool process::open(const string &program, const psl::vector<string> &args)
{
    return m_impl->open(program, args);
}

bool psl::process::is_open() const
{
    return m_impl->m_is_open;
}

bool psl::process::close()
{
    return m_impl->close();
}

bool process::write_intern(const uint8_t *data, size_t len)
{
    size_t bytes = ::write(m_impl->m_outpipefd, data, len);

    if (bytes != len)
    {
        psl::print(stderr, "bytes({}) != len({})\n", bytes, len);
        return false;
    }

    return true;
}

ssize_t process::read_intern(uint8_t *data, size_t len)
{
    return ::read(m_impl->m_inpipefd, data, len);
}

}

