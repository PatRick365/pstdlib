#include "psl_filesystem.h"


namespace psl
{
namespace filesystem
{

path::path() noexcept {}

path::path(const path &p) : m_str(p.m_str) {}

path::path(path &&p) noexcept : m_str(psl::move(p.m_str)) {}

path::path(string_type &&source, format /*fmt*/) : m_str(psl::move(source)) {}

const path::value_type *path::c_str() const noexcept { return m_str.c_str(); }

const path::string_type &path::native() const noexcept { return m_str; }

path::operator string_type() const { return m_str; }

void path::clear() { m_str.clear(); }

path path::root_name() const
{
    return path("/");
}

path path::root_directory() const
{
    return path("/");
}

path path::root_path() const
{
    return path("/");
}

path path::relative_path() const
{
    if (m_str.empty())
        return path();

    if (m_str.front() == preferred_separator)
    {
        return m_str.substr(1);
    }

    return path(m_str);
}

path path::parent_path() const
{
    if (m_str.empty())
        return path();

    if (m_str.back() == '/')
        return path(m_str);

    string_type::size_type pos_separator = m_str.rfind(preferred_separator);

    if (pos_separator == string_type::npos)
    {
        return path();
    }

    return m_str.substr(0, pos_separator);
}

psl::filesystem::path psl::filesystem::path::filename() const
{
    string_type::size_type pos_separator = m_str.rfind(preferred_separator);

    if (pos_separator == string_type::npos)
    {
        return path(m_str);
    }

    string_type::size_type pos_after_separator = pos_separator + 1;

    if (pos_after_separator >= m_str.length())
    {
        return path();
    }

    return m_str.substr(pos_after_separator);
}

path path::stem() const
{
    string_type str_filename = filename();

    if (str_filename.empty())
        return path();

    if ((str_filename == ".") || (str_filename == ".."))
        return path(str_filename);

    string_type::size_type pos_dot = str_filename.rfind('.');

    if ((pos_dot == string_type::npos) || (pos_dot == 0))
    {
        return path(str_filename);
    }

    return str_filename.substr(0, pos_dot);
}

path path::extension() const
{
    string_type str_filename = filename();

    if (str_filename.empty())
        return path();

    //printf("filname(%s) == ..: (%d)\n", str_filename.c_str(), (str_filename == ".."));
    //str_filename.dump();

    if ((str_filename == ".") || (str_filename == ".."))
        return path();

    string_type::size_type pos_dot = str_filename.rfind('.');

    if ((pos_dot == string_type::npos) || (pos_dot == 0))
    {
        return path();
    }

    return str_filename.substr(pos_dot);


}

bool path::is_absolute() const
{
    return !m_str.empty() && (m_str.front() == '/');
}

bool path::is_relative() const
{
    return !is_absolute();
}

path operator/(const path &lhs, const path &rhs)
{
    return path(lhs.m_str + path::preferred_separator + rhs.m_str);
}

}
}
