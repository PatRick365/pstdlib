#include "psl_time.h"

#include <ctime>


namespace psl
{

psl::time_point::time_point()
    : m_seconds(0)
{}

psl::time_point psl::time_point::now()
{
    psl::time_point time;

    struct timespec time_val;

    ::clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time_val);

    time.m_seconds = psl::time<psl::time_t::s>(time_val.tv_sec);
    //time.m_seconds += psl::time<psl::time_t::ns>(time_val.tv_nsec).to_unit<psl::time_t::s>();
    time.m_seconds = time.m_seconds + psl::time<psl::time_t::ns>(time_val.tv_nsec);

    return time;
}

timer::timer()
{
    start();
}

void timer::start()
{
    m_timestamp = time_point::now();
}

psl::time<time_t::s> timer::elapsed() const
{
    return time_point::now() - m_timestamp;
}

}
