#include "psl_thread_pool.h"


//static int n = 0;
namespace psl
{
thread_pool::thread_pool(size_t num_threads) : m_num_threads(num_threads)
{
    for (size_t i = 0; i < num_threads; ++i)
    {
        m_threads.push_back(psl::make_unique<psl::thread>(
            [this]
            {
                //int me = n++;
                for (;;)
                {
                    internal::thread_pool_data_base *data = nullptr;
                    if (m_msg_queue.receive(data))
                    {
                        data->execute();
                        delete data;
                    }
                    if (m_stop)
                    {
                        return;
                    }
                }
            }));
    }
}

thread_pool::~thread_pool()
{
    m_stop = true;
    m_msg_queue.clear();
    for (auto &thread : m_threads)
    {
        thread->join();
    }
}
}

