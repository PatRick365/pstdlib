#include "psl_string_builder.h"

namespace psl
{

string_builder::string_builder() {}

bool string_builder::is_open() const
{
    return true;
}

bool string_builder::close()
{
    return true;
}

string &string_builder::get_string()
{
    return m_str;
}
const string &string_builder::get_string() const
{
    return m_str;
}

bool string_builder::write_intern(const uint8_t *data, size_t len)
{
    m_str.reserve(m_str.capacity() + len);
    m_str.append(reinterpret_cast<const char *>(data), len);
    return true;
}

}
