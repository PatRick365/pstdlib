#include "psl_iodevice.h"
#include "psl_char_manip.h"


bool psl::o_device::write(const char *str)
{
    return write(reinterpret_cast<const uint8_t *>(str), psl::strlen(str));
}

bool psl::o_device::write(const psl::vector<uint8_t> &data)
{
    return write(data.data(), data.size());
}

bool psl::o_device::write(const string &data)
{
    return write(reinterpret_cast<const uint8_t *>(data.data()), data.length());
}

bool psl::o_device::write(const uint8_t *data, size_t len)
{
    return write_intern(data, len);
}

bool psl::o_device::write(const char *str, size_t len)
{
    return write(reinterpret_cast<const uint8_t *>(str), len);
}

ssize_t psl::i_device::read(uint8_t *data, size_t len)
{
    return read_intern(reinterpret_cast<uint8_t *>(data), len);
}

ssize_t psl::i_device::read(char *data, size_t len)
{
    return read_intern(reinterpret_cast<uint8_t *>(data), len);
}

template<typename Container>
bool read(psl::i_device &i_device, Container &data, size_t len)
{
    data.resize(len);
    ssize_t size_read = i_device.read(data.data(), len);

    if (size_read < 0)
        return false;

    data.resize(size_read);
    return true;
}

bool psl::i_device::read(psl::vector<uint8_t> &data, size_t len)
{
    return ::read(*this, data, len);
}
bool psl::i_device::read(psl::string &data, size_t len)
{
    return ::read(*this, data, len);
}

template<typename Container>
bool read_all(psl::i_device &i_device, Container &data)
{
    constexpr size_t read_size = 1024;
    size_t current_pos = 0;

    for (;;)
    {
        if (static_cast<size_t>(data.size()) < current_pos + read_size )
            data.resize(current_pos + read_size);

        ssize_t size_read = i_device.read(data.data() + current_pos, read_size);

        if (size_read < 0)
            return false;

        if (size_t(size_read) < read_size)
        {
            data.resize(current_pos + size_read);
            return true;
        }
        current_pos += read_size;
    }
}

bool psl::i_device::read_all(psl::vector<char> &data)
{
    return ::read_all(*this, data);
}
bool psl::i_device::read_all(string &data)
{
    return ::read_all(*this, data);
}

bool psl::i_device::read_line(string &data)
{
    data.clear();
    char c;
    for (;;)
    {
        ssize_t size_read = read(&c, 1);

        if (size_read < 0)
            return false;

        if (size_read == 0)
            return !data.empty();

        data.push_back(c);

        if (c == '\n')
            return true;
    }
}

psl::optional<psl::string> psl::i_device::read_all()
{
    psl::string data;
    if (read_all(data))
    {
        return data;
    }
    return {};
}

psl::io_pipe::io_pipe(i_device *from, o_device *to) : m_from(from), m_to(to)
{
    m_stopToken.store(false);

    m_thread = psl::thread([this]
    {
        uint8_t buff[100];
        for (;;)
        {
            if (m_stopToken.load())
                return;

            ssize_t read = m_from->read(&buff[0], sizeof(buff));

            if (read < 0)
            {
                return;
            }

            if (!m_to->write(&buff[0], read))
            {
                return;
            }
        }
    });
}

psl::io_pipe::~io_pipe()
{
    stop();
}

void psl::io_pipe::stop()
{
    m_stopToken.store(true);
    m_thread.join();
}
