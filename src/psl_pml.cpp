#include "psl_pml.h"
#include "psl_file.h"
#include "psl_from_string.h"
#include "psl_memory.h"
#include "psl_string_builder.h"
#include "psl_vector.h"

namespace psl
{

class pml_node_num : public pml_node
{
public:
    pml_node_num()
        : pml_node_num(psl::string(), value_type_num(0))
    {}
    explicit pml_node_num(const psl::string &name, value_type_num value)
        : pml_node(pml_node_type::num, name),
          value(value)
    {}
    value_type_num value;
};
class pml_node_str : public pml_node
{
public:
    pml_node_str()
        : pml_node_str(psl::string(), value_type_str())
    {}
    explicit pml_node_str(const psl::string &name, const value_type_str &value)
        : pml_node(pml_node_type::str, name),
          value(value)
    {}
    value_type_str value;
};
class pml_node_bool : public pml_node
{
public:
    pml_node_bool()
        : pml_node_bool(psl::string(), value_type_bool())
    {}
    explicit pml_node_bool(const psl::string &name, value_type_bool value)
        : pml_node(pml_node_type::bool_, name),
          value(value)
    {}
    value_type_bool value;
};


psl::value_type_num psl::pml_node::to_num() const
{
    if (!is_num())
    {
        throw psl::pml_type_error("to_num: wrong type");
    }
    return static_cast<const pml_node_num *>(this)->value;
}

pml_node::pml_node(const string &name)
    : m_type(pml_node_type::value_less), m_name(name)
{}

pml_node::pml_node(pml_node_type type, const string &name)
    : m_type(type),
      m_name(name)
{}

pml_node_type pml_node::get_type() const
{
    return m_type;
}

bool pml_node::is(pml_node_type type) const
{
    return m_type == type;
}

bool pml_node::has_value() const
{
    return m_type != pml_node_type::value_less;
}

bool pml_node::is_num() const
{
    return m_type == pml_node_type::num;
}

bool pml_node::is_str() const
{
    return m_type == pml_node_type::str;
}

bool pml_node::is_bool() const
{
    return m_type == pml_node_type::bool_;
}

const value_type_str &pml_node::to_str() const
{
    if (!is_str())
    {
        throw psl::pml_type_error("to_str: wrong type");
    }
    return static_cast<const pml_node_str *>(this)->value;
}

value_type_bool pml_node::to_bool() const
{
    if (!is_bool())
    {
        throw psl::pml_type_error("to_bool: wrong type");
    }
    return static_cast<const pml_node_bool *>(this)->value;
}

void pml_node::push_back(pml_node::ptr node)
{
    m_lst_children.emplace_back(psl::move(node));
}

size_t pml_node::get_children_size() const
{
    return m_lst_children.size();
}

const psl::vector<pml_node::ptr> &pml_node::get_children() const
{
    return m_lst_children;
}

psl::vector<pml_node::ptr> &pml_node::get_children()
{
    return m_lst_children;
}

pml_node::raw_ptr pml_node::get_child(size_t ix) const
{
    if (ix >= m_lst_children.size())
        return nullptr;

    return m_lst_children[ix].get();
}
pml_node::raw_ptr pml_node::get_child(const string_view &name) const
{
    auto it = psl::find_if(m_lst_children.begin(), m_lst_children.end(), [&name](const pml_node::ptr &node){ return string_view(node->m_name.data(), node->m_name.length()) == name; });
    if (it == m_lst_children.end())
        return nullptr;
    return it->get();
}

pml_node::raw_ptr pml_node::operator[](size_t ix) const
{
    return get_child(ix);
}
pml_node::raw_ptr pml_node::operator[](const string_view &name) const
{
    return get_child(name);
}


namespace pml
{

pml_node::ptr make(const string &name)
{
    return psl::make_unique<pml_node>(name);
}

pml_node::ptr make_num(const string &name, value_type_num value)
{
    return psl::make_unique<pml_node_num>(name, value);
}

pml_node::ptr make_str(const string &name, const value_type_str &value)
{
    return psl::make_unique<pml_node_str>(name, value);
}

pml_node::ptr make_bool(const string &name, value_type_bool value)
{
    return psl::make_unique<pml_node_bool>(name, value);
}

pml_node::ptr parse(psl::i_device &i_device)
{
    if (!i_device.is_open())
        return nullptr;

    psl::optional<psl::string> content = i_device.read_all();

    if (!content)
        return nullptr;

    return parse(*content);
}

pml_node::ptr parse(const filesystem::path &path)
{
    psl::file file(path.native(), psl::open_mode::read_only);
    return parse(file);
}

pml_node::ptr parse(const string &data)
{
    return parse(data.data(), data.length());
}

/*
<person>
    <name>Mark</name>
    <age>18</age>
    <hands>
        <>Left</>
        <>Right</>
        <>13</>
    </hands>
</person>
*/

bool is_space(char c)
{
    return c == ' ' || c == '\n' || c == '\t';
}
bool is_num(char c)
{
    return c >= '0' && c <= '9';
}
bool is_alpha(char c)
{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}
bool is_alpha_num(char c)
{
    return is_alpha(c) || is_num(c);
}

void consume_space(const char *data, size_t len, size_t &pos)
{
    for (; pos < len; ++pos)
    {
        char c = data[pos];
        if (!is_space(c))
            return;
    }
}

bool consume_char(const char *data, size_t len, size_t &pos, char c)
{
    consume_space(data, len, pos);

    if (data[pos] == c)
    {
        ++pos;
        return true;
    }
    return false;
}
bool consume_str(const char *data, size_t len, size_t &pos, const psl::string &str)
{
    consume_space(data, len, pos);

    int j = 0;
    for (; pos < len && j < str.size(); ++pos, ++j)
    {
        if (data[pos] != str[j])
        {
            return false;
        }
    }

    return j == str.size();
}

bool peek_char(const char *data, size_t len, size_t pos, char c)
{
    size_t p = pos;
    return consume_char(data, len, p, c);
}
bool peek_str(const char *data, size_t len, size_t &pos, const psl::string &str)
{
    size_t p = pos;
    return consume_str(data, len, p, str);
}

psl::string parse_name(const char *data, size_t len, size_t &pos)
{
    psl::string name;

    for (; pos < len; ++pos)
    {
        char c = data[pos];
        if (is_alpha_num(c))
            name.push_back(c);
        else
            break;
    }
    return name;
}
psl::string parse_value(const char *data, size_t len, size_t &pos)
{
    psl::string value;
    for (; pos < len; ++pos)
    {
        char c = data[pos];
        if (is_alpha_num(c) || is_space(c))
            value.push_back(c);
        else
            break;
    }
    return value;
}
psl::string consume_name(const char *data, size_t len, size_t &pos)
{
    consume_space(data, len, pos);

    psl::string name;

    for (; pos < len; ++pos)
    {
        char c = data[pos];
        if (is_alpha_num(c))
            name.push_back(c);
        else
            return name;
    }
    return name;
}
void consume_node_close(const char *data, size_t len, size_t &pos, const psl::string &name)
{
    if (!consume_str(data, len, pos, "</"))
        throw pml_parse_error(psl::format("unexpected token '{}' at pos {}. expected '</'.\n\t'{}'", data[pos], pos, data + pos));
    if (!consume_str(data, len, pos, name))
        throw pml_parse_error(psl::format("unexpected token '{}' at pos {}. expected name '{}'.\n\t'{}'", data[pos], pos, name, data + pos));
    if (!consume_char(data, len, pos, '>'))
        throw pml_parse_error(psl::format("unexpected token '{}' at pos {}. expected '>'.\n\t'{}'", data[pos], pos, data + pos));
}


pml_node::ptr make_node(const psl::string &name, const psl::string &value)
{
    if (value.empty())
    {
        return make(name);
    }

    if (psl::all_of(value.begin(), value.end(), is_space))
    {
        return make(name);
    }

    if (value == "false")
    {
        return make_bool(name, false);
    }

    if (value == "true")
    {
        return make_bool(name, true);
    }

    bool ok = false;
    value_type_num num = psl::from_string<value_type_num>(value, &ok);
    if (ok)
    {
        return make_num(name, num);
    }

    return make_str(name, value);
}


pml_node::ptr parse_node(const char *data, size_t len, size_t &pos)
{
    if (!consume_char(data, len, pos, '<'))
        throw pml_parse_error(psl::format("unexpected token '{}' at pos {}. expected '<'.\n\t'{}'", data[pos], pos, data + pos));

    psl::string name = parse_name(data, len, pos);

    if (!consume_char(data, len, pos, '>'))
        throw pml_parse_error(psl::format("unexpected token '{}' at pos {}. expected '>'.\n\t'{}'", data[pos], pos, data + pos));

    psl::string value = parse_value(data, len, pos);
    pml_node::ptr node = make_node(name, value);

    //psl::println("parsed start of node {}: {}", name, value);

    if (peek_str(data, len, pos, "</"))
    {
        consume_node_close(data, len, pos, name);
        //psl::println("parsed end of node {}: {}", name, value);
        return node;
    }

    // parse children
    while (pos < len)
    {
        if (!peek_char(data, len, pos, '<'))
            return node;

        auto child = parse_node(data, len, pos);
        if (!child)
        {
            return {};
        }
        node->push_back(psl::move(child));

        if (peek_str(data, len, pos, "</"))
        {
            consume_node_close(data, len, pos, name);
            //psl::println("parsed end of node {}: {}", name, value);
            return node;
        }
    }

    return node;
}

pml_node::ptr parse(const char *data, size_t len)
{
    auto root = make("root");

    if (len == 0)
        return root;//throw?

    size_t pos = 0;

    while (pos + 1 < len)
    {
        auto node = parse_node(data, len, pos);
        if (!node)
        {
            return {};
        }
        root->push_back(psl::move(node));
    }

    return root;
}

psl::string to_string(const pml_node::raw_ptr node, unsigned int indent)
{
    psl::string_builder builder;
    write(node, builder, indent);
    return psl::move(builder.get_string());
}

#define CHECK(x) do { if (!x) return false; } while(0)

bool write(const pml_node::raw_ptr node, o_device &o_device, unsigned int indent_size, unsigned int indent_count)
{
    psl::string ind1(' ', indent_size * indent_count);
    psl::string ind2(' ', indent_size * (indent_count + 1));
    CHECK(o_device.write(ind1));
    CHECK(o_device.write(psl::format("<{}>\n", node->get_name())));

    switch (node->get_type())
    {
    case pml_node_type::num:
        CHECK(o_device.write(ind2));
        CHECK(o_device.write(psl::format("{}\n", node->to_num())));
        break;
    case pml_node_type::str:
        CHECK(o_device.write(ind2));
        CHECK(o_device.write(psl::format("{}\n", node->to_str())));
        break;
    case pml_node_type::bool_:
        CHECK(o_device.write(ind2));
        CHECK(o_device.write(psl::format("{}\n", node->to_bool())));
        break;
    case pml_node_type::value_less:
        break;
    }

    for (const pml_node::ptr &child : node->get_children())
    {
        CHECK(write(child.get(), o_device, indent_size, indent_count + 1));
    }

    CHECK(o_device.write(ind1));
    CHECK(o_device.write(psl::format("</{}>\n", node->get_name())));

    return true;
}

bool write(const pml_node::raw_ptr node, o_device &o_device, unsigned int indent)
{
    return write(node, o_device, indent, 0);
}

}

}
