#include "psl_logging.h"

#include <cstdio>

#include <cstring>

/// @todo make time functionality
#include <cstdio>
#include <ctime>


namespace psl
{


/**
* psl::logging
**/
logging &psl::logging::get()
{
    static logging singleton;
    return singleton;
}

void logging::register_logger(i_logger *logger)
{
    if (is_registered(logger))
    {
        psl::print(stderr, "logger already registered...\n");
        return;
    }

    m_loggers.push_back(logger);
}

void logging::un_register_logger(i_logger *logger)
{
    auto it = psl::find(m_loggers.cbegin(), m_loggers.cend(), logger);

    if (it == m_loggers.cend())
    {
        psl::print(stderr, "logger not registered...\n");
        return;
    }

    m_loggers.erase(it);
}

bool logging::is_registered(i_logger *logger) const
{
    return psl::contains(m_loggers.begin(), m_loggers.end(), logger);
}

void logging::set_level_all(lvl level)
{
    for (auto logger : m_loggers)
        logger->set_level(level);
}









void logging::log_empty_line()
{
    log_pure("");
}

void psl::logging::log_empty_lines(unsigned int n)
{
    if (n == 0)
        return;

    if (n == 1)
    {
        log_empty_line();
        return;
    }

    psl::string str('\n', n - 1);
    log_pure(str.c_str());
}

bool logging::any_logger_active()
{
    return psl::any_of(m_loggers.cbegin(), m_loggers.cend(), [](i_logger *logger)
    {
        return logger->is_active();
    });
}

bool logging::any_logger_for_lvl(lvl level)
{
    return psl::any_of(m_loggers.cbegin(), m_loggers.cend(), [&level](i_logger *logger)
    {
        return logger->is_active_for_level(level);
    });
}

void logging::log_level_pure_unchecked(lvl level, const string &str)
{
    psl::for_each(m_loggers.begin(), m_loggers.end(), [&level, &str](i_logger *logger)
    {
        if (logger->is_active_for_level(level))
            logger->log(str);
    });
}

void logging::log_pure_unchecked(const string &str)
{
    psl::for_each(m_loggers.begin(), m_loggers.end(), [&str](i_logger *logger)
    {
        if (logger->is_active())
            logger->log(str);
    });
}

const char *logging::shorten_file_name(const char *file)
{
    auto len = psl::strlen(file);
    const char *ptr = file + len - 1;

    while (ptr > file)
    {
        if (*ptr == '/')
            return ptr + 1;
        --ptr;
    }
    return file;
}

/**
* psl::logger_base
**/
i_logger::i_logger()
{
    logging::get().register_logger(this);
}

i_logger::~i_logger()
{
    if (is_registered())
        logging::get().un_register_logger(this);
}

lvl i_logger::level() const
{
    return m_level;
}

void i_logger::set_level(const lvl &level)
{
    m_level = level;
}

bool i_logger::is_active() const
{
    return m_active;
}

void i_logger::set_active(bool active)
{
    m_active = active;
}

bool i_logger::is_active_for_level(lvl level)
{
    return m_active && (static_cast<int>(m_level) >= static_cast<int>(level));
}

bool i_logger::is_registered() const
{
    return logging::get().is_registered(const_cast<i_logger *>(this));
}

/**
* psl::logger_stdout
**/
void logger_stdout::log(const string &str)
{
    psl::println(str);
}

/**
* psl::logger_stderr
**/
void logger_stderr::log(const string &str)
{
    /// @todo add printls
    psl::print(stderr, "{}\n", str);
}

/**
* psl::log_entry
**/
log_entry::log_entry(const char *file, int line, const char *func, lvl level)
    : m_file(file), m_line(line), m_level(level), m_str(func)
{
    logging::get().log_level(m_file, m_line, m_level, psl::string("(>) ") + m_str);
}

log_entry::~log_entry()
{
    logging::get().log_level(m_file, m_line, m_level, psl::string("(<) ") + m_str);
}

/**
* free functions
**/
string to_string(lvl level)
{
    switch (level)
    {
    case lvl::base: return "base";
    case lvl::info: return "info";
    case lvl::debug: return "debug";
    case lvl::fullblown: return "fullblown";
    }

    return "invalid";
}

}
