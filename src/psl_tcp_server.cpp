#include "psl_tcp_server.h"

#include <sys/socket.h>
#include <netinet/in.h>


namespace psl
{

class tcp_server_impl
{
public:
    bool is_open() const
    {
        return m_is_open;
    }

    bool open(int port)
    {
        m_sockfd = socket(AF_INET, SOCK_STREAM, 0);

        if (m_sockfd < 0)
        {
            perror("ERROR opening socket");
            return false;
        }

        ::memset(&m_serv_addr, 0, sizeof(m_serv_addr));

        const int enable = 1;
        if (setsockopt(m_sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
        {
            perror("ERROR on setsockopt");
            return false;
        }

        m_serv_addr.sin_family = AF_INET;
        m_serv_addr.sin_addr.s_addr = INADDR_ANY;
        m_serv_addr.sin_port = htons(port);

        if (bind(m_sockfd, (struct sockaddr *) &m_serv_addr, sizeof(m_serv_addr)) < 0)
        {
            perror("ERROR on binding");
            return false;
        }

        listen(m_sockfd, 5);
        m_clilen = sizeof(m_cli_addr);
        m_newsockfd = accept(m_sockfd, (struct sockaddr *) &m_cli_addr, &m_clilen);
        if (m_newsockfd < 0)
        {
            perror("ERROR on accept");
            return false;
        }

        m_is_open = true;

        return true;

    }

    bool close()
    {
        if (!m_is_open)
        {
            return true;
        }

        ::shutdown(m_newsockfd, SHUT_RDWR);

        uint8_t data[128];

        while(read(data, sizeof(data)) > 0);

        ::close(m_newsockfd);
        ::close(m_sockfd);
        m_is_open = false;
        return true;
    }

    bool write(const uint8_t *data, size_t len)
    {
        ssize_t n = ::write(m_newsockfd, data, len);
        if (n < 0)
        {
            perror("ERROR writing to socket");
            return false;
        }
        return true;

    }
    ssize_t read(uint8_t *data, size_t len)
    {
        ssize_t n = ::read(m_newsockfd, data, len);
        if (n < 0)
        {
            perror("ERROR reading from socket");
            return -1;
        }
        return n;
    }


private:
    int m_sockfd, m_newsockfd;
    socklen_t m_clilen;
    struct sockaddr_in m_serv_addr, m_cli_addr;
    bool m_is_open = false;

};

tcp_server::tcp_server()
    : m_impl(psl::make_unique<psl::tcp_server_impl>())
{}

tcp_server::~tcp_server()
{
    m_impl->close();
}

bool tcp_server::is_open() const
{
    return m_impl->is_open();
}

bool tcp_server::open(int port)
{
    return m_impl->open(port);
}

bool tcp_server::close()
{
    return m_impl->close();
}

bool tcp_server::write_intern(const uint8_t *data, size_t len)
{
    return m_impl->write(data, len);
}

ssize_t tcp_server::read_intern(uint8_t *data, size_t len)
{
    return m_impl->read(data, len);
}

}


/*

#include <fcntl.h>

int flags = fcntl(m_sockfd, F_GETFL, 0);
if (flags == -1) return false;
flags = false ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
if (fcntl(m_sockfd, F_SETFL, flags) != 0)
{
    return error("ERROR on set non blocking");
}

*/
