#include "psl_process_impl.h"

#include <cstdio>


namespace psl
{

process_impl::process_impl()
{

}

process_impl::process_impl(const psl::string &program)
{

    char path[1035];

    /* Open the command for reading. */
    FILE *fp = popen(program.c_str(), "r");

    if (fp == nullptr)
    {
        printf("Failed to open process\n" );
        return;
    }

    /* Read the output a line at a time - output it. */
    while (fgets(path, sizeof(path), fp) != nullptr)
    {
        printf("%s", path);
    }

    /* close */
    pclose(fp);
}

}


bool psl::process_impl::is_open() const
{
    return m_is_open;
}

bool psl::process_impl::close()
{
    //pclose(fp);
}

bool psl::process_impl::write(char *data, size_t len)
{
}

bool psl::process_impl::read(psl::vector<char> &data)
{
}
