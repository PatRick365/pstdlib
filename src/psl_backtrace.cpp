#include "psl_backtrace.h"

#include <execinfo.h>
#include <cxxabi.h>


namespace psl
{

constexpr size_t BT_BUF_SIZE = 128;

backtrace::backtrace()
{
    void *callstack[BT_BUF_SIZE];
    m_frames = ::backtrace(callstack, BT_BUF_SIZE);
    m_strs = ::backtrace_symbols(callstack, m_frames);
}

backtrace::~backtrace()
{
    free(m_strs);
}

psl::vector<string_view> backtrace::frames() const
{
    if (m_frames <= 0 || !m_strs)
    {
        return {};
    }

    psl::vector<string_view> lst;
    for (int i = 0; i < m_frames; ++i)
    {
        lst.push_back(psl::string_view(m_strs[i]));
    }
    return lst;
}

psl::optional<psl::string> demangle(const psl::string &name)
{
    int status = 0;
    char *realname = abi::__cxa_demangle(name.c_str(), 0, 0, &status);

    if (status != 0)
    {
        free((void *) realname);
        return {};
    }

    psl::string result = realname;
    free(realname);
    return result;
}

}
