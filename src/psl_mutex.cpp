#include "psl_mutex.h"
#include "psl_format.h"

#define	EBUSY		16	/* Device or resource busy */


namespace psl
{

mutex::mutex()
{
    if (int ret = ::pthread_mutexattr_init(&m_attr))
    {
        psl::print(stderr, "mutex attr init has failed. error {}: {}\n", ret, ::strerror(ret));
        throw psl::system_error("mutex attr init has failed!");
    }

    if (int ret = ::pthread_mutexattr_settype(&m_attr, PTHREAD_MUTEX_RECURSIVE))
    {
        psl::print(stderr, "mutex attr set type has failed. error {}: {}\n", ret, ::strerror(ret));
        throw psl::system_error("mutex attr set type has failed!");
    }

    if (int ret = ::pthread_mutex_init(&m_pthread_mutex, &m_attr))
    {
        psl::print(stderr, "mutex init has failed. error {}: {}\n", ret, ::strerror(ret));
        throw psl::system_error("mutex init has failed!");
    }
}

mutex::~mutex()
{
    if (int ret = ::pthread_mutexattr_destroy(&m_attr))
    {
        psl::print(stderr, "mutex attr destruction has failed. error {}: {}\n", ret, ::strerror(ret));
    }

    if (int ret = ::pthread_mutex_destroy(&m_pthread_mutex))
    {
        psl::print(stderr, "mutex destruction has failed. error {}: {}\n", ret, ::strerror(ret));
    }
}

bool mutex::take()
{
    if (int ret = ::pthread_mutex_lock(&m_pthread_mutex))
    {
        psl::print(stderr, "pthread_mutex_lock failed. error {}: {}\n", ret, ::strerror(ret));
        return false;
    }
    return true;
}

bool mutex::try_take()
{
    if (int ret = ::pthread_mutex_trylock(&m_pthread_mutex))
    {
        if (ret != EBUSY)
            psl::print(stderr, "pthread_mutex_trylock failed. error {}: {}\n", ret, ::strerror(ret));
        return false;
    }
    return true;
}

bool mutex::give()
{
    if (int ret = ::pthread_mutex_unlock(&m_pthread_mutex))
    {
        psl::print(stderr, "pthread_mutex_unlock failed. error {}: {}\n", ret, ::strerror(ret));
        return false;
    }
    return true;
}


pthread_mutex_t &mutex::native_handle()
{
    return m_pthread_mutex;
}

region_lock::region_lock(mutex &mut) : m_mut(mut)
{
    if (!m_mut.take())
        throw psl::system_error("region_lock take failed!");
}

region_lock::~region_lock() noexcept(false)
{
    if (!m_mut.give())
        throw psl::system_error("region_lock give failed!");
}

}
