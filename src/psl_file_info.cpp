#include "psl_file_info.h"
#include "psl_file_manip.h"


namespace psl
{

string file_info::to_string(file_info::type t)
{
    switch (t)
    {
    case type::invalid: return "invalid";
    case type::file: return "file";
    case type::dir: return "dir";
    case type::char_dev: return "char_dev";
    case type::block_dev:  return "block_dev";
    case type::named_pipe: return "named_pipe";
    case type::sym_link:  return "sym_link";
    case type::socket: return "socket";
    }
    return "unknown";
}

file_info::file_info(int file_descriptor)
{
    m_exists = (psl::file_fstat(file_descriptor, m_stat) == 0);
    m_is_init = true;
}

file_info::file_info(const string &path)
{
    m_exists = (psl::file_stat(path.c_str(), m_stat) == 0);
    m_is_init = true;
}

bool file_info::is_type(type t) const
{
    if (m_is_init)
    {
        switch (t)
        {
        case type::invalid: return false;
        case type::file: return S_ISREG(m_stat.st_mode);
        case type::dir: return S_ISDIR(m_stat.st_mode);
        case type::char_dev: return S_ISCHR(m_stat.st_mode);
        case type::block_dev: return S_ISBLK(m_stat.st_mode);
        case type::named_pipe: return S_ISFIFO(m_stat.st_mode);
        case type::sym_link: return S_ISLNK(m_stat.st_mode);
        case type::socket: return S_ISSOCK(m_stat.st_mode);
        }
    }
    return false;
}

ssize_t file_info::size() const
{
    return m_stat.st_size;
}

bool file_info::exists() const
{
    return m_exists;
}

bool file_info::is_regular_file() const
{
    return is_type(type::file);
}

bool file_info::is_directory() const
{
    return is_type(type::dir);
}

bool file_info::is_symlink() const
{
    return is_type(type::sym_link);
}

}
