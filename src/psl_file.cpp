#include "psl_file.h"

#include "psl_char_manip.h"
#include "psl_file_manip.h"
#include "psl_file_info.h"


namespace psl
{

struct file_impl
{
    bool check_is_open() const
    {
        if (!m_is_open)
        {
            psl::print(stderr, "file '{}' is not open..\n", m_name);
            return false;
        }
        return true;
    }

    void sync_if_needed()
    {
        if (m_was_written_to)
        {
            psl::file_fsync(m_file_descriptor);
            m_was_written_to = false;
        }
    }

    psl::string m_name;
    int m_file_descriptor = 0;
    bool m_is_open = false;
    bool m_was_written_to = false;
};



file::~file() = default;

file::file()
    : m_impl(psl::make_unique<file_impl>())
{}

file::file(const string &path, open_mode mode)
    : m_impl(psl::make_unique<file_impl>())
{
    open(path, mode);
}

bool file::open(const string &path, open_mode mode)
{
    m_impl->m_file_descriptor = psl::file_open(path.c_str(), static_cast<int>(mode));

    if (m_impl->m_file_descriptor < 0)
    {
        return false;
    }

    if (!psl::file_info(m_impl->m_file_descriptor).is_regular_file())
    {
        psl::print(stderr, "file is not a regular file\n");
        return false;
    }

    m_impl->m_is_open = true;
    m_impl->m_name = path;

    if (psl::file_lseek(m_impl->m_file_descriptor, 0, SEEK_SET) != 0)
    {
        psl::print(stderr, "seek to begin failed\n");
    }

    return true;
}

void file::sync()
{
    m_impl->sync_if_needed();
}

psl::optional<string> file::read(ssize_t len)
{
    if (len <= 0)
        return psl::nullopt;

    if (!m_impl->check_is_open())
        return psl::nullopt;

    m_impl->sync_if_needed();

//    if (psl::file_lseek(m_file_descriptor, 0, SEEK_SET) != 0)
//    {
//        printf("seek failed...\n");
//        return psl::string();
//    }

    psl::string str(len);   // this is reserve grr.. bug was here...
    str.resize(len);

    /*ssize_t bytes_read = */psl::file_read(m_impl->m_file_descriptor, str.data(), static_cast<size_t>(len));
    str.data()[str.size()] = '\0';

    //printf(">>> len: %zd - bytes_read: %zd\n", len, bytes_read);

    return str;
}

psl::optional<string> file::read_all()
{
    if (!m_impl->check_is_open())
        return psl::nullopt;

    return read(psl::file_info(m_impl->m_file_descriptor).size());
}

bool file::remove()
{
    close();
    return (psl::file_unlink(m_impl->m_name.c_str()) == 0);

}

bool file::close()
{
    if (m_impl->m_is_open)
    {
        bool success = (file_close(m_impl->m_file_descriptor) == 0);

        if (success)
            m_impl->m_is_open = false;

        return success;
    }

    return true;
}

bool file::is_open() const
{
    return m_impl->m_is_open;
}

bool file::at_end() const
{
    psl::print("%s NOT IMPLEMENTED !!!", __PRETTY_FUNCTION__);
    abort();
}

ssize_t file::pos() const
{
    psl::print("%s NOT IMPLEMENTED !!!", __PRETTY_FUNCTION__);
    abort();
}

bool file::seek(ssize_t pos)
{
    if (!m_impl->check_is_open())
        return false;

    return (psl::file_lseek(m_impl->m_file_descriptor, pos, SEEK_SET) == 0);
}

bool file::seek_begin()
{
    return seek(0);
}

ssize_t file::size() const
{
    if (!m_impl->check_is_open())
        return -1;

    return psl::file_info(m_impl->m_file_descriptor).size();
}

string file::name() const
{
    return m_impl->m_name;
}

void file::dump_info() const
{
    psl::print("-v-dump_info\n");

    if (!m_impl->check_is_open())
    {
        psl::print(stderr, "file not open\n");
    }
    else
    {
        file_info info(m_impl->m_file_descriptor);
        psl::print("-v-dump_info\n");
        psl::print("size: {}\n", info.size());
        psl::print("is file: {}\n", info.is_type(file_info::type::file));
        psl::print("is dir: {}\n", info.is_type(file_info::type::dir));
        psl::print("is char_dev: {}\n", info.is_type(file_info::type::char_dev));
        psl::print("is block_dev: {}\n", info.is_type(file_info::type::block_dev));
        psl::print("is named_pipe: {}\n", info.is_type(file_info::type::named_pipe));
        psl::print("is sym_link: {}\n", info.is_type(file_info::type::sym_link));
        psl::print("is socket: {}\n", info.is_type(file_info::type::socket));
    }
    psl::print("-^----------\n\n");
}

bool file::remove(const string &path)
{
    return (psl::file_unlink(path.c_str()) == 0);
}

bool file::write_intern(const uint8_t *data, size_t len)
{
    if (!m_impl->check_is_open())
        return false;

    m_impl->m_was_written_to = true;
    return file_write(m_impl->m_file_descriptor, reinterpret_cast<const void *>(data), len) == static_cast<ssize_t>(len);

}

ssize_t file::read_intern(uint8_t *data, size_t len)
{
    return psl::file_read(m_impl->m_file_descriptor, data, len);
}

}

