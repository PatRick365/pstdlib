#include "psl_file_manip.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


namespace psl
{

int file_open(const char *path, int mode)
{
    return open(path, static_cast<int>(mode), S_IRWXU);
}

ssize_t file_write(int fd, const void *buf, size_t count)
{
    return write(fd, buf, count);
}

ssize_t file_read(int fd, void *buf, size_t count)
{
    return read(fd, buf, count);
}

int file_fsync(int fd)
{
    return fsync(fd);
}

int file_close(int fd)
{
    return close(fd);
}

int file_unlink(const char *path)
{
    return unlink(path);
}

off_t file_lseek(int fd, off_t offset, int whence)
{
    return lseek(fd, offset, whence);
}

int file_fstat(int fd, struct stat &s)
{
    return fstat(fd, &s);
}

int file_stat(const char *path, struct stat &s)
{
    return stat(path, &s);
}




}
