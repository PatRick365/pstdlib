#include "psl_stdio.h"


namespace psl
{

bool stdcout::write_stderr(const char *data, size_t len)
{
    ::write(STDERR_FILENO, data, len);
    return true;
}

bool stdcout::write_intern(const uint8_t *data, size_t len)
{
    ::write(STDOUT_FILENO, data, len);
    return true;
}

ssize_t stdcout::read_intern(uint8_t *data, size_t len)
{
    return ::read(STDIN_FILENO, data, len);
}


bool stdcerr::write_stdout(const char *data, size_t len)
{
    ::write(STDOUT_FILENO, data, len);
    return true;
}

bool stdcerr::write_intern(const uint8_t *data, size_t len)
{
    ::write(STDERR_FILENO, data, len);
    return true;
}

ssize_t stdcerr::read_intern(uint8_t *data, size_t len)
{
    return ::read(STDIN_FILENO, data, len);
}

}
