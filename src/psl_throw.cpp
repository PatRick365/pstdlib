#include "psl_throw.h"

#include "psl_format.h"
#include "psl_except.h"


namespace psl::internal
{

void throw_out_of_range(const char *function, size_t position, size_t size)
{
    throw psl::out_of_range(psl::format("{} out of range: sz = {} but position = {}", function, size, position));
}

void throw_exception(const char *what)
{
    throw psl::exception(what);
}

}
