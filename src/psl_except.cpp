#include "psl_except.h"
#include "psl_string.h"


namespace psl
{

void terminate() noexcept
{
    ::exit(1);
}

exception::exception()
    : m_what("exception occured.")
{
    //printf("%s\n\n", m_what.c_str());
}

psl::exception::exception(const psl::string &what)
    : m_what(what)
{}

const string &exception::what() const
{
    return m_what;
}

}
