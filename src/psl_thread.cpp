#include "psl_thread.h"


namespace psl
{

condition_variable::condition_variable()
    : m_condition(PTHREAD_COND_INITIALIZER)
{}

void condition_variable::notify_one() noexcept
{
    if (int ret = ::pthread_cond_signal(&m_condition))
    {
        psl::print(stderr, "pthread_cond_signal failed. error {}: {}\n", ret, ::strerror(ret));
    }
}

void condition_variable::notify_all() noexcept
{
    if (int ret = ::pthread_cond_broadcast(&m_condition))
    {
        psl::print(stderr, "pthread_cond_broadcast failed. error {}: {}\n", ret, ::strerror(ret));
    }
}

void condition_variable::wait(mutex &lock)
{
    if (int ret = ::pthread_cond_wait(&m_condition, &lock.native_handle()))
    {
        psl::print(stderr, "pthread_cond_broadcast failed. error {}: {}\n", ret, ::strerror(ret));
    }
}

barrier::barrier(size_t num_threads)
{
    if (int ret = ::pthread_barrier_init(&m_barrier, nullptr, num_threads))
    {
        psl::print(stderr, "pthread_barrier_init has failed. error {}: {}\n", ret, ::strerror(ret));
        psl::terminate();
    }
}

barrier::~barrier()
{
    if (int ret = ::pthread_barrier_destroy(&m_barrier))
    {
        psl::print(stderr, "pthread_barrier_destroy has failed. error {}: {}\n", ret, ::strerror(ret));
        psl::terminate();
    }
}

void barrier::wait()
{
    if (int ret = ::pthread_barrier_wait(&m_barrier))
    {
        if (ret != PTHREAD_BARRIER_SERIAL_THREAD)
        {
            psl::print(stderr, "pthread_barrier_wait has failed. error {}: {}\n", ret, ::strerror(ret));
            psl::terminate();
        }
    }
}


thread::thread(thread &&other)
    : m_id(other.m_id)
{
    other.m_id = id();
}

thread &thread::operator=(thread &&other)
{
    m_id = other.m_id;
    other.m_id = id();
    return *this;
}

thread::~thread()
{
    if (joinable())
    {
        psl::print(stderr, "error destroying thread, still running\n");
        psl::terminate();
    }
    // actually a jthread feature
    //if (joinable())
    //    join();
}

bool thread::join()
{
    if (!joinable())
    {
        psl::print(stderr, "error joining thread, not joinable.\n");
        return false;
    }

    if (int ret = ::pthread_join(m_id.m_handle, nullptr))
    {
        psl::print(stderr, "error joining thread: {}\n", ret);
        return false;
    }

    m_id = id();
    return true;
}

bool thread::try_join()
{
    if (int ret = ::pthread_tryjoin_np(m_id.m_handle, nullptr))
    {
        psl::print(stderr, "error try joining thread: {}\n", ret);
        return false;
    }
    return true;
}

bool thread::joinable() const
{
    return (get_id() != psl::thread::id());
}

pthread_t thread::native_handle() const
{
    return m_id.m_handle;
}

thread::id thread::get_id() const
{
    return m_id;
}

void thread::detach()
{
    // hmmmmm.. what if the thread already finished..???
    if (!joinable())
        throw system_error("can not detach not joinable thread");

    if (int ret = ::pthread_detach(m_id.m_handle))
    {
        psl::print(stderr, "pthread_detach failed. error {}: {}\n", ret, ::strerror(ret));
        return;
    }
    m_id = id();
}

}
