# pstdlib

`pstdlib` is a C++ library that provides a collection of utilities and data structures to enhance C++ programming. The library includes implementations for atomic operations, futures, threading, vectors, and message queues, among others. It aims to bring joy to the author while hacking around on it.

## Features

- **Vectors**: Includes dynamic array implementations that provide flexibility and performance for sequence data structures. The provided vector class supports efficient memory management, automatic resizing, and high-speed data access operations.

- **Memory Management Utilities**: Offers smart pointers, unique pointers, and other memory management tools to facilitate safe and efficient memory usage, reducing the risk of memory leaks.

- **File I/O Capabilities**: Provides utilities for efficient file reading and writing operations, including support for binary and text files. The library offers abstractions for handling file streams.

- **Futures and Promises**: Implements mechanisms for asynchronous computations, allowing functions to return results that will be available in the future. This feature is particularly useful for designing non-blocking applications and parallel task execution.

- **Thread Management**: Offers classes to create and manage threads, facilitating parallel execution of tasks. This includes utilities for managing worker threads, joining and detaching threads, and ensuring proper synchronization between tasks.

- **Thread Pool**: Includes a robust thread pool implementation that efficiently manages multiple worker threads to process queued tasks asynchronously. This helps optimize CPU utilization and improve application responsiveness.

- **Message Queues**: Implements thread-safe queues for inter-thread communication, essential for producer-consumer scenarios. These message queues allow threads to exchange data safely without the need for complex synchronization mechanisms.

- **Packaged Tasks**: Enables the encapsulation of function calls along with their future results, simplifying asynchronous task execution and deferred computations.

- **Synchronization Primitives**: Provides essential synchronization primitives such as mutexes, condition variables, and barriers, which help prevent race conditions and ensure proper coordination among threads.

- **Logging System**: Implements a lightweight logging system that supports different log levels (info, warning, error, etc.) and allows output to various destinations, such as console or files. This helps with debugging and monitoring application behavior.

## Installation

To include `pstdlib` in your project, follow these steps:

1. **Clone the Repository**:

   ```bash
   git clone --recurse-submodules https://gitlab.com/PatRick365/pstdlib.git
   ```

2. **Navigate to the Project Directory**:

   ```bash
   cd pstdlib
   ```

3. **Build the Library**:

   Ensure you have a C++ compiler and CMake installed. Then, run:

   ```bash
   mkdir build
   cd build
   cmake ..
   make
   ```

4. **Install the Library**:

   You may need administrative privileges for this step:

   ```bash
   sudo make install
   ```

## Usage

After installation, you can include `pstdlib` headers in your C++ projects:

```cpp
#include <psl_atomic.h>
#include <psl_future.h>
#include <psl_thread.h>
#include <psl_vector.h>
#include <psl_message_queue.h>
```

Ensure that your compiler can locate the installed headers and link against the `pstdlib` library during the build process.

## Examples

Here's a simple example demonstrating the creation of a thread using `pstdlib` and printing colored formatted output:

```cpp
#include <psl_thread.h>
#include <psl_format.h>

void say_hello()
{
    psl::string str = psl::format("Hello from {} thread!", "pstdlib");
    psl::print(psl::color::red, "<{}>\n", str);
}

int main()
{
    psl::thread my_thread(say_hello);
    my_thread.join();
    return 0;
}
```

This program creates a new thread that executes the `say_hello` function and waits for it to complete.

For more insight on what the library includes and how it works, see the test cases in the directory `tests`.

## License

This project is licensed under the MIT License. For more details, refer to the `LICENSE` file in the repository.